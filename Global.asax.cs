﻿#region

using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

#endregion


namespace Kapsters
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (custom == "username")
            {
                if (context.Request.IsAuthenticated)
                {
                    return context.User.Identity.Name;
                }
                return null;
            }

            return base.GetVaryByCustomString(context, custom);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Wa gebeurt hier
          
        }

        protected void Application_Start()
        {
            // Clear viewengines
            ViewEngines.Engines.Clear();

            // Add theme viewengine
            ViewEngines.Engines.Add(new Theme.ThemeViewEngine());

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}