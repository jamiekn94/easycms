﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using Common;
using Common.Models.Home.News;
using Common.Providers.Caching;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using DevTrends.MvcDonutCaching;
using Kapsters.Areas.Admin.Controllers;
using Kapsters.Classes;
using Kapsters.Classes.News;
using Kapsters.Classes.Seo;
using Microsoft.Ajax.Utilities;
using PagedList;

#endregion

namespace Kapsters.Controllers
{
    public class NieuwsController : Controller
    {
        private readonly NewsService _newsService = new NewsService();

        public ActionResult Index(int? page)
        {
            // Initialize return model
            var returnValueModel = new BundleViewNewsModel
            {
                NewsArticles = _newsService.GetNewsArticles(page),
                SearchNewsModel = new SearchNewsModel(),
                KeywordModel = new KeywordModel(),
                Keywords = _newsService.GetKeywords()
            };

            // Return view
            return View(returnValueModel);
        }

        public ActionResult Zoek()
        {
            return RedirectToAction("Index");
        }

        [HttpGet]
        [ActionName("Wijzig-Reactie")]
        public ActionResult WijzigReactie()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        [ActionName("Verwijder-Reactie")]
        public ActionResult VerwijderReactie(int removeNewsCommentId)
        {
            using (var newsCommentRepository = new NewsCommentsRepository())
            {
                var newsComment = newsCommentRepository.GetById(removeNewsCommentId);

                if (newsComment != null && newsComment.ReactieGebruikersId != null)
                {
                    if (UserProvider.GetUserId() == (Guid)newsComment.ReactieGebruikersId)
                    {
                        newsCommentRepository.Remove(newsComment);
                        newsCommentRepository.Save();

                        this.SetSuccess("Uw nieuws reactie is succesvol verwijderd.");
                    }
                }
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorize]
        [HttpPost]
        [ActionName("Wijzig-Reactie")]
        public ActionResult WijzigReactie(EditCommentModel EditCommentModel)
        {
            if (ModelState.IsValid)
            {
                using (var newsCommentRepository = new NewsCommentsRepository())
                {
                    using (var newsArticleRepository = new NieuwsRepository())
                    {
                        var newsComment = newsCommentRepository.GetById(EditCommentModel.CommentId);

                        if (newsComment != null && newsComment.ReactieGebruikersId != null)
                        {
                            if (UserProvider.GetUserId() == (Guid) newsComment.ReactieGebruikersId)
                            {
                                var newsArticle = newsArticleRepository.GetById(newsComment.ArtikelID);
                                newsComment.Bericht = EditCommentModel.Message;
                                newsCommentRepository.Save();

                                this.SetSuccess("Uw nieuws reactie is succesvol gewijzigd.");
                                return RedirectToAction("Artikel", new { @id = newsArticle.ArtikelID, @title = CleanUrl.GetCleanUrl(newsArticle.Titel)});
                            }
                        }
                    }
                }
            }

            var bundleNewsModel = new BundleNewsModel
                {
                    NewsModel = _newsService.GetBasicArticleInfo(EditCommentModel.ArticleId),
                    NewsReactionModels = _newsService.GetNewsComments(EditCommentModel.ArticleId),
                    EditCommentModel = EditCommentModel
                };

            this.SetSeo(bundleNewsModel.NewsModel.Title, bundleNewsModel.NewsModel.Intro, bundleNewsModel.NewsModel.Keywords);


            return View("Artikel", bundleNewsModel);
        }

        [HttpPost]
        public ActionResult Zoek(SearchNewsModel searchNewsModel)
        {
            BundleViewNewsModel returnValueModel;

            if (ModelState.IsValid)
            {
                // Initialize return model
                returnValueModel = new BundleViewNewsModel
                {
                    NewsArticles = _newsService.GetNewsArticlesBySearchTerm(searchNewsModel.CurrentPage, searchNewsModel.SearchTerm),
                    SearchNewsModel = new SearchNewsModel(),
                    KeywordModel = new KeywordModel(),
                    Keywords = _newsService.GetKeywords()
                };
            }
            else
            {
                // Initialize return model
                returnValueModel = new BundleViewNewsModel
                {
                    NewsArticles = _newsService.GetNewsArticles(searchNewsModel.CurrentPage),
                    SearchNewsModel = new SearchNewsModel(),
                    KeywordModel = new KeywordModel(),
                    Keywords = _newsService.GetKeywords()
                };
            }

            return View("Index", returnValueModel);
        }


        public ActionResult Artikel(int id)
        {

            // Get news info
            var newsInfo = _newsService.GetBasicArticleInfo(id);

            if (newsInfo != null)
            {
                var bundleNewsModel = new BundleNewsModel
                {
                    NewsModel = newsInfo,
                    NewsReactionModels = _newsService.GetNewsComments(id),
                    EditCommentModel = new EditCommentModel
                    {
                        ArticleId = id
                    }
                };

                this.SetSeo(bundleNewsModel.NewsModel.Title, bundleNewsModel.NewsModel.Intro, bundleNewsModel.NewsModel.Keywords);

                return View(bundleNewsModel);
            }

            // Blogarticle not found
            this.SetError("Dit blogartikel bestaat niet.");

            return RedirectToAction("Index", "Home");
        }

        public ActionResult ZoekKeywoord(KeywordModel keywordModel)
        {
            if (ModelState.IsValid)
            {
                return View("Index", new BundleViewNewsModel
                {
                    NewsArticles = _newsService.GetArticlesByKeyword(keywordModel.Page, keywordModel.Keyword),
                    SearchNewsModel = new SearchNewsModel(),
                    KeywordModel = new KeywordModel(),
                    Keywords = _newsService.GetKeywords()
                });
            }
            else
            {
                return View("Index", new BundleViewNewsModel
                {
                    NewsArticles = _newsService.GetNewsArticles(keywordModel.Page),
                    SearchNewsModel = new SearchNewsModel(),
                    KeywordModel = keywordModel,
                    Keywords = _newsService.GetKeywords()
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Reactie-Toevoegen")]
        public ActionResult ReactieToevoegen(NewsReactionModel AddNewsReactionModel)
        {
            if (ModelState.IsValid)
            {
                // Check if a name has been entered
                if (!User.Identity.IsAuthenticated && string.IsNullOrEmpty(AddNewsReactionModel.Name))
                {
                    ModelState.AddModelError("AddNewsReactionModel.Name", "Dit veld is verplicht");
                }
                else
                {
                    var newsReactionRepository = new NewsCommentsRepository();

                    var configurationRepository = new ConfiguratieRepository();
                    var configurationInfo = configurationRepository.GetById(1);

                    newsReactionRepository.Add(new NieuwsReactie
                    {
                        Bericht = AddNewsReactionModel.Message,
                        Datum = DateTime.Now,
                        ReactieGebruikersId =
                            User.Identity.IsAuthenticated
                                ? UserProvider.GetProviderUserKey(User.Identity.Name)
                                : (Guid?)null,
                        Name = User.Identity.IsAuthenticated == false ? AddNewsReactionModel.Name : string.Empty,
                        ArtikelID = AddNewsReactionModel.NewsId,
                        Status = !configurationInfo.ReactieNieuws
                    });

                    newsReactionRepository.Save();

                    this.SetSuccess(configurationInfo.ReactieNieuws
                        ? "Uw reactie is succesvol toegevoegd, zodra hij ge-accepteerd is door een medewerker dan is hij zichtbaar."
                        : "Uw reactie is succesvol toegevoegd.");

                    return Redirect(Request.UrlReferrer.ToString());
                }
            }

            var bundleNewsModel = new BundleNewsModel
                    {
                        AddNewsReactionModel = AddNewsReactionModel,
                        NewsModel = _newsService.GetBasicArticleInfo(AddNewsReactionModel.NewsId),
                        NewsReactionModels = _newsService.GetNewsComments(AddNewsReactionModel.NewsId)
                    };

            this.SetSeo(bundleNewsModel.NewsModel.Title, bundleNewsModel.NewsModel.Intro, bundleNewsModel.NewsModel.Keywords);

            return View("Artikel", bundleNewsModel);
        }
    }
}