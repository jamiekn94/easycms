﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using Common;
using Common.Models.Home.Blog;
using Common.Models.Home.News;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;
using Kapsters.Classes;
using Kapsters.Classes.Blog;
using Kapsters.Classes.Seo;
using PagedList;

#endregion

namespace Kapsters.Controllers
{
    public class BlogController : Controller
    {
        private readonly BlogService _blogService = new BlogService();

        public ActionResult Index(int? page)
        {
            var returnModel = new ViewBundleModel
            {
                BlogModels = _blogService.GetBlogArticles(page),
                SearchNewsModel = new SearchNewsModel()
            };
            return View(returnModel);
        }

        [Authorize]
        [ActionName("Verwijder-Reactie")]
        public ActionResult DeleteComment(int removeCommentId)
        {
            using (var blogCommentRepository = new BlogReactionRepository())
            {
                var blogComment = blogCommentRepository.GetById(removeCommentId);

                if (blogComment != null && blogComment.ReactieGebruikersID != null)
                {
                    if (UserProvider.GetUserId() == (Guid)blogComment.ReactieGebruikersID)
                    {
                        blogCommentRepository.Remove(blogComment);
                        blogCommentRepository.Save();

                        this.SetSuccess("Uw blog reactie is succesvol verwijderd.");
                    }
                }
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpGet]
        [ActionName("Wijzig-Reactie")]
        public ActionResult WijzigReactie()
        {
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        [ActionName("Wijzig-Reactie")]
        public ActionResult WijzigReactie(EditCommentModel EditCommentModel)
        {
            if (ModelState.IsValid)
            {
                using (var blogCommentRepository = new BlogReactionRepository())
                {
                    using (var blogArticleRepository = new BlogRepository())
                    {
                        var blogComment = blogCommentRepository.GetById(EditCommentModel.CommentId);

                        if (blogComment != null && blogComment.ReactieGebruikersID != null)
                        {
                            if (UserProvider.GetUserId() == (Guid)blogComment.ReactieGebruikersID)
                            {
                                var blogArticle = blogArticleRepository.GetById(blogComment.BlogID);
                                blogComment.Bericht = EditCommentModel.Message;
                                blogCommentRepository.Save();

                                this.SetSuccess("Uw blog reactie is succesvol gewijzigd.");
                                return RedirectToAction("Artikel", new { @id = blogArticle.BlogID, @title = CleanUrl.GetCleanUrl(blogArticle.Titel) });
                            }
                        }
                    }
                }
            }

            var bundleNewsModel = new BundleBlogModel
            {
                BlogModel = _blogService.GetBlogInfo(EditCommentModel.ArticleId),
                BlogReactionModels = _blogService.GetBlogComments(EditCommentModel.ArticleId),
                EditCommentModel = EditCommentModel
            };

            this.SetSeo(bundleNewsModel.BlogModel.Title, bundleNewsModel.BlogModel.Intro, bundleNewsModel.BlogModel.Keywords);


            return View("Artikel", bundleNewsModel);
        }

        public ActionResult Artikel(int id)
        {
            var blogInfo = _blogService.GetBlogInfo(id);

            if (blogInfo != null)
            {
                var returnModel = new BundleBlogModel
                {
                    BlogModel = blogInfo,
                    BlogReactionModels = _blogService.GetBlogComments(id),
                    EditCommentModel = new EditCommentModel
                    {
                        ArticleId = id
                    },
                    AddBlogReactionModel = new BlogReactionModel
                    {
                        BlogId = id
                    }
                };

                this.SetSeo(returnModel.BlogModel.Title, returnModel.BlogModel.Intro, returnModel.BlogModel.Keywords);

                return View(returnModel);
            }

            // Blogarticle not found
            this.SetError("Dit blogartikel bestaat niet.");

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Zoek()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Zoek(SearchNewsModel searchNewsModel)
        {
            ViewBundleModel returnValueModel;

            if (ModelState.IsValid)
            {
                // Initialize return model
                returnValueModel = new ViewBundleModel
                {
                    BlogModels = _blogService.GetNewsArticlesBySearchTerm(searchNewsModel.CurrentPage, searchNewsModel.SearchTerm)
                };
            }
            else
            {
                // Initialize return model
                returnValueModel = new ViewBundleModel
                {
                    BlogModels = _blogService.GetBlogArticles(searchNewsModel.CurrentPage)
                };
            }

            return View("Index", returnValueModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Reactie-Toevoegen")]
        public ActionResult ReactieToevoegen(BlogReactionModel addBlogReactionModel)
        {
            if (ModelState.IsValid)
            {
                 // Check if a name has been entered
                if (!User.Identity.IsAuthenticated && string.IsNullOrEmpty(addBlogReactionModel.Name))
                {
                    ModelState.AddModelError("AddBlogReactionModel.Name", "Dit veld is verplicht");
                }
                    else
                    {
                        var configurationRepository = new ConfiguratieRepository();
                        var configurationInfo = configurationRepository.GetById(1);

                        var blogReactionRepository = new BlogReactionRepository();

                        blogReactionRepository.Add(new BlogReactie
                        {
                            Bericht = addBlogReactionModel.Message,
                            Datum = DateTime.Now,
                            ReactieGebruikersID =
                                User.Identity.IsAuthenticated
                                    ? UserProvider.GetProviderUserKey(User.Identity.Name)
                                    : (Guid?) null,
                            Name = User.Identity.IsAuthenticated == false ? addBlogReactionModel.Name : string.Empty,
                            BlogID = addBlogReactionModel.BlogId,
                            Status = !configurationInfo.ReactieBlog
                        });

                        blogReactionRepository.Save();

                        this.SetSuccess(configurationInfo.ReactieBlog
                            ? "Uw reactie is succesvol toegevoegd, zodra hij ge-accepteerd is door een medewerker dan is hij zichtbaar."
                            : "Uw reactie is succesvol toegevoegd.");

                        return Redirect(Request.UrlReferrer.AbsoluteUri);
                    }
                }

            var returnModel = new BundleBlogModel
            {
                AddBlogReactionModel = addBlogReactionModel,
                BlogModel = _blogService.GetBlogInfo(addBlogReactionModel.BlogId),
                BlogReactionModels = _blogService.GetBlogComments(addBlogReactionModel.BlogId)
            };

            return View("Artikel", returnModel);
        }

        public ActionResult ZoekKeywoord(KeywordModel keywordModel)
        {
            if (ModelState.IsValid)
            {
                return View("Index", new ViewBundleModel
                {
                    BlogModels = _blogService.GetArticlesByKeyword(keywordModel.Page, keywordModel.Keyword),
                    SearchNewsModel = new SearchNewsModel()
                });
            }
            else
            {
                return View("Index", new ViewBundleModel
                {
                    BlogModels = _blogService.GetBlogArticles(keywordModel.Page),
                    SearchNewsModel = new SearchNewsModel()
                });
            }
        }
    }
}