﻿#region

using System.Web.Mvc;
using System.Web.Security;
using Common.Providers.Messages;

#endregion

namespace Kapsters.Controllers
{
    public class UitloggenController : Controller
    {
        //
        // GET: /Uitloggen/

        public ActionResult Index()
        {
            FormsAuthentication.SignOut();

            this.SetSuccess("U bent uitgelogd.");

            return RedirectToAction("Index", "Home");
        }

    }
}
