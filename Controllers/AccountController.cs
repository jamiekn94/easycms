﻿#region

using System.Linq;
using System.Web.Mvc;
using Common.Models.Home.Account;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;

#endregion

namespace Kapsters.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        #region Private Functions

        private AccountModel GetAccount()
        {
            var userInfo = UserProvider.GetUserInfo(HttpContext.User.Identity.Name);

            var accountModel = new AccountModel
            {
                Achternaam = userInfo.Lastname,
                AdresToevoeging = userInfo.AdressExtra,
                Email = userInfo.Email,
                Huisnummer = userInfo.HouseNumber,
                Postcode = userInfo.Postcode,
                Straat = userInfo.Street,
                Voornaam = userInfo.Firstname,
                Woonplaats = userInfo.Residence,
                HasAvatar = userInfo.HasAvatar,
                SubscriptionNewsletter = userInfo.SubscriptionNewsletter
            };

            return accountModel;
        }

        private PasswordModel GetPasswordModel()
        {
            var passwordModel = new PasswordModel();

            return passwordModel;
        }

        private SecurityModel GetSecurity()
        {
            var userRepository = new GebruikersRepository();

            var securityModel = new SecurityModel
            {
                BeveilingsVragenList = userRepository.GetBeveilingsVragen()
            };

            return securityModel;
        }
        #endregion

        public ActionResult Index()
        {
            var bundleAccountModel = new BundleAccountModel
            {
                AccountModel = GetAccount(),
                PasswordModel = GetPasswordModel(),
                SecurityModel = GetSecurity()
            };

            return View(bundleAccountModel);
        }

        [HttpPost]
        public ActionResult Index(AccountModel accountModel)
        {
            if (ModelState.IsValid)
            {
                UserProvider.UpdateUser(accountModel.Voornaam, accountModel.Achternaam,
                    accountModel.Email, accountModel.Straat, (int) accountModel.Huisnummer, accountModel.AdresToevoeging,
                    accountModel.Woonplaats, accountModel.Postcode, 1, null, User.Identity.Name, accountModel.SubscriptionNewsletter);

                this.SetSuccess("Uw account is succesvol gewijzigd.");

                return RedirectToAction("Index");
            }

            var bundleAccountModel = new BundleAccountModel
            {
                AccountModel = accountModel,
                PasswordModel = GetPasswordModel(),
                SecurityModel = GetSecurity(),
                ActiveTab = 1
            };

            return View(bundleAccountModel);
        }

        [HttpPost]
        public ActionResult Wachtwoord(PasswordModel passwordModel)
        {
            if (ModelState.IsValid)
            {
                var passwordStatus = UserProvider.SetNewPassword(passwordModel.OldPassword,
                    passwordModel.RepeatPassword, User.Identity.Name);

                switch (passwordStatus)
                {
                    case UserProvider.PasswordChange.ContainsName:
                    {
                        ModelState.AddModelError("PasswordModel.Password", "Uw wachtwoord mag niet uw voornaam, achternaam of gebruikersnaam bevatten.");
                        break;
                    }
                    case UserProvider.PasswordChange.InvalidPassword:
                    {
                        ModelState.AddModelError("PasswordModel.Password", "Uw wachtwoord komt niet overeen met het wachtwoord van uw account.");
                        break;
                    }
                    case UserProvider.PasswordChange.Success:
                    {
                        this.SetSuccess("Uw wachtwoord is gewijzigd.");

                        return RedirectToAction("Index");
                    }
                }
            }

            var bundleAccountModel = new BundleAccountModel
            {
                AccountModel = GetAccount(),
                PasswordModel = passwordModel,
                SecurityModel = GetSecurity(),
                ActiveTab = 2
            };
            return View("Index", bundleAccountModel);
        }

        [HttpPost]
        public ActionResult Security(SecurityModel securityModel)
        {
            if (ModelState.IsValid)
            {
                var userRepository = new GebruikersRepository();
                if (UserProvider.ChangePasswordQuestionAndAnswer(User.Identity.Name,
                    securityModel.Wachtwoord, userRepository.GetRegistrationQuestion(securityModel.BeveilingsVraagId),
                    securityModel.BeveilingsAntwoord))
                {
                    this.SetSuccess("Uw beveilingsvraag en antwoord zijn succesvol gewijzigd.");

                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("SecurityModel.Wachtwoord", "Uw wachtwoord klopt niet.");
                }
            }

            var bundleAccountModel = new BundleAccountModel
            {
                AccountModel = GetAccount(),
                PasswordModel = GetPasswordModel(),
                SecurityModel = GetSecurity(),
                ActiveTab = 3
            };

            bundleAccountModel.SecurityModel.BeveilingsAntwoord = securityModel.BeveilingsAntwoord;
            bundleAccountModel.SecurityModel.BeveilingsVragenList.First(x => x.Value == securityModel.BeveilingsVraagId.ToString()).Selected = true;

            return View("Index", bundleAccountModel);
        }

    }
}
