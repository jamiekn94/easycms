﻿#region

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Home.Blog;
using Common.Models.Home.Fotoalbum;
using Common.Models.Home.News;
using Common.Providers.Messages;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;
using Kapsters.Classes.PhotoAlbum;
using Kapsters.Classes.Seo;
using PagedList;

#endregion

namespace Kapsters.Controllers
{
    public class FotoalbumController : Controller
    {
        private readonly PhotoAlbumService _photoAlbumService = new PhotoAlbumService();

        //
        // GET: /Fotoalbum/

        public ActionResult Index(int? page)
        {
            var viewBundlePhotoAlbum = new ViewBundlePhotoAlbums
            {
                PhotoAlbums = _photoAlbumService.GetPhotoAlbums(page ?? 1),
                SearchModel = new SearchNewsModel()
            };

            return View(viewBundlePhotoAlbum);
        }

        public ActionResult Zoek()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Zoek(SearchNewsModel searchModel)
        {
            var returnModel = new ViewBundlePhotoAlbums();

            if (ModelState.IsValid)
            {
                returnModel.PhotoAlbums = _photoAlbumService.GetPhotoAlbumsBySearch(searchModel.SearchTerm, searchModel.CurrentPage);
                returnModel.SearchModel = new SearchNewsModel();
            }
            else
            {
                returnModel.PhotoAlbums = _photoAlbumService.GetPhotoAlbums(searchModel.CurrentPage);
                returnModel.SearchModel = searchModel;
            }

            return View("Index", returnModel);
        }

        public ActionResult Album(int id)
        {
            var photoAlbumInfo = _photoAlbumService.GetAlbum(id, true);

            if (photoAlbumInfo == null)
            {
                this.SetError("Deze fotoalbum bestaat niet!");
                return RedirectToAction("Index", "Home");
            }

            // Set seo
            this.SetSeo(photoAlbumInfo.Title, photoAlbumInfo.Description, photoAlbumInfo.Keywords);

            return View(photoAlbumInfo);
        }
    }
}
