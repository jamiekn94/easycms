﻿#region

using System.Web.Mvc;
using System.Web.Security;
using Common.Models.Home.Register;
using Common.Providers.Email;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using DotNetOpenAuth.AspNet;
using Kapsters.Areas.Admin.Controllers;
using Microsoft.Web.WebPages.OAuth;

#endregion

namespace Kapsters.Controllers
{
    public class RegistreerController : Controller
    {
        //
        // GET: /Registreer/

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                this.SetError("U bent al geregistreerd.");
                return RedirectToAction("Index", "Home");
            }
            var gebruikersModel = new RegisterModel();
            var userRepository = new GebruikersRepository();

            gebruikersModel.LandenList = userRepository.GetLanden();
            gebruikersModel.BeveilingsVragenList = userRepository.GetBeveilingsVragen();

            return View(gebruikersModel);
        }

        [HttpPost]
        public ActionResult Index(RegisterModel registerModel)
        {
            var userRepository = new GebruikersRepository();

            if (ModelState.IsValid)
            {
                var configurationRepository = new ConfiguratieRepository();
                var configurationInfo = configurationRepository.GetById(1);

                var statusUser = UserProvider.AddUser(registerModel.Voornaam,
                    registerModel.Achternaam,
                    registerModel.Email, registerModel.Straat, (int)registerModel.Huisnummer, registerModel.AdresToevoeging,
                    registerModel.Woonplaats, registerModel.Postcode, 1, registerModel.Gebruikersnaam,
                    registerModel.Wachtwoord, userRepository.GetRegistrationQuestion(registerModel.BeveilingsVraagId),
                    registerModel.BeveilingsAntwoord, "Leden", registerModel.ProfilePhoto, registerModel.SubscriptionNewsletter, !configurationInfo.GebruikersVerificatie);

                switch (statusUser)
                {
                    case MembershipCreateStatus.DuplicateEmail:
                        {
                            ModelState.AddModelError("Email", "Deze e-mail is al geregistreerd");
                            break;
                        }
                    case MembershipCreateStatus.DuplicateProviderUserKey:
                        {
                            ModelState.AddModelError("Gebruikersnaam", "Er bestaat al een gebruiker met dezelfde gebruikersnaam en wachtwoord");
                            break;
                        }
                    case MembershipCreateStatus.DuplicateUserName:
                        {
                            ModelState.AddModelError("Gebruikersnaam", "Er bestaat al een gebruiker met deze gebruikersnaam");
                            break;
                        }
                    // Uitzoeken wat de eisen zijn
                    case MembershipCreateStatus.InvalidAnswer:
                        {
                            ModelState.AddModelError("BeveilingsAntwoord", "Uw antwoord voldoet niet aan onze eisen.");
                            break;
                        }
                    case MembershipCreateStatus.InvalidEmail:
                        {
                            ModelState.AddModelError("Email", "Dit is geen geldige e-mail");
                            break;
                        }
                    case MembershipCreateStatus.InvalidPassword:
                        {
                            ModelState.AddModelError("Wachtwoord", "Uw wachtwoord voldoet niet aan onze eisen");
                            break;
                        }
                    case MembershipCreateStatus.InvalidQuestion:
                        {
                            ModelState.AddModelError("BeveilingsVraagId", "De beveilingsvraag voldoet niet aan onze eisen.");
                            break;
                        }
                    case MembershipCreateStatus.InvalidUserName:
                        {
                            ModelState.AddModelError("Gebruikersnaam", "De gebruikersnaam voldoet niet aan onze eisen.");
                            break;
                        }
                    case MembershipCreateStatus.UserRejected:
                        {
                            ModelState.AddModelError("Gebruikersnaam", "U bent niet gemachtigd om te registreren.");
                            break;
                        }
                    case MembershipCreateStatus.Success:
                        {
                            if (configurationInfo.WelkomEmail)
                            {
                                // Welcome email
                                var emailTemplate = new EmailTemplate();
                                emailTemplate.SendWelcomeEmail(registerModel.Gebruikersnaam, registerModel.Wachtwoord);
                            }

                            if (configurationInfo.GebruikersVerificatie)
                            {
                                this.SetSuccess("U bent succesvol geregistreerd, zodra u bent ge-accepteerd door een medewerker dan kunt u inloggen.");
                            }
                            else
                            {
                                UserProvider.ValidateUser(registerModel.Gebruikersnaam,
                                    registerModel.Wachtwoord);
                                FormsAuthentication.SetAuthCookie(registerModel.Gebruikersnaam, true);

                                this.SetSuccess("U bent succesvol geregistreerd!");
                            }


                            return RedirectToAction("Index", "Home");
                        }
                }
            }

            // Something went wrong
            registerModel.LandenList = userRepository.GetLanden();
            registerModel.BeveilingsVragenList = userRepository.GetBeveilingsVragen();

            return View(registerModel);
        }

        [HttpPost]
        public void Social(string provider)
        {
            OAuthWebSecurity.RequestAuthentication(provider,
               "http://" + Request.Url.Authority + "/Registreer/Callback");
        }

        public ActionResult Callback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(returnUrl);

            if (result.IsSuccessful)
            {
                // Check if user doesn't exist yet
                if (UserProvider.GetMembership(result.UserName) == null)
                {
                    string generatedPassword = System.Web.Security.Membership.GeneratePassword(15, 5);

                    var configurationRepository = new ConfiguratieRepository();
                    var configurationInfo = configurationRepository.GetById(1);

                    // Add user
                    var userResult = UserProvider.AddUser("", "", result.UserName, "", 0, "",
                        "", "", 0,
                        result.UserName, generatedPassword, null, null, "Leden", null, false,
                        !configurationInfo.GebruikersVerificatie);

                    // Check user status
                    if (userResult == MembershipCreateStatus.Success)
                    {
                        if (configurationInfo.GebruikersVerificatie)
                        {
                            this.SetSuccess("U bent succesvol geregistreerd, zodra u bent ge-accepteerd door een medewerker dan kunt u inloggen.");
                        }
                        else
                        {
                            // Success
                            this.SetSuccess("U bent succesvol geregistreerd!");

                            // Mail welcome email
                            if (configurationInfo.WelkomEmail)
                            {
                                // Welcome email
                                var emailTemplate = new EmailTemplate();
                                emailTemplate.SendWelcomeEmail(result.UserName, generatedPassword);
                            }

                            UserProvider.ValidateUser(result.UserName,
                                  generatedPassword);
                            FormsAuthentication.SetAuthCookie(result.UserName, true);
                        }

                        return RedirectToAction("Index", "Home");
                    }

                    // Get registration message
                    string registrationError;

                    switch (userResult)
                    {

                        case MembershipCreateStatus.DuplicateEmail:
                            {
                                registrationError = "Deze e-mail is al geregistreerd";
                                break;
                            }
                        case MembershipCreateStatus.DuplicateProviderUserKey:
                            {
                                registrationError = "Er bestaat al een gebruiker met dezelfde gebruikersnaam en wachtwoord";
                                break;
                            }
                        case MembershipCreateStatus.DuplicateUserName:
                            {
                                registrationError = "Er bestaat al een gebruiker met deze gebruikersnaam";
                                break;
                            }
                        // Uitzoeken wat de eisen zijn
                        case MembershipCreateStatus.InvalidAnswer:
                            {
                                registrationError = "Uw antwoord voldoet niet aan onze eisen.";
                                break;
                            }
                        case MembershipCreateStatus.InvalidEmail:
                            {
                                registrationError = "Dit is geen geldige e-mail";
                                break;
                            }
                        case MembershipCreateStatus.InvalidPassword:
                            {
                                registrationError = "Uw wachtwoord voldoet niet aan onze eisen";
                                break;
                            }
                        case MembershipCreateStatus.InvalidQuestion:
                            {
                                registrationError = "De beveilingsvraag voldoet niet aan onze eisen.";
                                break;
                            }
                        case MembershipCreateStatus.InvalidUserName:
                            {
                                registrationError = "De gebruikersnaam voldoet niet aan onze eisen.";
                                break;
                            }
                        case MembershipCreateStatus.UserRejected:
                            {
                                registrationError = "U bent niet gemachtigd om te registreren.";
                                break;
                            }

                        default:
                            {
                                registrationError = "Onbekende fout tijdens de registratie.";
                                break;
                            }
                    }

                    return View("Callback", (object) registrationError);

                }
            }
            // Error callback
            this.SetError(
                "Er heeft zich een onbekende fout plaatsgevonden tijdens de sociale registratie, probeer het later nog eens");

            return RedirectToAction("Index", "Registreer");
        }
    }
}
