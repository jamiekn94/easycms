﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.Internal;
using Common;
using Common.Models.Home.Fotoalbum;
using Common.Models.Home.News;
using Common.Models.Home.Widgets;
using Common.Repositories;
using Kapsters.Classes;
using Kapsters.Classes.Blog;
using Kapsters.Classes.News;
using Kapsters.Classes.PhotoAlbum;
using Microsoft.Ajax.Utilities;
using Microsoft.VisualBasic;
using OAuthTwitterWrapper;

namespace Kapsters.Controllers
{
    public class WidgetsController : Controller
    {
        public ActionResult Html(int activeWidgetId)
        {
            using (var htmlRepository = new HtmlWidgetRepository())
            {
                var htmlWidget = htmlRepository.GetByActiveId(activeWidgetId);

                return View(new HtmlWidgetModel
                {
                    Text = htmlWidget.Title
                });
            }
        }

        public ActionResult FotoAlbum(int activeWidgetId)
        {
            using (var photoWidgetRepository = new PhotoWidgetRepository())
            {
                var activePhotoWidget = photoWidgetRepository.GetByActiveId(activeWidgetId);

                if (activePhotoWidget == null) { return HandleUnknownWidget(); }

                return View(HandlePhotos(activePhotoWidget.Id, activePhotoWidget.IsSlide, activePhotoWidget.AmountItems));
            }
        }

        private ActionResult HandleUnknownWidget()
        {
            return View();
        }

        public ActionResult Nieuws(int activeWidgetId)
        {
            var newsService = new NewsService();

            using (var newsWidgetsRepository = new NewsWidgetRepository())
            {
                var blogWidgetInfo = newsWidgetsRepository.GetByActiveWidgetId(activeWidgetId);

                var arrayNews = new NewsWidgetModel
                {
                    NewsArticles = newsService.GetNewsArticles(1, blogWidgetInfo.AmountItems),
                    Slide = blogWidgetInfo.Slide,
                    AmountItems = blogWidgetInfo.AmountItems
                };

                return View(arrayNews);
            }
        }

        public ActionResult Blog(int activeWidgetId)
        {
            var blogService = new BlogService();

            using (var blogWidgetsRepository = new BlogWidgetsRepository())
            {
                var blogWidgetInfo = blogWidgetsRepository.GetByActiveWidgetId(activeWidgetId);

                return View(new BlogWidgetModel
                {
                    BlogArticles = blogService.GetBlogArticles(1, blogWidgetInfo.AmountItems),
                    Slide = blogWidgetInfo.Slide,
                    AmountItems = blogWidgetInfo.AmountItems
                });
            }
        }

        public ActionResult TwitterFeed(int activeWidgetId)
        {
            using (var twitterFeedRepository = new TwitterfeedRWidgetRepository())
            {
                var twitterWidgetInfo = twitterFeedRepository.GetByActiveId(activeWidgetId);

                var authenticationSettings = new AuthenticateSettings
                {
                    OAuthConsumerKey = twitterWidgetInfo.ConsumerKey,
                    OAuthConsumerSecret = twitterWidgetInfo.ConsumerSecret,
                    OAuthUrl = twitterWidgetInfo.ReturnUrl

                };

                var timelineSettings = new TimeLineSettings
                {
                    Count = twitterWidgetInfo.AmountTweets,
                    ExcludeReplies = twitterWidgetInfo.IncludeReplies ? "no" : "yes",
                    IncludeRts = twitterWidgetInfo.IncludeRetweets ? "yes" : "no",
                    ScreenName = twitterWidgetInfo.Username
                };

                var twitterReturnObject = new OAuthTwitterWrapper.OAuthTwitterWrapper(authenticationSettings, timelineSettings);

                return View(twitterReturnObject.GetMyTimeline());
            }
        }

        public ActionResult ShareThis(int activeWidgetId)
        {
            using (var shareWidgetRepository = new ShareWidgetRepository())
            {
                var shareWidgetInfo = shareWidgetRepository.GetByActiveId(activeWidgetId);

                return View(shareWidgetInfo);
            }
        }

        public ActionResult Statistics(int activeWidgetId)
        {
            using (var statisticsRepository = new StatisticsWidgetRepository())
            {
                var statisticsWidgetInfo = statisticsRepository.GetByActiveId(activeWidgetId);

                return View(GetStatisticsModel(statisticsWidgetInfo));
            }
        }

        #region Private Methods

        private StatisticsWidgetModelcs GetStatisticsModel(StatisticsWidget statisticsWidgetInfo)
        {
            var returnModel = new StatisticsWidgetModelcs
            {
                ShowBlogArticles = statisticsWidgetInfo.ShowBlogArticles,
                ShowHitsWebsite = statisticsWidgetInfo.ShowHitsWebsite,
                ShowNewsArticles = statisticsWidgetInfo.ShowNewsArticles,
                ShowPhotoAlbums = statisticsWidgetInfo.ShowPhotoAlbums,
                ShowPhotos = statisticsWidgetInfo.ShowPhotos,
                ShowUniqueVisitors = statisticsWidgetInfo.ShowUniqueVisitors,
                ShowUsers = statisticsWidgetInfo.ShowUsers
            };

            if (returnModel.ShowBlogArticles)
            {
                returnModel.AmountBlogArticles = GetAmountBlogArticles();
            }

            if (returnModel.ShowHitsWebsite)
            {
                returnModel.AmountHitsWebsite = GetAmountPageviews(DateTime.Now, GetEndDateOfMonth());
            }

            if (returnModel.ShowNewsArticles)
            {
                returnModel.AmountNewsArticles = GetAmountNewsArticles();
            }

            if (returnModel.ShowPhotoAlbums)
            {
                returnModel.AmountPhotoAlbums = GetAmountPhotoAlbums();
            }

            if (returnModel.ShowPhotos)
            {
                returnModel.AmountPhotos = GetAmountPhotos();
            }

            if (returnModel.ShowUniqueVisitors)
            {
                returnModel.AmountUniqueVisitors = GetAmountUniqueVisitors(DateTime.Now, GetEndDateOfMonth());
            }

            if (returnModel.ShowUsers)
            {
                returnModel.AmountUsers = GetAmountUsers();
            }

            return returnModel;
        }

        private int GetAmountUsers()
        {
            using (var usersRepository = new GebruikersRepository())
            {
                return usersRepository.GetAmountUsers();
            }
        }

        private int GetAmountBlogArticles()
        {
            using (var blogRepository = new BlogReactionRepository())
            {
                return blogRepository.GetAmountArticles();
            }
        }

        private int GetAmountNewsArticles()
        {
            using (var newsRepository = new NieuwsRepository())
            {
                return newsRepository.GetAmountArticles();
            }
        }

        private int GetAmountPhotoAlbums()
        {
            using (var photoAlbumRepository = new PhotoAlbumRepository())
            {
                return photoAlbumRepository.GetAmountPhotoAlbums();
            }
        }

        private int GetAmountPhotos()
        {
            using (var photoAlbumRepository = new PhotosInAlbumRepository())
            {
                return photoAlbumRepository.GetAmountPhotos();
            }
        }

        private DateTime GetEndDateOfMonth()
        {
            int amountDays = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            string strDate = string.Format("{0}/{1}/{2}", amountDays, DateTime.Now.Month, DateTime.Now.Year);

            return DateTime.Parse(strDate);
        }

        private int GetAmountUniqueVisitors(DateTime beginDate, DateTime endDate)
        {
            using (var statsVisitsRepository = new DashboardStatisticsRepository())
            {
                return statsVisitsRepository.GetUniqueVisitorsBetweenDates(beginDate, endDate);
            }
        }

        private BundleAlbumPhotoWidgetModel HandlePhotos(int photoWidgetId, bool slide, int amountItems)
        {
            var returnModel = new BundleAlbumPhotoWidgetModel();
            using (var albumInPhotosWidgetRepository = new AlbumInPhotoWidgetRepository())
            {
                var collectionAlbum = new Collection<AlbumPhotoWidgetModel>();
                var albumPhotoWidgets = albumInPhotosWidgetRepository.GetAllByPhotoWidgetId(photoWidgetId).Take(amountItems);

                using (var photoRepository = new PhotosInAlbumRepository())
                {
                    using (var albumPhotoRepository = new PhotoAlbumRepository())
                    {
                        foreach (var albumInPhotoWidget in albumPhotoWidgets)
                        {
                            var album = albumPhotoRepository.GetById(albumInPhotoWidget.AlbumId);
                            var photo = photoRepository.GetById(albumInPhotoWidget.PhotoId);

                            collectionAlbum.Add(new AlbumPhotoWidgetModel
                            {
                                AlbumId = albumInPhotoWidget.AlbumId,
                                Image = photo.Naam,
                                Url = CleanUrl.GetCleanUrl(album.Titel),
                                Name = album.Titel,
                            });
                        }
                    }
                }
                returnModel.Photos = collectionAlbum;
                returnModel.Slide = slide;

                return returnModel;
            }
        }

        private int GetAmountPageviews(DateTime beginDate, DateTime endDate)
        {
            using (var statsVisitsRepository = new DashboardStatisticsRepository())
            {
                return statsVisitsRepository.GetPageViewsBetweenDates(beginDate, endDate);
            }
        }

        #endregion
    }
}