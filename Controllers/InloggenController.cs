﻿#region

using System;
using System.Web.Mvc;
using System.Web.Security;
using Common.Models.Home.Login;
using Common.Providers.Email;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;

#endregion

namespace Kapsters.Controllers
{
    public class InloggenController : Controller
    {
        //
        // GET: /Inloggen/

        public ActionResult Index()
        {
            var date = DateTime.Now;
            if (User.Identity.IsAuthenticated)
            {
                this.SetError("U bent al ingelogd.");
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel loginModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                MembershipUser userInfo = System.Web.Security.Membership.GetUser(loginModel.Username);
                if (userInfo != null)
                {
                    // Check if the user is locked out and if we need to remove the lockout
                    if (CheckLockout(loginModel.Username, userInfo.IsLockedOut))
                    {
                        if (UserProvider.ValidateUser(loginModel.Username, loginModel.Password))
                        {
                            FormsAuthentication.SetAuthCookie(loginModel.Username, true);

                            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(Server.UrlDecode(returnUrl));
                            }
                            return RedirectToAction("Index", "Home");
                        }

                        ModelState.AddModelError("Password", "Verkeerd wachtwoord voor deze gebruiker.");
                    }
                    else
                    {
                        ModelState.AddModelError("membershipError",
                            userInfo.Comment);
                    }
                }
                else
                {
                    ModelState.AddModelError("Username", "Geen account gevonden met deze gebruikersnaam.");
                }
            }
            return View(loginModel);
        }

        public ActionResult ResetWachtwoord()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetWachtwoord(ResetPasswordModel resetPasswordModel)
        {
            if (ModelState.IsValid)
            {
                var membershipInfo = UserProvider.GetMembership(resetPasswordModel.Username);

                if (membershipInfo != null)
                {
                    return ResetWachtwoordStap2(resetPasswordModel);
                }
                else
                {
                    ModelState.AddModelError("Username", "Deze gebruiker bestaat niet");
                }
            }

            return View(resetPasswordModel);

        }

        public ActionResult ResetWachtwoordStap2(ResetPasswordModel resetPasswordModel)
        {
            var userInfo = UserProvider.GetUserInfo(resetPasswordModel.Username);

            var resetPasswordStep2Model = new ResetPasswordStep2Model
            {
                Question = userInfo.SecurityQuestion,
                Username = resetPasswordModel.Username
            };

            return View("ResetWachtwoordStap2", resetPasswordStep2Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetWachtwoordStap2(ResetPasswordStep2Model resetPasswordModel)
        {
            string newResetPassword = string.Empty;

            if (ModelState.IsValid)
            {
                if (UserProvider.ResetPassword(resetPasswordModel.Username,
                    ref newResetPassword))
                {
                    var emailTemplate = new EmailTemplate();
                    emailTemplate.SendPasswordReset(resetPasswordModel.Username, newResetPassword);

                    this.SetSuccess("Uw wachtwoord is succesvol ge-reset, het nieuwe email is verzonden naar uw e-mail account.");

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("Answer", "Uw antwoord klopt niet");
                }
            }

            var userInfo = UserProvider.GetUserInfo(resetPasswordModel.Username);
            resetPasswordModel.Question = userInfo.SecurityQuestion;

            return View(resetPasswordModel);
        }

        public ActionResult LoginNaam()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginNaam(UsernameModel usernameModel)
        {
            if (ModelState.IsValid)
            {
                var membershipRepository = new MembershipRepository();

                var membershipInfo = membershipRepository.GetByEmail(usernameModel.Email);

                if (membershipInfo != null)
                {
                    // Send email
                    var emailTemplate = new EmailTemplate();
                    emailTemplate.SendUsername(membershipInfo.User.UserName);

                    this.SetSuccess("Uw gebruikersnaam is succesvol verstuurd naar uw e-mail account.");
                }
                else
                {
                    ModelState.AddModelError("Email", "Geen account gevonden met deze e-mail account");
                }
            }
            return View(usernameModel);
        }

        #region Private Methods

        private bool CheckLockout(string username, bool lockout)
        {
            if (lockout)
            {
                using (var membershipRepository = new MembershipRepository())
                {
                    var membershipInfo = membershipRepository.GetByUsername(username);
                    int userAttemps = membershipInfo.FailedPasswordAttemptCount;
                    int maxAttemps = System.Web.Security.Membership.MaxInvalidPasswordAttempts;

                    if (maxAttemps == userAttemps)
                    {
                        if (membershipInfo.LastLockoutDate <= DateTime.UtcNow.AddMinutes(-30))
                        {
                            membershipInfo.IsLockedOut = false;
                            membershipInfo.FailedPasswordAttemptCount = 0;
                            membershipInfo.Comment = null;
                            membershipRepository.Save();
                            return true;
                        }
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }

        #endregion
    }
}
