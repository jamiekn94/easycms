﻿#region

using System.Collections.ObjectModel;
using System.Web.Mvc;
using Common;
using Common.Models.Home.Navigation;
using Common.Models.Home.Slider;
using Common.Providers.Caching;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;
using Kapsters.Classes;

#endregion

namespace Kapsters.Controllers
{
    public class HomeController : Controller
    {
        #region Private Methods

        private string GetBuildInUrl(int seoPageId)
        {
            var seoPageRepository = new SeoPageRepository();
            var seoPageInfo = seoPageRepository.GetById(seoPageId);
            string url  = (seoPageInfo.Controller)  + (seoPageInfo.Action == "Index" ? "" : "/" + seoPageInfo.Action);
            return url;
        }

        private string GetDynamicUrl(int pageId)
        {
            var pageRepository = new PaginaRepository();
            var pageInfo = pageRepository.GetById(pageId);

            return "Pagina/" + pageId + "/" + CleanUrl.GetCleanUrl(pageInfo.Titel);
        }

        private string GetNewsUrl(int newsId)
        {
            var newsRepository = new NieuwsRepository();
            var newsInfo = newsRepository.GetById(newsId);

            return "Nieuws/Artikel/" + newsId + "/" + CleanUrl.GetCleanUrl(newsInfo.Titel);
        }

        private string GetBlogUrl(int articleId)
        {
            var blogRepository = new BlogRepository();
            var blogInfo = blogRepository.GetById(articleId);

            return "Blog/Artikel/" + articleId + "/" + CleanUrl.GetCleanUrl(blogInfo.Titel);
        }

        private string DetermineUrl(Navigatie navigatie)
        {
            switch (navigatie.Type)
            {
                case 0:
                {
                    return GetBuildInUrl((int) navigatie.SeoPageId);
                }
                case 1:
                {
                    return GetDynamicUrl((int) navigatie.DynamicPageId);
                }
                case 2:
                {
                    return GetNewsUrl((int) navigatie.NewsArticleId);
                }
                case 3:
                {
                    return GetBlogUrl((int) navigatie.BlogArticleId);
                }
                case 4:
                {
                    return navigatie.ExternalUrl;
                }
                default:
                {
                    return "";
                }
            }
        }

        private string DetermineSubUrl(SubNavigatie subNavigatie)
        {
            switch (subNavigatie.Type)
            {
                case 0:
                    {
                        return GetBuildInUrl((int)subNavigatie.SeoPageId);
                    }
                case 1:
                    {
                        return GetDynamicUrl((int)subNavigatie.DynamicPageId);
                    }
                case 2:
                    {
                        return GetNewsUrl((int)subNavigatie.NewsArticleId);
                    }
                case 3:
                    {
                        return GetBlogUrl((int)subNavigatie.BlogArticleId);
                    }
                case 4:
                    {
                        return subNavigatie.ExternalUrl;
                    }
                default:
                    {
                        return "";
                    }
            }
        }

        #endregion

        public ActionResult Index()
        {
            var sliderRepository = new BannerSliderRepository();

            var returnListModel = new Collection<SliderModel>();

            var listSliders = sliderRepository.GetList();

            foreach (var slider in listSliders)
            {
                returnListModel.Add(new SliderModel
                {
                    Id = slider.BannerSliderId,
                    Title = slider.Title,
                    Description = slider.Description,
                    Image = "/Content/Shared/Banners/" + slider.BannerSliderId + "/" + slider.Image
                });
            }

            return View(returnListModel);
        }

        [ChildActionOnly]
        public ActionResult Navigation()
        {
            var navigationModel = new Collection<NavigationModel>();

            if (!ServerCachingProvider.IsSet("Navigation"))
            {
                var navigationRepository = new NavigatieRepository();
                var subNavigationRepository = new SubNavigatieRepository();

                var listNavigation = navigationRepository.GetList();

                foreach (var navigationItem in listNavigation)
                {
                    var newNavigationModel = new NavigationModel
                    {
                        Name = navigationItem.Naam,
                        FullUrl = DetermineUrl(navigationItem),
                        Visibility = navigationItem.Visibility,
                        Side = navigationItem.Side
                    };

                    var subCategories = subNavigationRepository.GetSubCategorieen(navigationItem.ID);

                    foreach (var subcategoryItem in subCategories)
                    {
                        newNavigationModel.SubNavigationModels.Add(new NavigationModel
                        {
                            Name = subcategoryItem.Naam,
                            FullUrl = DetermineSubUrl(subcategoryItem)
                        });
                    }

                    navigationModel.Add(newNavigationModel);

                    ServerCachingProvider.Set("Navigation", navigationModel, 15);
                }
            }
            else
            {
                navigationModel = (Collection<NavigationModel>)ServerCachingProvider.Get("Navigation");
            }

            return PartialView("Navigation", navigationModel);
        }
    }
}
