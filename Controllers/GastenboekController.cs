﻿#region

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Home.Guestbook;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;
using PagedList;

#endregion

namespace Kapsters.Controllers
{
    public class GastenboekController : Controller
    {
        //
        // GET: /Gastenboek/

        #region Private Methods

        private BundleGuestbookModel GetGuestbook(int? page)
        {
            var guestbookRepository = new GastenboekRepository();
            const int amountItems = 5;
            var listGuestbook = guestbookRepository.GetPage(page ?? 1, amountItems);

            var bundleGuestbookModel = new BundleGuestbookModel();
            var listGuestbookModel = new Collection<GuestBookModel>();
            var parentListGuestbook = listGuestbook.Where(x => x.ParentId == null);

            foreach (var guestbook in parentListGuestbook)
            {
                var guestbookModel = new GuestBookModel
                {
                    Author = guestbook.AutorId == null ? guestbook.Name : UserProvider.GetFullName((Guid)guestbook.AutorId),
                    Comment = guestbook.Comment,
                    Date = guestbook.Date,
                    GuestbookId = guestbook.GuestbookId,
                    ParentId = null
                };

                var subListGuestbook = guestbookRepository.GetSubGuestbook(guestbook.GuestbookId, amountItems);

                foreach (var subGuestbook in subListGuestbook)
                {
                    guestbookModel.SubGuestBookModels.Add(new GuestBookModel
                    {
                        Author = subGuestbook.AutorId == null ? subGuestbook.Name : UserProvider.GetFullName((Guid)subGuestbook.AutorId),
                        Comment = subGuestbook.Comment,
                        Date = subGuestbook.Date,
                        GuestbookId = subGuestbook.GuestbookId,
                        ParentId = null
                    });
                }

                listGuestbookModel.Add(guestbookModel);

            }

            var pagesAndItemsAmount = guestbookRepository.GetAmount(amountItems);
            var modelPagedList = new StaticPagedList<GuestBookModel>(listGuestbookModel, page ?? 1, amountItems,
                pagesAndItemsAmount[1]);
            bundleGuestbookModel.GuestBookModel = modelPagedList;
            bundleGuestbookModel.CommentGuestbookModel = new CommentGuestbookModel { PageId = page ?? 1 };
            return bundleGuestbookModel;
        }

        #endregion

        public ActionResult Index(int? page)
        {

            return View(GetGuestbook(page));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CommentGuestbookModel commentGuestbookModel)
        {
            BundleGuestbookModel bundleGuestbookModel;

            if (ModelState.IsValid)
            {
                // Validate the name field if the user is not logged in
                if (!User.Identity.IsAuthenticated)
                {
                    if (string.IsNullOrEmpty(commentGuestbookModel.Name))
                    {
                        ModelState.AddModelError("CommentGuestbookModel.Name", "Dit veld is verplicht");
                        bundleGuestbookModel = GetGuestbook(commentGuestbookModel.PageId);
                        bundleGuestbookModel.CommentGuestbookModel = commentGuestbookModel;

                        return View(bundleGuestbookModel);
                    }
                }

                // Add comment
                var guestbookRepository = new GastenboekRepository();
                var configurationRepository = new ConfiguratieRepository();

                var configurationInfo = configurationRepository.GetById(1);

                guestbookRepository.Add(new Guestbook
                {
                    Comment = commentGuestbookModel.Comment,
                    Name = User.Identity.IsAuthenticated == false ? commentGuestbookModel.Name : null,
                    Date = DateTime.Now,
                    Enabled = !configurationInfo.ReactieGastenboek,
                    ParentId = commentGuestbookModel.ParentId,
                    AutorId = 
                                User.Identity.IsAuthenticated
                                    ? UserProvider.GetProviderUserKey(User.Identity.Name)
                                    : (Guid?)null,
                });

                guestbookRepository.Save();

                this.SetSuccess(configurationInfo.ReactieGastenboek
                    ? "Uw gastenboek reactie is toegevoegd, wanneer een medewerker uw reactie heeft goedgekeurd dan wordt hij zichtbaar."
                    : "Uw gastenboek reactie is toegevoegd.");

                return RedirectToAction("Index", "Gastenboek", new { page = commentGuestbookModel.PageId });
            }

            bundleGuestbookModel = GetGuestbook(commentGuestbookModel.PageId);
            bundleGuestbookModel.CommentGuestbookModel = commentGuestbookModel;

            return View(bundleGuestbookModel);
        }

        public ActionResult LaadReacties(int page, int parentId)
        {
            var guestbookRepository = new GastenboekRepository();
            const int amountCommentsPage = 5;
            var listGuestbookComments = guestbookRepository.GetSubGuestbookPage(page, amountCommentsPage, parentId);
            var listModel = new Collection<GuestBookModel>();

            foreach (var guestbook in listGuestbookComments)
            {
                var guestbookModel = new GuestBookModel
                {
                    Author = guestbook.AutorId == null ? guestbook.Name : UserProvider.GetFullName((Guid)guestbook.AutorId),
                    Comment = guestbook.Comment,
                    Date = guestbook.Date,
                    GuestbookId = guestbook.GuestbookId,
                    ParentId = null
                };
                listModel.Add(guestbookModel);
            }

            if (listModel.Any())
            {
                return PartialView("~/Views/Shared/Gastenboek/Reacties.cshtml", listModel);
            }

            return Content("");
        }
    }
}
