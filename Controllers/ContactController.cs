﻿#region

using System;
using System.Text;
using System.Web.Mvc;
using Common.Models.Home.Contact;
using Common.Providers.Email;
using Common.Providers.Messages;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;

#endregion

namespace Kapsters.Controllers
{
    public class ContactController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ContactModel contactModel)
        {
            if (ModelState.IsValid)
            {
                var emailClass = new Email();

                var configurationRepository = new ConfiguratieRepository();
                var configurationInfo = configurationRepository.GetById(1);

                emailClass.SendEmail(configurationInfo.EmailInfoAccount, "Ingevuld contactformulier", GetEmailBody(contactModel.Message, contactModel.Name, contactModel.Email), true);

                this.SetSuccess("Uw formulier is verzonden en zal zo spoedig mogelijk beantwoord worden!");

                return RedirectToAction("Index");
            }
            return View(contactModel);
        }

        private string GetEmailBody(string contactMessage, string name, string email)
        {
            DateTime currentDate = DateTime.Now;
            var returnBody = new StringBuilder();

            returnBody.AppendLine("<p>Beste administratie,</p>");
            returnBody.AppendLine("<br>");
            returnBody.AppendLine("U heeft een nieuwe ingevulde contact formulier binnen gekregen.</p>");
            returnBody.AppendLine("<br>");
            returnBody.AppendLine("<br>");
            returnBody.AppendLine("<p><b>Naam: </b> " + name + "</p>");
            returnBody.AppendLine("<p><b>Email: </b> " + email + "</p>");
            returnBody.AppendLine("<p><b>Datum: </b> " + string.Format("{0:d MMM yyyy} op {0:H:mm}", currentDate) + "</p>");
            returnBody.AppendLine("<p><b>IP: </b> " + Request.ServerVariables["REMOTE_ADDR"] + "</p>");
            returnBody.AppendLine("<p><b>Bericht: </b></p>");
            returnBody.AppendLine(contactMessage);

            return returnBody.ToString();
        }
    }
}