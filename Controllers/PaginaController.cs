﻿#region

using System.Web.Mvc;
using Common.Models.Home.Page;
using Common.Providers.Messages;
using Common.Providers.Users;
using Common.Repositories;
using Kapsters.Areas.Admin.Controllers;
using Kapsters.Classes.Seo;

#endregion

namespace Kapsters.Controllers
{
    public class PaginaController : Controller
    {
        //
        // GET: /Pagina/

        public ActionResult Index(int id)
        {
            var pageRepository = new PaginaRepository();
            var pageInfo = pageRepository.GetById(1);

            var userInformation = UserProvider.GetUserInfo(pageInfo.AuteurId);

            if (pageInfo != null)
            {
                var pageModel = new PageModel
                {
                    Titel = pageInfo.Titel,
                    Url = Classes.CleanUrl.GetCleanUrl(pageInfo.Titel),
                    Author = userInformation.FullName,
                    Information = pageInfo.Informatie,
                    PageId = pageInfo.PaginaID,
                    SeoDescription = pageInfo.SEODescriptie,
                    SeoKeywords = pageInfo.SEOKeywoorden,
                    SeoTitle = pageInfo.SEOTitel,
                    AuthorHasAvatar = userInformation.HasAvatar,
                    CreatedDate = pageInfo.Datum,
                    AuthorUsername = userInformation.Username
                };

                // SEO
                this.SetSeo(pageModel.SeoTitle, pageModel.SeoDescription, pageModel.SeoKeywords);

                return View(pageModel);
            }
            else
            {
                this.SetError("Deze pagina bestaat niet.");
                return RedirectToAction("Index", "Home");
            }
        }

    }
}
