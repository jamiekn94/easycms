﻿$(document).ready(function () {
    var images = $('img');
    $.each(images, function () {

        // Get image source
        var imageSrc = $(this).attr('src');

        // Clean source attribute, prevent image loading
        $(this).attr('src', '');

        // Hide element
        $(this).hide();

        // Now load it again
        $(this).src = imageSrc;

    });

    // On image load
    $("img").one("load", function () {
    }).each(function () {
        if (this.complete) {
            $(this).fadeIn('fast');
        }
    });
});