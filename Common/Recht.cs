namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Recht")]
    public partial class Recht
    {
        public Recht()
        {
            AdminNavigation = new HashSet<AdminNavigation>();
            GroepsRecht = new HashSet<GroepsRecht>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RechtID { get; set; }

        [Required]
        [StringLength(75)]
        public string RechtNaam { get; set; }

        public bool Status { get; set; }

        public virtual ICollection<AdminNavigation> AdminNavigation { get; set; }

        public virtual ICollection<GroepsRecht> GroepsRecht { get; set; }
    }
}
