namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogReactie")]
    public partial class BlogReactie
    {
        [Key]
        public int ReactieID { get; set; }

        public Guid? ReactieGebruikersID { get; set; }

        [Required]
        [StringLength(1000)]
        public string Bericht { get; set; }

        public DateTime Datum { get; set; }

        public int BlogID { get; set; }

        [Required]
        [StringLength(75)]
        public string Name { get; set; }

        public bool Status { get; set; }

        public virtual Blog Blog { get; set; }

        public virtual Users Users { get; set; }
    }
}
