namespace Common
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataSource : DbContext
    {
        public DataSource()
            : base("name=DataSource")
        {
            
        }

        public virtual DbSet<ActionLog> ActionLog { get; set; }
        public virtual DbSet<ActiveWidget> ActiveWidget { get; set; }
        public virtual DbSet<AdminNavigation> AdminNavigation { get; set; }
        public virtual DbSet<Agenda> Agenda { get; set; }
        public virtual DbSet<AlbumInPhotoWidget> AlbumInPhotoWidget { get; set; }
        public virtual DbSet<Applications> Applications { get; set; }
        public virtual DbSet<BannerSlider> BannerSlider { get; set; }
        public virtual DbSet<Blog> Blog { get; set; }
        public virtual DbSet<BlogReactie> BlogReactie { get; set; }
        public virtual DbSet<BlogWidget> BlogWidget { get; set; }
        public virtual DbSet<CalendarWidget> CalendarWidget { get; set; }
        public virtual DbSet<Configuratie> Configuratie { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<ErrorLog> ErrorLog { get; set; }
        public virtual DbSet<Files> Files { get; set; }
        public virtual DbSet<Folder> Folder { get; set; }
        public virtual DbSet<FolderPermission> FolderPermission { get; set; }
        public virtual DbSet<FormFieldsWidget> FormFieldsWidget { get; set; }
        public virtual DbSet<FormFilledWidget> FormFilledWidget { get; set; }
        public virtual DbSet<FormWidget> FormWidget { get; set; }
        public virtual DbSet<Fotoalbum> Fotoalbum { get; set; }
        public virtual DbSet<Gebruiker> Gebruiker { get; set; }
        public virtual DbSet<GroepsRecht> GroepsRecht { get; set; }
        public virtual DbSet<Guestbook> Guestbook { get; set; }
        public virtual DbSet<HtmlWidget> HtmlWidget { get; set; }
        public virtual DbSet<Link> Link { get; set; }
        public virtual DbSet<Memberships> Memberships { get; set; }
        public virtual DbSet<Navigatie> Navigatie { get; set; }
        public virtual DbSet<Newsletter> Newsletter { get; set; }
        public virtual DbSet<NewsletterAttachment> NewsletterAttachment { get; set; }
        public virtual DbSet<NewsWidget> NewsWidget { get; set; }
        public virtual DbSet<Nieuwsartikel> Nieuwsartikel { get; set; }
        public virtual DbSet<NieuwsReactie> NieuwsReactie { get; set; }
        public virtual DbSet<Pagina> Pagina { get; set; }
        public virtual DbSet<PhotosInAlbum> PhotosInAlbum { get; set; }
        public virtual DbSet<PhotoWidget> PhotoWidget { get; set; }
        public virtual DbSet<Profiles> Profiles { get; set; }
        public virtual DbSet<Recht> Recht { get; set; }
        public virtual DbSet<RegistratieLanden> RegistratieLanden { get; set; }
        public virtual DbSet<RegistratieVragen> RegistratieVragen { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<SEOPage> SEOPage { get; set; }
        public virtual DbSet<ShareThisWidget> ShareThisWidget { get; set; }
        public virtual DbSet<StatisticsWidget> StatisticsWidget { get; set; }
        public virtual DbSet<StatsVisit> StatsVisit { get; set; }
        public virtual DbSet<SubNavigatie> SubNavigatie { get; set; }
        public virtual DbSet<Theme> Theme { get; set; }
        public virtual DbSet<ThemeSection> ThemeSection { get; set; }
        public virtual DbSet<Ticket> Ticket { get; set; }
        public virtual DbSet<TicketReactie> TicketReactie { get; set; }
        public virtual DbSet<TwitterFeedWidget> TwitterFeedWidget { get; set; }
        public virtual DbSet<UserActionLog> UserActionLog { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Widget> Widget { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActionLog>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<ActionLog>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<ActionLog>()
                .Property(e => e.Information)
                .IsUnicode(false);

            modelBuilder.Entity<ActionLog>()
                .HasMany(e => e.UserActionLog)
                .WithRequired(e => e.ActionLog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ActiveWidget>()
                .HasMany(e => e.FormWidget)
                .WithRequired(e => e.ActiveWidget)
                .HasForeignKey(e => e.ActiveId);

            modelBuilder.Entity<AdminNavigation>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<AdminNavigation>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<AdminNavigation>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<AdminNavigation>()
                .Property(e => e.Icon)
                .IsUnicode(false);

            modelBuilder.Entity<AdminNavigation>()
                .HasMany(e => e.AdminNavigation1)
                .WithOptional(e => e.AdminNavigation2)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<Agenda>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Agenda>()
                .Property(e => e.Color)
                .IsUnicode(false);

            modelBuilder.Entity<Applications>()
                .HasMany(e => e.Memberships)
                .WithRequired(e => e.Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Applications>()
                .HasMany(e => e.Roles)
                .WithRequired(e => e.Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Applications>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BannerSlider>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<BannerSlider>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<BannerSlider>()
                .Property(e => e.Image)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Titel)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Informatie)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Keywords)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Image)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Intro)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.BlogReactie)
                .WithRequired(e => e.Blog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.Link)
                .WithOptional(e => e.Blog)
                .HasForeignKey(e => e.BlogArticleId);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.Navigatie)
                .WithOptional(e => e.Blog)
                .HasForeignKey(e => e.BlogArticleId);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.SubNavigatie)
                .WithOptional(e => e.Blog)
                .HasForeignKey(e => e.BlogArticleId);

            modelBuilder.Entity<BlogReactie>()
                .Property(e => e.Bericht)
                .IsUnicode(false);

            modelBuilder.Entity<BlogReactie>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.Titel)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.Descriptie)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.Keywoorden)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.HoofdTitel)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.EmailInfoAccount)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.EmailInfoWachtwoord)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.EmailInfoSMTPHost)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.EmailNoReplyAccount)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.EmailNoReplyWachtwoord)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.EmailNoReplySMTPHost)
                .IsUnicode(false);

            modelBuilder.Entity<Configuratie>()
                .Property(e => e.SiteUrl)
                .IsUnicode(false);

            modelBuilder.Entity<EmailTemplate>()
                .Property(e => e.Soort)
                .IsUnicode(false);

            modelBuilder.Entity<EmailTemplate>()
                .Property(e => e.Omschrijving)
                .IsUnicode(false);

            modelBuilder.Entity<EmailTemplate>()
                .Property(e => e.Onderwerp)
                .IsUnicode(false);

            modelBuilder.Entity<EmailTemplate>()
                .Property(e => e.Bericht)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.GebruikersID)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Gebruikersnaam)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.InnerException)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Target)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Stacktrace)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Message)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Source)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.IP)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<ErrorLog>()
                .Property(e => e.Url)
                .IsUnicode(false);

            modelBuilder.Entity<Files>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<Folder>()
                .Property(e => e.FolderName)
                .IsUnicode(false);

            modelBuilder.Entity<Folder>()
                .Property(e => e.FolderPath)
                .IsUnicode(false);

            modelBuilder.Entity<Folder>()
                .HasMany(e => e.Files)
                .WithRequired(e => e.Folder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Folder>()
                .HasMany(e => e.FolderPermission)
                .WithRequired(e => e.Folder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FormFieldsWidget>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<FormFieldsWidget>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<FormFieldsWidget>()
                .HasMany(e => e.FormFilledWidget)
                .WithRequired(e => e.FormFieldsWidget)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FormFilledWidget>()
                .Property(e => e.Value)
                .IsUnicode(false);

            modelBuilder.Entity<FormWidget>()
                .HasMany(e => e.FormFieldsWidget)
                .WithRequired(e => e.FormWidget)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Fotoalbum>()
                .Property(e => e.Titel)
                .IsUnicode(false);

            modelBuilder.Entity<Fotoalbum>()
                .Property(e => e.Descriptie)
                .IsUnicode(false);

            modelBuilder.Entity<Fotoalbum>()
                .Property(e => e.Keywoorden)
                .IsUnicode(false);

            modelBuilder.Entity<Fotoalbum>()
                .Property(e => e.Linknaam)
                .IsUnicode(false);

            modelBuilder.Entity<Fotoalbum>()
                .HasMany(e => e.PhotosInAlbum)
                .WithRequired(e => e.Fotoalbum)
                .HasForeignKey(e => e.FotoAlbumID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gebruiker>()
                .Property(e => e.Voornaam)
                .IsUnicode(false);

            modelBuilder.Entity<Gebruiker>()
                .Property(e => e.Achternaam)
                .IsUnicode(false);

            modelBuilder.Entity<Gebruiker>()
                .Property(e => e.Straat)
                .IsUnicode(false);

            modelBuilder.Entity<Gebruiker>()
                .Property(e => e.AdresToevoeging)
                .IsUnicode(false);

            modelBuilder.Entity<Gebruiker>()
                .Property(e => e.Woonplaats)
                .IsUnicode(false);

            modelBuilder.Entity<Gebruiker>()
                .Property(e => e.Postcode)
                .IsUnicode(false);

            modelBuilder.Entity<Guestbook>()
                .Property(e => e.Comment)
                .IsUnicode(false);

            modelBuilder.Entity<Guestbook>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<HtmlWidget>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<HtmlWidget>()
                .Property(e => e.Text)
                .IsUnicode(false);

            modelBuilder.Entity<Link>()
                .Property(e => e.Naam)
                .IsUnicode(false);

            modelBuilder.Entity<Link>()
                .Property(e => e.LinkUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Link>()
                .Property(e => e.Omschrijving)
                .IsUnicode(false);

            modelBuilder.Entity<Link>()
                .Property(e => e.ExternalUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Navigatie>()
                .Property(e => e.Naam)
                .IsUnicode(false);

            modelBuilder.Entity<Navigatie>()
                .Property(e => e.ExternalUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Navigatie>()
                .HasMany(e => e.SubNavigatie)
                .WithRequired(e => e.Navigatie)
                .HasForeignKey(e => e.HoofdNavigatieId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Newsletter>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<Newsletter>()
                .Property(e => e.Body)
                .IsUnicode(false);

            modelBuilder.Entity<Newsletter>()
                .HasMany(e => e.NewsletterAttachment)
                .WithRequired(e => e.Newsletter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<NewsletterAttachment>()
                .Property(e => e.fileName)
                .IsUnicode(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .Property(e => e.Titel)
                .IsUnicode(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .Property(e => e.Informatie)
                .IsUnicode(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .Property(e => e.Keywords)
                .IsUnicode(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .Property(e => e.Image)
                .IsUnicode(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .Property(e => e.Intro)
                .IsUnicode(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .HasMany(e => e.Link)
                .WithOptional(e => e.Nieuwsartikel)
                .HasForeignKey(e => e.NewsArticleId);

            modelBuilder.Entity<Nieuwsartikel>()
                .HasMany(e => e.Navigatie)
                .WithOptional(e => e.Nieuwsartikel)
                .HasForeignKey(e => e.NewsArticleId);

            modelBuilder.Entity<Nieuwsartikel>()
                .HasMany(e => e.NieuwsReactie)
                .WithRequired(e => e.Nieuwsartikel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nieuwsartikel>()
                .HasMany(e => e.SubNavigatie)
                .WithOptional(e => e.Nieuwsartikel)
                .HasForeignKey(e => e.NewsArticleId);

            modelBuilder.Entity<NieuwsReactie>()
                .Property(e => e.Bericht)
                .IsUnicode(false);

            modelBuilder.Entity<NieuwsReactie>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Pagina>()
                .Property(e => e.Titel)
                .IsUnicode(false);

            modelBuilder.Entity<Pagina>()
                .Property(e => e.LinkNaam)
                .IsUnicode(false);

            modelBuilder.Entity<Pagina>()
                .Property(e => e.SEOTitel)
                .IsUnicode(false);

            modelBuilder.Entity<Pagina>()
                .Property(e => e.SEODescriptie)
                .IsUnicode(false);

            modelBuilder.Entity<Pagina>()
                .Property(e => e.SEOKeywoorden)
                .IsUnicode(false);

            modelBuilder.Entity<Pagina>()
                .HasMany(e => e.Link)
                .WithOptional(e => e.Pagina)
                .HasForeignKey(e => e.DynamicPageId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Pagina>()
                .HasMany(e => e.Navigatie)
                .WithOptional(e => e.Pagina)
                .HasForeignKey(e => e.DynamicPageId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Pagina>()
                .HasMany(e => e.SubNavigatie)
                .WithOptional(e => e.Pagina)
                .HasForeignKey(e => e.DynamicPageId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<PhotosInAlbum>()
                .Property(e => e.Naam)
                .IsUnicode(false);

            modelBuilder.Entity<PhotosInAlbum>()
                .HasMany(e => e.AlbumInPhotoWidget)
                .WithRequired(e => e.PhotosInAlbum)
                .HasForeignKey(e => e.PhotoId);

            modelBuilder.Entity<Recht>()
                .Property(e => e.RechtNaam)
                .IsUnicode(false);

            modelBuilder.Entity<Recht>()
                .HasMany(e => e.AdminNavigation)
                .WithOptional(e => e.Recht)
                .HasForeignKey(e => e.RightId);

            modelBuilder.Entity<Recht>()
                .HasMany(e => e.GroepsRecht)
                .WithRequired(e => e.Recht)
                .HasForeignKey(e => e.RightID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RegistratieLanden>()
                .Property(e => e.Land)
                .IsUnicode(false);

            modelBuilder.Entity<RegistratieLanden>()
                .HasMany(e => e.Gebruiker)
                .WithRequired(e => e.RegistratieLanden)
                .HasForeignKey(e => e.Land);

            modelBuilder.Entity<RegistratieVragen>()
                .Property(e => e.Vraag)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.GroepsRecht)
                .WithRequired(e => e.Roles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UsersInRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<SEOPage>()
                .Property(e => e.Titel)
                .IsUnicode(false);

            modelBuilder.Entity<SEOPage>()
                .Property(e => e.SEOKeywords)
                .IsUnicode(false);

            modelBuilder.Entity<SEOPage>()
                .Property(e => e.SEODescription)
                .IsUnicode(false);

            modelBuilder.Entity<SEOPage>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<SEOPage>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<SEOPage>()
                .Property(e => e.Area)
                .IsUnicode(false);

            modelBuilder.Entity<SEOPage>()
                .HasMany(e => e.ActiveWidget)
                .WithRequired(e => e.SEOPage)
                .HasForeignKey(e => e.PageId);

            modelBuilder.Entity<SubNavigatie>()
                .Property(e => e.Naam)
                .IsUnicode(false);

            modelBuilder.Entity<SubNavigatie>()
                .Property(e => e.ExternalUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.Image)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.Author)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.AuthorWebsite)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.ThemeWebsite)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .Property(e => e.Version)
                .IsUnicode(false);

            modelBuilder.Entity<Theme>()
                .HasMany(e => e.Configuratie)
                .WithRequired(e => e.Theme)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ThemeSection>()
                .Property(e => e.CodeName)
                .IsUnicode(false);

            modelBuilder.Entity<ThemeSection>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ThemeSection>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<ThemeSection>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<ThemeSection>()
                .Property(e => e.FriendlyName)
                .IsUnicode(false);

            modelBuilder.Entity<Ticket>()
                .Property(e => e.Onderwerp)
                .IsUnicode(false);

            modelBuilder.Entity<Ticket>()
                .HasMany(e => e.TicketReactie)
                .WithRequired(e => e.Ticket)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TicketReactie>()
                .Property(e => e.Reactie)
                .IsUnicode(false);

            modelBuilder.Entity<TwitterFeedWidget>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<TwitterFeedWidget>()
                .Property(e => e.ConsumerKey)
                .IsUnicode(false);

            modelBuilder.Entity<TwitterFeedWidget>()
                .Property(e => e.ConsumerSecret)
                .IsUnicode(false);

            modelBuilder.Entity<TwitterFeedWidget>()
                .Property(e => e.ReturnUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Blog)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.AuteurID);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.BlogReactie)
                .WithOptional(e => e.Users)
                .HasForeignKey(e => e.ReactieGebruikersID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Gebruiker)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.GebruikersID);

            modelBuilder.Entity<Users>()
                .HasOptional(e => e.Memberships)
                .WithRequired(e => e.Users);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Nieuwsartikel)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.AuteurId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.NieuwsReactie)
                .WithOptional(e => e.Users)
                .HasForeignKey(e => e.ReactieGebruikersId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Users>()
                .HasOptional(e => e.Profiles)
                .WithRequired(e => e.Users);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Ticket)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.TicketGebruikersId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.TicketReactie)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.GebruikersId);

            modelBuilder.Entity<Widget>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Widget>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Widget>()
                .HasMany(e => e.ActiveWidget)
                .WithRequired(e => e.Widget)
                .HasForeignKey(e => e.KindWidget);
        }
    }
}
