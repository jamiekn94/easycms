namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TwitterFeedWidget")]
    public partial class TwitterFeedWidget
    {
        [Key]
        public int TwitterFeedId { get; set; }

        [StringLength(50)]
        public string Username { get; set; }

        public int AmountTweets { get; set; }

        public int ActiveWidgetId { get; set; }

        [StringLength(150)]
        public string ConsumerKey { get; set; }

        [StringLength(150)]
        public string ConsumerSecret { get; set; }

        [StringLength(150)]
        public string ReturnUrl { get; set; }

        public bool IncludeReplies { get; set; }

        public bool IncludeRetweets { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
