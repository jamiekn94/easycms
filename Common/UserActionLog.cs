namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserActionLog")]
    public partial class UserActionLog
    {
        public int UserActionLogId { get; set; }

        public int ActionLogId { get; set; }

        public Guid UserId { get; set; }

        public virtual ActionLog ActionLog { get; set; }

        public virtual Users Users { get; set; }
    }
}
