namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Guestbook")]
    public partial class Guestbook
    {
        public int GuestbookId { get; set; }

        public Guid? AutorId { get; set; }

        [Required]
        [StringLength(500)]
        public string Comment { get; set; }

        public bool Enabled { get; set; }

        public int? ParentId { get; set; }

        public DateTime Date { get; set; }

        [StringLength(75)]
        public string Name { get; set; }
    }
}
