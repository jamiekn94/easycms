namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SEOPage")]
    public partial class SEOPage
    {
        public SEOPage()
        {
            ActiveWidget = new HashSet<ActiveWidget>();
            Link = new HashSet<Link>();
            Navigatie = new HashSet<Navigatie>();
            SubNavigatie = new HashSet<SubNavigatie>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Titel { get; set; }

        [StringLength(250)]
        public string SEOKeywords { get; set; }

        [StringLength(250)]
        public string SEODescription { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }

        public bool Hide { get; set; }

        [StringLength(50)]
        public string Area { get; set; }

        public virtual ICollection<ActiveWidget> ActiveWidget { get; set; }

        public virtual ICollection<Link> Link { get; set; }

        public virtual ICollection<Navigatie> Navigatie { get; set; }

        public virtual ICollection<SubNavigatie> SubNavigatie { get; set; }
    }
}
