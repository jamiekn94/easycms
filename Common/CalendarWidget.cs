namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CalendarWidget")]
    public partial class CalendarWidget
    {
        [Key]
        public int CalendarId { get; set; }

        public int ActiveWidgetId { get; set; }

        public bool AllowSlide { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
