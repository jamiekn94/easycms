namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Configuratie")]
    public partial class Configuratie
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Required]
        [StringLength(150)]
        public string Titel { get; set; }

        [Required]
        [StringLength(500)]
        public string Descriptie { get; set; }

        [Required]
        [StringLength(500)]
        public string Keywoorden { get; set; }

        [Required]
        [StringLength(15)]
        public string HoofdTitel { get; set; }

        public bool GebruikersVerificatie { get; set; }

        [Required]
        [StringLength(75)]
        public string EmailInfoAccount { get; set; }

        [Required]
        [StringLength(75)]
        public string EmailInfoWachtwoord { get; set; }

        [Required]
        [StringLength(75)]
        public string EmailInfoSMTPHost { get; set; }

        public int EmailInfoSMTPPort { get; set; }

        public bool EmailInfoSMTPSecure { get; set; }

        [Required]
        [StringLength(75)]
        public string EmailNoReplyAccount { get; set; }

        [Required]
        [StringLength(75)]
        public string EmailNoReplyWachtwoord { get; set; }

        [Required]
        [StringLength(75)]
        public string EmailNoReplySMTPHost { get; set; }

        public int EmailNoReplySMTPPort { get; set; }

        public bool EmailNoReplySMTPSecure { get; set; }

        public bool WelkomEmail { get; set; }

        public bool ReactieBlog { get; set; }

        public bool ReactieNieuws { get; set; }

        [Required]
        [StringLength(50)]
        public string SiteUrl { get; set; }

        public bool ReactieGastenboek { get; set; }

        public int ThemeId { get; set; }

        public bool OptimizeImages { get; set; }

        public virtual Theme Theme { get; set; }
    }
}
