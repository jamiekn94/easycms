namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NewsWidget")]
    public partial class NewsWidget
    {
        public int NewsWidgetId { get; set; }

        public bool Slide { get; set; }

        public int AmountItems { get; set; }

        public int ActiveWidgetId { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
