namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ErrorLog")]
    public partial class ErrorLog
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string GebruikersID { get; set; }

        [Required]
        public string Gebruikersnaam { get; set; }

        [Required]
        public string Controller { get; set; }

        [Required]
        public string Action { get; set; }

        [Required]
        public string InnerException { get; set; }

        [Required]
        public string Target { get; set; }

        [Required]
        public string Stacktrace { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        public string Source { get; set; }

        public DateTime Datum { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        [Required]
        public string Type { get; set; }

        [StringLength(150)]
        public string Url { get; set; }
    }
}
