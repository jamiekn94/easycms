namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Fotoalbum")]
    public partial class Fotoalbum
    {
        public Fotoalbum()
        {
            AlbumInPhotoWidget = new HashSet<AlbumInPhotoWidget>();
            PhotosInAlbum = new HashSet<PhotosInAlbum>();
        }

        [Key]
        public int AlbumID { get; set; }

        [Required]
        [StringLength(150)]
        public string Titel { get; set; }

        [Required]
        [StringLength(2500)]
        public string Descriptie { get; set; }

        [Required]
        [StringLength(250)]
        public string Keywoorden { get; set; }

        [Required]
        [StringLength(50)]
        public string Linknaam { get; set; }

        public bool Status { get; set; }

        public virtual ICollection<AlbumInPhotoWidget> AlbumInPhotoWidget { get; set; }

        public virtual ICollection<PhotosInAlbum> PhotosInAlbum { get; set; }
    }
}
