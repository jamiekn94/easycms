namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogWidget")]
    public partial class BlogWidget
    {
        [Key]
        [Column("BlogWidget")]
        public int BlogWidget1 { get; set; }

        public bool Slide { get; set; }

        public int ActiveWidgetId { get; set; }

        public int AmountItems { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
