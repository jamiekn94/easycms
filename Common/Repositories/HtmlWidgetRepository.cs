﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class HtmlWidgetRepository : AbstractRepository<HtmlWidget>, IRepository<HtmlWidget>
    {
        public HtmlWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.HtmlWidgetId == id);
        }

        public HtmlWidget GetByActiveId(int activeId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeId);
        }

        public IEnumerable<HtmlWidget> GetList()
        {
            return MyProperty.OrderByDescending(x => x.HtmlWidgetId);
        }

        public void Add(HtmlWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(HtmlWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}