﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class AlbumInPhotoWidgetRepository : AbstractRepository<AlbumInPhotoWidget>, IRepository<AlbumInPhotoWidget>
    {
        public AlbumInPhotoWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.Id == id);
        }

        public AlbumInPhotoWidget GetByPhotoAndWidgetId(int photoId, int photoWidgetId)
        {
            return MyProperty.First(x => x.PhotoId == photoId && x.PhotoWidgetId == photoWidgetId);
        }

        public IEnumerable<AlbumInPhotoWidget> GetByPhotoWidget(int photoWidgetId)
        {
            return MyProperty.Where(x => x.PhotoWidgetId == photoWidgetId);
        }

        public IEnumerable<AlbumInPhotoWidget> GetAllByPhotoWidgetId(int photoWidgetId)
        {
            return MyProperty.Where(x => x.PhotoWidgetId == photoWidgetId).OrderByDescending(x => x.PhotoWidgetId);
        }

        public IEnumerable<AlbumInPhotoWidget> GetList()
        {
            return MyProperty.OrderByDescending(x => x.Id);
        }

        public void Add(AlbumInPhotoWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(AlbumInPhotoWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}