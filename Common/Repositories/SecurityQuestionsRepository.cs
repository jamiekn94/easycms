﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class SecurityQuestionsRepository : AbstractRepository<Common.RegistratieVragen>, IRepository<RegistratieVragen>
    {
        public RegistratieVragen GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<RegistratieVragen> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ID);
        }

        public void Add(RegistratieVragen item)
        {
            MyProperty.Add(item);
        }

        public void Remove(RegistratieVragen item)
        {
            MyProperty.Remove(item);
        }

    }
}