﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class SubNavigatieRepository : AbstractRepository<SubNavigatie>, IRepository<SubNavigatie>
    {
        public SubNavigatie GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.SubNavigatieId == id);
        }

        public IEnumerable<SubNavigatie> GetList()
        {
            return MyProperty.OrderByDescending(x => x.SubNavigatieId);
        }

        public void Add(SubNavigatie item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(SubNavigatie item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public int GetAmountSubCategories(int parentCategory)
        {
            return MyProperty.Count(x => x.HoofdNavigatieId == parentCategory);
        }

        public IQueryable<SubNavigatie> GetSubCategorieen(int hoofdcategorie)
        {
            return MyProperty.Where(x => x.HoofdNavigatieId == hoofdcategorie && x.Status).OrderBy(x=> x.OrderNumber);
        }
    }
}