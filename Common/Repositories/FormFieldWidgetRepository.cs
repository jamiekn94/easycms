﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class FormFieldWidgetRepository : AbstractRepository<FormFieldsWidget>, IRepository<FormFieldsWidget>
    {
        public FormFieldsWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.FormId == id);
        }

        public IEnumerable<FormFieldsWidget> GetList()
        {
            return MyProperty;
        }

        public void Add(FormFieldsWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(FormFieldsWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}