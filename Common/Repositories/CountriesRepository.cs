﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class CountriesRepository : AbstractRepository<RegistratieLanden>, IRepository<RegistratieLanden>
    {
        public RegistratieLanden GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<RegistratieLanden> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ID);
        }

        public void Add(RegistratieLanden item)
        {
            MyProperty.Add(item);
        }

        public void Remove(RegistratieLanden item)
        {
            MyProperty.Remove(item);
        }

    }
}