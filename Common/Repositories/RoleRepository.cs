﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class RoleRepository : AbstractRepository<Common.Roles>, IRepository<Roles>
    {
        public Roles GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Roles GetById(Guid id)
        {
            return MyProperty.FirstOrDefault(x => x.RoleId == id);
        }

        public Roles GetByName(string name)
        {
            return MyProperty.FirstOrDefault(x => x.RoleName.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        }

        public IEnumerable<Roles> GetList()
        {
            return MyProperty.OrderByDescending(x => x.RoleId);
        }

        public void Add(Roles item)
        {
            MyProperty.Add(item);
        }

        public void Remove(Roles item)
        {
            MyProperty.Remove(item);
        }

    }
}