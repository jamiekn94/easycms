﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class MembershipRepository : AbstractRepository<Membership>, IRepository<Membership>, IDisposable
    {
        public Membership GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Membership GetByUsername(string username)
        {
            var userId = UserProvider.GetProviderUserKey(username);
            return MyProperty.First(x => x.UserId == userId);
        }

        public Membership GetByEmail(string email)
        {
            return MyProperty.First(x => x.Email.ToLower() == email.ToLower());
        }

        public IEnumerable<Membership> GetList()
        {
            return MyProperty;
        }

        public Membership GetByUserId(Guid id)
        {
           return MyProperty.FirstOrDefault(x => x.UserId == id);
        }

        public int GetPasswordFailures(string username)
        {
            var userId = UserProvider.GetProviderUserKey(username);
            return MyProperty.First(x => x.UserId == userId).FailedPasswordAttemptCount;
        }

        public int GetUnverifiedUsers()
        {
            return MyProperty.Count(x => x.IsApproved == false);
        }

        public void Add(Membership item)
        {
            throw new NotImplementedException();
        }

        public void Remove(Membership item)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            base.Save();
        }

        void IDisposable.Dispose()
        {
            Dispose();
        }
    }
}