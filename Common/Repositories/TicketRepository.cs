﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class TicketRepository :  AbstractRepository<Ticket>, IRepository<Ticket>
    {
        public Ticket GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.TicketID == id);
        }

        public IEnumerable<Ticket> GetList()
        {
            return MyProperty.OrderByDescending(x => x.TicketID);
        }

        public void Add(Ticket item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Ticket item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IQueryable<Ticket> Get5OpenTickets()
        {
            return MyProperty.Where(x => x.Status == 0).OrderByDescending(x => x.TicketID).Take(5);
        }

        public int GetAmountOpenTickets()
        {
            return MyProperty.Count(x => x.Status == 0);
        }

        public Ticket GetLastTicket()
        {
            return MyProperty.OrderByDescending(x => x.TicketID).First();
        }

        public Ticket GetFirstTicket()
        {
            return MyProperty.First();
        }
    }
}