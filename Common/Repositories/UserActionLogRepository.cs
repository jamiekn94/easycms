﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class UserActionLogRepository : AbstractRepository<UserActionLog>, IRepository<UserActionLog>
    {
        public UserActionLog GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.UserActionLogId == id);
        }

        public IEnumerable<UserActionLog> GetList()
        {
            return MyProperty.OrderByDescending(x => x.UserActionLogId);
        }

        public void Add(UserActionLog item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(UserActionLog item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public UserActionLog GetFirst(Guid userId)
        {
            return MyProperty.First(x => x.UserId == userId);
        }

        public bool HasActionLog(Guid userId)
        {
            return MyProperty.FirstOrDefault() != null;
        }

        public void RemoveAll()
        {
            MyProperty.DeleteAllOnSubmit(GetList());
        }

        public int GetAmountNewActionLogs()
        {
            var database = new DataClassesDataContext();

            return (from db in database.ActionLogs
                join ual in database.UserActionLogs on db.Id equals ual.ActionLogId
                select ual.ActionLogId).Count();
        }

        public IEnumerable<ActionsModel> GetNewActionLogs()
        {
            var database = new DataClassesDataContext();
            return (from db in database.ActionLogs
                join ual in database.UserActionLogs on db.Id equals ual.ActionLogId
                where ual.UserId == UserProvider.GetUserId()
                select new ActionsModel
                {
                    ActionLogId = ual.ActionLogId,
                    Information = db.Information,
                    FullName = UserProvider.GetFullName(ual.UserId),
                    TimeAgo = DateProvider.GetNiceDate((db.Date))
                }).Take(5).OrderBy(x => x.HasRead == false).ThenByDescending(x => x.ActionLogId);
        }
    }
}