﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    internal interface IRepository<T>
    {
        T GetById(int id);
        IEnumerable<T> GetList();
        void Add(T item);
        void Remove(T item);
        void Save();
    }
}