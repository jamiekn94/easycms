﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{
   public class ThemeRepository : AbstractRepository<Theme>, IRepository<Theme>
    {
        public Theme GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ThemeId == id);
        }

        public Theme GetByName(string name)
        {
            return MyProperty.FirstOrDefault(x => x.Name.ToLower() == name.ToLower());
        }

        public IEnumerable<Theme> GetList()
        {
            return MyProperty;
        }

        public void Add(Theme item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Theme item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

       public int GetLastAddedId()
       {
           return MyProperty.OrderByDescending(x => x.ThemeId).Select(x => x.ThemeId).First();
       }
    }
}
