﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Common.Providers.Caching;

namespace Common.Repositories
{
   public static class DatabaseExtensions
    {
        public static IEnumerable<T> FromCache<T>(this IQueryable<T> query, int timeMinutes)
        {
            // Get query hash key
            string queryHashKey = GetHashKey(query);

            // Return result
            IEnumerable<T> returnResult;

            // Add cache
            if (!ServerCachingProvider.IsSet(queryHashKey))
            {
                // Set cache and return result
                returnResult = SetCache(query, timeMinutes, queryHashKey);
            }
            // Get cache
            else
            {
                // Get result from cache
                returnResult = GetCache<T>(queryHashKey);
            }

            // Return cache
            return returnResult;
        }

        public static IEnumerable<T> FromCache<T>(this IEnumerable<T> query, int timeMinutes)
        {
            // Get query hash key
            string queryHashKey = GetHashKey(query);

            // Return result
            IEnumerable<T> returnResult;

            // Add cache
            if (!ServerCachingProvider.IsSet(queryHashKey))
            {
                // Set cache and return result
                returnResult = SetCache(query, timeMinutes, queryHashKey);
            }
            // Get cache
            else
            {
                // Get result from cache
                returnResult = GetCache<T>(queryHashKey);
            }

            // Return cache
            return returnResult;
        }

        private static IEnumerable<T> SetCache<T>(IEnumerable<T> query, int timeMinutes, string queryHashKey)
        {
            // Execute query
            IEnumerable<T> returnResult = query.ToList();

            // Cache query info
            ServerCachingProvider.Set(queryHashKey, returnResult, timeMinutes);

            // Return result
            return returnResult;
        }

        private static IEnumerable<T> GetCache<T>(string queryHashKey)
        {
            // Get query result from cache
            return (IEnumerable<T>)ServerCachingProvider.Get(queryHashKey);
        }

        public static IEnumerable<T> RefreshFromCache<T>(this IEnumerable<T> query, int timeMinutes)
        {
            // Get query hash key
            string queryHashKey = GetHashKey(query);

            // Remove cache
            if (!ServerCachingProvider.IsSet(queryHashKey))
            {
                ServerCachingProvider.Invalidate(queryHashKey);
            }

            // Set cache
            var returnResult = SetCache(query, timeMinutes, queryHashKey);

            // Return cache
            return returnResult;
        }

        private static string GetHashKey<T>(IEnumerable<T> query)
        {
            // Get code from this query
            #pragma warning disable 618
            return FormsAuthentication.HashPasswordForStoringInConfigFile(query.ToString(), "MD5");
            #pragma warning restore 618
        }
    }
}
