﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class PhotoWidgetRepository : AbstractRepository<PhotoWidget>, IRepository<PhotoWidget>
    {
        public PhotoWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.Id == id);
        }

        public PhotoWidget GetByActiveId(int activeId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeId);
        }

        public IEnumerable<PhotoWidget> GetList()
        {
            return MyProperty.OrderByDescending(x => x.Id);
        }

        public void Add(PhotoWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(PhotoWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}