﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class NewsCommentsRepository : AbstractRepository<NieuwsReactie>, IRepository<NieuwsReactie>
    {
        public NieuwsReactie GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ReactieID == id);
        }

        public IEnumerable<NieuwsReactie> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ReactieID);
        }

        public void Add(NieuwsReactie item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(NieuwsReactie item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public int GetAmountComments(int articleId)
        {
            return MyProperty.Count(x => x.Status && x.ArtikelID == articleId);
        }

        public int GetAmountOpenComments()
        {
            return MyProperty.Count(x => x.Status == false);
        }

        public IQueryable<NieuwsReactie> GetListByArticleId(int articleId)
        {
            return MyProperty.Where(x => x.Status && x.ArtikelID == articleId);
        }
    }
}