﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class AgendaRepository : AbstractRepository<Agenda>, IRepository<Agenda>
    {
        public Agenda GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.AgendaId == id);
        }

        public IEnumerable<Agenda> GetList()
        {
            return MyProperty.OrderByDescending(x => x.AgendaId);
        }

        public void Add(Agenda item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Agenda item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public Agenda GetLastAgenda()
        {
            return MyProperty.OrderByDescending(x => x.AgendaId).FirstOrDefault();
        }

        public IQueryable<Agenda> GetViewableAgenda(Guid userId)
        {
            return MyProperty.Where(x => x.PublicPoint || x.UserId == userId);
        }
    }
}