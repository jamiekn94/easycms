﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class PhotosInAlbumRepository : AbstractRepository<PhotosInAlbum>, IRepository<PhotosInAlbum>
    {
        public PhotosInAlbum GetById(int id)
        {
            return MyProperty.First(x => x.FotoID == id);
        }

        public IEnumerable<PhotosInAlbum> GetList()
        {
            return MyProperty.OrderByDescending(x => x.FotoAlbumID);
        }

        public int GetAmountPhotos()
        {
            return MyProperty.Count();
        }

        public void Add(PhotosInAlbum item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(PhotosInAlbum item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IQueryable<PhotosInAlbum> GetPhotosFromAlbum(int albumId)
        {
            return MyProperty.Where(x => x.FotoAlbumID == albumId);
        }

        public PhotosInAlbum GetFirstPhotoFromAlbum(int albumId)
        {
            return MyProperty.FirstOrDefault(x => x.FotoAlbumID == albumId);
        }

        public int GetAmountPhotos(int photoAlbumId)
        {
            return MyProperty.Count(x => x.FotoAlbumID == photoAlbumId);
        }

        public void RemoveAll(IEnumerable<PhotosInAlbum> PhotosList)
        {
            MyProperty.DeleteAllOnSubmit(PhotosList);
        }
    }
}