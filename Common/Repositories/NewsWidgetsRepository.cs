﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class NewsWidgetRepository : AbstractRepository<NewsWidget>, IRepository<NewsWidget>
    {
        public NewsWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.NewsWidgetId == id);
        }

        public NewsWidget GetByActiveWidgetId(int activeWidgetId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeWidgetId);
        }

        public IEnumerable<NewsWidget> GetList()
        {
            return MyProperty.OrderByDescending(x => x.NewsWidgetId);
        }

        public void Add(NewsWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(NewsWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}