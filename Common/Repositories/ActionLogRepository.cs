﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class ActionLogRepository : AbstractRepository<ActionLog>, IRepository<ActionLog>
    {
        public ActionLog GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<ActionLog> GetList()
        {
            return MyProperty.OrderByDescending(x => x.Id);
        }

        public void Add(ActionLog item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(ActionLog item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public int GetHighestActionId()
        {
            return MyProperty.Max(x => x.Id);
        }

        public void RemoveAll()
        {
            MyProperty.DeleteAllOnSubmit(GetList());
        }

        public int GetAmountNewActionLogs()
        {
            var userActionLogRepository = new UserActionLogRepository();

            var userActionLogInfo = userActionLogRepository.GetFirst(UserProvider.GetUserId());

            return GetHighestActionId() - userActionLogInfo.ActionLogId;
        }

        public IEnumerable<ActionsModel> GetNewActionLogs()
        {
            var userRepository = new GebruikersRepository();
            var arrayActionsModel = new Collection<ActionsModel>();

           var arrayDbActionModels = GetList().OrderByDescending(x => x.Id).Take(5);

            foreach (var arrayDbActionModel in arrayDbActionModels)
            {
                var userInfo = userRepository.GetFirstAndLastName(arrayDbActionModel.UserId);

                arrayActionsModel.Add(new ActionsModel
                {
                    ActionLogId = arrayDbActionModel.Id,
                    Information = arrayDbActionModel.Information,
                    FullName = userInfo.Firstname + " " + userInfo.Lastname,
                    TimeAgo = DateProvider.GetNiceDate((arrayDbActionModel.Date))
                });   
            }

            return arrayActionsModel;
        }
    }
}