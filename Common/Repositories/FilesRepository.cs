﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class FilesRepository : AbstractRepository<File>, IRepository<File>
    {
        public File GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.FileId == id);
        }

        public IEnumerable<File> GetList()
        {
            return MyProperty.OrderByDescending(x => x.FileId);
        }

        public void Add(File item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(File item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IQueryable<File> GetFilesByFolderId(int folderId)
        {
            return MyProperty.Where(x => x.FolderId == folderId);
        }

        public void RemoveAll(IEnumerable<File> items)
        {
            MyProperty.DeleteAllOnSubmit(items);
        }

        public bool FileExists(int folderId, string fileName)
        {
            return MyProperty.Any(x => x.FolderId == folderId && x.FileName == fileName);
        }
    }
}