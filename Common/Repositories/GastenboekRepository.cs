﻿using System.Collections.Generic;
using System.Linq;
using PagedList;

namespace Common.Repositories
{
    public class GastenboekRepository : AbstractRepository<Guestbook>
    {
        public Guestbook GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.GuestbookId == id);
        }

        public IEnumerable<Guestbook> GetList()
        {
            return MyProperty.OrderByDescending(x => x.GuestbookId).ToList();
        }

        public void Add(Guestbook item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Guestbook item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public int GetAmountOpenComments()
        {
            return MyProperty.Count(x => x.Enabled == false);
        }

        public void Save()
        {
            base.Save();
        }

        public IPagedList<Guestbook> GetPage(int page, int amount)
        {
            return MyProperty.Where(x=> x.ParentId == null).ToPagedList(page, amount);
        }

        public IQueryable<Guestbook> GetSubGuestbook(int parentId, int amount)
        {
            return MyProperty.Where(x => x.ParentId == parentId).Take(amount);
        }

        public IQueryable<Guestbook> GetSubGuestbookPage(int page, int pageAmount, int parentId)
        {
            return MyProperty.Where(x => x.ParentId == parentId).Skip(page*pageAmount).Take(pageAmount);
        }

        /// <summary>
        /// Get amount pages and the amount items
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public int[] GetAmount(int amount)
        {
            var amountItems = MyProperty.Count(x=> x.ParentId == null);
            return new[] { amountItems / amount, amountItems };
        }
    }
}