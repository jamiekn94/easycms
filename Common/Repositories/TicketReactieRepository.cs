﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class TicketReactieRepository : AbstractRepository<TicketReactie>, IRepository<TicketReactie>
    {
        public TicketReactie GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.TicketID == id);
        }

        public IEnumerable<TicketReactie> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ReactieID);
        }

        public void Add(TicketReactie item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(TicketReactie item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public TicketReactie GetFirstReaction(int ticketId)
        {
            return MyProperty.First(x => x.TicketID == ticketId);
        }

        public TicketReactie GetLastReaction(int ticketId)
        {
            return MyProperty.OrderByDescending(x => x.ReactieID).First(x => x.TicketID == ticketId);
        }

        public IEnumerable<TicketReactie> GetReactiesById(int ticketId)
        {
            return MyProperty.Where(x => x.TicketID == ticketId).ToList();
        }

        public void RemoveAll(IEnumerable<TicketReactie> ticketReacties)
        {
            MyProperty.DeleteAllOnSubmit(ticketReacties);
        }
    }
}