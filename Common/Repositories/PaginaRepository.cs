﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class PaginaRepository : AbstractRepository<Pagina>, IRepository<Pagina>
    {
        public Pagina GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.PaginaID == id);
        }

        public IEnumerable<Pagina> GetList()
        {
            return MyProperty.OrderByDescending(x => x.PaginaID);
        }

        public void Add(Pagina item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Pagina item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}