﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Common.Repositories
{
    public class NavigatieRepository : AbstractRepository<Navigatie>, IRepository<Navigatie>
    {
        public Navigatie GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<Navigatie> GetList()
        {
            return MyProperty.OrderByDescending(x => x.OrderNumber);
        }

        public void Add(Navigatie item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Navigatie item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IEnumerable<SelectListItem> GetHoofdCategorieen(int id = -1)
        {
            var categorieenList = GetList();
            var itemList = new List<SelectListItem>();

            foreach (var categorie in categorieenList)
            {
                itemList.Add(new SelectListItem
                {
                    Text = categorie.Naam,
                    Value = categorie.ID.ToString(),
                    Selected = categorie.ID == id
                });
            }

            return itemList;
        }
    }
}