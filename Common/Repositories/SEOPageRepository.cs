﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class SeoPageRepository : AbstractRepository<SEOPage>, IRepository<SEOPage>
    {
        public SEOPage GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.Id == id);
        }

        public int GetLastAddedPageId()
        {
            return MyProperty.OrderByDescending(x => x.Id).Select(x => x.Id).First();
        }

        public IEnumerable<SEOPage> GetList()
        {
            return MyProperty.OrderByDescending(x => x.Id);
        }

        public string GetAdminSiteTitle(string controller, string action)
        {
            var seo = GetSeoByControllerAndAction(controller, action, true);
            return seo != null ? seo.Titel : string.Empty;
        }

        private SEOPage GetSeoByControllerAndAction(string controller, string action, bool isAdmin)
        {
            if (isAdmin)
            {
                return
                    GetList().FromCache(60).FirstOrDefault(
                        x =>
                            x.Controller.ToLower() == controller.ToLower() && x.Action.ToLower() == action.ToLower() &&
                            x.Area.ToLower() == "admin");
            }

            return GetList().FromCache(60).FirstOrDefault(
                x =>
                    x.Controller.ToLower() == controller.ToLower() && x.Action.ToLower() == action.ToLower() &&
                    x.Area == null);
        }

        public void Add(SEOPage item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(SEOPage item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public SEOPage GetSeo(string controller, string action, bool adminPage)
        {
            return GetSeoByControllerAndAction(controller, action, adminPage);
        }
    }
}