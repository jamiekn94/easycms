﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class ConfiguratieRepository : AbstractRepository<Configuratie>, IRepository<Configuratie>
    {
        public const string ThemePath = "ThemePath";

        public Configuratie GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<Configuratie> GetList()
        {
            throw new NotImplementedException();
        }

        public void Add(Configuratie item)
        {
            throw new NotImplementedException();
        }

        public void Remove(Configuratie item)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            base.Save();
        }

        public void UpdateCachedThemeLocation()
        {
            Common.Providers.Caching.ServerCachingProvider.Set(ThemePath, GetThemePath(), 0, true);
        }

        private string GetThemePath()
        {
            int themeId = MyProperty.Select(x => x.ThemeId).First();
            var themeRepository = new ThemeRepository();
            return themeRepository.GetById(themeId).Location;
        }

        public string GetCachedThemePath()
        {
            var cachedThemePath = Common.Providers.Caching.ServerCachingProvider.Get(ThemePath);

            if (cachedThemePath != null)
            {
                return cachedThemePath.ToString();
            }

            UpdateCachedThemeLocation();

            return Common.Providers.Caching.ServerCachingProvider.Get(ThemePath).ToString();
        }
    }
}