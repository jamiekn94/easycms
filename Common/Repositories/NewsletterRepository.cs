﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class NewsletterRepository : AbstractRepository<Newsletter>, IRepository<Newsletter>
    {
        public Newsletter GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.NewsletterId == id);
        }

        public IEnumerable<Newsletter> GetList()
        {
            return MyProperty.OrderByDescending(x => x.NewsletterId);
        }

        public void Add(Newsletter item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Newsletter item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public Newsletter GetLastAdded()
        {
            return MyProperty.OrderByDescending(x => x.NewsletterId).First();
        }
    }
}