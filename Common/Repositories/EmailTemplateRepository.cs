﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class EmailTemplateRepository : AbstractRepository<EmailTemplate>, IRepository<EmailTemplate>
    {
        public EmailTemplate GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<EmailTemplate> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ID);
        }

        public void Add(EmailTemplate item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(EmailTemplate item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}