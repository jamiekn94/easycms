﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class WidgetRepository : AbstractRepository<Widget>, IRepository<Widget>
    {
        public Widget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.WidgetId == id);
        }

        public IEnumerable<Widget> GetList()
        {
            return MyProperty;
        }

        public void Add(Widget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Widget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public int GetLastAddedId()
        {
            return MyProperty.OrderByDescending(x => x.WidgetId).Select(x => x.WidgetId).First();
        }

        public void Save()
        {
            base.Save();
        }
    }
}
