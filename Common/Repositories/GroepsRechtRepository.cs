﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Roles;

namespace Common.Repositories
{
    public class GroepsRechtRepository : AbstractRepository<GroepsRecht>, IRepository<GroepsRecht>
    {
        public GroepsRecht GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GroepsRecht> GetList()
        {
            return MyProperty.OrderBy(x => x.Id);
        }

        public void Add(GroepsRecht item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(GroepsRecht item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IEnumerable<GroepsRecht> GetRightsByRole(Guid roleId)
        {
            var db = new DataClassesDataContext();
            return (from database in db.GroepsRechts
                join rights in db.Rechts on database.RightID equals rights.RechtID
                where database.RoleId == roleId
                orderby rights.RechtNaam[0]
                select database).AsParallel();
        }

        public void AddList(IEnumerable<GroepsRecht> rights)
        {
            MyProperty.InsertAllOnSubmit(rights);
        }

        public bool HasPemission(Guid roleId, int rightId)
        {
            var permission = MyProperty.FirstOrDefault(x => x.RoleId == roleId && x.RightID == rightId);
            return permission != null;
        }

        /// <summary>
        /// Krijg een lijst van alle rechten van een bepaalde groep
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RechtenGroepModel> GetRechtenFromGroepId(string roleName)
        {
            var roleId = RoleProvider.GetRoleIdByName(roleName);

            return GetRightsFromGroupId(roleId);
        }

        /// <summary>
        /// Krijg een lijst van alle rechten van een bepaalde groep
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RechtenGroepModel> GetRightsFromGroupId(Guid roleId)
        {
            var rightRepository = new RightRepository();

            var arrayReturnModel = new Collection<RechtenGroepModel>();

            var listRightIds = GetList().Where(x => x.RoleId == roleId).Select(x => x.RightID).ToList();

            var arrayRights = rightRepository.GetList().Select(x=> new Recht
            {
                RechtNaam = x.RechtNaam,
                RechtID = x.RechtID
            }).ToList();

            foreach (var rightId in listRightIds)
            {
                arrayReturnModel.Add(new RechtenGroepModel
                {
                    RechtId = rightId,
                    RechtNaam = arrayRights.First(x => x.RechtID == rightId).RechtNaam,
                });
            }
            return arrayReturnModel;
        }

        public void RemoveAll(IEnumerable<GroepsRecht> groepsRechtsModels)
        {
            MyProperty.DeleteAllOnSubmit(groepsRechtsModels);
        }
    }
}