﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using Common.Providers.Caching;

namespace Common.Repositories
{
    public abstract class AbstractRepository<TEntityType> : IDisposable where TEntityType : class
    {
        private readonly DataClassesDataContext _dataSource;

        protected AbstractRepository()
        {
            _dataSource = new DataClassesDataContext();
        }

        protected Table<TEntityType> MyProperty
        {
            get
            {
                if (_dataSource == null) { throw new Exception("The datasource has already been disposed"); }

                return _dataSource.GetTable<TEntityType>();
            }
        }

        public void Attach(TEntityType entity)
        {
            MyProperty.Attach(entity);
        }

        public void Refresh(TEntityType entity)
        {
           _dataSource.Refresh(RefreshMode.KeepCurrentValues, entity);
        }

        public void Save()
        {
            try
            {
                MyProperty.Context.SubmitChanges(ConflictMode.ContinueOnConflict);
            }
            catch (ChangeConflictException ce)
            {

                MyProperty.Context.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                MyProperty.Context.Refresh(RefreshMode.KeepChanges);

                MyProperty.Context.SubmitChanges();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~AbstractRepository()
        {
            Dispose(false);
        }

        protected void Dispose(Boolean disposing)
        {
            // free unmanaged ressources here
            if (disposing)
            {
                // free managed ressources here
                if (_dataSource != null)
                {
                    _dataSource.Dispose();
                }
            }
        }
    }
}