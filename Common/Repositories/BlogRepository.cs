﻿using System.Collections.Generic;
using System.Linq;
using PagedList;

namespace Common.Repositories
{
    public class BlogRepository : AbstractRepository<Blog>, IRepository<Blog>
    {
        public Blog GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.BlogID == id);
        }

        public Blog GetLastAded()
        {
            return MyProperty.OrderByDescending(x => x.BlogID).First();
        }

        public IEnumerable<Blog> GetList()
        {
            return MyProperty.OrderByDescending(x => x.BlogID);
        }

        public IEnumerable<Blog> GetPage(int page, int amount)
        {
            return MyProperty.Where(x=> x.Status).OrderByDescending(x => x.BlogID).ToPagedList(page, amount);
        }

        public IEnumerable<Blog> GetArticlesByKeyword(string keyword, int page, int amount)
        {
            return MyProperty.Where(x => x.Status && x.Keywords.Contains(keyword)).ToPagedList(page, amount);
        }

        public IEnumerable<Blog> GetArticlesBySearchTerm(string searchTerm, int page, int amount)
        {
            return
                MyProperty.Where(
                    x => x.Status &&
                        x.Titel.Contains(searchTerm) || x.Keywords.Contains(searchTerm) || x.Intro.Contains(searchTerm) ||
                               x.Informatie.Contains(searchTerm)).ToPagedList(page, amount);
        }

        /// <summary>
        /// Get amount pages and the amount items
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public int[] GetAmount(int amount)
        {
            var amountItems = MyProperty.Count(x=> x.Status);
            return new[] { amountItems / amount, amountItems };
        }

        /// <summary>
        /// Get amount pages and the amount of items with this keyword
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public int[] GetAmountKeywords(int amount, string keyword)
        {
            var amountItems = MyProperty.Count(x => x.Status && x.Keywords.Contains(keyword));
            return new[] { amountItems / amount, amountItems };
        }

        /// <summary>
        /// Get amount pages and the amount items with a search term
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public int[] GetAmount(int amount, string searchTerm)
        {
            var amountItems = MyProperty.Count(x =>
                x.Status &&
                x.Titel.Contains(searchTerm) || x.Keywords.Contains(searchTerm) || x.Intro.Contains(searchTerm) ||
                x.Informatie.Contains(searchTerm));
            return new[] { amountItems / amount, amountItems };
        }

        public void Add(Blog item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Blog item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}