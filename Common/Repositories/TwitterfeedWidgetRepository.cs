﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class TwitterfeedRWidgetRepository : AbstractRepository<TwitterFeedWidget>, IRepository<TwitterFeedWidget>
    {
        public TwitterFeedWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.TwitterFeedId == id);
        }

        public TwitterFeedWidget GetByActiveId(int activeId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeId);
        }

        public IEnumerable<TwitterFeedWidget> GetList()
        {
            return MyProperty;
        }

        public void Add(TwitterFeedWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(TwitterFeedWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}