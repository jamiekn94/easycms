﻿using System.Collections.Generic;
using System.Linq;
using PagedList;

namespace Common.Repositories
{
    public class PhotoAlbumRepository : AbstractRepository<Fotoalbum>, IRepository<Fotoalbum>
    {
        public Fotoalbum GetById(int id)
        {
            return MyProperty.First(x => x.AlbumID == id);
        }

        public IEnumerable<Fotoalbum> GetList()
        {
            return MyProperty.OrderByDescending(x => x.AlbumID);
        }

        public void Add(Fotoalbum item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Fotoalbum item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public Fotoalbum GetLast()
        {
            return MyProperty.OrderByDescending(x => x.AlbumID).First();
        }

        public IEnumerable<Fotoalbum> GetPage(int page, int amount)
        {
            return MyProperty.Where(x=> x.Status).ToPagedList(page, amount);
        }

        public int GetAmountPhotoAlbums()
        {
            return MyProperty.Count(x => x.Status);
        }

        public IEnumerable<Fotoalbum> GetPagesBySearch(string searchTerm, int pageNumber, int pageSize)
        {
            return
                MyProperty.Where(x => x.Status && x.Titel.Contains(searchTerm) || x.Descriptie.Contains(searchTerm))
                    .ToPagedList(pageNumber, pageSize);
        }

        /// <summary>
        /// Get amount pages and the amount items
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public int[] GetAmount(int amount)
        {
            var amountItems = MyProperty.Count(x=> x.Status);
            return new[] { amountItems / amount, amountItems };
        }

        /// <summary>
        /// Get amount pages and the amount items from a search term
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public int[] GetAmount(int amount, string searchTerm)
        {
            var amountItems = MyProperty.Count(x => x.Status && x.Titel.Contains(searchTerm) || x.Descriptie.Contains(searchTerm));
            return new[] { amountItems / amount, amountItems };
        }
    }
}