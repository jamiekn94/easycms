﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class LinkRepository : AbstractRepository<Link>, IRepository<Link>
    {
        public Link GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.LinkID == id);
        }

        public IEnumerable<Link> GetList()
        {
            return MyProperty.OrderByDescending(x => x.LinkID);
        }

        public void Add(Link item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Link item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}