﻿using System.Collections.Generic;
using System.Linq;
using PagedList;

namespace Common.Repositories
{
    public class AdminNavigationRepository : AbstractRepository<AdminNavigation>, IRepository<AdminNavigation>
    {
        public AdminNavigation GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.NavigationId == id);
        }

        public IEnumerable<AdminNavigation> GetList()
        {
            return MyProperty.ToList();
        }

        public void Add(AdminNavigation item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(AdminNavigation item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}