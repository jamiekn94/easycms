﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Providers.Roles;

namespace Common.Repositories
{
    public class FolderPermissionRepository : AbstractRepository<FolderPermission>, IRepository<FolderPermission>
    {
        public FolderPermission GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.FolderPermissionId == id);
        }

        public IEnumerable<FolderPermission> GetList()
        {
            return MyProperty.OrderByDescending(x => x.FolderPermissionId);
        }

        public void Add(FolderPermission item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(FolderPermission item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IQueryable<FolderPermission> GetPermissionsByRoleName(string roleName)
        {
            var roleId = RoleProvider.GetRoleIdByName(roleName);

            return MyProperty.Where(x => x.RoleId == roleId);
        }

        public IQueryable<FolderPermission> GetByFolderId(int folderId)
        {
            return MyProperty.Where(x => x.FolderId == folderId);
        }

        public void RemoveAll(IEnumerable<FolderPermission> items)
        {
            MyProperty.DeleteAllOnSubmit(items);
        }
    }
}