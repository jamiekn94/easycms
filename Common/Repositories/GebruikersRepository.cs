﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Security;
using Common.Models.Admin.Users;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class GebruikersRepository : AbstractRepository<Gebruiker>
    {
        public Gebruiker GetById(object providerUserId)
        {
            return MyProperty.FirstOrDefault(x => Equals(x.ProviderGebruikersId, providerUserId));
        }

        public UserModelInfo GetFirstAndLastName(Guid providerUserId)
        {
            return MyProperty.Where(x => x.GebruikersID == providerUserId).Select(x => new UserModelInfo
            {
                Firstname = x.Voornaam,
                Lastname = x.Achternaam
            }).First();
        }

        public IEnumerable<Gebruiker> GetList(int amount = -1)
        {
            if (amount == -1)
            {
                return MyProperty;
            }
            return MyProperty.Take(amount);
        }

        public int GetAmountUsers()
        {
            return MyProperty.Count();
        }

        public void Add(Gebruiker item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public void Remove(Gebruiker item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public IQueryable<Gebruiker> GetEmailsForNewsletter()
        {
            return
                MyProperty.Where(x => x.Newsletter)
                    .Select(x => new Gebruiker {ProviderGebruikersId = x.ProviderGebruikersId});
        }

        internal string GenerateSalt(int length)
        {
            var rng = new RNGCryptoServiceProvider();
            var buffer = new byte[length];
            rng.GetBytes(buffer);
            return Convert.ToBase64String(buffer);
        }

        /// <summary>
        ///     Return alle landen van de database
        /// </summary>
        /// <returns>IEnumerable<SelectedListItem></SelectedListItem></returns>
        public IEnumerable<SelectListItem> GetLanden(int selectedId = -1)
        {
            var selectedListItemsLanden = new List<SelectListItem>();
            using (var context = new DataClassesDataContext())
            {
                var landenList = context.RegistratieLandens.ToList();
                selectedListItemsLanden.AddRange(landenList.Select(landItem => new SelectListItem
                {
                    Value = landItem.ID.ToString(CultureInfo.InvariantCulture),
                    Text = landItem.Land,
                    Selected = selectedId != -1 && landItem.ID == selectedId
                }));
            }
            return selectedListItemsLanden;
        }

        public string GetRegistrationQuestion(int id)
        {
            using (var context = new DataClassesDataContext())
            {
                return context.RegistratieVragens.Where(x => x.ID == id).Select(x => x.Vraag).First();
            }
        }

        public int GetRegistrationQuestion(string question)
        {
            using (var context = new DataClassesDataContext())
            {
                return context.RegistratieVragens.Where(x => x.Vraag == question).Select(x => x.ID).First();
            }
        }

        /// <summary>
        ///     Return alle beveilingsvragen van de database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetBeveilingsVragen(int selectedId = -1)
        {
            var selectedListItemsVragen = new List<SelectListItem>();
            using (var context = new DataClassesDataContext())
            {
                var vragenList = context.RegistratieVragens.ToList();
                selectedListItemsVragen.AddRange(vragenList.Select(vraagItem => new SelectListItem
                {
                    Value = vraagItem.ID.ToString(CultureInfo.InvariantCulture),
                    Text = vraagItem.Vraag,
                    Selected = selectedId != -1 && vraagItem.ID == selectedId
                }));
            }
            return selectedListItemsVragen;
        }

        public IEnumerable<SelectListItem> GetGroepen(string roleName = null)
        {
            var selectedListGroepen = new List<SelectListItem>();
            var userRolesList = Roles.GetAllRoles();

            selectedListGroepen.AddRange(userRolesList.Select(roleItem => new SelectListItem
            {
                Value = roleItem,
                Text = roleItem,
                Selected = roleItem != null && roleName == roleItem
            }));

            return selectedListGroepen;
        }
    }
}