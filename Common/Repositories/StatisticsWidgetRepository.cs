﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class StatisticsWidgetRepository : AbstractRepository<StatisticsWidget>, IRepository<StatisticsWidget>
    {
        public StatisticsWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.StatisticsId == id);
        }

        public StatisticsWidget GetByActiveId(int activeId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeId);
        }

        public IEnumerable<StatisticsWidget> GetList()
        {
            return MyProperty;
        }

        public void Add(StatisticsWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(StatisticsWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

    }
}