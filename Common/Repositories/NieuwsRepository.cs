﻿using System.Collections.Generic;
using System.Linq;
using PagedList;

namespace Common.Repositories
{
    public class NieuwsRepository : AbstractRepository<Nieuwsartikel>, IRepository<Nieuwsartikel>
    {
        public Nieuwsartikel GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ArtikelID == id);
        }

        public IEnumerable<Nieuwsartikel> GetList()
        {
            return MyProperty.Where(x=> x.Status).OrderByDescending(x => x.ArtikelID);
        }

        public IEnumerable<Nieuwsartikel> GetPage(int page, int amount)
        {
            return MyProperty.Where(x=> x.Status).OrderByDescending(x=> x.ArtikelID).ToPagedList(page, amount);
        }

        public Nieuwsartikel GetLastAdded()
        {
            return MyProperty.OrderByDescending(x => x.ArtikelID).First();
        }

        public int GetAmountArticles()
        {
            return MyProperty.Count(x=> x.Status);
        }

        public IEnumerable<Nieuwsartikel> GetArticlesBySearchTerm(string searchTerm, int page, int amount)
        {
            return
                MyProperty.Where(
                    x => x.Status &&
                        x.Titel.Contains(searchTerm) || x.Keywords.Contains(searchTerm) || x.Intro.Contains(searchTerm) ||
                               x.Informatie.Contains(searchTerm)).ToPagedList(page, amount);
        }

        public IEnumerable<string> GetKeywords()
        {
            return MyProperty.Where(x=> x.Status).Select(x => x.Keywords);
        }

        public IEnumerable<Nieuwsartikel> GetArticlesByKeyword(string keyword, int page, int amount)
        {
            return MyProperty.Where(x => x.Status && x.Keywords.Contains(keyword)).ToPagedList(page, amount);
        }

        /// <summary>
        /// Get amount pages and the amount items
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public int[] GetAmount(int amount)
        {
            var amountItems = MyProperty.Count(x => x.Status);
            return new[] {amountItems/amount, amountItems};
        }

        /// <summary>
        /// Get amount pages and the amount items with a search term
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public int[] GetAmountSearchTerm(int amount, string searchTerm)
        {
            var amountItems = MyProperty.Count(x =>
                x.Status &&
                x.Titel.Contains(searchTerm) || x.Keywords.Contains(searchTerm) || x.Intro.Contains(searchTerm) ||
                x.Informatie.Contains(searchTerm));
            return new[] { amountItems / amount, amountItems };
        }

        /// <summary>
        /// Get amount pages and the amount of items with this keyword
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public int[] GetAmountKeywords(int amount, string keyword)
        {
            var amountItems = MyProperty.Count(x => x.Status && x.Keywords.Contains(keyword));
            return new[] { amountItems / amount, amountItems };
        }

        public void Add(Nieuwsartikel item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Nieuwsartikel item)
        {
            MyProperty.DeleteOnSubmit(item);
        }
    }
}