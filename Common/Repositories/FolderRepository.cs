﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class FolderRepository : AbstractRepository<Folder>, IRepository<Folder>
    {
        public Folder GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.FolderId == id);
        }

        public IEnumerable<Folder> GetList()
        {
            return MyProperty.OrderByDescending(x => x.FolderId);
        }

        public void Add(Folder item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(Folder item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public Folder GetLastFolder()
        {
            return MyProperty.OrderByDescending(x => x.FolderId).FirstOrDefault();
        }

        public Folder GetFolderByPath(string folderPath)
        {
            return MyProperty.First(x => x.FolderPath.ToLower() == folderPath.ToLower());
        }

        public void RemoveAll(IEnumerable<Folder> items)
        {
            MyProperty.DeleteAllOnSubmit(items);
        }
    }
}