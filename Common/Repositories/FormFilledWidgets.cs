﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class FormFilledWidgetRepository : AbstractRepository<FormFilledWidget>, IRepository<FormFilledWidget>
    {
        public FormFilledWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.FormFilledWidgetsId == id);
        }

        public IEnumerable<FormFilledWidget> GetList()
        {
            return MyProperty;
        }

        public void Add(FormFilledWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(FormFilledWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}