﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class BlogWidgetsRepository : AbstractRepository<BlogWidget>, IRepository<BlogWidget>
    {
        public BlogWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.BlogWidget1 == id);
        }

        public BlogWidget GetByActiveWidgetId(int activeWidgetId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeWidgetId);
        }

        public IEnumerable<BlogWidget> GetList()
        {
            return MyProperty.OrderByDescending(x => x.BlogWidget1);
        }

        public void Add(BlogWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(BlogWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}