﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{
    public class ThemeSectionRepository : AbstractRepository<ThemeSection>, IRepository<ThemeSection>
    {
        public ThemeSection GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.SectionId == id);
        }

        public ThemeSection GetByName(string codeName)
        {
            return MyProperty.FirstOrDefault(x => x.CodeName.ToLower() == codeName.ToLower());
        }

        public IEnumerable<ThemeSection> GetList()
        {
            return MyProperty;
        }

        public void Add(ThemeSection item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(ThemeSection item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public int GetSectionId(int themeId, string sectionCodeName)
        {
            return MyProperty.First(x => x.ThemeId == themeId && x.CodeName.ToLower() == sectionCodeName.ToLower()).SectionId;
        }

        public IQueryable<ThemeSection> GetSectionsByThemeId(int themeId)
        {
            return MyProperty.Where(x => x.ThemeId == themeId);
        }
    }
}
