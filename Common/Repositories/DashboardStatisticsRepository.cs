﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class DashboardStatisticsRepository : AbstractRepository<StatsVisit>
    {
        public StatsVisit GetById(DateTime date)
        {
            return MyProperty.FirstOrDefault(x => x.Date == date);
        }

        public List<StatsVisit> GetList()
        {
            return MyProperty.ToList();
        }

        public int GetUniqueVisitorsBetweenDates(DateTime beginDate, DateTime endDate)
        {
            return MyProperty.Where(x => x.Date >= beginDate && x.Date <= endDate).Sum(x => x.Visitors);
        }

        public int GetPageViewsBetweenDates(DateTime beginDate, DateTime endDate)
        {
            return MyProperty.Where(x => x.Date >= beginDate && x.Date <= endDate).Sum(x => x.Visitors);
        }

        public IQueryable<StatsVisit> GetStatisticsLast7Days()
        {
            return MyProperty.OrderByDescending(x => x.Date).Take(7).OrderBy(x => x.Date);
        }

        public void Add(StatsVisit item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(StatsVisit item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public bool HasTodayColumn()
        {
            return MyProperty.Any(x => x.Date == DateTime.Now.Date);
        }
        public void UpdatePageViews()
        {
            MyProperty.OrderByDescending(x => x.Date).First().Pageviews++;
        }

        public void UpdateVisitors()
        {
            MyProperty.OrderByDescending(x => x.Date).First().Visitors++;
        }
    }
}