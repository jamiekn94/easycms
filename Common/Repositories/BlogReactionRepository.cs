﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class BlogReactionRepository : AbstractRepository<BlogReactie>, IRepository<BlogReactie>
    {
        public BlogReactie GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ReactieID == id);
        }

        public IEnumerable<BlogReactie> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ReactieID);
        }

        public void Add(BlogReactie item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(BlogReactie item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public int GetAmountComments(int articleId)
        {
         return MyProperty.Count(x => x.Status && x.BlogID == articleId);
        }

        public int GetAmountOpenComments()
        {
            return MyProperty.Count(x => x.Status == false);
        }

        public int GetAmountArticles()
        {
            return MyProperty.Count(x=> x.Status);
        }

        public IQueryable<BlogReactie> GetListByArticleId(int articleId)
        {
            return MyProperty.Where(x => x.Status && x.BlogID == articleId);
        }
    }
}