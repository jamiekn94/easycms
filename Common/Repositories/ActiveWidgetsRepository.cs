﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{
    public class ActiveWidgetRepository : AbstractRepository<ActiveWidget>, IRepository<ActiveWidget>
    {
        public ActiveWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == id);
        }

        public IEnumerable<ActiveWidget> GetList()
        {
            return MyProperty;
        }

        public void Add(ActiveWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(ActiveWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public IQueryable<ActiveWidget> GetWidgetsByPageId(int pageId)
        {
            return MyProperty.Where(x => x.PageId == pageId).OrderBy(x => x.OrderWidget);
        }
       
        public IQueryable<ActiveWidget> GetBySection(int sectionId, int pageId)
        {
            return MyProperty.Where(x => x.SectionId == sectionId && x.PageId == pageId).OrderBy(x => x.OrderWidget);
        }

        public int GetLastAddedActiveWidgetId()
        {
            return MyProperty.OrderByDescending(x => x.ActiveWidgetId).First().ActiveWidgetId;
        }
    }
}
