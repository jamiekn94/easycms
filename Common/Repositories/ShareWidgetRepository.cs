﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Common.Models.Admin.ViewModel;
using Common.Providers.Date;
using Common.Providers.Users;

namespace Common.Repositories
{
    public class ShareWidgetRepository : AbstractRepository<ShareThisWidget>, IRepository<ShareThisWidget>
    {
        public ShareThisWidget GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ShareId == id);
        }

        public ShareThisWidget GetByActiveId(int activeId)
        {
            return MyProperty.FirstOrDefault(x => x.ActiveWidgetId == activeId);
        }

        public IEnumerable<ShareThisWidget> GetList()
        {
            return MyProperty;
        }

        public void Add(ShareThisWidget item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(ShareThisWidget item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}