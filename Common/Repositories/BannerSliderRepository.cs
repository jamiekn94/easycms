﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class BannerSliderRepository : AbstractRepository<BannerSlider>, IRepository<BannerSlider>
    {
        public BannerSlider GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.BannerSliderId == id);
        }

        public BannerSlider GetLastAdded()
        {
            return MyProperty.OrderByDescending(x => x.BannerSliderId).First();
        }

        public IEnumerable<BannerSlider> GetList()
        {
            return MyProperty.OrderByDescending(x => x.BannerSliderId);
        }

        public void Add(BannerSlider item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(BannerSlider item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }
    }
}