﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class RightRepository : AbstractRepository<Recht>, IRepository<Recht>
    {
        public IEnumerable<Recht> GetList()
        {
            return MyProperty.OrderByDescending(x => x.Status);
        }

        public void Add(Recht item)
        {
            throw new NotImplementedException();
        }

        public void Remove(Recht item)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public Recht GetById(int id)
        {
            return MyProperty.First(x => x.RechtID == id);
        }

        public int GetByName(string name)
        {
            return MyProperty.Where(x => x.RechtNaam == name).Select(x => x.RechtID).First();
        }
    }
}