﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class ErrorLogRepository : AbstractRepository<ErrorLog>, IRepository<ErrorLog>
    {
        public ErrorLog GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<ErrorLog> GetList()
        {
            return MyProperty.OrderByDescending(x => x.ID);
        }

        public void Add(ErrorLog item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(ErrorLog item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public void RemoveAll()
        {
            MyProperty.DeleteAllOnSubmit(GetList());
        }
    }
}