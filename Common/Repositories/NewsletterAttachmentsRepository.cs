﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Repositories
{
    public class NewsletterAttachmentsRepository : AbstractRepository<NewsletterAttachment>,
        IRepository<NewsletterAttachment>
    {
        public NewsletterAttachment GetById(int id)
        {
            return MyProperty.FirstOrDefault(x => x.AttachmentId == id);
        }

        public IEnumerable<NewsletterAttachment> GetList()
        {
            return MyProperty.OrderByDescending(x => x.AttachmentId);
        }

        public void Add(NewsletterAttachment item)
        {
            MyProperty.InsertOnSubmit(item);
        }

        public void Remove(NewsletterAttachment item)
        {
            MyProperty.DeleteOnSubmit(item);
        }

        public void Save()
        {
            base.Save();
        }

        public void RemoveAll(IEnumerable<NewsletterAttachment> attachments)
        {
            MyProperty.DeleteAllOnSubmit(attachments);
        }

        public IQueryable<NewsletterAttachment> GetAttachments(int newsletterId)
        {
            return MyProperty.Where(x => x.newsLetterId == newsletterId);
        }

        public int GetAmountAttachments(int newsletterId)
        {
            return MyProperty.Count(x => x.newsLetterId == newsletterId);
        }
    }
}