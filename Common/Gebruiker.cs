namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Gebruiker")]
    public partial class Gebruiker
    {
        [Key]
        [StringLength(100)]
        public string ProviderGebruikersId { get; set; }

        public Guid GebruikersID { get; set; }

        [Required]
        [StringLength(50)]
        public string Voornaam { get; set; }

        [Required]
        [StringLength(50)]
        public string Achternaam { get; set; }

        [StringLength(50)]
        public string Straat { get; set; }

        public int? Huisnummer { get; set; }

        [StringLength(15)]
        public string AdresToevoeging { get; set; }

        [StringLength(75)]
        public string Woonplaats { get; set; }

        [StringLength(10)]
        public string Postcode { get; set; }

        public int Land { get; set; }

        public bool HasAvatar { get; set; }

        public bool Newsletter { get; set; }

        public bool DisableClientCaching { get; set; }

        public virtual RegistratieLanden RegistratieLanden { get; set; }

        public virtual Users Users { get; set; }
    }
}
