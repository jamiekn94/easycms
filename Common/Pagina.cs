namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Pagina")]
    public partial class Pagina
    {
        public Pagina()
        {
            Link = new HashSet<Link>();
            Navigatie = new HashSet<Navigatie>();
            SubNavigatie = new HashSet<SubNavigatie>();
        }

        public int PaginaID { get; set; }

        [Required]
        [StringLength(75)]
        public string Titel { get; set; }

        [Required]
        [StringLength(25)]
        public string LinkNaam { get; set; }

        [Required]
        [StringLength(250)]
        public string SEOTitel { get; set; }

        [Required]
        [StringLength(500)]
        public string SEODescriptie { get; set; }

        [Required]
        [StringLength(500)]
        public string SEOKeywoorden { get; set; }

        [Required]
        [StringLength(4000)]
        public string Informatie { get; set; }

        public DateTime Datum { get; set; }

        public Guid AuteurId { get; set; }

        public bool Enabled { get; set; }

        public virtual ICollection<Link> Link { get; set; }

        public virtual ICollection<Navigatie> Navigatie { get; set; }

        public virtual ICollection<SubNavigatie> SubNavigatie { get; set; }
    }
}
