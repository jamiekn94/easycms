namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Roles
    {
        public Roles()
        {
            FolderPermission = new HashSet<FolderPermission>();
            GroepsRecht = new HashSet<GroepsRecht>();
            Users = new HashSet<Users>();
        }

        public Guid ApplicationId { get; set; }

        [Key]
        public Guid RoleId { get; set; }

        [Required]
        [StringLength(256)]
        public string RoleName { get; set; }

        [StringLength(256)]
        public string Description { get; set; }

        public virtual Applications Applications { get; set; }

        public virtual ICollection<FolderPermission> FolderPermission { get; set; }

        public virtual ICollection<GroepsRecht> GroepsRecht { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
