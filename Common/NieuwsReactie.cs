namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NieuwsReactie")]
    public partial class NieuwsReactie
    {
        [Key]
        public int ReactieID { get; set; }

        [Required]
        [StringLength(1000)]
        public string Bericht { get; set; }

        public DateTime Datum { get; set; }

        public int ArtikelID { get; set; }

        public Guid? ReactieGebruikersId { get; set; }

        public bool Status { get; set; }

        [StringLength(75)]
        public string Name { get; set; }

        public virtual Nieuwsartikel Nieuwsartikel { get; set; }

        public virtual Users Users { get; set; }
    }
}
