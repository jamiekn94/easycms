namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmailTemplate")]
    public partial class EmailTemplate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Required]
        [StringLength(75)]
        public string Soort { get; set; }

        [StringLength(150)]
        public string Omschrijving { get; set; }

        [Required]
        [StringLength(250)]
        public string Onderwerp { get; set; }

        [Required]
        public string Bericht { get; set; }
    }
}
