namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StatsVisit")]
    public partial class StatsVisit
    {
        [Key]
        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int Visitors { get; set; }

        public int Pageviews { get; set; }
    }
}
