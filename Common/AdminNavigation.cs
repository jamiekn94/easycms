namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AdminNavigation")]
    public partial class AdminNavigation
    {
        public AdminNavigation()
        {
            AdminNavigation1 = new HashSet<AdminNavigation>();
        }

        [Key]
        public int NavigationId { get; set; }

        [Required]
        [StringLength(75)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Controller { get; set; }

        [StringLength(50)]
        public string Action { get; set; }

        public int? ParentId { get; set; }

        public int? RightId { get; set; }

        [StringLength(75)]
        public string Icon { get; set; }

        public virtual ICollection<AdminNavigation> AdminNavigation1 { get; set; }

        public virtual AdminNavigation AdminNavigation2 { get; set; }

        public virtual Recht Recht { get; set; }
    }
}
