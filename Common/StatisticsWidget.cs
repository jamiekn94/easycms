namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StatisticsWidget")]
    public partial class StatisticsWidget
    {
        [Key]
        public int StatisticsId { get; set; }

        public int ActiveWidgetId { get; set; }

        public bool ShowUsers { get; set; }

        public bool ShowBlogArticles { get; set; }

        public bool ShowNewsArticles { get; set; }

        public bool ShowPhotoAlbums { get; set; }

        public bool ShowUniqueVisitors { get; set; }

        public bool ShowHitsWebsite { get; set; }

        public bool ShowPhotos { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
