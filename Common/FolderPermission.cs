namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FolderPermission")]
    public partial class FolderPermission
    {
        public int FolderPermissionId { get; set; }

        public int FolderId { get; set; }

        public Guid? RoleId { get; set; }

        public bool IsPublic { get; set; }

        public virtual Folder Folder { get; set; }

        public virtual Roles Roles { get; set; }
    }
}
