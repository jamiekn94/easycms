namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActionLog")]
    public partial class ActionLog
    {
        public ActionLog()
        {
            UserActionLog = new HashSet<UserActionLog>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }

        [StringLength(1000)]
        public string Information { get; set; }

        public Guid UserId { get; set; }

        public DateTime Date { get; set; }

        public virtual Users Users { get; set; }

        public virtual ICollection<UserActionLog> UserActionLog { get; set; }
    }
}
