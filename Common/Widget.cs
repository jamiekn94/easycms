namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Widget")]
    public partial class Widget
    {
        public Widget()
        {
            ActiveWidget = new HashSet<ActiveWidget>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WidgetId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        public virtual ICollection<ActiveWidget> ActiveWidget { get; set; }
    }
}
