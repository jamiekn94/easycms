namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Navigatie")]
    public partial class Navigatie
    {
        public Navigatie()
        {
            SubNavigatie = new HashSet<SubNavigatie>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(25)]
        public string Naam { get; set; }

        public bool Status { get; set; }

        public byte Visibility { get; set; }

        public byte Side { get; set; }

        public byte OrderNumber { get; set; }

        public byte Type { get; set; }

        public int? DynamicPageId { get; set; }

        public int? SeoPageId { get; set; }

        public int? NewsArticleId { get; set; }

        public int? BlogArticleId { get; set; }

        [StringLength(255)]
        public string ExternalUrl { get; set; }

        public virtual Blog Blog { get; set; }

        public virtual Nieuwsartikel Nieuwsartikel { get; set; }

        public virtual Pagina Pagina { get; set; }

        public virtual SEOPage SEOPage { get; set; }

        public virtual ICollection<SubNavigatie> SubNavigatie { get; set; }
    }
}
