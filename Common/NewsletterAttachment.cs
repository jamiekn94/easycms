namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NewsletterAttachment")]
    public partial class NewsletterAttachment
    {
        [Key]
        public int AttachmentId { get; set; }

        [Required]
        [StringLength(255)]
        public string fileName { get; set; }

        public int newsLetterId { get; set; }

        public virtual Newsletter Newsletter { get; set; }
    }
}
