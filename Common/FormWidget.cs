namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormWidget")]
    public partial class FormWidget
    {
        public FormWidget()
        {
            FormFieldsWidget = new HashSet<FormFieldsWidget>();
        }

        [Key]
        public int FormId { get; set; }

        public int ActiveId { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }

        public virtual ICollection<FormFieldsWidget> FormFieldsWidget { get; set; }
    }
}
