﻿using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Common.Helpers.Alert
{
   public static class AlertHelper
    {
       public enum TypeAlert
       {
           Error, Info, Success
       }

       private static string GetAlertType(TypeAlert typeAlert)
       {
           switch (typeAlert)
           {
               case TypeAlert.Info:
               {
                   return "info";
               }
               case TypeAlert.Success:
               {
                   return "success";
               }
               default:
               {
                   return "danger";
               }
           }
       }

       public static IHtmlString GenerateAlert(this HtmlHelper htmlHelper, string content, TypeAlert typeAlert)
       {
           string alertMessage = string.Format("<div class='alert alert-{0}'><h3>Error!</h3> <p>{1}</p></div>",
               GetAlertType(typeAlert), content);
           return htmlHelper.Raw(alertMessage);
       }
    }
}
