namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActiveWidget")]
    public partial class ActiveWidget
    {
        public ActiveWidget()
        {
            BlogWidget = new HashSet<BlogWidget>();
            CalendarWidget = new HashSet<CalendarWidget>();
            FormWidget = new HashSet<FormWidget>();
            HtmlWidget = new HashSet<HtmlWidget>();
            NewsWidget = new HashSet<NewsWidget>();
            PhotoWidget = new HashSet<PhotoWidget>();
            ShareThisWidget = new HashSet<ShareThisWidget>();
            StatisticsWidget = new HashSet<StatisticsWidget>();
            TwitterFeedWidget = new HashSet<TwitterFeedWidget>();
        }

        public int ActiveWidgetId { get; set; }

        public int SectionId { get; set; }

        public int PageId { get; set; }

        public int OrderWidget { get; set; }

        public int KindWidget { get; set; }

        public virtual SEOPage SEOPage { get; set; }

        public virtual ThemeSection ThemeSection { get; set; }

        public virtual Widget Widget { get; set; }

        public virtual ICollection<BlogWidget> BlogWidget { get; set; }

        public virtual ICollection<CalendarWidget> CalendarWidget { get; set; }

        public virtual ICollection<FormWidget> FormWidget { get; set; }

        public virtual ICollection<HtmlWidget> HtmlWidget { get; set; }

        public virtual ICollection<NewsWidget> NewsWidget { get; set; }

        public virtual ICollection<PhotoWidget> PhotoWidget { get; set; }

        public virtual ICollection<ShareThisWidget> ShareThisWidget { get; set; }

        public virtual ICollection<StatisticsWidget> StatisticsWidget { get; set; }

        public virtual ICollection<TwitterFeedWidget> TwitterFeedWidget { get; set; }
    }
}
