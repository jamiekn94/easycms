namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormFilledWidget")]
    public partial class FormFilledWidget
    {
        [Key]
        public int FormFilledWidgetsId { get; set; }

        public int FormFieldWidgetId { get; set; }

        [Required]
        public string Value { get; set; }

        public virtual FormFieldsWidget FormFieldsWidget { get; set; }
    }
}
