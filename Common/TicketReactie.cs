namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TicketReactie")]
    public partial class TicketReactie
    {
        [Key]
        public int ReactieID { get; set; }

        public DateTime Datum { get; set; }

        [Required]
        [StringLength(2500)]
        public string Reactie { get; set; }

        public int TicketID { get; set; }

        public Guid GebruikersId { get; set; }

        public virtual Ticket Ticket { get; set; }

        public virtual Users Users { get; set; }
    }
}
