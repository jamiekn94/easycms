namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Folder")]
    public partial class Folder
    {
        public Folder()
        {
            Files = new HashSet<Files>();
            FolderPermission = new HashSet<FolderPermission>();
        }

        public int FolderId { get; set; }

        [Required]
        [StringLength(25)]
        public string FolderName { get; set; }

        [Required]
        [StringLength(250)]
        public string FolderPath { get; set; }

        public int ParentId { get; set; }

        public virtual ICollection<Files> Files { get; set; }

        public virtual ICollection<FolderPermission> FolderPermission { get; set; }
    }
}
