﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Navigation
{
    public enum VisibilityOptions
    {
        NotSpecified = -1,
        Everyone = 0,
        LoggedIn = 1,
        NotLoggedIn = 2
    }
}
