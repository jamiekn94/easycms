﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Common.Models.Admin.Navigation
{
    public class AdminNavigationModel
    {
        public string Name { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public IEnumerable<AdminNavigationModel> ArraySubNavigationModels { get; set; }
        public bool CurrentPage { get; set; }
        public string Icon { get; set; }

        public AdminNavigationModel()
        {
            ArraySubNavigationModels = new Collection<AdminNavigationModel>();
        }
    }
}
