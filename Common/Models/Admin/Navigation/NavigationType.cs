﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Navigation
{
    public enum NavigationType
    {
        BuildIn = 0,
        Dynamic = 1,
        News = 2,
        Blog = 3,
        External = 4,
        Photo = 5
    }
}
