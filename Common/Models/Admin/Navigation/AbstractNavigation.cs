﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Models.Admin.Navigation
{
    public abstract class AbstractNavigation
    {
        [DisplayName("Navigatie")]
        public NavigationType NavigationType { get; set; }

        public Dictionary<NavigationType, string> NavigationTypes { get; set; }

        /// <summary>
        /// Get and set the build in pages
        /// </summary>
        public IEnumerable<SelectListItem> BuildInPages { get; set; }
        [DisplayName("Ingebouwde pagina's")]
        public int BuildInPageId { get; set; }

        /// <summary>
        /// Get and set the news articles
        /// </summary>
        public IEnumerable<SelectListItem> NewsArticles { get; set; }
        [DisplayName("Nieuwsartikel")]
        public int NewsArticleId { get; set; }

        /// <summary>
        /// Get and set the blog articles
        /// </summary>
        public IEnumerable<SelectListItem> BlogArticles { get; set; }
        [DisplayName("Blogartikel")]
        public int BlogArticleId { get; set; }

        /// <summary>
        /// Get and set the news articles
        /// </summary>
        public IEnumerable<SelectListItem> PhotoAlbums { get; set; }
        [DisplayName("Foto album")]
        public int PhotoAlbumId { get; set; }

        /// <summary>
        /// Get and set own created dynamic pages
        /// </summary>
        public IEnumerable<SelectListItem> DynamicPages { get; set; }
        [DisplayName("Dynamische pagina")]
        public int DynamicPageId { get; set; }

        [DisplayName("Externe url")]
        public string ExternalLink { get; set; }
    }
}
