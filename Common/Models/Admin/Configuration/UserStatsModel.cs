﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class UserStatsModel
    {
        public int UsersAllTime { get; set; }
        public int UsersThisYear { get; set; }
        public int UsersThisMonth { get; set; }
        public int UsersLastWeek { get; set; }
    }
}
