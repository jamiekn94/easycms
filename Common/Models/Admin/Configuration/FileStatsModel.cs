﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class FileStatsModel
    {
        public float DirectorySize { get; set; }
        public int AmountFiles { get; set; }
    }
}
