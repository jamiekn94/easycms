﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class UserGraphicStatsModel
    {
        public int PageViews { get; set; }
        public int Visitors { get; set; } 
        public string Maand { get; set; }
    }
}
