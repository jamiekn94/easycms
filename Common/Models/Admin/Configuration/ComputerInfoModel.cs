﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class ComputerInfoModel
    {
        public int InstalledMemoryAmount { get; set; }
        public int UsedMemoryAmount { get; set; }

        public int CpuUsage { get; set; }
        public int AppCpuUsage { get; set; }
        public string ProcessorName { get; set; }

        public string WindowsVersion { get; set; }
    }
}
