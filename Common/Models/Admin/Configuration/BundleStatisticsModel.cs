﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class BundleStatisticsModel
    {
        public ArticleStatsModel NewsStats { get; set; }
        public ArticleStatsModel BlogStats { get; set; }
        public UserStatsModel UserStats { get; set; }
        public VisitsStatsModel VisitsStats { get; set; }
        public FileStatsModel FileStats { get; set; }
        public ComputerInfoModel ComputerInfo { get; set; }
        public int AmountGuestbookComments { get; set; }
        public int AmountRoles { get; set; }
        public int AmountNewsletters { get; set; }
    }
}
