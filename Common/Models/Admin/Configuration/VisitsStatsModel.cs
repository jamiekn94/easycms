﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class VisitsStatsModel
    {
        public int VisitsThisWeek { get; set; }
        public int VisitsThisMonth { get; set; }
        public int VisitsThisYear { get; set; }
        public int VisitsAllTime { get; set; }

        public int HitsThisWeek { get; set; }
        public int HitsThisMonth { get; set; }
        public int HitsThisYear { get; set; }
        public int HitsAllTime { get; set; }
    }
}
