﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Configuration
{
    public class ArticleStatsModel
    {
        public int AmountArticles { get; set; }
        public int AmountComments { get; set; }
    }
}
