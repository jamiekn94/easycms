﻿namespace Common.Models.Admin.Data
{
    public class Select2Model
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}