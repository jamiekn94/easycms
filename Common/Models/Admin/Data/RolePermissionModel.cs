﻿using System.Collections.Generic;

namespace Common.Models.Admin.Data
{
    public class RolePermissionModel
    {
        public RolePermissionModel()
        {
            Rights = new List<string>();
        }

        public string RoleName { get; set; }
        public List<string> Rights { get; set; }
    }
}