﻿using System.Collections.Generic;

namespace Common.Models.Admin.Data
{
    public class ViewRolePermissionsModel
    {
        public List<Children> Children;
        public Permissions Permissions { get; set; }

        public ViewRolePermissionsModel()
        {
            Children = new List<Children>();
        }
    }

    // Sub class
    public class Permissions
    {
        public string RoleName { get; set; }
        public List<RightModel> Rights { get; set; }

        public Permissions()
        {
            Rights = new List<RightModel>();
        }
    }

    // Sub class
    public class RightModel
    {
        public int RightId { get; set; }
        public string RightName { get; set; }
    }

    // Sub class
    public class Children
    {
        public int RightId { get; set; }
        public string RoleName { get; set; }
        public bool Selected { get; set; }
    }
}