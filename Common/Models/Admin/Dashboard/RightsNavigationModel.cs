﻿using System.Collections.Generic;

namespace Common.Models.Admin.Dashboard
{
    public class RightsNavigationModel
    {
        public string ActiveController { get; set; }

        public IEnumerable<GroepsRecht> GroupRights { get; set; }
    }
}