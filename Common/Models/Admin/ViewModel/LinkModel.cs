﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Common.Filters;
using Common.Models.Admin.Navigation;

namespace Common.Models.Admin.ViewModel
{
    public class LinkModel : AbstractNavigation
    {
        public int Id { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(75, ErrorMessage = "Dit veld mag niet langer zijn dan 75 tekens")]
        public string Naam { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [LinkName]
        [StringLength(25, ErrorMessage = "Dit veld mag niet langer zijn dan 25 tekens")]
        public string LinkUrl { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan 250 tekens")]
        public string Omschrijving { get; set; }

        public bool Status { get; set; }
    }
}