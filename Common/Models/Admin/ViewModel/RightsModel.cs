﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Models.Admin.ViewModel
{
    public class RightsModel
    {
        [DisplayName("Rolnaam")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(25, ErrorMessage = "Dit veld mag niet meer dan 25 tekens bevatten")]
        public string RoleName { get; set; }

        public List<Rights> Rights { get; set; }

        public RightsModel()
        {
            Rights = new List<Rights>();
        }
    }

    public class Rights
    {
        public string RightName { get; set; }
        public bool Selected { get; set; }
        public int RightId { get; set; }
    }
}