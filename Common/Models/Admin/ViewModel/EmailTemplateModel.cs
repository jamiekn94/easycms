﻿namespace Common.Models.Admin.ViewModel
{
    public class EmailTemplateModel
    {
        public int TemplateId { get; set; }
        public string Titel { get; set; }
        public string Onderwerp { get; set; }
        public string Bericht { get; set; }
    }
}