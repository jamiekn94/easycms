﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Common.Filters;

namespace Common.Models.Admin.ViewModel
{
    public class ConfiguratieModel
    {
        // SEO
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 150 tekens bevatten")]
        public string Titel { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 500 tekens bevatten")]
        public string Descriptie { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 500 tekens bevatten")]
        public string Keywoorden { get; set; }

        [DisplayName("Hoofd titel")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 15 tekens bevatten")]
        public string HoofdTitel { get; set; }

        // Reacties
        [DisplayName("Nieuws reacties")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool ReactieNieuws { get; set; }

        [DisplayName("Blog reacties")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool ReactieBlog { get; set; }

        // Gebruiker registratie
        [DisplayName("Gebruikers verificatie")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool GebruikerVerificatie { get; set; }

        [DisplayName("Welkom email")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool WelkomEmail { get; set; }

        // Info email
        [DisplayName("E-mailadres account")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 75 tekens bevatten")]
        [Email(ErrorMessage = "Het veld {0} is geen geldige e-mail account")]
        public string EmailInfoAccount { get; set; }

        [DisplayName("E-mailadres wachtwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 75 tekens bevatten")]
        public string EmailInfoWachtwoord { get; set; }

        [DisplayName("E-mailadres host")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 75 tekens bevatten")]
        public string EmailInfoSmtpHost { get; set; }

        [DisplayName("E-mailadres port")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [Numbers(ErrorMessage = "Het veld {0} mag alleen nummers bevatten")]
        public int EmailInfoSmtpPort { get; set; }

        [DisplayName("E-mailadres beveiligde verbinding")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool EmailInfoSmtpSecure { get; set; }

        // No-Reply email
        [DisplayName("E-mailadres account")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 75 tekens bevatten")]
        [Email(ErrorMessage = "Het veld {0} is geen geldige e-mail account")]
        public string EmailNoReplyAccount { get; set; }

        [DisplayName("E-mailadres wachtwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 75 tekens bevatten")]
        public string EmailNoReplyWachtwoord { get; set; }

        [DisplayName("E-mailadres host")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(150, ErrorMessage = "Het veld {0} mag maximaal 75 tekens bevatten")]
        public string EmailNoReplySmtpHost { get; set; }

        [DisplayName("E-mailadres port")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        [Numbers(ErrorMessage = "Het veld {0} mag alleen nummers bevatten")]
        public int EmailNoReplySmtpPort { get; set; }

        [DisplayName("E-mailadres beveiligde verbinding")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool EmailNoReplySmtpSecure { get; set; }

        [DisplayName("Optimaliseer afbeeldingen")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool OptimizeImages { get; set; }

        [DisplayName("Client cache")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool DeleteClientCache { get; set; }

        [DisplayName("Server cache")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Het veld {0} is verplicht")]
        public bool DeleteServerCache { get; set; }
    }
}