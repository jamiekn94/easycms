﻿using System;

namespace Common.Models.Admin.ViewModel
{
    public class NewsReactionsModel
    {
        public int ReactionId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public bool Status { get; set; }
    }
}