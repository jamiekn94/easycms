﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Common.Models.Admin.ViewModel
{
    public class BannerSliderModel
    {
        public int BannerSliderId { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan 150 tekens")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan 500 tekens")]
        public string Description { get; set; }

        [DisplayName("Afbeelding")]
        [Required(ErrorMessage = "U moet een afbeelding uploaden")]
        [Filters.FileExtensions(FileExtensions = ".bmp,.jpg,.png,.gif,.jpeg")]
        public HttpPostedFileBase Image { get; set; }
    }

    public class ViewBannerSliderModel
    {
        public int BannerSliderId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
    }
}