﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class FolderBundleModel
    {
        public FolderModel FolderModel { get; set; }
        public FolderActionModel FolderActionModel { get; set; }
        public UploadFileModel FileModel { get; set; }
        public EditFileModel EditFileModel { get; set; }
        public DeleteFileModel DeleteFileModel { get; set; }
        public DeleteFolderModel DeleteFolderModel { get; set; }
    }

    public class DeleteFolderModel
    {
        public int FolderId { get; set; }
    }

    public class DeleteFileModel
    {
        public int FileId { get; set; }
    }

    public class EditFileModel
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FolderName { get; set; }

        [AllowHtml]
        public string Content { get; set; }
    }

    public class viewFileModel
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
    }


    public class UploadFileModel
    {
        [DisplayName("Folder")]
        public int ValueFolderId { get; set; }

        [DisplayName("Bestanden")]
        [Required(ErrorMessage = "Dit veld is verplicht.")]
        public HttpPostedFileBase[] PostedFile { get; set; }
    }

    public class FolderActionModel
    {
        public FolderActionModel()
        {
            FolderList = new List<SelectListItem>();
            RolesList = new List<SelectListItem>();
        }

        [DisplayName("Foldernaam")]
        [System.ComponentModel.DataAnnotations.Required]
        [MaxLength(25, ErrorMessage = "Dit veld mag niet langer zijn dan 25 tekens")]
        public string FolderName { get; set; }

        [DisplayName("Folder")]
        public int ValueFolderId { get; set; }

        [DisplayName("Rollen")]
        public string[] ValueRole { get; set; }

        [DisplayName("Openbare map")]
        public bool PublicMap { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
        public List<SelectListItem> FolderList { get; set; }
    }
}