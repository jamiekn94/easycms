﻿using System.Collections.Generic;

namespace Common.Models.Admin.ViewModel
{
    public class GroepsrechtenModel
    {
        public List<GroepenModel> Groepen { get; set; }
        public List<RechtenModel> Rechten { get; set; }

        public class GroepenModel
        {
            public int GroepModelID { get; set; }
            public string GroepsNaam { get; set; }
        }

        public class RechtenModel
        {
            public int GroepModelID { get; set; }
            public int RechtID { get; set; }
            public string NaamRecht { get; set; }
        }
    }
}