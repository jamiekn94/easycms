﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class AgendaModel
    {
        public int AgendaId { get; set; }

        [DisplayName("Naam")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(50, ErrorMessage = "Dit veld mag niet langer zijn dan 50 tekens")]
        public string Title { get; set; }

        [DisplayName("Begindatum")]
        public DateTime? StartDate { get; set; }

        [DisplayName("Einddatum")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Hele dag")]
        public bool AllDay { get; set; }

        [DisplayName("Hele dag")]
        public bool SendReminder { get; set; }

        [DisplayName("Openbaar")]
        public bool Public { get; set; }

        [DisplayName("Kleur")]
        public string ColorValue { get; set; }

        public IEnumerable<SelectListItem> AvailableColorsList { get; set; }
    }
}