﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class NewsletterModel
    {
        public int NewsletterId { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(75, ErrorMessage = "Dit veld mag niet langer zijn dan 75 tekens")]
        [DisplayName("Titel")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(5000, ErrorMessage = "Dit veld mag niet langer zijn dan 5000 tekens")]
        [DisplayName("Bericht")]
        [AllowHtml]
        public string Body { get; set; }

        [DisplayName("Bijlagen")]
        [Filters.FileSize(10)]
        public HttpPostedFileBase[] Attachments { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [DisplayName("Start datum")]
        public DateTime? DateLaunch { get; set; }

        [DisplayName("Inplannen")]
        public bool Schedule { get; set; }
    }

    public class viewNewsletterModel
    {
        public int NewsletterId { get; set; }
        public string Subject { get; set; }
        public int AmountAttachments { get; set; }
        public DateTime DateLaunch { get; set; }

        public bool Status { get; set; }
    }

    public class EditNewsLetterModel
    {
        public int NewsletterId { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(75, ErrorMessage = "Dit veld mag niet langer zijn dan 75 tekens")]
        [DisplayName("Titel")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(5000, ErrorMessage = "Dit veld mag niet langer zijn dan 5000 tekens")]
        [DisplayName("Bericht")]
        public string Body { get; set; }

        [DisplayName("Bijlagen")]
        [Filters.FileSize(10)]
        public HttpPostedFileBase[] UploadAttachments { get; set; }

        [DisplayName("Lanceerdatum")]
        public DateTime? DateLaunch { get; set; }

        [DisplayName("Inplannen")]
        public bool Schedule { get; set; }

        public Collection<Attachment> OldAttachments { get; set; }
    }

    public class Attachment
    {
        public string FileName { get; set; }
        public int AttachmentId { get; set; }
        public bool Checked { get; set; }
    }
}