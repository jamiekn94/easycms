﻿using System;
using System.Collections.ObjectModel;

namespace Common.Models.Admin.ViewModel
{
    public class NotApprovedUsersModel
    {
        public NotApprovedUsersModel()
        {
            Users = new Collection<NotApprovedUserModel>();
        }

        public int AmountNotApprovedUsers { get; set; }
        public Collection<NotApprovedUserModel> Users { get; set; }
    }

    public class NotApprovedUserModel
    {
        public string Username { get; set; }
        public DateTime DateRegistration { get; set; }
        public string FullName { get; set; }
    }
}