﻿using System.Collections.Generic;

namespace Common.Models.Admin.ViewModel
{
    public class FolderModel
    {
        public FolderModel()
        {
            SubFolderModels = new List<FolderModel>();
            FileModels = new List<FileModel>();
        }

        public string FolderName { get; set; }
        public int FolderId { get; set; }
        public List<FolderModel> SubFolderModels { get; set; }
        public List<FileModel> FileModels { get; set; }
    }

    public class FileModel
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
    }
}