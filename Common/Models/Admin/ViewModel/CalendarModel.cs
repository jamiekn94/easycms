﻿namespace Common.Models.Admin.ViewModel
{
    public class CalendarModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public bool allDay { get; set; }
        public string url { get; set; }
        public string color { get; set; }
    }
}