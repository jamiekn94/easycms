﻿using System.ComponentModel.DataAnnotations;

namespace Common.Models.Admin.ViewModel
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Dit veld is verplicht.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht.")]
        public string Password { get; set; }
    }
}