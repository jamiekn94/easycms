﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class GuestbookModel
    {
        public int GuestbookId { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(500, ErrorMessage = "Dit veld mag niet meer dan 500 tekens bevatten")]
        [AllowHtml]
        public string Comment { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        public bool Enabled { get; set; }
    }
}