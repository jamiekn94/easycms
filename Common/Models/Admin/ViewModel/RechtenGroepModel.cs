﻿namespace Common.Models.Admin.ViewModel
{
    public class RechtenGroepModel
    {
        public int RechtId { get; set; }
        public string RechtNaam { get; set; }
    }
}