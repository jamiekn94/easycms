﻿using System;
using System.Collections.Generic;

namespace Common.Models.Admin.ViewModel
{
    public class ListErrorLogModel : List<ErrorLogModel>
    {
    }

    public class ErrorLogModel
    {
        public int ID { get; set; }
        public int GebruikersID { get; set; }
        public string Gebruikersnaam { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Innerexception { get; set; }
        public string Target { get; set; }
        public string Stacktrace { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public DateTime Datum { get; set; }
        public string IP { get; set; }
        public string Type { get; set; }
    }
}