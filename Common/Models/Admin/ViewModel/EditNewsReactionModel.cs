﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Models.Admin.ViewModel
{
    public class EditNewsReactionModel
    {
        public int ReactionId { get; set; }

        [DisplayName("Reactie")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(1000, ErrorMessage = "Dit veld mag niet meer dan 1000 tekens bevatten")]
        public string Message { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        public bool Status { get; set; }
    }
}