﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Common.Filters;

namespace Common.Models.Admin.ViewModel
{
    public class FotoAlbumModel
    {
        public int AlbumId { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan 150 tekens.")]
        public string Titel { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan 2500 tekens.")]
        public string Descriptie { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan 250 tekens.")]
        public string Keywoorden { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [LinkName]
        public string Linknaam { get; set; }

        public bool Status { get; set; }

        public int AantalFotos { get; set; }

        [Filters.FileExtensions(FileExtensions = ".bmp,.jpg,.png,.gif,.jpeg")]
        [Filters.FileSize(5)]
        public HttpPostedFileBase[] Fotos { get; set; }
    }

    public class EditFotoAlbum
    {
        public EditFotoAlbum()
        {
            OudeFotos = new Collection<EditFotosModel>();
            NieuweFotos = new HttpPostedFileBase[0];
        }

        public int AlbumId { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan 150 tekens.")]
        public string Titel { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan 2500 tekens.")]
        public string Descriptie { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan 250 tekens.")]
        public string Keywoorden { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [LinkName]
        public string Linknaam { get; set; }

        public bool Status { get; set; }
        public HttpPostedFileBase[] NieuweFotos { get; set; }
        public Collection<EditFotosModel> OudeFotos { get; set; }
    }

    public class EditFotosModel
    {
        public int FotoId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool Checked { get; set; }
    }
}