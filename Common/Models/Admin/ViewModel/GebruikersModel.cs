﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Common.Filters;

namespace Common.Models.Admin.ViewModel
{
    public class BundleGebruikersPasswordModel
    {
        public WijzigGebruikersModel WijzigGebruikersModel { get; set; }
        public PasswordModel PasswordModel { get; set; }
        public ProfilePhotoModel ProfilePhotoModel { get; set; }
    }

    public class ProfilePhotoModel
    {
        public HttpPostedFileBase Photo { get; set; }
        public bool Delete { get; set; }
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        public string Username { get; set; }
    }

    public class WijzigGebruikersModel : AbstractGebruikersModel
    {
        public string GebruikersNaam { get; set; }

        [DisplayName("Groep")]
        [Letters]
        public string GroepenNaam { get; set; }

        public IEnumerable<SelectListItem> GroepenList { get; set; }

        public bool HasAvatar { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [DisplayName("Land")]
        public int LandId { get; set; }

        [DisplayName("Nieuwsletter")]
        public bool SubscriptionNewsletter { get; set; }
    }

    public class GebruikersModel : AbstractGebruikersModel
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Username(ErrorMessage = "Dit is geen geldige gebruikersnaam, een gebruikersnaam mag alleen de volgende tekens bevatten: letters, cijfers, punten, streepjes.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string Gebruikersnaam { get; set; }

        [DisplayName("Groep")]
        [Letters]
        public string GroepNaam { get; set; }

        public IEnumerable<SelectListItem> GroepenList { get; set; }

        [DisplayName("Land")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        public int LandId { get; set; }

        public IEnumerable<SelectListItem> BeveilingsVragenList { get; set; }

        [DisplayName("Antwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string BeveilingsAntwoord { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        [Password(ErrorMessage = "Uw wachtwoord mag niet uw voornaam, achternaam of gebruikersnaam bevatten.")]
        public string Wachtwoord { get; set; }

        [DisplayName("Nieuwsletter")]
        public bool SubscriptionNewsletter { get; set; }
    }

    public class PasswordModel
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [Length(MinimumLength = 5, MaximumLength = 25)]
        public string Wachtwoord { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [Length(MinimumLength = 5, MaximumLength = 25)]
        [DisplayName("Herhaal wachtwoord")]
        [Match(fieldName = "Wachtwoord", otherFieldName = "HerhaalWachtwoord")]
        public string HerhaalWachtwoord { get; set; }

        public string Gebruikersnaam { get; set; }
    }
}