﻿using System.Collections.Generic;
using Common.Models.Admin.Users;

namespace Common.Models.Admin.ViewModel
{
    public class BundleDashboardModel
    {
        public BundleDashboardModel()
        {
            UsersModels = new List<UserModelInfo>();
            NewsModels = new List<NieuwsModel>();
        }

        public List<UserModelInfo> UsersModels { get; set; }
        public List<NieuwsModel> NewsModels { get; set; }

        public int BlogReactions { get; set; }
        public bool HasBlogPermission { get; set; }

        public int NewsReactions { get; set; }
        public bool HasNewsPermission { get; set; }

        public int GuestbookReactions { get; set; }
        public bool HasGuestbookPermission { get; set; }

        public int TicketsReactions { get; set; }
        public bool HasTicketPermission { get; set; }

        public int UsersAwaiting { get; set; }
        public bool HasUsersPermission { get; set; }

        public bool HasAgendaPermission { get; set; }
    }
}