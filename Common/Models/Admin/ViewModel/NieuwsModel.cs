﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class NieuwsModel
    {
        public int ArtikelId { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet meer dan 250 tekens bevatten")]
        public string Titel { get; set; }

        [DisplayName("Bericht")]
        [AllowHtml]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(5000, ErrorMessage = "Dit veld mag niet meer dan 5000 tekens bevatten")]
        public string Informatie { get; set; }


        [DisplayName("Introductie")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(500, ErrorMessage = "Dit veld mag niet langer zijn dan {2} tekens")]
        public string Intro { get; set; }

        public DateTime Datum { get; set; }
        public int AuteurId { get; set; }
        public int AantalReacties { get; set; }
        public string AuteurNaam { get; set; }
        public string Image { get; set; }

        [DisplayName("Status")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public bool Status { get; set; }

        [DisplayName("Reacties toestaan")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public bool ReactiesToegestaan { get; set; }

        [Required(ErrorMessage = "Dit veld is verlicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string Keywoorden { get; set; }

        [DisplayName("Afbeelding")]
        [Filters.FileSize(5)]
        [Filters.FileExtensions(FileExtensions = ".png,.gif,.jpeg,.jpg,.bmp")]
        public HttpPostedFileBase UploadImage { get; set; }
    }

}