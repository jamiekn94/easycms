﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Common.Filters;

namespace Common.Models.Admin.ViewModel
{
    public class ListPaginaModel : List<PaginaModel>
    {
    }

    public class PaginaModel
    {
        public int PaginaId { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(75, ErrorMessage = "Dit veld mag niet langer zijn dan 75 tekens")]
        public string Titel { get; set; }

        [DisplayName("Link naam")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [LinkName]
        public string Linknaam { get; set; }

        [DisplayName("Titel")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Length(MinimumLength = 0, MaximumLength = 250)]
        public string SeoTitel { get; set; }

        [DisplayName("Descriptie")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Length(MinimumLength = 0, MaximumLength = 500)]
        public string SeoDescriptie { get; set; }

        [DisplayName("Keywoorden")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(500, ErrorMessage = "Dit veld mag niet langer zijn dan 500 tekens")]
        public string SeoKeywoorden { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [AllowHtml]
        [StringLength(4000, ErrorMessage = "Dit veld mag niet langer zijn dan 4000 tekens")]
        public string Informatie { get; set; }

        public int AuteurId { get; set; }

        [DisplayName("Zichtbaar")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        public bool Status { get; set; }

        public string AuteurNaam { get; set; }
        public DateTime Datum { get; set; }
    }
}