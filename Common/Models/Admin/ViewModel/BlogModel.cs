﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class BlogModel
    {
        public int BlogId { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan 250 tekens")]
        public string Titel { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [DisplayName("Bericht")]
        [StringLength(5000, ErrorMessage = "Dit veld mag niet langer zijn dan 5000 tekens")]
        [AllowHtml]
        public string Informatie { get; set; }

        public DateTime Datum { get; set; }
        public int AuteurId { get; set; }
        public int AantalReacties { get; set; }
        public string AuteurNaam { get; set; }

        [DisplayName("Reacties toestaan")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public bool ReactiesToegestaan { get; set; }

        [DisplayName("Introductie")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(500, ErrorMessage = "Dit veld mag niet langer zijn dan {2} tekens")]
        public string Intro { get; set; }

        public bool Status { get; set; }

        [DisplayName("Keywoorden")]
        public string Keywords { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [DisplayName("Afbeelding")]
        [Filters.FileSize(5)]
        [Filters.FileExtensions(FileExtensions = ".png,.gif,.jpeg,.jpg,.bmp")]
        public HttpPostedFileBase Image { get; set; }
        // public List<StaffGebruikers> StaffGebruikers { get; set; }
    }

    public class BlogReactionModel
    {
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public int BlogReactionId { get; set; }
        [DisplayName("Reactie")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(1000, ErrorMessage = "Dit veld mag niet meer dan 1000 tekens bevatten")]
        public string Message { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        public bool Status { get; set; }

        public DateTime Date { get; set; }

        public string Name { get; set; }
    }

    //public class StaffGebruikers
    //{
    //    public int GebruikersID { get; set; }
    //    public string Voornaam { get; set; }
    //    public string Achternaam { get; set; }
    //}
}