﻿using System.Collections.Generic;

namespace Common.Models.Admin.ViewModel
{
    public class ActionLogModel
    {
        public int AmountNewActionLogs { get; set; }
        public IEnumerable<ActionsModel> ActionLogModels { get; set; }
    }

    public class ActionsModel
    {
        public int ActionLogId { get; set; }
        public string Information { get; set; }
        public string FullName { get; set; }
        public bool HasRead { get; set; }
        public string TimeAgo { get; set; }
    }
}