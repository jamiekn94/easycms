﻿namespace Common.Models.Admin.ViewModel
{
    public class StatisticsDashboardModel
    {
        public long Id { get; set; }
        public string Date { get; set; }
        public int Pageviews { get; set; }
        public int Visitors { get; set; }
        public string Name { get; set; }
    }
}