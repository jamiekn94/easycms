﻿using System.ComponentModel.DataAnnotations;

namespace Common.Models.Admin.ViewModel
{
    public class ContactDashboardModel
    {
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        public string Message { get; set; }
    }
}