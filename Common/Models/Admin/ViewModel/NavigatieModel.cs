﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Common.Filters;
using Common.Models.Admin.Navigation;

namespace Common.Models.Admin.ViewModel
{
    public class NavigatieModel : AbstractNavigation
    {
        public int NavigatieId { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(25, ErrorMessage = "Dit veld mag niet meer dan 25 tekens bevatten")]
        public string Naam { get; set; }

        [DisplayName("Ingeschakeld")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        public bool Status { get; set; }

        [DisplayName("Zichtbaarheid")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        public byte Visibility { get; set; }

        [DisplayName("Plaats")]
        [Numbers(ErrorMessage = "Dit veld mag alleen nummers bevatten")]
        public byte OrderNumber { get; set; }

        public int AmountSubCategories { get; set; }

        public IEnumerable<SelectListItem> VisibilityOptions;
    }

    public class SubNavigatieModel : AbstractNavigation
    {
        public int SubCategorieId { get; set; }

        [DisplayName("Hoofdcategorie")]
        public int HoofdCategorieId { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(25, ErrorMessage = "Dit veld mag niet meer dan 25 tekens bevatten")]
        public string Naam { get; set; }

        public bool Status { get; set; }
        public IEnumerable<SelectListItem> HoofdCategorieen { get; set; }

        [DisplayName("Plaats")]
        [Numbers(ErrorMessage = "Dit veld mag alleen nummers bevatten")]
        public byte OrderNumber { get; set; }

        public List<SubNavigatieModel> SubCategorieen { get; set; }
    }
}