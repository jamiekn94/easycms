﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Pages
{
    public class SeoPageModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(50, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string Titel { get; set; }

        [DisplayName("Zoekwoorden")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string SeoKeywords { get; set; }

        [DisplayName("Omschrijving")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string SeoDescription { get; set; }

        [DisplayName("Zichtbaar")]
        public bool Hide { get; set; }
    }
}
