﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Guestbook
{
    public class ViewGuestbookModel
    {
        public string Fullname { get; set; }
        public DateTime Date { get; set; }
        public bool Enabled { get; set; }
        public int Id { get; set; }
    }
}
