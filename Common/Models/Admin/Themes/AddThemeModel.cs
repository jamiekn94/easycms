﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Common.Filters;

namespace Common.Models.Admin.Themes
{
    public class AddThemeModel
    {
        [Filters.FileExtensions(FileExtensions = ".zip")]
        [FileSize(25)]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Display(Name = "Thema")]
        public HttpPostedFileBase PluginPostedFileBase { get; set; }
    }
}
