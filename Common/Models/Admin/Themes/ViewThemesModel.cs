﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Themes
{
   public class ViewThemesModel
    {
       public IEnumerable<Theme> Themes { get; set; }
        public int ActiveThemeId { get; set; }
    }
}
