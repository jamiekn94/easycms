﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Models.Admin.Themes.Edit
{
    public class EditFileModel
    {
        public string FilePath { get; set; }
        public string DisplayFilePath { get; set; }
        public string FileContent { get; set; }
        public HttpPostedFileBase UploadedImage { get; set; }
        public bool IsImage { get; set; }
    }
}
