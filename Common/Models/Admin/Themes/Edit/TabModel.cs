﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Themes.Edit
{
    public class TabModel
    {
        public string Action { get; set; }
        public string Name { get; set; }
    }
}
