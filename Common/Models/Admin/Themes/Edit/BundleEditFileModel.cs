﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Themes.Edit
{
    public class BundleEditFileModel
    {
        public FileModel FileModel { get; set; }
        public EditFileModel EditFileModel { get; set; }
    }
}
