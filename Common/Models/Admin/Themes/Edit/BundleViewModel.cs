﻿using System;
using System.Collections.Generic;
using Common.Providers.Routes;

namespace Common.Models.Admin.Themes.Edit
{
    public class BundleViewModel
    {
        public FileTabModel[] ArrayFileTabs { get; set; }
        public BundleEditFileModel BundleEditFileModel { get; set; }
        public string ActiveAction { get; set; }
        
        public IEnumerable<TabModel> ArrayTabModels
        {
            get
            {
                return new[]
                {
                    new TabModel {Action = "Stylesheets", Name = "Stylesheets"},
                    new TabModel {Action = "Structuur", Name = "Structuur"},
                    new TabModel {Action = "Javascripts", Name = "Javascripts"},
                    new TabModel {Action = "Afbeeldingen", Name = "Afbeeldingen"}
                };
            }
        }

        public bool IsActiveTab(string action)
        {
            return action.Equals(ActiveAction, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}