﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Themes.Edit
{
   public class FileModel
    {
       public string Name { get; set; }
       public string FilePath { get; set; }
       public string Content { get; set; }
       public DateTime LastEdited { get; set; }
    }
}
