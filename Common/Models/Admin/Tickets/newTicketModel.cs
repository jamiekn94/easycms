﻿using System.Collections.ObjectModel;
using Common.Models.Admin.ViewModel;

namespace Common.Models.Admin.Tickets
{
    public class NewTicketModel
    {
        public NewTicketModel()
        {
            TicketModels = new Collection<NewTicketArticleModel>();
        }

        public int AmountNewTickets { get; set; }

        // Do we have permission for this right?
        public bool Status { get; set; }
        public Collection<NewTicketArticleModel> TicketModels { get; set; }
    }

    public class NewTicketArticleModel : TicketModel
    {
        public string TimeAgo { get; set; }
    }
}