﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Tickets
{
    public enum TicketType
    {
        Open = 1,
        Review = 2,
        Closed = 3,
        All = 4
    }
}
