﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Common.Models.Admin.ViewModel
{
    public class TicketModel
    {
        public int TicketID { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(25, ErrorMessage = "Dit veld mag niet meer dan 250 tekens bevatten")]
        public string Onderwerp { get; set; }
        public DateTime Datum { get; set; }
        public int AuteurID { get; set; }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public string Bericht { get; set; }
        public string Gebruikersnaam { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        public int Status { get; set; }
    }

    public class CreateTicketModel
    {
        public CreateTicketModel()
        {
            Users = new Collection<SelectListItem>();
        }

        [DisplayName("Onderwerp")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan 250 tekens")]
        public string Subject { get; set; }

        [DisplayName("Bericht")]
        [AllowHtml]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(250, ErrorMessage = "Dit veld mag niet langer zijn dan 2500 tekens")]
        public string Message { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [DisplayName("Gebruiker")]
        public string Username { get; set; }

        public Collection<SelectListItem> Users { get; set; }
    }

    public class TicketReactiesModel
    {
        public int ReactieID { get; set; }
        public string GebruikersGroepNaam { get; set; }
        public bool isAdministratief { get; set; }
        public int ReactieGebruikerID { get; set; }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public DateTime Datum { get; set; }

        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(25, ErrorMessage = "Dit veld mag niet meer dan 2500 tekens bevatten")]
        public string Bericht { get; set; }
    }


    public class TicketReplyModel
    {
        public int TicketId { get; set; }
        [AllowHtml]
        [Required(ErrorMessage= "Dit veld is verplicht")]
        public string Bericht { get; set; }
    }

    public class TicketActionModel
    {
        public int Action { get; set; }
        public int TicketId { get; set; }
    }

    public class BundleTicketModel
    {
        public List<TicketReactiesModel> TicketReactieModel { get; set; }
        public TicketModel TicketModel { get; set; }
        public TicketReplyModel TicketReplyModel { get; set; }
        public TicketActionModel TicketActionModel { get; set; }
    }
}