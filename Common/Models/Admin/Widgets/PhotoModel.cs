﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Models.Admin.Widgets
{
    public class PhotoModel
    {
        public IEnumerable<PhotoEditModel> Photos { get; set; }
        public IEnumerable<PhotoEditModel> SelectedPhotos { get; set; }
        public int PhotoId { get; set; }

        public PhotoModel()
        {
            Photos = new Collection<PhotoEditModel>();
            SelectedPhotos = new Collection<PhotoEditModel>();
        }
    }
}
