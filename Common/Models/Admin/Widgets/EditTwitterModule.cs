﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Models.Admin.Widgets
{
    public class EditTwitterModule
    {
        public int TwitterWidgetId { get; set; }

        [DisplayName("Twitter gebruikersnaam")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(50, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string Username { get; set; }

        [DisplayName("Aantal tweets")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public int AmountTweets { get; set; }

        [DisplayName("Twitter geheime sleutel")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string ConsumerKey { get; set; }

        [DisplayName("Twitter geheime secret")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string ConsumerSecret { get; set; }

        [DisplayName("Website url")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string ReturnUrl { get; set; }

        [DisplayName("Reacties")]
        public bool IncludeReplies { get; set; }

        [DisplayName("Retweets")]
        public bool IncludeRetweets { get; set; }

        public IEnumerable<SelectListItem> ListAmount()
        {
            return new Collection<SelectListItem>
           {
               new SelectListItem {Selected = false, Text = "1", Value = "1"},
               new SelectListItem {Selected = false, Text = "2", Value = "2"},
               new SelectListItem {Selected = false, Text = "3", Value = "3"},
               new SelectListItem {Selected = false, Text = "4", Value = "4"},
               new SelectListItem {Selected = false, Text = "5", Value = "5"},
               new SelectListItem {Selected = false, Text = "6", Value = "6"},
               new SelectListItem {Selected = false, Text = "7", Value = "7"},
               new SelectListItem {Selected = false, Text = "8", Value = "8"},
               new SelectListItem {Selected = false, Text = "9", Value = "9"},
               new SelectListItem {Selected = false, Text = "10", Value = "10"}
           };
        }
    }
}
