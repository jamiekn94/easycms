﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Widgets
{
    public enum KindWidget : int
    {
        Html = 1,
        Blog = 2,
        News = 3,
        Photo = 4,
        Twitter = 5,
        Social = 6
    }
}
