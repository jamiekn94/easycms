﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Common.Models.Admin.Widgets
{
    public class BundleViewWidgetsModel
    {
        public int SeoPageId { get; set; }
        public IEnumerable<SelectListItem> SeoPages { get; set; }
        public IEnumerable<Widget> Widgets { get; set; }
        public IEnumerable<InstalledWidgetsModel> InstalledWidgets { get; set; }

        public BundleViewWidgetsModel()
        {
            SeoPages = new Collection<SelectListItem>();
            Widgets = new Collection<Widget>();
            InstalledWidgets = new Collection<InstalledWidgetsModel>();
        }
    }
}
