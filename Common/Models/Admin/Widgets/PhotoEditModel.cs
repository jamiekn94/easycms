﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Models.Admin.Widgets
{
    public class PhotoEditModel
    {
        public SelectListItem Photo { get; set; }
        public int AlbumId { get; set; }
    }
}
