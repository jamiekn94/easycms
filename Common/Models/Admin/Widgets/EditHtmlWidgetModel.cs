﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Models.Admin.Widgets
{
    public class EditHtmlWidgetModel
    {
        [DisplayName("Titel")]
        public int HtmlWidgetId { get; set; }
        [AllowHtml]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(5000, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string Html { get; set; }

        [DisplayName("Informatie")]
        [AllowHtml]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(150, ErrorMessage = "Dit veld mag niet langer zijn dan {1} tekens")]
        public string Title { get; set; }
    }
}
