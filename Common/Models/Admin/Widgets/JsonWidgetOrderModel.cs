﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Widgets
{
    public class JsonWidgetOrderModel
    {
        public int ActiveWidgetId { get; set; }
        public int WidgetOrder { get; set; }
    }
}
