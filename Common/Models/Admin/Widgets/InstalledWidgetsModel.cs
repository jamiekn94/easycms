﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Widgets
{
    public class InstalledWidgetsModel
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public IEnumerable<ActiveWidgetModel> ActiveWidgets { get; set; }

        public InstalledWidgetsModel()
        {
            ActiveWidgets = new Collection<ActiveWidgetModel>();
        }
    }
}
