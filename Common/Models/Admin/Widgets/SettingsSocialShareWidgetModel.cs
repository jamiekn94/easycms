﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Widgets
{
    public class SettingsSocialShareWidgetModel
    {
        public int ShareId { get; set; }

        [DisplayName("Facebook")]
        public bool HasFacebook { get; set; }
        [DisplayName("Twitter")]
        public bool HasTwitter { get; set; }
        [DisplayName("LinkedIn")]
        public bool HasLinkedIn { get; set; }
        [DisplayName("Pinterest")]
        public bool HasPinterest { get; set; }
        [DisplayName("Email")]
        public bool HasEmail { get; set; }
        [DisplayName("Google+")]
        public bool HasGooglePlus { get; set; }
    }
}
