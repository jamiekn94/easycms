﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Common.Filters;

namespace Common.Models.Admin.Widgets
{
    public class EditPhotoWidget
    {
        public PhotoModel Photos { get; set; }
        public IEnumerable<SelectListItem> Albums { get; set; }
        public int AlbumId { get; set; }
        public int PhotoWidgetId { get; set; }
        public bool Slide { get; set; }

        [DisplayName("Aantal foto's weergeven")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Numbers(ErrorMessage = "Dit is geen geldig getal")]
        [Range(1, 25, ErrorMessage = "Getal moet tussen de {0} en {1} liggen")]
        public int AmountItems { get; set; }

        public EditPhotoWidget()
        {
            Albums = new Collection<SelectListItem>();
            Photos = new PhotoModel();
        }
    }
}
