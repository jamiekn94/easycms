﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Common.Filters;

namespace Common.Models.Admin.Widgets
{
    public class AddWidgetModel
    {
        [Filters.FileExtensions(FileExtensions = ".zip")]
        [FileSize(25)]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Display(Name = "Widget")]
        public HttpPostedFileBase WidgetPostedFileBase { get; set; }
    }
}
