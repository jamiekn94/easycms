﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Admin.Widgets
{
    public class ActiveWidgetModel
    {
        public int WidgetId { get; set; }
        public int KindWidget { get; set; }
        public int ActiveWidgetId { get; set; }
        public string FriendlyName { get; set; }
        public string Description { get; set; }
    }
}
