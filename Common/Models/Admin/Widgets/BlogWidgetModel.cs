﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Models.Admin.Widgets
{
   public class BlogWidgetModel
    {
       public int BlogWidgetId { get; set; }
       public bool Slide { get; set; }

       [DisplayName("Aantal artikelen")]
       public int Amount { get; set; }

       public IEnumerable<SelectListItem> ListAmount()
       {
           return new Collection<SelectListItem>
           {
               new SelectListItem {Selected = false, Text = "1", Value = "1"},
               new SelectListItem {Selected = false, Text = "2", Value = "2"},
               new SelectListItem {Selected = false, Text = "3", Value = "3"},
               new SelectListItem {Selected = false, Text = "4", Value = "4"},
               new SelectListItem {Selected = false, Text = "5", Value = "5"},
               new SelectListItem {Selected = false, Text = "6", Value = "6"},
               new SelectListItem {Selected = false, Text = "7", Value = "7"},
               new SelectListItem {Selected = false, Text = "8", Value = "8"},
               new SelectListItem {Selected = false, Text = "9", Value = "9"},
               new SelectListItem {Selected = false, Text = "10", Value = "10"}
           };
       }
    }
}
