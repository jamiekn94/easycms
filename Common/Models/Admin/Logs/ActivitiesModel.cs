﻿using System;

namespace Common.Models.Admin.Logs
{
    public class ActivitiesModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Information { get; set; }
        public string Fullname { get; set; }
        public DateTime Date { get; set; }
    }
}