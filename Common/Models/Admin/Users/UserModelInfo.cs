﻿using System;

namespace Common.Models.Admin.Users
{
    public class UserModelInfo
    {
        public string ProviderUserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string AdressExtra { get; set; }
        public string Residence { get; set; }
        public string Postcode { get; set; }
        public int Land { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsLockedOut { get; set; }
        public bool IsAccepted { get; set; }
        public bool HasAvatar { get; set; }
        public bool SubscriptionNewsletter { get; set; }
        public string SecurityQuestion { get; set; }


        public string FullName
        {
            get { return Firstname + " " + Lastname; }
        }
    }
}
