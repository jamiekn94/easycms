﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Common.Filters;

namespace Common.Models.Admin.Users
{
    namespace Kapsters.Areas.Admin.Models
    {
        public abstract class AbstractGebruikersModel
        {
            [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
            [PersonName(ErrorMessage = "Dit is geen geldige naam")]
            [StringLength(50, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
            public string Voornaam { get; set; }

            [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
            [PersonName(ErrorMessage = "Dit is geen geldige naam")]
            [StringLength(50, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
            public string Achternaam { get; set; }

            [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
            [StringLength(50, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
            [Street(ErrorMessage = "Dit is geen geldige straat naam")]
            public string Straat { get; set; }

            [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
            [HouseNumber(ErrorMessage = "Dit is geen geldig huisnummer")]
            public int? Huisnummer { get; set; }

            [DisplayName("Adres toevoeging")]
            [StringLength(15, MinimumLength = 0, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
            public string AdresToevoeging { get; set; }

            [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
            [StringLength(75, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
            [Place(ErrorMessage = "Dit is geen geldige woonplaatsnaam")]
            public string Woonplaats { get; set; }

            [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
            [Postcode(ErrorMessage = "Dit is geen geldige postcode")]
            public string Postcode { get; set; }

            public IEnumerable<SelectListItem> LandenList { get; set; }

            [Email]
            [StringLength(255, MinimumLength = 5, ErrorMessage = "Dit veld moet tussne de {2} en {1} tekens zijn")]
            public string Email { get; set; }

            [DisplayName("Vraag")]
            [Numbers]
            public int BeveilingsVraagId { get; set; }

            [DisplayName("Profielfoto")]
            public HttpPostedFileBase ProfilePhoto { get; set; }
        }
    }
}
