﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.Widgets
{
    public class HtmlWidgetModel
    {
        public string Text { get; set; }
    }
}
