﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.Widgets
{
    public class StatisticsWidgetModelcs
    {
        // Enabled
        public bool ShowUsers { get; set; }
        public bool ShowBlogArticles { get; set; }
        public bool ShowNewsArticles { get; set; }
        public bool ShowPhotoAlbums { get; set; }
        public bool ShowPhotos { get; set; }
        public bool ShowUniqueVisitors { get; set; }
        public bool ShowHitsWebsite { get; set; }
        
        // Amount
        public int AmountUsers { get; set; }
        public int AmountBlogArticles { get; set; }
        public int AmountNewsArticles { get; set; }
        public int AmountPhotoAlbums { get; set; }
        public int AmountPhotos { get; set; }
        public int AmountUniqueVisitors { get; set; }
        public int AmountHitsWebsite { get; set; }
    }
}
