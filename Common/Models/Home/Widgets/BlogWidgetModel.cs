﻿namespace Common.Models.Home.Widgets
{
    public class BlogWidgetModel
    {
        public PagedList.StaticPagedList<Blog.BlogModel> BlogArticles { get; set; }
        public bool Slide { get; set; }
        public int AmountItems { get; set; }
    }
}
