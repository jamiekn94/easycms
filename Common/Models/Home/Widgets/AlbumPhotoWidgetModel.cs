﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.Widgets
{
    public class AlbumPhotoWidgetModel
    {
        // Title album
        public string Name { get; set; }
        public int AlbumId { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
    }
}
