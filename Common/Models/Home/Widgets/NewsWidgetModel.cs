﻿namespace Common.Models.Home.Widgets
{
    public class NewsWidgetModel
    {
        public PagedList.StaticPagedList<News.NewsModel> NewsArticles { get; set; }
        public bool Slide { get; set; }
        public int AmountItems { get; set; }
    }
}
