﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.Widgets
{
    public class PhotoInPhotoWidgetModel
    {
        public string Name { get; set; }
        public int AlbumId { get; set; }
        public int PhotoId { get; set; }
    }
}
