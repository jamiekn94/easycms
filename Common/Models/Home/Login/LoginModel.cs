﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Common.Filters;

namespace Common.Models.Home.Login
{
    public class LoginModel
    {
        [DisplayName("Gebruikersnaam")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Username(ErrorMessage = "Dit is geen geldige gebruikersnaam, een gebruikersnaam mag alleen de volgende tekens bevatten: letters, cijfers, punten, streepjes.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string Username { get; set; }

        [DisplayName("Wachtwoord")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string Password { get; set; }

        [DisplayName("Onthouden")]
        public bool Remember { get; set; }
    }
}