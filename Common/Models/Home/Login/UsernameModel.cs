﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Models.Home.Login
{
    public class UsernameModel
    {
        [DisplayName("E-mail adres")]
        [EmailAddress(ErrorMessage = "Dit is geen geldige e-mail adres")]
        public string Email { get; set; }
    }
}