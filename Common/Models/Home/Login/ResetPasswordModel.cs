﻿namespace Common.Models.Home.Login
{
    public class ResetPasswordStep2Model
    {
        public string Username { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }

    public class ResetPasswordModel
    {
        public string Username { get; set; }
    }
}