﻿using PagedList;

namespace Common.Models.Home.Guestbook
{
    public class BundleGuestbookModel
    {
        public CommentGuestbookModel CommentGuestbookModel { get; set; }
        public StaticPagedList<GuestBookModel> GuestBookModel { get; set; }
    }
}