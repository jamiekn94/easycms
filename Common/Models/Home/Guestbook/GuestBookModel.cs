﻿using System;
using System.Collections.ObjectModel;

namespace Common.Models.Home.Guestbook
{
    public class GuestBookModel
    {
        public int GuestbookId { get; set; }
        public string Comment { get; set; }
        public int? ParentId { get; set; }
        public DateTime Date { get; set; }
        public string Author { get; set; }
        public Collection<GuestBookModel> SubGuestBookModels { get; set; }
        public GuestBookModel()
        {
            SubGuestBookModels = new Collection<GuestBookModel>();
        }
    }
}