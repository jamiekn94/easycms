﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Models.Home.Guestbook
{
    public class CommentGuestbookModel
    {
        public int PageId { get; set; }
        public int? ParentId { get; set; }
        [DisplayName("Naam")]
        [MaxLength(75, ErrorMessage = "Het veld {0} mag niet langer zijn dan {1} tekens")]
        public string Name { get; set; }
        [DisplayName("Reactie")]
        [Required(ErrorMessage = "Het veld {0} is verplicht")]
        [StringLength(500, MinimumLength = 10, ErrorMessage = "Het veld {0} mag maximaal {1} en moet minimaal {2} tekens bevatten.")]
        public string Comment { get; set; }
    }
}