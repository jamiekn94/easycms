﻿using System;

namespace Common.Models.Home.Blog
{
    public class BlogModel
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; }
        public string Information { get; set; }
        public DateTime Date { get; set; }
        public string AuthorName { get; set; }
        public string Username { get; set; }
        public bool CommentsEnabled { get; set; }
        public int AmountComments { get; set; }
        public string Keywords { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public bool UserHasAvatar { get; set; }
    }
}