﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models.Home.News;
using PagedList;

namespace Common.Models.Home.Blog
{
   public class ViewBundleModel
    {
        public StaticPagedList<BlogModel> BlogModels { get; set; }
        public SearchNewsModel SearchNewsModel { get; set; }

        public ViewBundleModel()
        {
          BlogModels = new StaticPagedList<BlogModel>(new Collection<BlogModel>(), 1, 1, 0);  
        }
    }
}
