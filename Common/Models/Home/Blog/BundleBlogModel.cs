﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Common.Models.Home.News;

namespace Common.Models.Home.Blog
{
    public class BundleBlogModel
    {
        public BlogModel BlogModel { get; set; }
        public IEnumerable<BlogReactionModel> BlogReactionModels { get; set; }
        public BlogReactionModel AddBlogReactionModel { get; set; }
        public EditCommentModel EditCommentModel { get; set; }

        public int RemoveCommentId { get; set; }

        public BundleBlogModel()
        {
            BlogReactionModels = new Collection<BlogReactionModel>();
            AddBlogReactionModel = new BlogReactionModel();
            EditCommentModel = new EditCommentModel();
        }
    }
}