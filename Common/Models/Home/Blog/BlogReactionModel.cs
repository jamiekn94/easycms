﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Models.Home.Blog
{
    public class BlogReactionModel
    {
        [DisplayName("Bericht")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(1000, ErrorMessage = "Dit veld mag niet langer zijn dan {2} tekens")]
        public string Message { get; set; }
        [DisplayName("Naam")]
        [StringLength(75, ErrorMessage = "Dit veld mag niet langer zijn dan {2} tekens")]
        public string Name { get; set; }
        
        public string Username { get; set; }
        public int BlogId { get; set; }
        public DateTime Date { get; set; }

        public int CommentId { get; set; }
    }
}