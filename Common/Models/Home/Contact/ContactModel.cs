﻿using System.ComponentModel.DataAnnotations;

namespace Common.Models.Home.Contact
{
    public class ContactModel
    {
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [EmailAddress(ErrorMessage = "Dit is geen geldig e-emailadres")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Dit veld is verplicht")]
        public string Message { get; set; }
    }
}