﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Common.Filters;
using Common.Models.Admin.Users.Kapsters.Areas.Admin.Models;

namespace Common.Models.Home.Register
{
    public class RegisterModel : AbstractGebruikersModel
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [Username(ErrorMessage = "Dit is geen geldige gebruikersnaam, een gebruikersnaam mag alleen de volgende tekens bevatten: letters, cijfers, punten, streepjes.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string Gebruikersnaam { get; set; }

        public IEnumerable<SelectListItem> BeveilingsVragenList { get; set; }

        [DisplayName("Antwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string BeveilingsAntwoord { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        [Password(ErrorMessage = "Uw wachtwoord mag niet uw voornaam, achternaam of gebruikersnaam bevatten.")]
        public string Wachtwoord { get; set; }

        [DisplayName("Nieuwsletter")]
        public bool SubscriptionNewsletter { get; set; }
    }
}