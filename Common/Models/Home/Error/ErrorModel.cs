﻿namespace Common.Models.Home.Error
{
    public class ErrorModel
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
    }
}