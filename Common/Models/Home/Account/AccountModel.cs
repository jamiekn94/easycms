﻿using System.ComponentModel;
using Common.Models.Admin.Users.Kapsters.Areas.Admin.Models;

namespace Common.Models.Home.Account
{
    public class AccountModel : AbstractGebruikersModel
    {
        public bool HasAvatar { get; set; }

        [DisplayName("Nieuwsletter")]
        public bool SubscriptionNewsletter { get; set; }
    }
}