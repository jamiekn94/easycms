﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Models.Home.Account
{
    public class PasswordModel
    {
        [DisplayName("Huidige wachtwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string OldPassword { get; set; }

        [DisplayName("Wachtwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string Password { get; set; }

        [DisplayName("Herhaal wachtwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        [Compare("Password", ErrorMessage = "Uw wachtwoorden komen niet overeen")]
        public string RepeatPassword { get; set; }
    }
}