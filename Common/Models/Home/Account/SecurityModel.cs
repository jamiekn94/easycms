﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Common.Filters;

namespace Common.Models.Home.Account
{
    public class SecurityModel
    {
        public IEnumerable<SelectListItem> BeveilingsVragenList { get; set; }

        [DisplayName("Huidige wachtwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht.")]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string Wachtwoord { get; set; }

        [DisplayName("Nieuwe vraag")]
        [Numbers]
        public int BeveilingsVraagId { get; set; }

        [DisplayName("Nieuwe antwoord")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(150, MinimumLength = 5, ErrorMessage = "Dit veld moet tussen de {2} en {1} tekens zijn")]
        public string BeveilingsAntwoord { get; set; }
    }
}