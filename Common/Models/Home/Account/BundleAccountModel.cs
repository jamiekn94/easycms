﻿namespace Common.Models.Home.Account
{
    public class BundleAccountModel
    {
        public AccountModel AccountModel { get; set; }
        public PasswordModel PasswordModel { get; set; }
        public SecurityModel SecurityModel { get; set; }

        private int _ActiveTab { get; set; }

        public int ActiveTab
        {
            get
            {
                if (_ActiveTab == 0)
                {
                    _ActiveTab = 1;
                }
                return _ActiveTab;
            }
            set { _ActiveTab = value; }
        }
    }
}