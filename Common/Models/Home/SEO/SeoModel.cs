﻿namespace Common.Models.Home.SEO
{
    public class SeoModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public bool Hide { get; set; }
    }
}