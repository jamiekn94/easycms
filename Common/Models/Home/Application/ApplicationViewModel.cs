﻿using System.Collections.Generic;

namespace Common.Models.Home.Application
{
    public class ApplicationViewModel
    {
        public ApplicationViewModel()
        {
            MainMenuItems = new List<MenuItem>();
            SidebarMenuItems = new List<MenuItem>();
        }

        public List<MenuItem> MainMenuItems { get; private set; }
        public List<MenuItem> SidebarMenuItems { get; private set; }
    }
}