﻿namespace Common.Models.Home.Application
{
    public class MenuItem
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public decimal Order { get; set; }
        public string Owner { get; set; }
    }
}