﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Common.Models.Home.Navigation
{
    public class NavigationModel
    {
        public string Name { get; set; }
        public string FullUrl { get; set; }
        public int Visibility { get; set; }
        public int Side { get; set; }
        public ICollection<NavigationModel> SubNavigationModels { get; set; }

        public NavigationModel()
        {
            SubNavigationModels = new Collection<NavigationModel>();
        }
    }
}