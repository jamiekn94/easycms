﻿using System;

namespace Common.Models.Home.Page
{
    public class PageModel
    {
        public int PageId { get; set; }
        public string Titel { get; set; }
        public string Information { get; set; }
        public string Url { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string Author { get; set; }
        public bool AuthorHasAvatar { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AuthorUsername { get; set; }
    }
}