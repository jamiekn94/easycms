﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.News
{
   public class SearchNewsModel
    {
        [DisplayName("Zoekterm")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [MaxLength(150, ErrorMessage = "Het zoekveld mag niet langer zijn dan {1} tekens")]
        public string SearchTerm { get; set; }

       public int CurrentPage { get; set; }

       public SearchNewsModel()
       {
           CurrentPage = 1;
       }
    }
}
