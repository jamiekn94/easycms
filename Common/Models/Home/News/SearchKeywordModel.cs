﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.News
{
    class SearchKeywordModel
    {
        public int Page { get; set; }

        [Required(ErrorMessage = "Het veld {0} is verplicht")]
        [MaxLength(25, ErrorMessage = "Het veld {0} mag niet langer zijn dan {1} tekens")]
        public string Keyword { get; set; }
    }
}
