﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Common.Models.Home.News
{
    public class BundleNewsModel
    {
        public NewsModel NewsModel { get; set; }
        public IEnumerable<NewsReactionModel> NewsReactionModels { get; set; }
        public NewsReactionModel AddNewsReactionModel { get; set; }
        public EditCommentModel EditCommentModel { get; set; }
        public int RemoveNewsCommentId { get; set; }

        public BundleNewsModel()
        {
            NewsReactionModels = new Collection<NewsReactionModel>();
            AddNewsReactionModel = new NewsReactionModel();
            EditCommentModel = new EditCommentModel();
        }
    }
}