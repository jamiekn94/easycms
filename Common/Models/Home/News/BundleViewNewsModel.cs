﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;

namespace Common.Models.Home.News
{
    public class BundleViewNewsModel
    {
        public StaticPagedList<NewsModel> NewsArticles { get; set; }

        public SearchNewsModel SearchNewsModel { get; set; }

        public KeywordModel KeywordModel { get; set; }

        public IEnumerable<string> Keywords { get; set; }

        public int CurrentPage
        {
            get { return NewsArticles.PageNumber; }
        }

        public BundleViewNewsModel()
        {
            NewsArticles = new StaticPagedList<NewsModel>(new Collection<NewsModel>(), 1, 1, 0);
        }
    }
}
