﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Home.News
{
    public class EditCommentModel
    {
        [DisplayName("Bericht")]
        [Required(ErrorMessage = "Dit veld is verplicht")]
        [StringLength(1000, ErrorMessage = "Dit veld mag niet langer zijn dan {2} tekens")]
        public string Message { get; set; }

        public int CommentId { get; set; }

        public int ArticleId { get; set; }
    }
}
