﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Common.Models.Home.Fotoalbum
{
    public class PhotoAlbumModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Url { get; set; }
        public int AlbumId  { get; set; }
        public IEnumerable<PhotoModel> Photos { get; set; }

        public PhotoAlbumModel()
        {
            Photos = new Collection<PhotoModel>();
        }
    }
}