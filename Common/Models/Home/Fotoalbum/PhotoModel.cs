﻿namespace Common.Models.Home.Fotoalbum
{
    public class PhotoModel
    {
        public string Name { get; set; }
        public int PhotoId { get; set; }
    }
}