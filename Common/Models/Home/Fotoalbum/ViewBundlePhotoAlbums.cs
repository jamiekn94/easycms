﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models.Home.News;
using PagedList;

namespace Common.Models.Home.Fotoalbum
{
    public class ViewBundlePhotoAlbums
    {
        public StaticPagedList<PhotoAlbumModel> PhotoAlbums { get; set; }
        public SearchNewsModel SearchModel { get; set; }
    }
}
