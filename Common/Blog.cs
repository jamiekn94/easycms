namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Blog")]
    public partial class Blog
    {
        public Blog()
        {
            BlogReactie = new HashSet<BlogReactie>();
            Link = new HashSet<Link>();
            Navigatie = new HashSet<Navigatie>();
            SubNavigatie = new HashSet<SubNavigatie>();
        }

        public int BlogID { get; set; }

        [Required]
        [StringLength(250)]
        public string Titel { get; set; }

        [Required]
        [StringLength(5000)]
        public string Informatie { get; set; }

        public Guid AuteurID { get; set; }

        public DateTime Datum { get; set; }

        public bool ReactiesToegestaan { get; set; }

        [StringLength(250)]
        public string Keywords { get; set; }

        [Required]
        [StringLength(255)]
        public string Image { get; set; }

        public bool Status { get; set; }

        [Required]
        [StringLength(500)]
        public string Intro { get; set; }

        public virtual Users Users { get; set; }

        public virtual ICollection<BlogReactie> BlogReactie { get; set; }

        public virtual ICollection<Link> Link { get; set; }

        public virtual ICollection<Navigatie> Navigatie { get; set; }

        public virtual ICollection<SubNavigatie> SubNavigatie { get; set; }
    }
}
