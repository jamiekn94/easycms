namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThemeSection")]
    public partial class ThemeSection
    {
        public ThemeSection()
        {
            ActiveWidget = new HashSet<ActiveWidget>();
        }

        [Key]
        public int SectionId { get; set; }

        [Required]
        [StringLength(50)]
        public string CodeName { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public int ThemeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }

        [Required]
        [StringLength(50)]
        public string FriendlyName { get; set; }

        public virtual ICollection<ActiveWidget> ActiveWidget { get; set; }

        public virtual Theme Theme { get; set; }
    }
}
