namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormFieldsWidget")]
    public partial class FormFieldsWidget
    {
        public FormFieldsWidget()
        {
            FormFilledWidget = new HashSet<FormFilledWidget>();
        }

        [Key]
        public int FormFieldWidgetId { get; set; }

        public int FormId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string DisplayName { get; set; }

        public int Type { get; set; }

        public virtual FormWidget FormWidget { get; set; }

        public virtual ICollection<FormFilledWidget> FormFilledWidget { get; set; }
    }
}
