namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GroepsRecht")]
    public partial class GroepsRecht
    {
        public int Id { get; set; }

        public int RightID { get; set; }

        public Guid RoleId { get; set; }

        public virtual Recht Recht { get; set; }

        public virtual Roles Roles { get; set; }
    }
}
