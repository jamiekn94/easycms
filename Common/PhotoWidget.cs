namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhotoWidget")]
    public partial class PhotoWidget
    {
        public PhotoWidget()
        {
            AlbumInPhotoWidget = new HashSet<AlbumInPhotoWidget>();
        }

        public int Id { get; set; }

        public int ActiveWidgetId { get; set; }

        public int AmountItems { get; set; }

        public bool IsSlide { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }

        public virtual ICollection<AlbumInPhotoWidget> AlbumInPhotoWidget { get; set; }
    }
}
