namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RegistratieLanden")]
    public partial class RegistratieLanden
    {
        public RegistratieLanden()
        {
            Gebruiker = new HashSet<Gebruiker>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(75)]
        public string Land { get; set; }

        public virtual ICollection<Gebruiker> Gebruiker { get; set; }
    }
}
