namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Agenda
    {
        public int AgendaId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool AllDay { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public bool SendReminder { get; set; }

        public bool PublicPoint { get; set; }

        [Required]
        [StringLength(10)]
        public string Color { get; set; }

        public Guid UserId { get; set; }

        public virtual Users Users { get; set; }
    }
}
