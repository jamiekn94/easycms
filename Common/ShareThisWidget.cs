namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShareThisWidget")]
    public partial class ShareThisWidget
    {
        [Key]
        public int ShareId { get; set; }

        public int ActiveWidgetId { get; set; }

        public bool HasFacebook { get; set; }

        public bool HasTwitter { get; set; }

        public bool HasLinkedIn { get; set; }

        public bool HasPinterest { get; set; }

        public bool HasEmail { get; set; }

        public bool HasGooglePlus { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
