namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhotosInAlbum")]
    public partial class PhotosInAlbum
    {
        public PhotosInAlbum()
        {
            AlbumInPhotoWidget = new HashSet<AlbumInPhotoWidget>();
        }

        [Key]
        public int FotoID { get; set; }

        [Required]
        [StringLength(255)]
        public string Naam { get; set; }

        public int FotoAlbumID { get; set; }

        public virtual ICollection<AlbumInPhotoWidget> AlbumInPhotoWidget { get; set; }

        public virtual Fotoalbum Fotoalbum { get; set; }
    }
}
