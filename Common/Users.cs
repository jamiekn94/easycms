namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        public Users()
        {
            ActionLog = new HashSet<ActionLog>();
            Agenda = new HashSet<Agenda>();
            Blog = new HashSet<Blog>();
            BlogReactie = new HashSet<BlogReactie>();
            Gebruiker = new HashSet<Gebruiker>();
            Nieuwsartikel = new HashSet<Nieuwsartikel>();
            NieuwsReactie = new HashSet<NieuwsReactie>();
            Ticket = new HashSet<Ticket>();
            TicketReactie = new HashSet<TicketReactie>();
            UserActionLog = new HashSet<UserActionLog>();
            Roles = new HashSet<Roles>();
        }

        public Guid ApplicationId { get; set; }

        [Key]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        public bool IsAnonymous { get; set; }

        public DateTime LastActivityDate { get; set; }

        public virtual ICollection<ActionLog> ActionLog { get; set; }

        public virtual ICollection<Agenda> Agenda { get; set; }

        public virtual Applications Applications { get; set; }

        public virtual ICollection<Blog> Blog { get; set; }

        public virtual ICollection<BlogReactie> BlogReactie { get; set; }

        public virtual ICollection<Gebruiker> Gebruiker { get; set; }

        public virtual Memberships Memberships { get; set; }

        public virtual ICollection<Nieuwsartikel> Nieuwsartikel { get; set; }

        public virtual ICollection<NieuwsReactie> NieuwsReactie { get; set; }

        public virtual Profiles Profiles { get; set; }

        public virtual ICollection<Ticket> Ticket { get; set; }

        public virtual ICollection<TicketReactie> TicketReactie { get; set; }

        public virtual ICollection<UserActionLog> UserActionLog { get; set; }

        public virtual ICollection<Roles> Roles { get; set; }
    }
}
