namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Theme")]
    public partial class Theme
    {
        public Theme()
        {
            Configuratie = new HashSet<Configuratie>();
            ThemeSection = new HashSet<ThemeSection>();
        }

        public int ThemeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string Image { get; set; }

        [Required]
        [StringLength(255)]
        public string Location { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        public string Author { get; set; }

        [Required]
        [StringLength(250)]
        public string AuthorWebsite { get; set; }

        [Required]
        [StringLength(250)]
        public string ThemeWebsite { get; set; }

        [Required]
        [StringLength(50)]
        public string Version { get; set; }

        public virtual ICollection<Configuratie> Configuratie { get; set; }

        public virtual ICollection<ThemeSection> ThemeSection { get; set; }
    }
}
