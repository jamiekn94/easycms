namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AlbumInPhotoWidget")]
    public partial class AlbumInPhotoWidget
    {
        public int Id { get; set; }

        public int PhotoWidgetId { get; set; }

        public int AlbumId { get; set; }

        public int PhotoId { get; set; }

        public virtual Fotoalbum Fotoalbum { get; set; }

        public virtual PhotosInAlbum PhotosInAlbum { get; set; }

        public virtual PhotoWidget PhotoWidget { get; set; }
    }
}
