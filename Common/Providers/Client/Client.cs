﻿using System.Web;

namespace Common.Providers.Client
{
    public static class Client
    {
        public static string GetIp()
        {
            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}