﻿using System;
using System.Collections.Generic;
using Common.Providers.Users;
using Common.Repositories;

namespace Common.Providers.Email
{
    public class EmailTemplate
    {
        #region Tickets

        public void SendClosedTicket(int ticketId, string currentUsername)
        {
            var emailTemplateRepository = new EmailTemplateRepository();
            var ticketRepository = new TicketRepository();
            var ticketReplyRepository = new TicketReactieRepository();
            var configurationRepository = new ConfiguratieRepository();

            var ticketInfo = ticketRepository.GetById(ticketId);
            var emailTemplateInfo = emailTemplateRepository.GetById(2);
            var userInfo = UserProvider.GetUserInfo(currentUsername);
            var firstTicketReply = ticketReplyRepository.GetFirstReaction(ticketInfo.TicketID);
            var urlWebsite = configurationRepository.GetById(1).SiteUrl;

            var ticketReactie = firstTicketReply.Reactie.Substring(3, firstTicketReply.Reactie.Length - 7);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[ticketOnderwerp]", ticketInfo.Onderwerp},
                {"[ticketBericht]", ticketReactie},
                {"[ticketOpenDatum]", string.Format("{0:d-M-yyyy H:mm}", ticketInfo.DatumOpen)},
                {"[ticketGeslotenDatum]", string.Format("{0:d-M-yyyy H:mm}", ticketInfo.DatumGesloten)},
                {"[ticketUrl]", string.Format("{0}/Tickets/{1}", urlWebsite, ticketId)}
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplateInfo.Bericht);

            var emailClass = new Email();
            emailClass.SendEmail(userInfo.Email, emailTemplateInfo.Onderwerp, replacedText, false);
        }

        public void SendReactionTicket(int ticketId, string currentUsername)
        {
            var emailTemplateRepository = new EmailTemplateRepository();
            var ticketRepository = new TicketRepository();
            var ticketReplyRepository = new TicketReactieRepository();
            var configurationRepository = new ConfiguratieRepository();

            var ticketInfo = ticketRepository.GetById(ticketId);
            var emailTemplateInfo = emailTemplateRepository.GetById(1);
            var userInfo = UserProvider.GetUserInfo(currentUsername);
            var lastTicketReply = ticketReplyRepository.GetLastReaction(ticketInfo.TicketID);
            var firstTicketReply = ticketReplyRepository.GetFirstReaction(ticketInfo.TicketID);
            var urlWebsite = configurationRepository.GetById(1).SiteUrl;

            var ticketBericht = firstTicketReply.Reactie.Substring(3, firstTicketReply.Reactie.Length - 7);
            var ticketNieuweBericht = lastTicketReply.Reactie.Substring(3, lastTicketReply.Reactie.Length - 7);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[ticketOnderwerp]", ticketInfo.Onderwerp},
                {"[ticketBericht]", ticketBericht},
                {"[ticketOpenDatum]", string.Format("{0:d-M-yyyy H:mm}", ticketInfo.DatumOpen)},
                {"[ticketUrl]", string.Format("{0}/Tickets/{1}", urlWebsite, ticketId)},
                {"[ticketReactie]", ticketNieuweBericht},
                {"[ticketDatumReactie]", string.Format("{0:d-M-yyyy H:mm}", lastTicketReply.Datum)},
                {"[ticketReactieGebruiker]", userInfo.FullName}
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplateInfo.Bericht);

            var emailClass = new Email();
            emailClass.SendEmail(userInfo.Email, emailTemplateInfo.Onderwerp, replacedText, false);
        }

        public void SendOpenTicket(int ticketId, string currentUsername)
        {
            var emailTemplateRepository = new EmailTemplateRepository();
            var ticketRepository = new TicketRepository();
            var ticketReplyRepository = new TicketReactieRepository();
            var configurationRepository = new ConfiguratieRepository();

            var ticketInfo = ticketRepository.GetById(ticketId);
            var emailTemplateInfo = emailTemplateRepository.GetById(0);
            var userInfo = UserProvider.GetUserInfo(currentUsername);
            var firstTicketReply = ticketReplyRepository.GetFirstReaction(ticketInfo.TicketID);
            var urlWebsite = configurationRepository.GetById(1).SiteUrl;

            var ticketReactie = firstTicketReply.Reactie.Substring(3, firstTicketReply.Reactie.Length - 7);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[ticketOnderwerp]", ticketInfo.Onderwerp},
                {"[ticketBericht]", ticketReactie},
                {"[ticketOpenDatum]", string.Format("{0:d-M-yyyy H:mm}", ticketInfo.DatumOpen)},
                {"[ticketUrl]", string.Format("{0}/Tickets/{1}", urlWebsite, ticketId)}
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplateInfo.Bericht);

            var emailClass = new Email();
            emailClass.SendEmail(userInfo.Email, emailTemplateInfo.Onderwerp, replacedText, false);
        }

        public void SendDeletedTicket(int ticketId, string currentUsername)
        {
            var emailTemplateRepository = new EmailTemplateRepository();
            var ticketRepository = new TicketRepository();
            var ticketReplyRepository = new TicketReactieRepository();
            var configurationRepository = new ConfiguratieRepository();

            var ticketInfo = ticketRepository.GetById(ticketId);
            var emailTemplateInfo = emailTemplateRepository.GetById(3);
            var userInfo = UserProvider.GetUserInfo(currentUsername);
            var firstTicketReply = ticketReplyRepository.GetFirstReaction(ticketInfo.TicketID);
            var urlWebsite = configurationRepository.GetById(1).SiteUrl;

            var ticketReactie = firstTicketReply.Reactie.Substring(3, firstTicketReply.Reactie.Length - 7);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[ticketOnderwerp]", ticketInfo.Onderwerp},
                {"[ticketBericht]", ticketReactie},
                {"[ticketOpenDatum]", string.Format("{0:d-M-yyyy H:mm}", ticketInfo.DatumOpen)},
                {"[ticketUrl]", string.Format("{0}/Tickets/{1}", urlWebsite, ticketId)}
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplateInfo.Bericht);

            var emailClass = new Email();
            emailClass.SendEmail(userInfo.Email, emailTemplateInfo.Onderwerp, replacedText, false);
        }

        #endregion

        #region Agenda

        public void SendAgendaReminder(int agendaId)
        {
            var emailTemplateRepository = new EmailTemplateRepository();
            var agendaRepository = new AgendaRepository();
            var agendaInfo = agendaRepository.GetById(agendaId);

            var userInfo = UserProvider.GetUserInfo("Jamieknoef");


            var emailTemplate = emailTemplateRepository.GetById(4);

            var replaceValues = new Dictionary<string, string>
            {
                {"[startDatum]", string.Format("{0:d m yyyy H:mm}", agendaInfo.StartDate)},
                {
                    "[eindDatum]",
                    agendaInfo.EndDate == null
                        ? string.Format("{0:d m yyyy}", agendaInfo.StartDate)
                        : string.Format("{0:d m yyyy H:mm}", Convert.ToDateTime(agendaInfo.EndDate))
                },
                {"[heleDag]", agendaInfo.AllDay ? "Ja" : "Nee"},
                {"[titel]", agendaInfo.Title},
                {"[volledigeNaam]", userInfo.FullName}
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplate.Bericht);

            var email = new Email();

            email.SendEmail(userInfo.Email, emailTemplate.Onderwerp, replacedText, false);
        }

        #endregion

        #region Welcome
        public void SendWelcomeEmail(string username, string password)
        {
            var emailTemplateRepository = new EmailTemplateRepository();
            var userInfo = UserProvider.GetUserInfo(username);


            var emailTemplate = emailTemplateRepository.GetById(5);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[wachtwoord]", password},
                {
                    "[registratieDatum]",string.Format("{0:d MM yyyy}", userInfo.CreatedDate)
                },
                {"[gebruikersNaam]", username},
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplate.Bericht);

            var email = new Email();

            email.SendEmail(userInfo.Email, emailTemplate.Onderwerp, replacedText, false);
        }
        #endregion

        #region Password Reset
        public void SendPasswordReset(string username, string newPassword)
        {
            var emailTemplateRepository = new EmailTemplateRepository();

            var userInfo = UserProvider.GetUserInfo(username);

            var emailTemplate = emailTemplateRepository.GetById(6);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[wachtwoord]", newPassword },
                {"[gebruikersNaam]", username},
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplate.Bericht);

            var email = new Email();

            email.SendEmail(userInfo.Email, emailTemplate.Onderwerp, replacedText, false);
        }
        #endregion

        #region Gebruikersnaam request
        public void SendUsername(string username)
        {
            var emailTemplateRepository = new EmailTemplateRepository();

            var userInfo = UserProvider.GetUserInfo(username);

            var emailTemplate = emailTemplateRepository.GetById(6);

            var replaceValues = new Dictionary<string, string>
            {
                {"[volledigeNaam]", userInfo.FullName},
                {"[gebruikersNaam]", username},
            };

            var replacedText = Replace.Replace.ReplaceText(replaceValues, emailTemplate.Bericht);

            var email = new Email();

            email.SendEmail(userInfo.Email, emailTemplate.Onderwerp, replacedText, false);
        }
        #endregion
    }
}