﻿namespace Common.Providers.Email
{
    internal interface IEmail
    {
        void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers);
        void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers, object attachments);

        void QueueEmail(string toAccount, string subject, string body, bool canReceiveAnswers, object attachments);
        void QueueEmail(string toAccount, string subject, bool canReceiveAnswers, string body);
    }
}