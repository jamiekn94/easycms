﻿using System;

namespace Common.Providers.Email
{
    public class MockEmail : IEmail
    {
        public void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers)
        {
            throw new NotImplementedException();
        }

        public void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers, object attachments)
        {
            throw new NotImplementedException();
        }

        public void QueueEmail(string toAccount, string subject, string body, bool canReceiveAnswers, object attachments)
        {
            throw new NotImplementedException();
        }

        public void QueueEmail(string toAccount, string subject, bool canReceiveAnswers, string body)
        {
            throw new NotImplementedException();
        }
    }
}