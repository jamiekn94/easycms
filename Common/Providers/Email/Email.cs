﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using Common.Repositories;

namespace Common.Providers.Email
{
    public class Email
    {
        public void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers)
        {
            var emailThread = new Thread(() => _SendEmail(toAccount, subject, body, canReceiveAnswers));
            emailThread.Start();
        }

        private void _SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers)
        {
            var configurationRepository = new ConfiguratieRepository();
            var configuratationInfo = configurationRepository.GetById(1);

            var mail = new MailMessage();
            mail.To.Add(toAccount);
            mail.From = canReceiveAnswers
                ? new MailAddress(configuratationInfo.EmailInfoAccount)
                : new MailAddress(configuratationInfo.EmailNoReplyAccount);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            var smtp = new SmtpClient(configuratationInfo.EmailNoReplySMTPHost, configuratationInfo.EmailNoReplySMTPPort)
            {
                Credentials =
                    new NetworkCredential(configuratationInfo.EmailNoReplyAccount,
                        configuratationInfo.EmailNoReplyWachtwoord),
                EnableSsl = configuratationInfo.EmailNoReplySMTPSecure
            };

            smtp.Send(mail);
        }


        public void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers,
            HttpPostedFileBase[] attachments, Configuratie configuratationInfo = null)
        {
            if (configuratationInfo == null)
            {
                var configurationRepository = new ConfiguratieRepository();
                configuratationInfo = configurationRepository.GetById(1);
            }

            var mail = new MailMessage();
            mail.To.Add(toAccount);
            mail.From = new MailAddress(configuratationInfo.EmailNoReplyAccount);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            var smtp = new SmtpClient(configuratationInfo.EmailNoReplySMTPHost, configuratationInfo.EmailNoReplySMTPPort)
            {
                Credentials =
                    new NetworkCredential(configuratationInfo.EmailNoReplyAccount,
                        configuratationInfo.EmailNoReplyWachtwoord),
                EnableSsl = configuratationInfo.EmailNoReplySMTPSecure
            };

            if (attachments.Any())
            {
                foreach (var attachment in attachments)
                {
                    var newAttachment = new Attachment(attachment.InputStream, attachment.FileName);
                    mail.Attachments.Add(newAttachment);
                }
            }

            smtp.Send(mail);
        }

        public void SendEmail(string toAccount, string subject, string body, bool canReceiveAnswers,
            IEnumerable<FileInfo> attachments, Configuratie configuratationInfo = null)
        {
            if (configuratationInfo == null)
            {
                var configurationRepository = new ConfiguratieRepository();
                configuratationInfo = configurationRepository.GetById(1);
            }

            var mail = new MailMessage();
            mail.To.Add(toAccount);
            mail.From = new MailAddress(configuratationInfo.EmailNoReplyAccount);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            var smtp = new SmtpClient(configuratationInfo.EmailNoReplySMTPHost, configuratationInfo.EmailNoReplySMTPPort)
            {
                Credentials =
                    new NetworkCredential(configuratationInfo.EmailNoReplyAccount,
                        configuratationInfo.EmailNoReplyWachtwoord),
                EnableSsl = configuratationInfo.EmailNoReplySMTPSecure
            };

            if (attachments.Any())
            {
                foreach (var attachment in attachments)
                {
                    var newAttachment = new Attachment(attachment.OpenRead(), attachment.Name);
                    mail.Attachments.Add(newAttachment);
                }
            }

            smtp.Send(mail);
        }

        public void QueueEmail(string toAccount, string subject, string body, bool canReceiveAnswers, object attachments)
        {
            throw new NotImplementedException();
        }

        public void QueueEmail(string toAccount, string subject, bool canReceiveAnswers, string body)
        {
            throw new NotImplementedException();
        }
    }
}