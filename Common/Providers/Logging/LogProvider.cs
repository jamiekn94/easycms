﻿using System;
using System.Web;
using System.Web.Mvc;
using Common.Providers.Users;
using Common.Repositories;

namespace Common.Providers.Logging
{
    public static class LogProvider
    {
        public enum Method
        {
            Edit,
            Delete,
            Add
        }

        public static void LogAction(string information)
        {
            var logRepository = new ActionLogRepository();
            var logTable = new ActionLog
            {
                Action = HttpContext.Current.Request.RequestContext.RouteData.Values["Action"].ToString(),
                Controller = HttpContext.Current.Request.RequestContext.RouteData.Values["Controller"].ToString(),
                Information = information,
                UserId = UserProvider.GetUserId(),
                Date = DateTime.Now
            };

            logRepository.Add(logTable);
            logRepository.Save();;
        }

        public static void LogError(ExceptionContext context)
        {
            var logRepository = new ErrorLogRepository();
            var logTable = new ErrorLog();

            var innerException = context.Exception.InnerException != null
                ? context.Exception.InnerException.ToString()
                : "None";

            var action = context.RouteData.Values["action"] != null
                ? context.RouteData.Values["action"].ToString()
                : "Unknown";

            var membership = UserProvider.GetMembershipUserInfo();

            logTable.Url = context.HttpContext.Request.Url != null
                ? context.HttpContext.Request.Url.AbsoluteUri
                : "Onbekend";
            logTable.InnerException = innerException;
            logTable.Action = action;
            logTable.Controller = context.Controller.ToString();
            logTable.GebruikersID = membership != null ? membership.ProviderUserKey.ToString() : "Onbekend";
            logTable.Gebruikersnaam = membership != null ? membership.UserName : "Onbekend";
            logTable.IP = context.HttpContext.Request.ServerVariables["REMOTE_ADDR"];
            logTable.Source = context.Exception.Source;
            logTable.Message = context.Exception.Message;
            logTable.Stacktrace = context.Exception.StackTrace;
            logTable.Target = context.Exception.TargetSite.ToString();
            logTable.Type = context.Exception.GetType().ToString();
            logTable.Datum = DateTime.Now;
            logRepository.Add(logTable);
            logRepository.Save();
        }
    }
}