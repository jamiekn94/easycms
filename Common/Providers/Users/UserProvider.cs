﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using Common.Models.Admin.Users;
using Common.Repositories;
using RoleProvider = Common.Providers.Roles.RoleProvider;

namespace Common.Providers.Users
{
    public static class UserProvider
    {
        #region Enum Password change
        public enum PasswordChange
        {
            InvalidPassword, ContainsName, Success
        }
        #endregion

        public static string GetFullName()
        {
            var userInfo = GetUserInfo(GetUsername(GetUserId()));
            var fullName = userInfo.FullName;
            return fullName;
        }

        public static bool ResetPassword(string username, ref string passwordAnswer)
        {
            bool boolPasswordReset;

            var membershipInfo = GetMembership(username);
            try
            {
                passwordAnswer = membershipInfo.ResetPassword(passwordAnswer.ToLower());
                boolPasswordReset = true;
            }
            catch (MembershipPasswordException)
            {
                boolPasswordReset = false;
            }
            return boolPasswordReset;
        }

        public static void LockUser(string username, string comment)
        {
            for (var i = 0; i < System.Web.Security.Membership.MaxInvalidPasswordAttempts; i++)
            {
                System.Web.Security.Membership.ValidateUser(username, "g8-1T1zq,v4.7?1");
            }

           SetComment(username, comment);
        }

        public static bool IsUserLocked(string username)
        {
            return GetMembership(username).IsLockedOut;
        }

        public static void SetComment(string username, string comment)
        {
            var membership = GetMembership(username);
            membership.Comment = comment;
            System.Web.Security.Membership.UpdateUser(membership);
        }

        public static void UnlockUser(string username)
        {
            var membershipUser = GetMembership(username);
            membershipUser.Comment = null;
            System.Web.Security.Membership.UpdateUser(membershipUser);
            membershipUser.UnlockUser();
        }

        public static bool ValidateUser(string username, string password)
        {
            var membershipRepository = new MembershipRepository();
            
            var validationLogin = System.Web.Security.Membership.ValidateUser(username, password);

            var membershipInfo = membershipRepository.GetByUsername(username);
            var userPassFailures = membershipInfo.FailedPasswordAttemptCount;

            if (userPassFailures == System.Web.Security.Membership.MaxInvalidPasswordAttempts)
            {
                membershipInfo.Comment =
                    string.Format(
                        "Uw account is geblokkeerd voor 30 minuten omdat u te vaak een verkeerd wachtwoord heeft ingevuld.");

                membershipRepository.Save();
            }

            return validationLogin;
        }

        public static MembershipUserCollection GetAllMemberships()
        {
            return System.Web.Security.Membership.GetAllUsers();
        }

        public static string GetFullName(Guid providerUserId)
        {
            var userRepository = new GebruikersRepository();
            var userInfo = userRepository.GetFirstAndLastName(providerUserId);

            return string.Format("{0} {1}", userInfo.Firstname, userInfo.Lastname);
        }

        public static Guid GetUserId()
        {
            // ReSharper disable PossibleNullReferenceException
            var userGuid =
                (Guid) System.Web.Security.Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;
            // ReSharper restore PossibleNullReferenceException
            return userGuid;
        }

        public static Guid GetProviderUserKey(string username)
        {
            return (Guid) System.Web.Security.Membership.GetUser(username).ProviderUserKey;
        }

        public static bool IsLoggedIn()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public static string GetUsername(Guid providerUserId)
        {
            if (IsLoggedIn())
            {
                // ReSharper disable once PossibleNullReferenceException
                return GetMembership(providerUserId).UserName;
            }
            return "Niet ingelogd";
        }

        public static string GetUsername()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        public static bool UnlockUser(MembershipUser membershipUser)
        {
            return membershipUser.UnlockUser();
        }

        public static string ResetPassword(MembershipUser membershipUser)
        {
            return membershipUser.ResetPassword();
        }

        public static void UpdateMemberShip(MembershipUser membership)
        {
            System.Web.Security.Membership.UpdateUser(membership);
        }

        public static MembershipUser GetMembership(object providerUserId)
        {
            return System.Web.Security.Membership.GetUser(new Guid(providerUserId.ToString()));
        }

        public static MembershipUser GetMembership(string username)
        {
            return System.Web.Security.Membership.GetUser(username);
        }

        public static MembershipUser GetMembershipUserInfo()
        {
            return System.Web.Security.Membership.GetUser();
        }

        public static UserModelInfo GetUserInfo(object providerUserId)
        {
            var userRepository = new GebruikersRepository();

            var membershipUserInfo = GetMembership(providerUserId);
            var userInfo = userRepository.GetById(membershipUserInfo.ProviderUserKey);
            var userRole = RoleProvider.GetUserRoleByUsername(membershipUserInfo.UserName);

            var userModelInfo = new UserModelInfo
            {
                AdressExtra = userInfo.AdresToevoeging,
                Firstname = userInfo.Voornaam,
                Lastname = userInfo.Achternaam,
                HouseNumber = (int) userInfo.Huisnummer,
                Land = userInfo.Land,
                Postcode = userInfo.Postcode,
                Residence = userInfo.Woonplaats,
                Street = userInfo.Straat,
                // ReSharper disable once PossibleNullReferenceException
                ProviderUserId = membershipUserInfo.ProviderUserKey.ToString(),
                Username = membershipUserInfo.UserName,
                Email = membershipUserInfo.Email,
                RoleName = userRole,
                HasAvatar = userInfo.HasAvatar,
                SubscriptionNewsletter = userInfo.Newsletter
            };

            return userModelInfo;
        }

        public static UserModelInfo GetUserInfo(string username)
        {
            var userRepository = new GebruikersRepository();

            var membershipUserInfo = GetMembership(username);
            var userInfo = userRepository.GetById(membershipUserInfo.ProviderUserKey);
            var userRole = RoleProvider.GetUserRoleByUsername(username);

            var userModelInfo = new UserModelInfo
            {
                AdressExtra = userInfo.AdresToevoeging,
                Firstname = userInfo.Voornaam,
                Lastname = userInfo.Achternaam,
                HouseNumber = (int) userInfo.Huisnummer,
                Land = userInfo.Land,
                Postcode = userInfo.Postcode,
                Residence = userInfo.Woonplaats,
                Street = userInfo.Straat,
                // ReSharper disable once PossibleNullReferenceException
                ProviderUserId = membershipUserInfo.ProviderUserKey.ToString(),
                Username = membershipUserInfo.UserName,
                Email = membershipUserInfo.Email,
                RoleName = userRole,
                HasAvatar = userInfo.HasAvatar,
                SecurityQuestion = membershipUserInfo.PasswordQuestion,
                SubscriptionNewsletter = userInfo.HasAvatar
            };

            return userModelInfo;
        }

        public static bool ChangePasswordQuestionAndAnswer(string username, string currentPassword,
            string securityQuestion, string securityAnswer)
        {
            var membership = GetMembership(username);

          return  membership.ChangePasswordQuestionAndAnswer(currentPassword, securityQuestion, securityAnswer);
        }

        public static PasswordChange SetNewPassword(string oldPassword, string newPassword, string username)
        {
            var membership = GetMembership(username);
            var userinfo = GetUserInfo(username);
            newPassword = newPassword.ToLower();

            // Check if the password doesn't contain the firstname, lastname or username
            if (newPassword.Contains(userinfo.Firstname.ToLower()) || newPassword.Contains(userinfo.Lastname.ToLower()) ||
                newPassword.Contains(userinfo.Username.ToLower()))
            {
                return PasswordChange.ContainsName;
            }
            else
            {
                // Check if the password is right
                if (!membership.ChangePassword(oldPassword, newPassword))
                {
                    return PasswordChange.InvalidPassword;
                }
            }

            // Return success
            return PasswordChange.Success;
        }

        public static void UpdateUser(string firstname, string lastname, string email, string street, int housenumber,
            string adressExtra, string residence, string postcode, int land, HttpPostedFileBase profilePhoto, string username, bool newsletter)
        {
            var memberShipUser = GetMembership(username);

            if (memberShipUser != null)
            {
                memberShipUser.Email = email;
                System.Web.Security.Membership.UpdateUser(memberShipUser);
            }

            var userRepository = new GebruikersRepository();
            var userInfo = userRepository.GetById(memberShipUser.ProviderUserKey);

            userInfo.Achternaam = lastname;
            userInfo.AdresToevoeging = adressExtra;
            userInfo.Huisnummer = housenumber;
            userInfo.Land = land;
            userInfo.Postcode = postcode;
            userInfo.Straat = street;
            userInfo.Voornaam = firstname;
            userInfo.Woonplaats = residence;
            userInfo.Newsletter = newsletter;

            // Add profile photo
            if (profilePhoto != null)
            {
                // Check if we made an directory
                var fullPath = HttpContext.Current.Server.MapPath("~/Shared/ProfilePhotos");
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                // Check if there are any previous pictures
                if (System.IO.File.Exists(fullPath + "/" + username + ".png"))
                {
                   System.IO.File.Delete(fullPath + "/" + username + ".png"); 
                }

                // Save image
                profilePhoto.SaveAs(fullPath + "/" + username + ".png");

                userInfo.HasAvatar = true;
            }

            userRepository.Save();
        }

        public static bool ChangePassword(string oldPassword, string newPassword)
        {
            var memberShipUser = System.Web.Security.Membership.GetUser();
            if (memberShipUser != null)
            {
                return memberShipUser.ChangePassword(oldPassword, newPassword);
            }
            return false;
        }



        public static void DeleteUser(string username)
        {
            // TODO: DELETE ALL THE OTHER FIELDS
            System.Web.Security.Membership.DeleteUser(username, true);
        }

        public static bool UpdatePassword(string oldPassword, string newPassword, MembershipUser membershipUser)
        {
            return membershipUser.ChangePassword(oldPassword, newPassword);
        }

        public static IEnumerable<UserModelInfo> GetAllUsers(int amount = -1)
        {
            var userRepository = new GebruikersRepository();

            var usersInfoList = amount == -1
                ? userRepository.GetList()
                : userRepository.GetList(amount);

            return Enumerable.ToList((from userInfo in usersInfoList.AsParallel()
                let userMembership = GetMembership(userInfo.GebruikersID)
                let userRole = RoleProvider.GetUserRoleByUsername(userMembership.UserName)
                select new UserModelInfo
                {
                    AdressExtra = userInfo.AdresToevoeging,
                    Firstname = userInfo.Voornaam,
                    HouseNumber = (int) userInfo.Huisnummer,
                    Land = userInfo.Land,
                    Lastname = userInfo.Achternaam,
                    Postcode = userInfo.Postcode,
                    ProviderUserId = userInfo.ProviderGebruikersId,
                    Residence = userInfo.Woonplaats,
                    RoleName = userRole,
                    Street = userInfo.Straat,
                    Username = userMembership.UserName,
                    Email = userMembership.Email,
                    CreatedDate = userMembership.CreationDate,
                    IsAccepted = userMembership.IsApproved,
                    IsLockedOut = userMembership.IsLockedOut,
                    HasAvatar = userInfo.HasAvatar,
                    SubscriptionNewsletter = userInfo.Newsletter,
                    LastLoginDate = userMembership.LastLoginDate
                }));
        }

        public static MembershipCreateStatus AddUser(string firstname, string lastname, string email, string street,
            int housenumber, string adressExtra, string residence, string postcode, int land, string username,
            string password, string passwordQuestion, string passwordAnswer, string roleName, HttpPostedFileBase profilePhoto, bool newsletter, bool approved)
        {
            MembershipCreateStatus status;
            var membershipUser = System.Web.Security.Membership.CreateUser(username, password, email,
                passwordQuestion, passwordAnswer!= null ? passwordAnswer.ToLower() : passwordAnswer, approved, out status);

            if (status == MembershipCreateStatus.Success)
            {
                // Add user to role
                RoleProvider.AddUserToRole(username, roleName);

                var userRepository = new GebruikersRepository();

                var gebruikerInfo = new Gebruiker
                {
                    Achternaam = lastname,
                    AdresToevoeging = adressExtra,
                    Huisnummer = housenumber,
                    Land = land,
                    ProviderGebruikersId = membershipUser.ProviderUserKey.ToString(),
                    Straat = street,
                    Voornaam = firstname,
                    Postcode = postcode,
                    Woonplaats = residence,
                    GebruikersID = (Guid) membershipUser.ProviderUserKey,
                    Newsletter = newsletter
                };

                #region User verification
                // Check if user verification is enabled
                var configurationRepository = new ConfiguratieRepository();
                var configurationInfo = configurationRepository.GetById(1);

                if (configurationInfo.GebruikersVerificatie)
                {
                    SetComment(username, "Uw account is nog niet goedgekeurd.");
                }
                #endregion

                #region Profile Photo
                // Add profile photo
                if (profilePhoto != null)
                {
                    // Check if we made an directory
                    var fullPath = HttpContext.Current.Server.MapPath("~/ProfielFotos");
                    if (!Directory.Exists(fullPath))
                    {
                        Directory.CreateDirectory(fullPath);
                    }

                    profilePhoto.SaveAs(fullPath + "/" + username + ".png");

                    gebruikerInfo.HasAvatar = true;
                }
                #endregion
                
                // Add user
                userRepository.Add(gebruikerInfo);

                // Save user
                userRepository.Save();

                // Get user
                var userInfo = GetMembership(username);

                #region Action log
                // Add user action log
                if (!roleName.Equals("Leden", StringComparison.CurrentCultureIgnoreCase))
                {
                    var userActionLogRepository = new UserActionLogRepository();
                    var actionLogRepository = new ActionLogRepository();

                    userActionLogRepository.Add(new UserActionLog
                    {
                        UserId = (Guid) userInfo.ProviderUserKey,
                        ActionLogId = actionLogRepository.GetHighestActionId(),
                    });

                    userActionLogRepository.Save();
                }
                #endregion
            }

            return status;
        }
    }
}