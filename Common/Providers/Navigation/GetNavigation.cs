using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Common.Models.Admin.Navigation;
using Common.Repositories;

namespace Common.Providers.Navigation
{
    public class GetNavigation
    {
        public Dictionary<NavigationType, string> GetTypes()
        {
            var listTypes = new Dictionary<NavigationType, string>
            {
                
                {NavigationType.BuildIn, "Ingebouwde pagina"},
                {NavigationType.Dynamic, "Dynamische pagina"},
                {NavigationType.News, "Nieuwsartikel"},
                {NavigationType.Blog, "Blogartikel"},
                {NavigationType.External, "Externe link"},
                {NavigationType.Photo, "Fotoalbum"}
            };

            return listTypes;
        }

        public IEnumerable<SelectListItem> GetDynamicPages(int seoPageId = 0)
        {
            var dynamicPageRepository = new Repositories.PaginaRepository();
            var listDynamicPages = dynamicPageRepository.GetList();

            var returnCollection = new Collection<SelectListItem>();

            foreach (var page in listDynamicPages)
            {
                returnCollection.Add(new SelectListItem
                {
                    Text = page.Titel,
                    Value = page.PaginaID.ToString(CultureInfo.InvariantCulture),
                    Selected = page.PaginaID == seoPageId
                });
            }
            return returnCollection.OrderByDescending(x => x.Selected);
        }

        public IEnumerable<SelectListItem> GetNewsArticles(int newsarticleId = 0)
        {
            var newsRepository = new Repositories.NieuwsRepository();
            var listNewsArticles = newsRepository.GetList();

            var returnCollection = new Collection<SelectListItem>();

            foreach (var article in listNewsArticles)
            {
                returnCollection.Add(new SelectListItem
                {
                    Text = article.Titel,
                    Value = article.ArtikelID.ToString(CultureInfo.InvariantCulture),
                    Selected = article.ArtikelID == newsarticleId
                });
            }
            return returnCollection.OrderByDescending(x => x.Selected);
        }

        public IEnumerable<SelectListItem> GetPhotoAlbums(int photoAlbumId = 0)
        {
            using (var photoAlbumRepository = new PhotoAlbumRepository())
            {
                var listPhotoAlbums = photoAlbumRepository.GetList();

                return listPhotoAlbums.Select(x => new SelectListItem
                {
                    Text = x.Titel,
                    Value = x.AlbumID.ToString(CultureInfo.InvariantCulture),
                    Selected = x.AlbumID == photoAlbumId
                }).OrderBy(x=> x.Selected);
            }
        }

        public IEnumerable<SelectListItem> GetBlogArticles(int blogArticleId = 0)
        {
            var blogRepository = new Repositories.BlogRepository();
            var listBlogArticles = blogRepository.GetList();

            var returnCollection = new Collection<SelectListItem>();

            foreach (var article in listBlogArticles)
            {
                returnCollection.Add(new SelectListItem
                {
                    Text = article.Titel,
                    Value = article.BlogID.ToString(CultureInfo.InvariantCulture),
                    Selected = article.BlogID == blogArticleId
                });
            }
            return returnCollection.OrderByDescending(x => x.Selected);
        }

        public IEnumerable<SelectListItem> GetBuildInPages(int buildInPageId = 0)
        {
            var dynamicPageRepository = new SeoPageRepository();
            var listBuildInPages = dynamicPageRepository.GetList();

            var returnCollection = new Collection<SelectListItem>();

            foreach (var page in listBuildInPages)
            {
                returnCollection.Add(new SelectListItem
                {
                    Text = page.Titel,
                    Value = page.Id.ToString(CultureInfo.InvariantCulture),
                    Selected = page.Id == buildInPageId
                });
            }
            return returnCollection.OrderByDescending(x => x.Selected);
        }
    }
}