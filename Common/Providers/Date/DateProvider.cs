﻿using System;

namespace Common.Providers.Date
{
    public static class DateProvider
    {
        public static string GetNiceDate(DateTime dateTime)
        {
            var dateDifference = DateTime.Now - dateTime;
            var niceDate = string.Empty;
            var formattedTime = 0;
            // minutes
            if (dateDifference.TotalMinutes < 60)
            {
                formattedTime = (int) dateDifference.TotalMinutes;
                niceDate = formattedTime == 1
                    ? string.Format("{0} minuut geleden", formattedTime)
                    : string.Format("{0} minuten geleden", formattedTime);
            }
                // Hours
            else if (dateDifference.TotalHours < 24)
            {
                formattedTime = (int) dateDifference.TotalHours;
                niceDate = formattedTime == 1
                    ? string.Format("{0} uur geleden", formattedTime)
                    : string.Format("{0} uren geleden", formattedTime);
            }
                // Days
            else if (dateDifference.TotalDays < 7)
            {
                formattedTime = (int) dateDifference.TotalDays;
                niceDate = formattedTime == 1
                    ? string.Format("{0} dag geleden", formattedTime)
                    : string.Format("{0} dagen geleden", formattedTime);
            }
                // Weeks
            else if (dateDifference.TotalDays > 7 && dateDifference.TotalDays < 30)
            {
                formattedTime = (int) Math.Round(dateDifference.TotalDays/7, 0);
                niceDate = formattedTime == 1
                    ? string.Format("{0} week geleden", formattedTime)
                    : string.Format("{0} weken geleden", formattedTime);
            }
                // Months
            else if (dateDifference.TotalDays > 30 && dateDifference.TotalDays < 364)
            {
                formattedTime = (int) Math.Round(dateDifference.TotalDays/31, 0);
                niceDate = formattedTime == 1
                    ? string.Format("{0} maand geleden", formattedTime)
                    : string.Format("{0} maanden geleden", formattedTime);
            }
                // Years
            else
            {
                formattedTime = (int) Math.Round(dateDifference.TotalDays*12/365, 0);
                niceDate = formattedTime == 1
                    ? string.Format("{0} jaar geleden", formattedTime)
                    : string.Format("{0} jaren geleden", formattedTime);
            }

            return niceDate;
        }

        /// <summary>
        /// Get een lange datum zonder tijd
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>cijferdag lettermaand cijferjaar</returns>
        public static string GetLongDate(DateTime dateTime)
        {
            return string.Format("{0:d MMMM yyyy}", dateTime);
        }

        /// <summary>
        /// Get een lange datum met tijd
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>cijferdag lettermaand cijferjaar cijferuur:cijferminuut</returns>
        public static string GetLongDateTime(DateTime dateTime)
        {
            return string.Format("{0:d MMMM yyyy HHH:mm}", dateTime);
        }
    }
}