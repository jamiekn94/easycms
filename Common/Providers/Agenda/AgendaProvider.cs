﻿using System;
using System.Linq;
using System.Threading;
using Common.Repositories;

namespace Common.Providers.Agenda
{
    public class AgendaProvider
    {
        public void SendReminder(object agendaId)
        {
            int _agendaId = Convert.ToInt16(agendaId);

            var emailTemplate = new Email.EmailTemplate();
            emailTemplate.SendAgendaReminder(_agendaId);
        }

        public void ScheduleAgenda(int agendaId)
        {
            var agendaRepository = new AgendaRepository();

            var agendaInfo = agendaRepository.GetById(agendaId);

            var disablePeriodic = new TimeSpan(0, 0, 0, 0, -1);
            var timeDifference = Convert.ToDateTime(agendaInfo.StartDate.AddDays(-1)) - DateTime.Now;

            var agendaTimer = new Timer(SendReminder, agendaId, timeDifference, disablePeriodic);

            AgendaSchedules.AddTimer(new AgendaSchedules.ScheduledTimersClass
            {
                AgendaId = agendaId,
                AgendaTimer = agendaTimer
            });
        }

        public void ScheduleAllAgenda()
        {
            var agendaRepository = new AgendaRepository();

            var agendaList =
                agendaRepository.GetList().Where(x => x.StartDate.AddDays(-1) >= DateTime.Now);

            foreach (var agenda in agendaList)
            {
                ScheduleAgenda(agenda.AgendaId);
            }
        }
    }
}