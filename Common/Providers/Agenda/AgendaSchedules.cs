﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace Common.Providers.Agenda
{
    public class AgendaSchedules
    {
        private static readonly Collection<ScheduledTimersClass> ScheduledTimers =
            new Collection<ScheduledTimersClass>();

        public static ScheduledTimersClass GetScheduledTimer(int agendaId)
        {
            return ScheduledTimers.FirstOrDefault(x => x.AgendaId == agendaId);
        }

        public static void RemoveTimer(int agendaId)
        {
            var scheduledTimer = GetScheduledTimer(agendaId).AgendaTimer;
            scheduledTimer.Dispose();

            ScheduledTimers.Remove(GetScheduledTimer(agendaId));
        }

        public static void AddTimer(ScheduledTimersClass scheduledTimer)
        {
            ScheduledTimers.Add(scheduledTimer);
        }

        public class ScheduledTimersClass
        {
            public Timer AgendaTimer { get; set; }
            public int AgendaId { get; set; }
        }
    }
}