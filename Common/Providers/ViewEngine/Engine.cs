﻿using System.Web.Mvc;

namespace Common.Providers.ViewEngine
{
    public class Engine : RazorViewEngine
    {
        public Engine()
        {
            ViewLocationFormats = new[]
            {
                "~/Themes/" + "Test" + "/Views/{1}/{0}.cshtml",
                "~/Themes/" + "Test" + "/Views/%1/Shared/{0}.cshtml"
            };


            MasterLocationFormats = new[]
            {
                "~/Themes/" + "Test" + "/Views/%1/{1}/{0}.cshtml",
                "~/Themes/" + "Test" + "/Views/%1/Shared/{0}.cshtml"
            };


            PartialViewLocationFormats = new[]
            {
                "~/Themes/" + "Test" + "/Views/%1/{1}/{0}.cshtml",
                "~/Themes/" + "Test" + "/Views/%1/Shared/{0}.cshtml"
            };
        }
    }
}