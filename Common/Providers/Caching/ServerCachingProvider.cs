﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Common.Providers.Caching
{
    public static class ServerCachingProvider
    {
        private static ObjectCache Cache
        {
            get { return MemoryCache.Default; }
        }


        public static object Get(string key)
        {
            return Cache[key];
        }

        public static void Set(string key, object data, int cacheTime, bool noExpirationDate = false)
        {
            if (!noExpirationDate)
            {
                var policy = new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cacheTime)
                };

                Cache.Add(new CacheItem(key, data), policy);
            }
            else
            {
                Cache.Add(key, data, ObjectCache.InfiniteAbsoluteExpiration);
            }
        }

        public static bool IsSet(string key)
        {
            return (Cache[key] != null);
        }

        public static void Invalidate(string key)
        {
            Cache.Remove(key);
        }

        public static void RemoveAllServerCache()
        {
            var keys = Cache.Select(item => item.Key).ToList();

            foreach (string key in keys)
            {
                var cacheItem = Cache.Get(key);
                cacheItem = null;

                Cache.Remove(key);
            }
        }
    }
}
