﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Providers.Caching
{
    public class ClientCachingProvider
    {
        public static void AddClientCache(string name, object item)
        {
            HttpContext.Current.Cache.Insert(name, item);
        }

        public static void RemoveClientCache(string name)
        {
            HttpContext.Current.Cache.Remove(name);
        }

        public static bool ClientCacheExists(string name)
        {
            return HttpContext.Current.Cache.Get(name) != null;
        }

        public static object GetClientCache(string name)
        {
            return HttpContext.Current.Cache.Get(name);
        }

        public static void RemoveAllClientCache()
        {
            var keys = new List<string>();
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();

            while (enumerator.MoveNext())
                keys.Add(enumerator.Key.ToString());


            foreach (string key in keys)
                HttpContext.Current.Cache.Remove(key);
        }
    }
}
