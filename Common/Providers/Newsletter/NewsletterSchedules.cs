﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace Common.Providers.Newsletter
{
    public static class NewsletterSchedules
    {
        private static readonly Collection<ScheduledTimersClass> ScheduledTimers =
            new Collection<ScheduledTimersClass>();

        public static ScheduledTimersClass GetScheduledTimer(int newsletterId)
        {
            return ScheduledTimers.FirstOrDefault(x => x.NewsLetterId == newsletterId);
        }

        public static void RemoveScheduler(int newsletterId)
        {
            var scheduledTimer = GetScheduledTimer(newsletterId).NewsletterTimer;
            scheduledTimer.Dispose();

            ScheduledTimers.Remove(GetScheduledTimer(newsletterId));
        }

        public static void AddScheduler(ScheduledTimersClass scheduledTimer)
        {
            ScheduledTimers.Add(scheduledTimer);
        }

        public class ScheduledTimersClass
        {
            public Timer NewsletterTimer { get; set; }
            public int NewsLetterId { get; set; }
        }
    }
}