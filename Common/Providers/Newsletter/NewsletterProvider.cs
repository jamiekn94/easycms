﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Security;
using Common.Providers.Files;
using Common.Providers.Users;
using Common.Repositories;

namespace Common.Providers.Newsletter
{
    public class NewsletterProvider
    {
        public void Send(int id, string subject, string content, IEnumerable<HttpPostedFileBase> attachments)
        {
            var t = new Thread(() => StartSending(id, subject, content, attachments));
            t.Start();
        }

        private void StartSending(int id, string subject, string content, IEnumerable<HttpPostedFileBase> attachments)
        {
            var userRepository = new GebruikersRepository();

            // Get all users that have a newsletter subscription
            var listUsers = userRepository.GetEmailsForNewsletter();

            // Get all user memberships
            var listMemberships = UserProvider.GetAllMemberships();

            // Create a new collection that stores the memberships that have a subscription to the newsletter
            var listNewsletterUsers = new Collection<MembershipUser>();

            // Foreach membership
            foreach (var membership in listMemberships.Cast<MembershipUser>())
            {
                // Foreach user
                foreach (var user in listUsers)
                {
                    if ((Guid)membership.ProviderUserKey != user.GebruikersID)
                    {
                        listNewsletterUsers.Add(membership);
                    }
                }
            }

            // Get configuration
            var configurationRepository = new ConfiguratieRepository();
            var configurationInfo = configurationRepository.GetById(1);

            var emailProvider = new Email.Email();

            var listSendAttachments = new Collection<FileInfo>();

            foreach (var attachment in attachments)
            {
                listSendAttachments.Add(new FileInfo(HostingEnvironment.MapPath("~/Shared/Newsletter/" + id) + "/" +
                                       attachment.FileName));
            }

            // Send the newsletter to the user
            foreach (var user in listNewsletterUsers)
            {
                emailProvider.SendEmail(user.Email, subject, content, false,
                    listSendAttachments, configurationInfo);
                Thread.Sleep(500);
            }

            // Delete timer
            var scheduleTimer = NewsletterSchedules.GetScheduledTimer(id);

            if (scheduleTimer != null)
            {
                NewsletterSchedules.RemoveScheduler(id);
            }

            // Update newsletter
            var newsletterRepository = new NewsletterRepository();
            var newsletterInfo = newsletterRepository.GetById(id);

            if (newsletterInfo != null)
            {
                newsletterInfo.Status = true;
                newsletterRepository.Save();
            }

        }

        public void ScheduleNewsarticle(int newsArticleId)
        {
            var newsletterRepository = new NewsletterRepository();

            var newsletterInfo = newsletterRepository.GetById(newsArticleId);

            var disablePeriodic = new TimeSpan(0, 0, 0, 0, -1);
            var timeDifference = Convert.ToDateTime(newsletterInfo.ScheduledTime) - DateTime.Now;

            var newsletterTimer = new Timer(StartScheduledNewsletter, newsletterInfo.NewsletterId, timeDifference,
                disablePeriodic);

            NewsletterSchedules.AddScheduler(new NewsletterSchedules.ScheduledTimersClass
            {
                NewsLetterId = newsArticleId,
                NewsletterTimer = newsletterTimer
            });
        }

        public void ScheduleAllNewsletters()
        {
            var newsletterRepository = new NewsletterRepository();
            var newsletterList = newsletterRepository.GetList();

            foreach (var newsletter in newsletterList)
            {
                ScheduleNewsarticle(newsletter.NewsletterId);
            }
        }

        private void StartScheduledNewsletter(object newsletterId)
        {
            int _newsletterId = Convert.ToInt16(newsletterId);

            var newsletterRepository = new NewsletterRepository();
            var newsletterAttachmentRepository = new NewsletterAttachmentsRepository();

            var newsletterInfo = newsletterRepository.GetById(_newsletterId);
            var newsletterAttachments =
                newsletterAttachmentRepository.GetAttachments(_newsletterId);

            newsletterInfo.Status = false;

            newsletterRepository.Save();

            var usersList = System.Web.Security.Membership.GetAllUsers();
            var configurationRepository = new ConfiguratieRepository();
            var configurationInfo = configurationRepository.GetById(1);

            var emailProvider = new Email.Email();

            var attachments = new Collection<FileInfo>();

            foreach (var attachment in newsletterAttachments)
            {
                string newsletterFolderPath = FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Newsletter);
                string newsletterFullFolderPath = newsletterFolderPath + "/" +
                                                  attachment.newsLetterId.ToString(CultureInfo.InvariantCulture);

                string fullFilePath = newsletterFullFolderPath + "/" + attachment.fileName;

                if(!System.IO.File.Exists(fullFilePath)){throw new FileNotFoundException(string.Format("The file: {0} doesn't exist", fullFilePath));}
               
                attachments.Add(
                    new FileInfo(fullFilePath));
            }

            foreach (var user in usersList.Cast<MembershipUser>())
            {
                emailProvider.SendEmail(user.Email, newsletterInfo.Subject, newsletterInfo.Body, false, attachments,
                    configurationInfo);
                Thread.Sleep(500);
            }

            newsletterInfo.Status = true;

            newsletterRepository.Save();
        }
    }
}