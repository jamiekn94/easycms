﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Models.Admin.Users;
using Common.Models.Admin.ViewModel;
using Common.Providers.Users;
using Common.Repositories;

namespace Common.Providers.Roles
{
    public static class RoleProvider
    {
        private static System.Web.Security.RoleProvider GetRoleProvider()
        {
            var roleProvider = System.Web.Security.Roles.Provider;
            return roleProvider;
        }

        public static Guid GetRoleIdByName(string roleName)
        {
            var db = new DataClassesDataContext();
            return db.Roles.Where(x => x.RoleName == roleName).Select(x => x.RoleId).First();
        }

        public static string GetRoleNameById(Guid roleId)
        {
            var db = new DataClassesDataContext();
            return db.Roles.Where(x => x.RoleId == roleId).Select(x => x.RoleName).First();
        }

        public static string GetUserRoleByUsername(string username)
        {
            var db = new DataClassesDataContext();
            return (from users in db.Users
                join userRoles in db.UsersInRoles on users.UserId equals userRoles.UserId
                join roles in db.Roles on userRoles.RoleId equals roles.RoleId
                where users.UserName == username
                select roles.RoleName).First();

        }

        public static IEnumerable<UserModelInfo> GetUsersByRoleId(string roleName)
        {
            var usersinRole = GetRoleProvider().GetUsersInRole(roleName);
            var usersList = new List<UserModelInfo>();

            foreach (var roleUser in usersinRole)
            {
                usersList.Add(UserProvider.GetUserInfo(roleUser));
            }

            return usersList;
        }

        public static void AddUserToRole(string username, string roleName)
        {
            System.Web.Security.Roles.AddUserToRole(username, roleName);
        }

        public static string[] GetAllRoles()
        {
            return GetRoleProvider().GetAllRoles();
        }

        public static void AddRole(string roleName, List<Rights> rights)
        {
            var rightRepository = new GroepsRechtRepository();

            GetRoleProvider().CreateRole(roleName);

            var roleId = GetRoleIdByName(roleName);

            var listRights = rights.Select(right => new GroepsRecht
            {
                RoleId = roleId,
                RightID = right.RightId
            }).ToList();

            rightRepository.AddList(listRights);

            rightRepository.Save();
        }

        public static Guid GetRoleId()
        {
            var db = new DataClassesDataContext();
            var userGuid = UserProvider.GetUserId();
            return db.UsersInRoles.First(x => x.UserId == userGuid).RoleId;
        }

        public static Guid GetRoleId(Guid userId)
        {
            var db = new DataClassesDataContext();
            return db.UsersInRoles.First(x => x.UserId == userId).RoleId;
        }

        public static bool HasRight(int rightId, Guid roleId)
        {
            var roleRightsRepository = new GroepsRechtRepository();
            return roleRightsRepository.HasPemission(roleId, rightId);
        }
    }
}