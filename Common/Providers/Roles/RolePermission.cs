﻿using System.Web;
using System.Web.Mvc;
using Common.Providers.Users;
using Common.Repositories;

namespace Common.Providers.Roles
{
    public class RolePermission : ActionFilterAttribute
    {
        public enum PermissionName
        {
            Blog,
            Configuration,
            EmailTemplate,
            FotoAlbum,
            User,
            Right,
            Links,
            Navigation,
            News,
            Page,
            Ticket,
            Log,
            Nieuwsbrief,
            Gastenboek,
            Bestanden,
            BannerSlider,
            Agenda,
            Plugins,
            Widgets,
            Thema
        }

        public PermissionName permissionName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult(string.Format("/Inloggen?returnUrl=" + filterContext.RequestContext.HttpContext.Request.Url.LocalPath), false);
                return;
            }
            var rightId = 0;

            var rightRepository = new RightRepository();
            var rightPermissionRepository = new GroepsRechtRepository();

            var roleName = RoleProvider.GetUserRoleByUsername(UserProvider.GetMembershipUserInfo().UserName);
            var roleId = RoleProvider.GetRoleIdByName(roleName);

            switch (permissionName)
            {
                case PermissionName.Blog:
                    {
                        rightId = rightRepository.GetByName("Blog");
                        break;
                    }
                case PermissionName.Configuration:
                    {
                        rightId = rightRepository.GetByName("Configuratie");
                        break;
                    }
                case PermissionName.FotoAlbum:
                    {
                        rightId = rightRepository.GetByName("Foto albums");
                        break;
                    }
                case PermissionName.User:
                    {
                        rightId = rightRepository.GetByName("Gebruikers");
                        break;
                    }
                case PermissionName.Right:
                    {
                        rightId = rightRepository.GetByName("Rechten");
                        break;
                    }
                case PermissionName.Links:
                    {
                        rightId = rightRepository.GetByName("Links");
                        break;
                    }
                case PermissionName.Navigation:
                    {
                        rightId = rightRepository.GetByName("Navigatie");
                        break;
                    }
                case PermissionName.News:
                    {
                        rightId = rightRepository.GetByName("Nieuws");
                        break;
                    }
                case PermissionName.Page:
                    {
                        rightId = rightRepository.GetByName("Pagina's");
                        break;
                    }
                case PermissionName.Ticket:
                    {
                        rightId = rightRepository.GetByName("Tickets");
                        break;
                    }
                case PermissionName.Log:
                    {
                        rightId = rightRepository.GetByName("Logs");
                        break;
                    }
                case PermissionName.EmailTemplate:
                    {
                        rightId = rightRepository.GetByName("Email templates");
                        break;
                    }
                case PermissionName.Bestanden:
                    {
                        rightId = rightRepository.GetByName("Bestanden");
                        break;
                    }
                case PermissionName.Nieuwsbrief:
                    {
                        rightId = rightRepository.GetByName("Nieuwsbrief");
                        break;
                    }
                case PermissionName.Gastenboek:
                    {
                        rightId = rightRepository.GetByName("Gastenboek");
                        break;
                    }
                case PermissionName.BannerSlider:
                    {
                        rightId = rightRepository.GetByName("BannerSlider");
                        break;
                    }
                case PermissionName.Agenda:
                    {
                        rightId = rightRepository.GetByName("Agenda");
                        break;
                    }
                case PermissionName.Plugins:
                    {
                        rightId = rightRepository.GetByName("Plugins");
                        break;
                    }
                case PermissionName.Widgets:
                    {
                        rightId = rightRepository.GetByName("Widgets");
                        break;
                    }
                case PermissionName.Thema:
                    {
                        rightId = rightRepository.GetByName("Thema's");
                        break;
                    }
            }

            var hasPermission = rightPermissionRepository.HasPemission(roleId, rightId);

            if (!hasPermission)
            {
                const string errorMessage = "U heeft geen recht om deze pagina te zien.";
                filterContext.Result =
                    new RedirectResult(string.Format("/Admin/Dashboard?Error={0}", HttpUtility.UrlEncode(errorMessage)));
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
