﻿using System.Web.Mvc;

namespace Common.Providers.Messages
{
    /// <summary>
    ///     Static class that sets an error or success message
    /// </summary>
    public static class Message
    {
        public static void SetError(this Controller controller, string errorMessage)
        {
            controller.TempData["Error"] = errorMessage;
        }

        public static void SetError(this ControllerBase controller, string errorMessage)
        {
            controller.TempData["Error"] = errorMessage;
        }

        public static void SetSuccess(this Controller controller, string succesMessage)
        {
            controller.TempData["Success"] = succesMessage;
        }

        public static void SetSuccess(this ControllerBase controller, string errorMessage)
        {
            controller.TempData["Success"] = errorMessage;
        }
    }
}