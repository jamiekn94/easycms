﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Providers.Routes
{
    public class RouteProvider
    {
        public static string GetAction()
        {
            return HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
        }

        public static string GetController()
        {
            return HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
        }

        public static string GetArea()
        {
            var area =
                HttpContext.Current.Request.RequestContext.RouteData.Values.FirstOrDefault(x => x.Key == "area");

            return (string) area.Value ?? string.Empty;
        }
    }
}
