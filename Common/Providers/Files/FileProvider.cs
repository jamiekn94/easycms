using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using Common.Repositories;
using nQuant;

namespace Common.Providers.Files
{
    public static class FileProvider
    {
        /// <summary>
        /// Holds the folder names of all the available image folders
        /// </summary>
        private static Dictionary<FilePaths, string> FolderNames { get; set; }

        /// <summary>
        /// Fill the available image folder names
        /// </summary>
        static FileProvider()
        {
                FolderNames = new Dictionary<FilePaths, string>
                {
                    {FilePaths.Blogs, "Blog"},
                    {FilePaths.Banners, "Banners"},
                    {FilePaths.News, "News"},
                    {FilePaths.Newsletter, "Newsletter"},
                    {FilePaths.PhotoAlbums, "PhotoAlbum"},
                    {FilePaths.ProfilePhotos, "ProfilePhotos"},
                    {FilePaths.Files, string.Empty}
                };
        }

        public static bool IsImage(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                var arrayImageFileExtensions = new Collection<string>
                {
                    ".png",
                    ".jpeg",
                    ".jpg",
                    ".bmp",
                    ".gif",
                    ".tiff",
                    ".svg"
                };

                string fileExtension = Path.GetExtension(filePath).ToLower();

                return arrayImageFileExtensions.Contains(fileExtension);
            }
            return false;
        }

        /// <summary>
        ///  Get the full path of the given image path
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="includeServerPath"></param>
        /// <returns></returns>
        public static string GetImagePath(Common.Providers.Files.FilePaths filePath, bool includeServerPath = true)
        {
            string serverPath;
            if (filePath != FilePaths.Files)
            {
               serverPath = includeServerPath ? HttpContext.Current.Server.MapPath("~/Content/Shared") : "/Content/Shared";
            }
            else
            {
                serverPath = includeServerPath ? HttpContext.Current.Server.MapPath("~/Files") : "/Files";
            }

            string imageFolderName = FolderNames.First(x => x.Key == filePath).Value;

            return string.Format("{0}/{1}/", serverPath, imageFolderName);
        }

        /// <summary>
        /// Delete a file
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="filename"></param>
        /// <param name="mustExist"></param>
        public static void DeleteFile(FilePaths filePath, string folderName, string filename, bool mustExist)
        {
            string imageFolderPath = GetImagePath(filePath);

            string fullImageFilePath = imageFolderPath + "/" + folderName + "/" + filename;

            if (!System.IO.File.Exists(fullImageFilePath) && mustExist == false) { throw new FileNotFoundException(string.Format("De afbeelding: {0} bestaat niet.", fullImageFilePath)); }

            System.IO.File.Delete(fullImageFilePath);
        }

        public static void DeleteDirectory(FilePaths filePath, string folderName, bool mustExist)
        {
            string directoryPath = GetImagePath(filePath);

            string fullDirectoryPath = directoryPath + "/" + folderName;

            if (!Directory.Exists(fullDirectoryPath) && mustExist) { throw new DirectoryNotFoundException(string.Format("The directory: {0} doesn't exist.", fullDirectoryPath)); }

            Directory.Delete(fullDirectoryPath, true);
        }

        /// <summary>
        /// Add a new image folder directory
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="folderName"></param>
        /// <param name="canExist"></param>
        public static void AddFolderDirectory(FilePaths filePath, string folderName, bool canExist)
        {
            string imageFolderPath = GetImagePath(filePath);

            string fullFolderPath = imageFolderPath + "/" + folderName;

            if (Directory.Exists(fullFolderPath) && canExist == false) { throw new DuplicateNameException("This directory already exists: " + fullFolderPath); }

            Directory.CreateDirectory(fullFolderPath);
        }

        /// <summary>
        /// Add a file to the disk
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <param name="filePath"></param>
        /// <param name="folderName"></param>
        public static void AddFile(HttpPostedFileBase uploadedFile, FilePaths filePath, string folderName)
        {
            string folderPath = GetImagePath(filePath);

            string fullFolderPath = folderPath + "/" + folderName;

            if(!Directory.Exists(fullFolderPath)){throw new DirectoryNotFoundException(string.Format("The directory: {0} doesn't exist.", fullFolderPath));}

            string pathToFile = fullFolderPath + "/" + uploadedFile.FileName;

            uploadedFile.SaveAs(pathToFile);
        }

        public static string GetPrefferedImageExtension()
        {
            return ".jpg";
        }

        public static string GetNewImageName(HttpPostedFileBase uploadedFile)
        {
            string defaultExtension = GetPrefferedImageExtension();

            string oldFileName = uploadedFile.FileName;

            string oldExtension = Path.GetExtension(oldFileName);

            string oldFileNameWithoutExtension = oldFileName.Replace(oldExtension, "");

            return string.Format("{0}{1}", oldFileNameWithoutExtension, defaultExtension);
        }

        /// <summary>
        ///  Add optimized image to the target path and return the image name inclusive its extension name
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <param name="filePath"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public static string AddAndReturnImageName(HttpPostedFileBase uploadedFile, FilePaths filePath, string folderName)
        {
            var originalbitmap = Image.FromStream(uploadedFile.InputStream);

            string newImageName  = GetNewImageName(uploadedFile);

            string imageFolderPath = GetImagePath(filePath);

            string fullImageFolderPath = Path.Combine(imageFolderPath, folderName);

            if (!Directory.Exists(fullImageFolderPath)) { throw new DirectoryNotFoundException(string.Format("The directory: {0} doesn't exist.", fullImageFolderPath)); }

            string newFullFileNameWithPath = Path.Combine(fullImageFolderPath, newImageName);

            var configurationRepository = new ConfiguratieRepository();
            var configurationInfo = configurationRepository.GetById(1);

            // Optimize images
            if (configurationInfo.OptimizeImages)
            {
                var quantizer = new WuQuantizer();

                using (var bitmap = new Bitmap(originalbitmap))
                {
                    // Default - 10 - 70
                    using (var quantized = quantizer.QuantizeImage(bitmap))
                    {
                        quantized.Save(newFullFileNameWithPath, ImageFormat.Jpeg);
                    }
                }
            }
            // Don't optimize images
            else
            {
                originalbitmap.Save(newFullFileNameWithPath);
            }

            return newImageName;
        }
    }
}
