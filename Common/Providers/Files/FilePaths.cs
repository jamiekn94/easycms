namespace Common.Providers.Files
{
    public enum FilePaths
    {
        Blogs,
        Banners,
        News,
        Newsletter,
        PhotoAlbums,
        ProfilePhotos,
        Files
    }
}