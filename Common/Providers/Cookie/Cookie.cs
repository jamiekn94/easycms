﻿using System;
using System.Web;

namespace Common.Providers.Cookie
{
    public class Cookie
    {
        public static void SetCookie(string name, string value, DateTime endDate)
        {
            if (HttpContext.Current == null) { throw new Exception("No HttpContext found!"); }

            var cookie = new HttpCookie(name) {Expires = endDate, Value = value, HttpOnly = true};
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void UpdateCookie(string name, string value)
        {
            if (HttpContext.Current == null) { throw new Exception("No HttpContext found!"); }

            var httpCookie = HttpContext.Current.Response.Cookies[name];
            if (httpCookie != null)
                httpCookie.Value = value;
        }

        public static void RemoveCookie(string name)
        {
            if (HttpContext.Current == null) { throw new Exception("No HttpContext found!"); }

            var cookie = new HttpCookie(name)
            {
                Value = null,
                Expires = DateTime.Now.AddYears(-1) // or any other time in the past
            };

            HttpContext.Current.Response.Cookies.Set(cookie);
        }

        public static string GetCookie(string name)
        {
            if (HttpContext.Current == null) { throw new Exception("No HttpContext found!"); }

            string strCookie = null;

            if (HttpContext.Current.Request.Cookies[name] != null)
            {
                strCookie = HttpContext.Current.Request.Cookies[name].Value;
            }
            return strCookie;
        }
    }
}