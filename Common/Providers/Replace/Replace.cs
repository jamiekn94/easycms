﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Providers.Replace
{
    public static class Replace
    {
        public static string ReplaceText(Dictionary<string, string> replacementValues, string text)
        {
            return replacementValues.Aggregate(text, (current, item) => current.Replace(item.Key, item.Value));
        }
    }
}