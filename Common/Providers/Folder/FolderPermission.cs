﻿using System;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

namespace Common.Providers.Folder
{
    public class FolderPermission : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var fullPath = context.Request.Url.PathAndQuery;
            var fullFolder = fullPath.Substring(0, fullPath.LastIndexOf("/", StringComparison.Ordinal));
            var fileName = fullPath.Substring(fullPath.LastIndexOf("/", StringComparison.Ordinal) + 1,
                fullPath.Length - fullPath.LastIndexOf("/", StringComparison.Ordinal) - 1);

            var folderRepository = new FolderRepository();
            var folderInfo = folderRepository.GetFolderByPath(fullFolder);


            // Check if the folder exists
            if (folderInfo != null)
            {
                var fileRepository = new FilesRepository();

                // Check if the file exists
                if (fileRepository.FileExists(folderInfo.FolderId, fileName))
                {
                    var folderPermissions = new FolderPermissionRepository();
                    var listFolderPermissions = folderPermissions.GetByFolderId(folderInfo.FolderId);

                    var nameRole =
                        RoleProvider.GetUserRoleByUsername(UserProvider.GetUsername());
                    var roleGuid = RoleProvider.GetRoleIdByName(nameRole);

                    if (listFolderPermissions.Any(x => x.IsPublic || (Guid)x.RoleId == roleGuid))
                    {
                        context.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                        context.Response.WriteFile(fullFolder + "/" + fileName, true);
                    }
                    else
                    {
                        context.Response.Write("<html>\r\n");
                        context.Response.Write("<head><title>EasyCMS - File downloader</title></head>\r\n");
                        context.Response.Write("<body>\r\n");
                        context.Response.Write("U heeft geen permissie om dit bestand te downloaden!");
                        context.Response.Write("</body>\r\n");
                        context.Response.Write("</html>");
                    }
                }
            }

            context.Response.Write("<html>\r\n");
            context.Response.Write("<head><title>EasyCMS - File downloader</title></head>\r\n");
            context.Response.Write("<body>\r\n");
            context.Response.Write("Bestand niet gevonden!");
            context.Response.Write("</body>\r\n");
            context.Response.Write("</html>");
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            throw new NotImplementedException();
        }
    }
}
