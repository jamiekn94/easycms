﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Providers.Content
{
    class ContentError : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            // Get the file name
            string fileName = GetFileName(context);
            
            // Get full path to file
            var fullPathFile = GetFullFilePath(context, fileName);

            // Get file extension
            string fileExtension = Path.GetExtension(fileName);

            // Handle response
            HandleResponse(context, fullPathFile, fileExtension);
        }

        private string GetFullFilePath(HttpContext context, string fileName)
        {
          // Get the location of the file
            string pathContentMap = context.Request.RequestContext.HttpContext.Server.MapPath("");

            // Get the full absolute path of the file
            string fullPathFile = pathContentMap + "/" + fileName;
            return fullPathFile;
        }

        private string GetFileName(HttpContext context)
        {
            // Get index of the last slash
            int indexLastSlash = context.Request.Url.PathAndQuery.LastIndexOf("/", StringComparison.Ordinal) + 1;

            // Get filename
            string fileName = context.Request.Url.PathAndQuery.Substring(indexLastSlash,
                context.Request.Url.PathAndQuery.Length - indexLastSlash);
            return fileName;
        }

        private bool FileExists(string fullPathFile)
        {
            return System.IO.File.Exists(fullPathFile);
        }

        private void HandleResponse(HttpContext context, string fullPathFile, string extension)
        {
                switch (extension)
                {
                    // Handle image
                    case ".png":
                    case ".jpeg":
                    case ".jpg":
                    case ".bmp":
                    case ".gif":
                        {
                            context.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                            if (!FileExists(fullPathFile))
                            {
                                context.Response.TransmitFile("/Content/img/imageNotFound.jpg");
                            }

                            break;
                        }
                    case ".js":
                        {
                            context.Response.ContentType = "application/javascript";
                            
                            //if (!FileExists(fullPathFile))
                            //{
                            //    context.Response.StatusCode = 200;
                            //    context.Response.Write("");
                            //}
                            
                            break;
                        }
                    default:
                        {
                            context.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                            break;
                        }
                }
        }

        public bool IsReusable { get; private set; }
    }
}
