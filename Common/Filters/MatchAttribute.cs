﻿using System.ComponentModel.DataAnnotations;

namespace Common.Filters
{
    public class MatchAttribute : ValidationAttribute
    {
        public string fieldName { get; set; }
        public string otherFieldName { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var propertyFieldInfo = validationContext.ObjectType.GetProperty(fieldName);
            var propertyOtherFieldInfo = validationContext.ObjectType.GetProperty(otherFieldName);

            if (propertyFieldInfo == null || propertyOtherFieldInfo == null)
            {
                return new ValidationResult("Een van deze velden bestaat niet in het model");
            }
            var propertyFieldInfoValue = propertyFieldInfo.GetValue(validationContext.ObjectInstance).ToString();
            var propertyOtherFieldInfoValue =
                propertyOtherFieldInfo.GetValue(validationContext.ObjectInstance).ToString();

            if (propertyFieldInfoValue == propertyOtherFieldInfoValue)
            {
                return ValidationResult.Success;
            }
            return
                new ValidationResult(string.Format("Het veld {0} en {1} moeten dezelfde waarde hebben.",
                    propertyFieldInfo.Name, propertyOtherFieldInfo.Name));
        }
    }
}