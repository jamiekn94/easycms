﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class EmailAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit is geen geldig email-adres",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", @"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, @"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}");
            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit is geen geldig email-adres.";
            }
            return isMatch;
        }
    }
}