﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class StreetAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit is geen geldige straatnaam, een straatnaam mag geen cijfers bevatten.",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", "^[^0-9]{1,}$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, "^[^0-9]{1,}$");

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit is geen geldige straatnaam, een straatnaam mag geen cijfers bevatten.";
            }

            return isMatch;
        }
    }
}