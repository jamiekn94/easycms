﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class AlphaNumericAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit veld mag alleen cijfers en letters bevatten.",
                ValidationType = "letters"
            };
            rule.ValidationParameters.Add("pattern", @"[0-9a-zA-Z]+$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, "[0-9a-zA-Z]+$");

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit veld mag alleen cijfers en letters bevatten.";
            }

            return isMatch;
        }
    }
}