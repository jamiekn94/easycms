﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class LettersAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit veld mag alleen letters bevatten",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", "^[a-zA-Z]+$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, "^[a-zA-Z]+$");

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit veld mag alleen letters bevatten";
            }

            return isMatch;
        }
    }
}