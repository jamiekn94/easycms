﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class PostcodeAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit veld is geen geldige postcode, voorbeeld goede postcode: 6921GP.",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", @"^\d{4,4}\s{0,1}\w{0,2}$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, @"^\d{4,4}\s{0,1}\w{0,2}$");

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit veld is geen geldige postcode, voorbeeld goede postcode: 6921GP.";
            }

            return isMatch;
        }
    }
}