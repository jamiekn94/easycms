﻿using System.ComponentModel.DataAnnotations;

namespace Common.Filters
{
    public class PasswordAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var strValue = value == null ? "" : value.ToString();

            var propertyVoornaam = validationContext.ObjectType.GetProperty("Voornaam");
            var propertyAchternaam = validationContext.ObjectType.GetProperty("Achternaam");
            var propertyGebruikersnaam = validationContext.ObjectType.GetProperty("Gebruikersnaam");


            if (propertyVoornaam == null || propertyAchternaam == null || propertyGebruikersnaam == null)
            {
                return new ValidationResult("Een van deze velden bestaat niet in het model");
            }
            else
            {
                var voornaam = propertyVoornaam.GetValue(validationContext.ObjectInstance) ?? "";
                var achternaam = propertyAchternaam.GetValue(validationContext.ObjectInstance) ?? "";
                var gebruikersnaam = propertyGebruikersnaam.GetValue(validationContext.ObjectInstance) ?? "";

                if (strValue.Contains(voornaam.ToString().ToLower()) ||
              strValue.Contains(achternaam.ToString().ToLower()) ||
              strValue.Contains(gebruikersnaam.ToString().ToLower()))
                {
                    return
                        new ValidationResult(
                            "Uw wachtwoord mag niet uw voornaam, achternaam of gebruikersnaam bevatten.");
                }
            }
                
          
            return ValidationResult.Success;
        }
    }
}