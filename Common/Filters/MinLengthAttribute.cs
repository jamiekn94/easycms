﻿using System.ComponentModel.DataAnnotations;

namespace Common.Filters
{
    public class LengthAttribute : ValidationAttribute
    {
        public int MinimumLength { get; set; }
        public int MaximumLength { get; set; }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();

            // Length is too long or too short
            if (MinimumLength > strValue.Length || strValue.Length > MaximumLength)
            {
                ErrorMessage = string.Format("Dit veld moet minimaal {0} en maximaal {1} tekens bevatten.",
                    MinimumLength,
                    MaximumLength);
                return false;
            }
            return true;
        }
    }
}