﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Common.Filters
{
    public class FileSizeAttribute : ValidationAttribute, IClientValidatable
    {

        public FileSizeAttribute(int maxMb)
            : base(() => string.Format("Het bestand mag niet groter zijn dan {0} Mb.", maxMb))
        {
            MaxMb = maxMb;
        }

        public int MaxMb { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            // Get max bytes
            double maxBytes = MaxMb * 1048576;

            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Het bestand mag niet groter zijn dan: " + MaxMb + " Mb",
                ValidationType = "maxfile"
            };
            rule.ValidationParameters.Add("sizebytes", maxBytes);

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                // Get value type
                var valueType = value.GetType();

                if (valueType.IsArray)
                {
                    var httpPostedFileBases = value as IEnumerable<HttpPostedFileBase>;

                    if (httpPostedFileBases != null)
                    {
                        return httpPostedFileBases.All(ValidateFile);
                    }
                }
                else
                {
                    if (ValidateFile(value as HttpPostedFileBase)) return true;
                }
            }

            return false;
        }

        private bool ValidateFile(HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                // Get max bytes
                double maxBytes = MaxMb * 1048576;

                if (maxBytes >= postedFile.ContentLength)
                {
                    return true;
                }
            }
            else
            {
                ErrorMessage = "Een bestand is verplicht";
            }
            return false;
        }
    }
}