﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class HouseNumberAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit veld is geen geldig huisnummer.",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", @"^\d{1,4}[A-Za-z]{0,1}$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, @"^\d{1,4}[A-Za-z]{0,1}$");

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit veld is geen geldig huisnummer.";
            }

            return isMatch;
        }
    }
}