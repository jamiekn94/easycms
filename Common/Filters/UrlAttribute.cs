﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.Filters
{
    public class UrlAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var strUrl = value == null ? "" : value.ToString();
            Uri url;
            var isMatch = Uri.TryCreate(strUrl, UriKind.Absolute, out url);

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit is geen geldige link, voorbeeld goede link: http://google.nl";
            }

            return isMatch;
        }
    }
}