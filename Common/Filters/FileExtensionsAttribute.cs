﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Common.Filters
{
    public class FileExtensionsAttribute : ValidationAttribute, IClientValidatable
    {
        public string FileExtensions { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "De extensie van het bestand is niet toegestaan.",
                ValidationType = "fileextension"
            };
            rule.ValidationParameters.Add("allowedextensions", FileExtensions);

            yield return rule;
        }
        private bool IsValidFile(HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                return FileExtensions.Split(',').Any(x => x.Contains(Path.GetExtension(postedFile.FileName)));
            }
            return false;
        }

        public override bool IsValid(object value)
        {
            var isMatch = true;

            ErrorMessage = "Dit bestand heeft geen geldige extensie";

            if (value != null && value.GetType() == typeof(HttpPostedFileBase))
            {
                var postedFile = (HttpPostedFileBase)value;
                isMatch = IsValidFile(postedFile);

                ErrorMessage = !isMatch ? string.Format("Het bestand: {0} heeft een extensie die niet is toegestaan", postedFile.FileName) : null;
            }
            else if (value != null && value.GetType() == typeof(HttpPostedFileBase[]))
            {
                ((IEnumerable<HttpPostedFileBase>)value).FirstOrDefault(x =>
                 {
                     if (x != null && !IsValidFile(x))
                     {
                         ErrorMessage = string.Format("Het bestand: {0} heeft een extensie die niet is toegestaan", x.FileName);
                         isMatch = false;
                         return true;
                     }

                     return false;
                 });
            }


            return isMatch;
        }
    }
}