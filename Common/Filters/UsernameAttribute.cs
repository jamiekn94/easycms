﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class UsernameAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage =
                    "Dit is geen geldige gebruikersnaam, een gebruikersnaam mag alleen de volgende tekens bevatten: letters, cijfers, punten, streepjes.",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", @"^[a-zA-Z0-9_.-]*$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, "^[a-zA-Z0-9_.-]*$");

            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage =
                    "Dit is geen geldige gebruikersnaam, een gebruikersnaam mag alleen de volgende tekens bevatten: letters, cijfers, punten, streepjes.";
            }

            return isMatch;
        }
    }
}