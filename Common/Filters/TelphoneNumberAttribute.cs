﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Common.Filters
{
    public class TelphoneNumberAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = "Dit is geen geldig telefoonnummer, voorbeeld goed telefoonnummer: 0316-280250.",
                ValidationType = "customregex"
            };
            rule.ValidationParameters.Add("pattern", @"^\d{2,4}-? ?\d{5,9}$");

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var strValue = value == null ? "" : value.ToString();
            var isMatch = Regex.IsMatch(strValue, @"^\d{2,4}-? ?\d{5,9}$");


            if (!isMatch && ErrorMessage == null)
            {
                ErrorMessage = "Dit is geen geldig telefoonnummer, voorbeeld goed telefoonnummer: 0316-280250";
            }

            return isMatch;
        }
    }
}