namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ticket")]
    public partial class Ticket
    {
        public Ticket()
        {
            TicketReactie = new HashSet<TicketReactie>();
        }

        public int TicketID { get; set; }

        [Required]
        [StringLength(250)]
        public string Onderwerp { get; set; }

        public byte Status { get; set; }

        public DateTime DatumOpen { get; set; }

        public DateTime? DatumGesloten { get; set; }

        public Guid TicketGebruikersId { get; set; }

        public virtual Users Users { get; set; }

        public virtual ICollection<TicketReactie> TicketReactie { get; set; }
    }
}
