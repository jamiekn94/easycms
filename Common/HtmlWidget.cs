namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HtmlWidget")]
    public partial class HtmlWidget
    {
        public int HtmlWidgetId { get; set; }

        [StringLength(150)]
        public string Title { get; set; }

        public string Text { get; set; }

        public int ActiveWidgetId { get; set; }

        public virtual ActiveWidget ActiveWidget { get; set; }
    }
}
