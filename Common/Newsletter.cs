namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Newsletter")]
    public partial class Newsletter
    {
        public Newsletter()
        {
            NewsletterAttachment = new HashSet<NewsletterAttachment>();
        }

        public int NewsletterId { get; set; }

        [Required]
        [StringLength(75)]
        public string Subject { get; set; }

        [Required]
        [StringLength(5000)]
        public string Body { get; set; }

        public DateTime? ScheduledTime { get; set; }

        public bool Status { get; set; }

        public virtual ICollection<NewsletterAttachment> NewsletterAttachment { get; set; }
    }
}
