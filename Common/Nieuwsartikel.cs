namespace Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Nieuwsartikel")]
    public partial class Nieuwsartikel
    {
        public Nieuwsartikel()
        {
            Link = new HashSet<Link>();
            Navigatie = new HashSet<Navigatie>();
            NieuwsReactie = new HashSet<NieuwsReactie>();
            SubNavigatie = new HashSet<SubNavigatie>();
        }

        [Key]
        public int ArtikelID { get; set; }

        [Required]
        [StringLength(250)]
        public string Titel { get; set; }

        [Required]
        [StringLength(5000)]
        public string Informatie { get; set; }

        public DateTime Datum { get; set; }

        public bool ReactiesToegestaan { get; set; }

        public Guid AuteurId { get; set; }

        public bool Status { get; set; }

        [Required]
        [StringLength(250)]
        public string Keywords { get; set; }

        [StringLength(255)]
        public string Image { get; set; }

        [Required]
        [StringLength(500)]
        public string Intro { get; set; }

        public DateTime LastEdited { get; set; }

        public DateTime ImageEdited { get; set; }

        public virtual ICollection<Link> Link { get; set; }

        public virtual ICollection<Navigatie> Navigatie { get; set; }

        public virtual Users Users { get; set; }

        public virtual ICollection<NieuwsReactie> NieuwsReactie { get; set; }

        public virtual ICollection<SubNavigatie> SubNavigatie { get; set; }
    }
}
