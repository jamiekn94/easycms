﻿#region

using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;
using Common.Models.Admin.ViewModel;
using RoleProvider = Common.Providers.Roles.RoleProvider;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                MembershipUser userInfo = System.Web.Security.Membership.GetUser(User.Identity.Name);
                if (userInfo != null)
                {
                    if (!userInfo.IsLockedOut)
                    {
                        if (
                            !RoleProvider.GetUserRoleByUsername(userInfo.UserName)
                                .Equals("Leden", StringComparison.CurrentCultureIgnoreCase))
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                        else
                        {
                            ModelState.AddModelError("membershipError", "U bent niet gemachtigd om in te loggen.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("membershipError",
                            "Uw account is geblokkeerd vanwege te vaak een verkeerd wachtwoord te gebruiken.");
                    }
                }
                else
                {
                    ModelState.AddModelError("membershipError", "Geen account gevonden met deze gebruikersnaam.");
                }
            }
            return View();
        }

        [System.Web.Mvc.AllowAnonymous]
        [System.Web.Mvc.HttpPost]
        public ActionResult Index(LoginModel loginModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                MembershipUser userInfo = System.Web.Security.Membership.GetUser(loginModel.Username);
                if (userInfo != null)
                {
                    if (!userInfo.IsLockedOut)
                    {
                        if (System.Web.Security.Membership.ValidateUser(loginModel.Username, loginModel.Password))
                        {
                            if (!RoleProvider.GetUserRoleByUsername(userInfo.UserName)
                                .Equals("Leden", StringComparison.CurrentCultureIgnoreCase))
                            {
                                FormsAuthentication.SetAuthCookie(loginModel.Username, true);

                                if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                                {
                                    return Redirect(Server.UrlDecode(returnUrl));
                                }
                                return RedirectToAction("Index", "Dashboard");
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("membershipError", "Verkeerd wachtwoord voor deze gebruiker.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("membershipError",
                            "Uw account is geblokkeerd vanwege te vaak een verkeerd wachtwoord te gebruiken.");
                    }
                }
                else
                {
                    ModelState.AddModelError("membershipError", "Geen account gevonden met deze gebruikersnaam.");
                }
            }
            return View(loginModel);
        }
    }
}