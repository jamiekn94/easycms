﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.Tickets;
using Common.Models.Admin.Users;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;
using EmailTemplate = Common.Providers.Email.EmailTemplate;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class TicketsController : ApplicationController
    {
        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("_Open-tickets-weergeven")]
        public ActionResult _OpenTicketsWeergeven()
        {
            return View(GetTickets(1));
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("Open-tickets-weergeven")]
        public ActionResult OpenTicketsWeergeven()
        {
            return View();
        }

        [ActionName("_Gesloten-tickets-weergeven")]
        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult _GeslotenTicketsWeergeven()
        {
            return View(GetTickets(2));
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("Gesloten-tickets-weergeven")]
        public ActionResult GeslotenTicketsWeergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("_Behandelde-tickets-weergeven")]
        public ActionResult _BehandeldeTicketsWeergeven()
        {
            return View(GetTickets(1));
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("Behandelde-tickets-weergeven")]
        public ActionResult BehandeldeTicketsWeergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("_Alle-tickets-weergeven")]
        public ActionResult _AlleTicketsWeergeven()
        {
            return View(GetTickets(3));
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [ActionName("Alle-tickets-weergeven")]
        public ActionResult AlleTicketsWeergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult Toevoegen()
        {
            IEnumerable<UserModelInfo> usersList = UserProvider.GetAllUsers();
            var createTicketModel = new CreateTicketModel();

            foreach (UserModelInfo user in usersList)
            {
                createTicketModel.Users.Add(new SelectListItem
                {
                    Text = user.Firstname + " " + user.Lastname,
                    Value = user.Username
                });
            }
            return View(createTicketModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [HttpPost]
        public ActionResult Toevoegen(CreateTicketModel createTicketModel)
        {
            if (ModelState.IsValid)
            {
                // Get provider user key
                Guid providerUserKey = UserProvider.GetProviderUserKey(createTicketModel.Username);

                // Initialize ticket repository
                var ticketRepository = new TicketRepository();

                // Initialize ticket reaction repository
                var ticketCommentRepository = new TicketReactieRepository();

                // Add ticket
                ticketRepository.Add(new Ticket
                {
                    DatumOpen = DateTime.Now,
                    Onderwerp = createTicketModel.Subject,
                    Status = 0,
                    TicketGebruikersId = providerUserKey
                });

                // Save ticket
                ticketRepository.Save();

                // Get ticket
                Ticket latestTicket = ticketRepository.GetLastTicket();

                // Add ticket reaction repository
                ticketCommentRepository.Add(new TicketReactie
                {
                    Datum = DateTime.Now,
                    GebruikersId = providerUserKey,
                    Reactie = createTicketModel.Message,
                    TicketID = latestTicket.TicketID
                });

                // Save ticket reaction
                ticketCommentRepository.Save();

                // Mail to user
                var emailTemplate = new EmailTemplate();
                emailTemplate.SendOpenTicket(latestTicket.TicketID, createTicketModel.Username);

                // Success message
                this.SetSuccess("De ticket is succesvol toegevoegd!");

                // Log
                LogProvider.LogAction(
                    string.Format("De ticket #{0} is toegevoegd", latestTicket.TicketID));

                // Redirect
                return RedirectToAction("Alle-tickets-weergeven");
            }
            return View(createTicketModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult ReactieToevoegen(TicketReplyModel ticketReplyModel)
        {
            if (ModelState.IsValid)
            {
                var ticketReactieRepository = new TicketReactieRepository();
                var ticketInfo = new TicketReactie
                {
                    GebruikersId = UserProvider.GetUserId(),
                    Datum = DateTime.Now,
                    Reactie = ticketReplyModel.Bericht,
                    TicketID = ticketReplyModel.TicketId
                };

                ticketReactieRepository.Add(ticketInfo);
                ticketReactieRepository.Save();

                // Mail to user
                var emailTemplate = new EmailTemplate();
                emailTemplate.SendReactionTicket(ticketReplyModel.TicketId, UserProvider.GetUsername());

                LogProvider.LogAction(
                    string.Format("Reactie toegevoegd aan de ticket #{0}", ticketInfo.TicketID));

                this.SetSuccess("Uw reactie is toegevoegd aan de ticket.");

                if (Request.UrlReferrer != null)
                {
                    return RedirectToAction(Request.UrlReferrer.AbsoluteUri);
                }
                return RedirectToAction("alle-tickets-weergeven");
            }

            BundleTicketModel bundleTicketModel = GetTicketPageInfo(ticketReplyModel.TicketId);
            bundleTicketModel.TicketReplyModel = ticketReplyModel;

            return View(bundleTicketModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult Actie(int ticketId, int actionId)
        {
            var ticketRepository = new TicketRepository();
            Ticket ticketInfo = ticketRepository.GetById(ticketId);

            // Sluiten
            if (actionId == 2)
            {
                ticketInfo.Status = 2;
                ticketRepository.Save();

                // Email ticket is gesloten
                var emailTemplate = new EmailTemplate();
                emailTemplate.SendClosedTicket(ticketId, UserProvider.GetUsername());

                this.SetSuccess("De ticket is succesvol gesloten!");
            }
            // Her-openen
            else
            {
                ticketInfo.Status = 0;
                ticketRepository.Save();

                // Email ticket is her-opened
                var emailTemplate = new EmailTemplate();
                emailTemplate.SendOpenTicket(ticketId, UserProvider.GetUsername());

                this.SetSuccess("De ticket is succesvol heropend!");
            }

            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.AbsolutePath);
            }
            return RedirectToAction("Alle-tickets-weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult _Info(int id)
        {
            return View(GetTicketPageInfo(id));
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult Info(int id)
        {
            return View(id);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        [HttpPost]
        public ActionResult Info(TicketReplyModel ticketReplyModel)
        {
            if (ModelState.IsValid)
            {
                var ticketReactionRepository = new TicketReactieRepository();
                ticketReactionRepository.Add(new TicketReactie
                {
                    Datum = DateTime.Now,
                    GebruikersId = UserProvider.GetUserId(),
                    Reactie = ticketReplyModel.Bericht,
                    TicketID = ticketReplyModel.TicketId
                });

                ticketReactionRepository.Save();

                this.SetSuccess("Uw reactie is succesvol toegevoegd.");

                // Redirect
                return Redirect(Request.Url.AbsolutePath);
            }

            // Return model
            BundleTicketModel ticketInfo = GetTicketPageInfo(ticketReplyModel.TicketId);
            ticketInfo.TicketReplyModel = ticketReplyModel;

            return View(ticketInfo);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Ticket)]
        public ActionResult Verwijder(int id)
        {
            var ticketReactionRepository = new TicketReactieRepository();
            var ticketRepository = new TicketRepository();

            Ticket ticketInfo = ticketRepository.GetById(id);

            if (ticketInfo != null)
            {
                // Send email
                var emailTemplate = new EmailTemplate();
                emailTemplate.SendDeletedTicket(id, UserProvider.GetUsername());

                IEnumerable<TicketReactie> ticketReactions = ticketReactionRepository.GetReactiesById(id);
                ticketReactionRepository.RemoveAll(ticketReactions);
                ticketReactionRepository.Save();

                ticketRepository.Remove(ticketInfo);
                ticketRepository.Save();

                // Log
                LogProvider.LogAction(
                    string.Format("De ticket #{0} is verwijderd", id));

                this.SetSuccess("De ticket is succesvol verwijderd!");
            }
            else
            {
                this.SetError("De ticket is niet verwijderd want hij bestaat niet");
            }


            return RedirectToAction("Alle-tickets-weergeven");
        }

        #region Private Methods

        private IEnumerable<TicketModel> GetTickets(int action)
        {
            var ticketRepository = new TicketRepository();
            List<Ticket> ticketInfoList;

            switch ((TicketType)action)
            {
                case TicketType.Open:
                case TicketType.Review:
                case TicketType.Closed:
                    {
                        ticketInfoList = ticketRepository.GetList().Where(x => x.Status == action).ToList();
                        break;
                    }
                default:
                    {
                        ticketInfoList = ticketRepository.GetList().ToList();
                        break;
                    }
            }

            return (from ticket in ticketInfoList
                let userInfo = UserProvider.GetUserInfo(ticket.TicketGebruikersId)
                select new TicketModel
                {
                    Onderwerp = ticket.Onderwerp, Status = ticket.Status, TicketID = ticket.TicketID, Voornaam = userInfo.Firstname, Achternaam = userInfo.Lastname, Datum = ticket.DatumOpen
                }).ToList();
        }

        private BundleTicketModel GetTicketPageInfo(int ticketId)
        {
            var ticketRepository = new TicketRepository();
            var ticketReactieRepository = new TicketReactieRepository();


            var bundleTicketModel = new BundleTicketModel
            {
                TicketReactieModel = new List<TicketReactiesModel>(),
                TicketModel = new TicketModel()
            };


            var ticketInfo = ticketRepository.GetById(ticketId);
            var ticketReacties = ticketReactieRepository.GetReactiesById(ticketId);

            var userinfo = UserProvider.GetUserInfo(ticketInfo.TicketGebruikersId);

            bundleTicketModel.TicketModel.Onderwerp = ticketInfo.Onderwerp;
            bundleTicketModel.TicketModel.Status = ticketInfo.Status;
            bundleTicketModel.TicketModel.TicketID = ticketInfo.TicketID;
            bundleTicketModel.TicketModel.Gebruikersnaam = userinfo.Username;
            bundleTicketModel.TicketModel.Voornaam = userinfo.Firstname;
            bundleTicketModel.TicketModel.Achternaam = userinfo.Lastname;
            bundleTicketModel.TicketModel.Datum = ticketInfo.DatumOpen;


            bundleTicketModel.TicketActionModel = new TicketActionModel
            {
                TicketId = ticketId
            };

            foreach (var ticketCommentNodel in from reactie in ticketReacties
                                               let reactionUserInfo = UserProvider.GetUserInfo(reactie.GebruikersId)
                                               select new TicketReactiesModel
                                               {
                                                   Bericht = reactie.Reactie,
                                                   Datum = reactie.Datum,
                                                   Voornaam = reactionUserInfo.Firstname,
                                                   Achternaam = reactionUserInfo.Lastname,
                                                   GebruikersGroepNaam = RoleProvider.GetUserRoleByUsername(reactionUserInfo.Username)
                                               })
            {
                bundleTicketModel.TicketReactieModel.Add(ticketCommentNodel);
            }

            bundleTicketModel.TicketReplyModel = new TicketReplyModel { TicketId = ticketId };

            return bundleTicketModel;
        }
        #endregion
    }
}