﻿#region

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.Guestbook;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class GastenboekController : ApplicationController
    {

        [RolePermission(permissionName = RolePermission.PermissionName.Gastenboek)]
        public ActionResult _Weergeven()
        {
            var guestbookRepository = new GastenboekRepository();
            var listGuestbook = guestbookRepository.GetList();
            var returnListModel = new Collection<ViewGuestbookModel>();

            foreach (var guestbookComment in listGuestbook)
            {
                returnListModel.Add(new ViewGuestbookModel
                {
                    Date = guestbookComment.Date,
                    Enabled = guestbookComment.Enabled,
                    Fullname = guestbookComment.AutorId == null ? guestbookComment.Name : UserProvider.GetFullName((Guid)guestbookComment.AutorId),
                    Id = guestbookComment.GuestbookId
                });
            }

            return View(returnListModel);
        }

         [RolePermission(permissionName = RolePermission.PermissionName.Gastenboek)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Gastenboek)]
        public ActionResult Wijzig(int id)
        {
            var guestbookRepository = new GastenboekRepository();

            var commentInfo = guestbookRepository.GetById(id);

            var guestbookModel = new GuestbookModel
            {
                Comment = commentInfo.Comment,
                Enabled = commentInfo.Enabled,
                GuestbookId = commentInfo.GuestbookId
            };

            return View(guestbookModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Gastenboek)]
        public ActionResult Verwijder(int id)
        {
            var guestbookRepository = new GastenboekRepository();

            var commentInfo = guestbookRepository.GetById(id);

            if (commentInfo != null)
            {
                // Remove sub comments
                DeleteSubComments(commentInfo.GuestbookId, commentInfo.ParentId ?? 0);

                // Remove sub comments
                guestbookRepository.Remove(commentInfo);
                guestbookRepository.Save();

                LogProvider.LogAction(
                    string.Format("De gastenboek reactie #{0} is verwijderd", commentInfo.GuestbookId));
                this.SetSuccess("De reactie is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De reactie is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.Gastenboek)]
        public ActionResult Wijzig(GuestbookModel guestbookModel)
        {
            if (ModelState.IsValid)
            {
                var guestbookRepository = new GastenboekRepository();

                Guestbook commentInfo = guestbookRepository.GetById(guestbookModel.GuestbookId);

                commentInfo.Enabled = guestbookModel.Enabled;
                commentInfo.Comment = guestbookModel.Comment;

                guestbookRepository.Save();

                LogProvider.LogAction(
                    string.Format("De gastenboek reactie #{0} is gewijzigd", guestbookModel.GuestbookId));

                this.SetSuccess("De reactie is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            return View(guestbookModel);
        }

        #region Private Methods
        private void DeleteSubComments(int guestbookId, int parentId)
        {
            // If this guestbook is a parent
            if (parentId == 0)
            {
                var guestbookRepository = new GastenboekRepository();

                foreach (var guestbookComment in guestbookRepository.GetList().Where(x => x.ParentId == guestbookId))
                {
                    guestbookRepository.Remove(guestbookComment);
                }

                // Remove sub guestbook comment
                guestbookRepository.Save();
            }
        }
        #endregion
    }
}