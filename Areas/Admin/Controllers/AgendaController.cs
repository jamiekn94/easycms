﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Agenda;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
     [RolePermission(permissionName = RolePermission.PermissionName.Agenda)]
    public class AgendaController : ApplicationController
    {
        private IEnumerable<SelectListItem> GetColors()
        {
            var colorsList = new Collection<SelectListItem>
            {
                new SelectListItem {Text = "Blauw", Value = "#0070ab"},
                new SelectListItem {Text = "Rood", Value = "#AA0000"},
                new SelectListItem {Text = "Groen", Value = "#14813B"},
                new SelectListItem {Text = "Oranje", Value = "#E47600"},
                new SelectListItem {Text = "Paars", Value = "#A400E4"},
                new SelectListItem {Text = "Zwart", Value = "#000000"}
            };

            return colorsList;
        }

        //
        // GET: /Agenda/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Toevoegen()
        {
            var agendaModel = new AgendaModel {AvailableColorsList = GetColors()};
            return View(agendaModel);
        }

        [HttpPost]
        public ActionResult Toevoegen(AgendaModel agendaModel)
        {
            if (ModelState.IsValid)
            {
                var agendaRepository = new AgendaRepository();

                var newAgendaInfo = new Agenda
                {
                    AllDay = agendaModel.AllDay,
                    Title = agendaModel.Title,
                    EndDate = agendaModel.AllDay == false ? agendaModel.EndDate : null,
                    SendReminder = agendaModel.SendReminder,
                    StartDate = Convert.ToDateTime(agendaModel.StartDate),
                    PublicPoint = agendaModel.Public,
                    Color = agendaModel.ColorValue,
                    UserId = UserProvider.GetUserId()
                };

                agendaRepository.Add(newAgendaInfo);

                agendaRepository.Save();

                Agenda addedAgenda = agendaRepository.GetLastAgenda();

                var agendaProvider = new AgendaProvider();
                agendaProvider.ScheduleAgenda(addedAgenda.AgendaId);

                this.SetSuccess("Het agenda punt is succesvol toegevoegd.");

                if (agendaModel.Public)
                {
                    LogProvider.LogAction(
                        string.Format("Het agenda punt {0} is toegevoegd", agendaModel.Title));
                }

                return RedirectToAction("Weergeven");
            }

            agendaModel.AvailableColorsList = GetColors();

            return View(agendaModel);
        }

        public ActionResult Wijzig(int id)
        {
            var agendaRepository = new AgendaRepository();

            Agenda agendaInfo = agendaRepository.GetById(id);

            if (agendaInfo.PublicPoint || agendaInfo.UserId == UserProvider.GetUserId())
            {
                var agendaModel = new AgendaModel
                {
                    AgendaId = agendaInfo.AgendaId,
                    AllDay = agendaInfo.AllDay,
                    Title = agendaInfo.Title,
                    EndDate = agendaInfo.EndDate,
                    SendReminder = agendaInfo.SendReminder,
                    StartDate = agendaInfo.StartDate,
                    Public = agendaInfo.PublicPoint,
                    ColorValue = agendaInfo.Color,
                    AvailableColorsList = GetColors()
                };

                foreach (SelectListItem color in agendaModel.AvailableColorsList)
                {
                    if (color.Value == agendaInfo.Color)
                    {
                        color.Selected = true;
                        break;
                    }
                }

                return View(agendaModel);
            }

            this.SetError("U heeft geen recht om dit agenda punt te wijzigen.");

            return RedirectToAction("Weergeven");
        }

        [HttpPost]
        public ActionResult Wijzig(AgendaModel agendaModel)
        {
            if (ModelState.IsValid)
            {
                var agendaRepository = new AgendaRepository();

                Agenda agendaInfo = agendaRepository.GetById(agendaModel.AgendaId);

                if (agendaInfo.PublicPoint || agendaInfo.UserId == UserProvider.GetUserId())
                {
                    bool previousPublic = agendaInfo.PublicPoint;

                    agendaInfo.AllDay = agendaModel.AllDay;
                    agendaInfo.Title = agendaModel.Title;
                    agendaInfo.EndDate = agendaModel.AllDay == false ? agendaModel.EndDate : null;
                    agendaInfo.SendReminder = agendaModel.SendReminder;
                    agendaInfo.StartDate = Convert.ToDateTime(agendaModel.StartDate);
                    agendaInfo.PublicPoint = agendaModel.Public;
                    agendaInfo.Color = agendaModel.ColorValue;

                    agendaRepository.Save();

                    this.SetSuccess("Het agenda punt is succesvol gewijzigd.");

                    // Initialize agenda provider
                    var agendaProvider = new AgendaProvider();

                    // Remove agenda
                    AgendaSchedules.RemoveTimer(agendaInfo.AgendaId);

                    // Add agenda
                    agendaProvider.ScheduleAgenda(agendaInfo.AgendaId);

                    // Was het eerst een agenda punt die iedereen mocht zien?
                    if (previousPublic)
                    {
                        LogProvider.LogAction(
                            string.Format("Het agenda punt {0} is gewijzigd", agendaModel.Title));
                    }
                }
                else
                {
                    this.SetError("U heeft geen recht om dit agenda punt te wijzigen.");
                }


                return RedirectToAction("Weergeven");
            }

            return View(agendaModel);
        }


        public ActionResult Weergeven()
        {
            return View();
        }

        public JsonResult GetAgenda()
        {
            var agendaRepository = new AgendaRepository();
            var returnCalendarList = new Collection<CalendarModel>();

            IQueryable<Agenda> calendarList = agendaRepository.GetViewableAgenda(UserProvider.GetUserId());

            foreach (Agenda calendar in calendarList)
            {
                returnCalendarList.Add(new CalendarModel
                {
                    id = calendar.AgendaId,
                    title = calendar.Title,
                    allDay = calendar.AllDay,
                    end = calendar.AllDay == false ? string.Format("{0:yyyy/MM/dd}", calendar.EndDate) : string.Empty,
                    start = string.Format("{0:yyyy/MM/dd}", calendar.StartDate).Replace("-", "/"),
                    url = "/Agenda/Wijzig?id=" + calendar.AgendaId,
                    color = calendar.Color
                });
            }

            return Json(returnCalendarList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Verwijder(int id)
        {
            var agendaRepository = new AgendaRepository();

            Agenda agendaInfo = agendaRepository.GetById(id);

            if (agendaInfo != null)
            {
                agendaRepository.Remove(agendaInfo);
                agendaRepository.Save();

                this.SetSuccess("Het agenda punt is succesvol verwijderd.");

                if (agendaInfo.PublicPoint)
                {
                    LogProvider.LogAction(
                        string.Format("Het agenda punt {0} verwijderd", agendaInfo.Title));
                }
            }
            else
            {
                this.SetError("Het agenda punt is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }
    }
}