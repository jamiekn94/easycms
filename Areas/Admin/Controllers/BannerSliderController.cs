﻿#region

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class BannerSliderController : ApplicationController
    {
        private BannerSliderRepository BannerSliderRepository { get; set; }

        public BannerSliderController()
        {
            BannerSliderRepository = new BannerSliderRepository();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.BannerSlider)]
        public ActionResult _Weergeven()
        {
            var bannerSliderList = new Collection<ViewBannerSliderModel>();

            var bannerSliders = BannerSliderRepository.GetList();

            foreach (BannerSlider bannerSlider in bannerSliders)
            {
                bannerSliderList.Add(new ViewBannerSliderModel
                {
                    BannerSliderId = bannerSlider.BannerSliderId,
                    Description = bannerSlider.Description,
                    ImageName = bannerSlider.Image,
                    Title = bannerSlider.Title
                });
            }

            return View(bannerSliderList);
        }

        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.BannerSlider)]
        public ActionResult Toevoegen()
        {
            return View();
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.BannerSlider)]
        public ActionResult Toevoegen(BannerSliderModel addBannerSliderModel)
        {
            if (ModelState.IsValid)
            {
                using (var newBannerSliderRepository = new BannerSliderRepository())
                {
                   var bannerSlider = new BannerSlider
                    {
                        Description = addBannerSliderModel.Description,
                        Title = addBannerSliderModel.Title,
                        Image = FileProvider.GetNewImageName(addBannerSliderModel.Image)
                    };

                    newBannerSliderRepository.Add(bannerSlider);

                    newBannerSliderRepository.Save();

                    AddImage(addBannerSliderModel.Image, bannerSlider.BannerSliderId);
                }

                LogProvider.LogAction(
                    string.Format("De banner {0} is toegevoegd aan de slider", addBannerSliderModel.Image.FileName));

                this.SetSuccess("De afbeelding voor de slider is succesvol toegevoegd.");

                return RedirectToAction("Weergeven");
            }
            return View(addBannerSliderModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.BannerSlider)]
        public ActionResult Wijzig(int id)
        {
            BannerSlider bannerSliderInfo = BannerSliderRepository.GetById(id);

            var bannerSliderModel = new BannerSliderModel
            {
                Description = bannerSliderInfo.Description,
                Title = bannerSliderInfo.Title,
                BannerSliderId = bannerSliderInfo.BannerSliderId
            };

            return View(bannerSliderModel);
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.BannerSlider)]
        public ActionResult Wijzig(BannerSliderModel editBannerSliderModel)
        {
            if (ModelState.IsValid)
            {
                BannerSlider bannerSliderInfo = BannerSliderRepository.GetById(editBannerSliderModel.BannerSliderId);

                bannerSliderInfo.Description = editBannerSliderModel.Description;
                bannerSliderInfo.Title = editBannerSliderModel.Title;
                bannerSliderInfo.Image = editBannerSliderModel.Image != null ? FileProvider.GetNewImageName(editBannerSliderModel.Image) : bannerSliderInfo.Image;

                string oldImageName = bannerSliderInfo.Image;

                EditImage(editBannerSliderModel.BannerSliderId, editBannerSliderModel.Image);

                BannerSliderRepository.Save();

                LogProvider.LogAction(
                    string.Format("De banner {0} is gewijzigd in de slider", oldImageName));

                this.SetSuccess("De afbeelding voor de slider is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            return View(editBannerSliderModel);
        }


        [RolePermission(permissionName = RolePermission.PermissionName.BannerSlider)]
        public ActionResult Verwijder(int id)
        {
            BannerSlider bannerSliderInfo = BannerSliderRepository.GetById(id);

            if (bannerSliderInfo != null)
            {
                string fullPath = FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Banners);

                // Remove old image   
                System.IO.File.Delete(fullPath + bannerSliderInfo.Image);

                // Remove in database
                BannerSliderRepository.Remove(bannerSliderInfo);

                BannerSliderRepository.Save();

                LogProvider.LogAction(
                    string.Format("De banner {0} is verwijderd in de slider", bannerSliderInfo.Image));

                this.SetSuccess("De afbeelding voor de slider is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De afbeelding voor de slider is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        #region Private Methods

        private void AddImage(HttpPostedFileBase postedFile, int bannerId)
        {
            FileProvider.AddFolderDirectory(FilePaths.Banners,
                bannerId.ToString(CultureInfo.InvariantCulture), false);

            FileProvider.AddAndReturnImageName(postedFile, FilePaths.Banners, bannerId.ToString(CultureInfo.InvariantCulture));
        }

        private void EditImage(int bannerSliderId, HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                // Get banner info
                var bannerInfo = BannerSliderRepository.GetById(bannerSliderId);

                // Check if the news article exists
                if (bannerInfo != null)
                {
                    // Delete previous image
                    FileProvider.DeleteFile(FilePaths.Banners, bannerSliderId.ToString(CultureInfo.InvariantCulture),
                         bannerInfo.Image, false);

                    // Create directory
                    FileProvider.AddFolderDirectory(FilePaths.Banners, bannerSliderId.ToString(CultureInfo.InvariantCulture),
                        true);

                    // Save image
                    string fileName = FileProvider.AddAndReturnImageName(postedFile, FilePaths.Banners,
                        bannerSliderId.ToString(CultureInfo.InvariantCulture));

                    // Update banner
                    bannerInfo.Image = fileName;

                    // Save changes
                    BannerSliderRepository.Save();
                }
            }
        }
        #endregion
    }
}