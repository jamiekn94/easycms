﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.Data;
using Common.Models.Admin.ViewModel;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;
using File = Common.File;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
     [RolePermission(permissionName = RolePermission.PermissionName.Bestanden)]
    public class FilesController : ApplicationController
    {

        #region Properties
        // Properties - TODO: Nog fatsoenlijk maken..
        private IEnumerable<Folder> _foldersList;
        private string _fullFilePath = string.Empty;
        #endregion

        #region Non ajax Calls

        public ActionResult _Weergeven()
        {
            var folderRepository = new FolderRepository();

            var bundleModel = new FolderBundleModel
            {
                FolderModel = new FolderModel(),
            };

            _foldersList = folderRepository.GetList().ToList();

            foreach (var folder in _foldersList.Where(x => x.ParentId == 1))
            {
                bundleModel.FolderModel.SubFolderModels.AddRange(GetFolders(folder.FolderId));
            }

            bundleModel.FolderActionModel = new FolderActionModel();

            // Get folders to add a new folder
            foreach (var folder in _foldersList.Where(x => x.FolderId != 10).ToList())
            {
                _fullFilePath = null;
                GetFullFolderName(folder);
                bundleModel.FolderActionModel.FolderList.Add(new SelectListItem
                {
                    Text =
                        string.IsNullOrEmpty(_fullFilePath)
                            ? folder.FolderName
                            : folder.FolderName + " - " + _fullFilePath,
                    Value = folder.FolderId.ToString()
                });
            }

            // Get roles
            bundleModel.FolderActionModel.RolesList = ReturnRoles();

            return View(bundleModel);
        }

        public ActionResult Weergeven()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MapToevoegen(FolderActionModel folderActionModel)
        {
            if (ModelState.IsValid)
            {
                // Add folder to disk and database
                var folderInfo = AddAndReturnFolder(folderActionModel.FolderName, folderActionModel.ValueFolderId);

                // Add folder permission
                AddFolderPermissions(folderInfo.FolderId, folderActionModel.ValueRole, folderActionModel.PublicMap);

                LogProvider.LogAction(
                    string.Format("De nieuwe map {0} is toegevoegd", folderInfo.FolderName));

                this.SetSuccess("De folder is succesvol toegevoegd.");
            }
            else
            {
                this.SetError(
                    "De folder is niet toegevoegd, het veld mag maximaal 25 en moet minimaal 5 tekens bevatten.");
            }
            return RedirectToAction("Weergeven");
        }

        public ActionResult VerwijderFolder(DeleteFolderModel deleteFolderModel)
        {
            if (ModelState.IsValid)
            {
                var folderRepository = new FolderRepository();

                Folder folderInfo = folderRepository.GetById(deleteFolderModel.FolderId);

                if (folderInfo != null)
                {
                    VerwijderMappen(folderInfo.FolderId);
                    Directory.Delete(Server.MapPath(folderInfo.FolderPath));
                    this.SetSuccess("De folder is succesvol verwijderd.");
                    LogProvider.LogAction(
                        string.Format("De map {0} is verwijderd", folderInfo.FolderName));
                }
                else
                {
                    this.SetError("De folder is niet verwijderd want hij bestaat niet.");
                }
            }

            return RedirectToAction("Weergeven");
        }

        public ActionResult MapWijzigen(FolderActionModel folderActionModel)
        {
            if (ModelState.IsValid)
            {
                var folderRepository = new FolderRepository();

                var folderInfo = folderRepository.GetById(folderActionModel.ValueFolderId);

                var serverPath = Server.MapPath("").Replace("Files", "").Replace(@"\\\\", "");

                string oldFolderPath = folderInfo.FolderPath.Substring(1, folderInfo.FolderPath.Length - 1);

                string newFolderPath = oldFolderPath.Replace(folderInfo.FolderName, "") + "/" +
                                       folderActionModel.FolderName;

                string oldFolderName = folderInfo.FolderName;

                // Update in database
                folderInfo.FolderName = folderActionModel.FolderName;

                folderInfo.FolderPath = folderInfo.FolderPath.Replace(oldFolderName,
                    folderActionModel.FolderName);

                // Save in db
                folderRepository.Save();

                // Move directories
                Directory.Move(serverPath + oldFolderPath, serverPath + newFolderPath);

                // Rename folder
                RenameFolder(folderActionModel.FolderName, folderRepository, folderInfo);
                
                // Delete all previous folder permissions
                DeleteFolderPermissions(folderActionModel.ValueFolderId);
                
                // Add new folder permissions
                AddFolderPermissions(folderActionModel.ValueFolderId, folderActionModel.RolesList.Where(x=> x.Selected).Select(x=> x.Text), folderActionModel.PublicMap);

                this.SetSuccess("De map is succesvol gewijzigd");

                LogProvider.LogAction(
                    string.Format("De map {0} is verplaatst", folderInfo.FolderName));
            }
            else
            {
                this.SetError("Het veld foldernaam mag niet langer zijn dan 25 tekens");
            }
            return RedirectToAction("Weergeven");
        }

        private static void DeleteFolderPermissions(int folderId)
        {
            var folderPermissionsRepository = new FolderPermissionRepository();

            var listPermissions = folderPermissionsRepository.GetByFolderId(folderId);

            folderPermissionsRepository.RemoveAll(listPermissions);
           
            folderPermissionsRepository.Save();
        }
        public ActionResult BestandToevoegen(UploadFileModel fileModel)
        {
            if (ModelState.IsValid)
            {
                var errorMessages = AddFilesAndReturnErrorMessages(fileModel.PostedFile, fileModel.ValueFolderId);

                LogProvider.LogAction(
                       string.Format("Er zijn {0} nieuwe bestanden toegevoegd", fileModel.PostedFile.Count()));

                if (errorMessages.Any())
                {
                    this.SetSuccess(fileModel.PostedFile.Count() == 1
                        ? "De bestanden zijn succesvol toegevoegd!"
                        : "Het bestand is succesvol toegevoegd.");
                }
                else
                {
                    TempData["ErrorMessage"] = errorMessages;
                }
            }

            return RedirectToAction("Weergeven");
        }

        public ActionResult VerwijderBestand(DeleteFileModel deleteFileModel)
        {
            if (ModelState.IsValid)
            {
                var fileRepository = new FilesRepository();

                var fileInfo = fileRepository.GetById(deleteFileModel.FileId);

                if (fileInfo != null && DeleteFileReturnStatus(deleteFileModel.FileId))
                {
                    this.SetSuccess("Het bestand is succesvol verwijderd.");

                    LogProvider.LogAction(string.Format("Het bestand {0} is verwijderd.", fileInfo.FileName));
                }
                else
                {
                    this.SetError("Het bestand is niet verwijderd want hij bestaat niet.");
                }
            }

            return RedirectToAction("Weergeven");
        }

        [HttpPost]
        public ActionResult WijzigBestand(EditFileModel editFileModel)
        {
            if (ModelState.IsValid)
            {
                var fileRepository = new FilesRepository();
                var folderRepository = new FolderRepository();

                var fileInfo = fileRepository.GetById(editFileModel.FileId);

                Folder folderInfo = folderRepository.GetById(fileInfo.FolderId);

                // Different folder - Move the file
                if (editFileModel.FileName.ToLower() != fileInfo.FileName.ToLower())
                {
                    string sourceFilePath = FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Files) + "/" + folderInfo.FolderPath  + "/" + fileInfo.FileName;

                    string destinationFilePath = FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Files) + "/" + folderInfo.FolderPath + "/" + editFileModel.FileName;

                    // Move file
                    System.IO.File.Move(sourceFilePath, destinationFilePath);

                    // Update database file location
                    fileInfo.FileName = editFileModel.FileName;
                }

                // Update file content - if it isn't empty
                if (!string.IsNullOrEmpty(editFileModel.Content))
                {
                    // Write new content to file
                    var writer = new StreamWriter(System.IO.File.OpenWrite(FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Files) + "/" + folderInfo.FolderPath + "/" + fileInfo.FileName));
                    writer.Write(editFileModel.Content);
                    writer.Close();
                }

                // Save
                fileRepository.Save();

                LogProvider.LogAction(
                    string.Format("Het bestand {0} is gewijzigd", fileInfo.FileName));

                this.SetSuccess("Het bestand is succesvol gewijzigd");
            }
            else
            {
                this.SetError("Het bestand is niet gewijzigd, controleer uw velden.");
            }

            return RedirectToAction("Weergeven");
        }

        public FileResult Download(int id)
        {
            var fileRepository = new FilesRepository();
            var fileInfo = fileRepository.GetById(id);

            if (fileInfo != null)
            {
                var permissionRepository = new FolderPermissionRepository();
                var listPermissions = permissionRepository.GetByFolderId(fileInfo.FolderId);
                var userRoleName =
                    RoleProvider.GetUserRoleByUsername(
                        UserProvider.GetUsername());
                var roleId = RoleProvider.GetRoleIdByName(userRoleName);

                // We may download this
                if (listPermissions.Any(x => x.IsPublic || (Guid)x.RoleId == roleId))
                {
                    var folderRepository = new FolderRepository();
                    var folderInfo = folderRepository.GetById(fileInfo.FolderId);

                    return File(folderInfo.FolderPath + "/" + fileInfo.FileName, System.Net.Mime.MediaTypeNames.Application.Octet, fileInfo.FileName);
                }
                else
                {
                    this.SetError("U heeft geen permissie om dit bestand te downloaden.");
                }
            }
            else
            {
                this.SetError("Dit bestand kon niet gedownload worden omdat hij niet (meer) bestaat.");
            }
            return null;
        }

        #endregion

        #region Functions

        private IEnumerable<SelectListItem> ReturnRoles()
        {
            var arrayRoles = RoleProvider.GetAllRoles();

            var returnArrayRoles = new Collection<SelectListItem>();

            foreach (string role in arrayRoles)
            {
                returnArrayRoles.Add(new SelectListItem
                {
                    Text = role,
                    Value = role
                });
            }

            return returnArrayRoles;
        }

        public Folder AddAndReturnFolder(string folderName, int folderParentId)
        {
            var folderRepository = new FolderRepository();

            Folder parentFolderInfo = folderRepository.GetById(folderParentId);

            // Hoofdpad heeft alleen een slash - verwijder de slash
            parentFolderInfo.FolderPath = parentFolderInfo.FolderPath == "/" ? "" : parentFolderInfo.FolderPath;

            string fullFolderPath = parentFolderInfo.FolderPath + "/" + folderName;

            folderRepository.Add(new Folder
            {
                FolderName = folderName,
                FolderPath = fullFolderPath,
                ParentId = parentFolderInfo.FolderId
            });

            folderRepository.Save();

            FileProvider.AddFolderDirectory(FilePaths.Files, fullFolderPath, false);

            return folderRepository.GetLastFolder();
        }

        private bool DeleteFileReturnStatus(int fileId)
        {
            var fileRepository = new FilesRepository();

            var fileInformation = fileRepository.GetById(fileId);

            if (fileInformation != null)
            {
                var folderRepository = new FolderRepository();

                Folder folderInfo = folderRepository.GetById(fileInformation.FolderId);

                FileProvider.DeleteFile(FilePaths.Files, folderInfo.FolderPath, fileInformation.FileName, true);

                fileRepository.Remove(fileInformation);

                fileRepository.Save();
            }

            return fileInformation != null;
        }

        private void RenameFolder(string newFolderName, FolderRepository folderRepository,
           Folder folderInfo)
        {
            foreach (Folder subfolder in folderRepository.GetList().Where(x => x.ParentId == folderInfo.FolderId))
            {
                subfolder.FolderPath = subfolder.FolderPath.Replace(folderInfo.FolderName.Replace(@"/", ""),
                    newFolderName);

                RenameFolder(newFolderName, folderRepository, subfolder);
            }

        }

        private string[] GetNotAllowedFileExtensions()
        {
            return new[] { ".exe", ".bat", ".cgi", ".com", ".exe", ".gadget", ".jar", ".pif", ".vb", ".wsf" };
        }

        private void VerwijderMappen(int folderId)
        {
            var folderRepository = new FolderRepository();
            var permissionRepository = new FolderPermissionRepository();
            var fileRepository = new FilesRepository();

            var listFolders = new List<Folder>();
            var listPermissions = new List<FolderPermission>();
            var listFiles = new List<Common.File>();

            // Main folder
            listFolders.Add(folderRepository.GetById(folderId));

            // Main permissions
            listPermissions.AddRange(permissionRepository.GetByFolderId(folderId));

            // Main files
            listFiles.AddRange(fileRepository.GetFilesByFolderId(folderId));

            foreach (Folder subfolder in folderRepository.GetList().Where(x => x.ParentId == folderId))
            {
                var folderModel = new Folder
                {
                    FolderId = subfolder.FolderId,
                    FolderName = subfolder.FolderName
                };

                listFolders.Add(folderModel);

                VerwijderMappen(folderModel.FolderId);

                listFiles.AddRange(fileRepository.GetList().Where(x => x.FolderId == subfolder.FolderId).ToList());

                listPermissions.AddRange(permissionRepository.GetByFolderId(subfolder.FolderId));
            }

            fileRepository.RemoveAll(listFiles);
            fileRepository.Save();

            permissionRepository.RemoveAll(listPermissions);
            permissionRepository.Save();

            folderRepository.RemoveAll(listFolders);
            folderRepository.Save();
        }

        private IEnumerable<string> AddFilesAndReturnErrorMessages(IEnumerable<HttpPostedFileBase> uploadedFiles, int folderId)
        {
            var folderRepository = new FolderRepository();

            Folder folderInfo = folderRepository.GetById(folderId);

            var fileRepository = new FilesRepository();

            var errorMessages = new List<string>();

            var arrayNotAllowedFileExtensions = GetNotAllowedFileExtensions();

            foreach (HttpPostedFileBase file in uploadedFiles)
            {
                // Check if the extension of the file is valid
                if (!arrayNotAllowedFileExtensions.Contains(Path.GetExtension(file.FileName)))
                {
                    // Check if the length isn't too long
                    if (255 >= file.FileName.Length)
                    {
                        string fileName = file.FileName;

                        // Add file into the directory
                        if (FileProvider.IsImage(file.FileName))
                        {
                            fileName = FileProvider.AddAndReturnImageName(file, FilePaths.Files, folderInfo.FolderName);
                        }
                        else
                        {
                            FileProvider.AddFile(file, FilePaths.Files, folderInfo.FolderName);
                        }

                        // Add file to db
                        fileRepository.Add(new File
                        {
                            FileName = fileName,
                            FolderId = folderInfo.FolderId
                        });

                        // Save db
                        fileRepository.Save();
                    }
                    else
                    {
                        errorMessages.Add("De naam van het bestand: " + file.FileName + " is te lang.");
                    }
                }
                else
                {
                    errorMessages.Add("De extensie van het bestand: " + file.FileName + " is niet toegestaan.");
                }
            }
            return errorMessages;
        }

        private List<FolderModel> GetFolders(int folderId)
        {
            var listFolderModel = new List<FolderModel>();
            var folderPermissionsRepository = new FolderPermissionRepository();
            IQueryable<FolderPermission> folderRolePermissions =
                folderPermissionsRepository.GetPermissionsByRoleName(
                    RoleProvider.GetUserRoleByUsername(UserProvider.GetUsername()));

            // Foreach folder
            foreach (Folder subfolder in _foldersList.Where(x => x.ParentId == folderId))
            {
                // Does this role have permission to view this folder? - Show the first folder anyway
                if (folderRolePermissions.Any(x => x.FolderId == subfolder.FolderId) || subfolder.FolderId == 3)
                {
                    var folderModel = new FolderModel
                    {
                        FolderId = subfolder.FolderId,
                        FolderName = subfolder.FolderName,
                        SubFolderModels = GetFolders(subfolder.FolderId)
                    };
                    listFolderModel.Add(folderModel);
                }
            }
            return listFolderModel;
        }

        private void AddFolderPermissions(int folderId, IEnumerable<string> selectedRoles, bool isPublicFolder)
        {
            // Only specific roles have permissions for this folder
            var folderPermissionRepository = new FolderPermissionRepository();

            if (!isPublicFolder)
            {
                foreach (string role in selectedRoles)
                {
                    folderPermissionRepository.Add(new FolderPermission
                    {
                        FolderId = folderId,
                        RoleId = RoleProvider.GetRoleIdByName(role),
                        IsPublic = isPublicFolder
                    });
                }
            }
            // Is public map
            else
            {
                folderPermissionRepository.Add(new FolderPermission
                {
                    FolderId = folderId,
                    IsPublic = isPublicFolder
                });
            }

            folderPermissionRepository.Save();
        }

        private void GetFullFolderName(Folder folder)
        {
            if (folder != null)
            {
                var folderRepository = new FolderRepository();
                Folder folderInfo = folderRepository.GetById(folder.ParentId);
                if (folderInfo != null)
                {
                    _fullFilePath += folderInfo.FolderPath;

                    GetFullFolderName(folderRepository.GetById(folder.ParentId));
                }
                else
                {
                    if (_fullFilePath[0].ToString() == "/")
                    {
                        _fullFilePath = _fullFilePath.Substring(1, _fullFilePath.Length - 1);
                    }

                    if (_fullFilePath.Length > 0)
                    {
                        if (_fullFilePath[_fullFilePath.Length - 1].ToString() == "/")
                        {
                            _fullFilePath = _fullFilePath.Substring(0, _fullFilePath.Length - 2);
                        }
                    }
                }
            }
        }

        #endregion

        #region Ajax Calls

        [HttpGet]
        public JsonResult GetFolderPermissions(int folderId)
        {
            var folderPermissionsRepository = new FolderPermissionRepository();
            var select2List = new Collection<Select2Model>();

            IQueryable<FolderPermission> folderPermissions = folderPermissionsRepository.GetByFolderId(folderId);

            foreach (FolderPermission permission in folderPermissions.Where(x => x.IsPublic == false))
            {
                Guid guid = Guid.Empty;
                if (permission.RoleId != null)
                {
                    guid = permission.RoleId.Value;
                }

                string roleName = RoleProvider.GetRoleNameById(guid);

                select2List.Add(new Select2Model
                {
                    Id = roleName,
                    Text = roleName
                });
            }

            return Json(System.Web.Helpers.Json.Encode(select2List), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetFolderName(int folderId)
        {
            var folderRepository = new FolderRepository();
            string folderName = folderRepository.GetById(folderId).FolderName;

            return Content(folderName);
        }

        [HttpGet]
        public JsonResult GetFile(int fileId)
        {
            var folderRepository = new FolderRepository();
            var fileRepository = new FilesRepository();
            var editFileModel = new EditFileModel();

            File fileInfo = fileRepository.GetById(fileId);
            Folder folderInfo = folderRepository.GetById(fileInfo.FolderId);

            string filePath = Server.MapPath(folderInfo.FolderPath + "/" + fileInfo.FileName);

            // Get extension
            string extension = Path.GetExtension(filePath);

            if (extension == ".txt")
            {
                StreamReader fileStream = System.IO.File.OpenText(filePath);
                editFileModel.Content = fileStream.ReadToEnd();
                fileStream.Close();
            }


            editFileModel.FileId = fileId;
            editFileModel.FileName = fileInfo.FileName;

            editFileModel.FolderName = folderInfo.FolderName;

            return Json(System.Web.Helpers.Json.Encode(editFileModel), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LoadFiles(int folderId)
        {
            var fileRepository = new FilesRepository();
            var folderPermissionsRepository = new FolderPermissionRepository();
            var filesList = new List<File>();
            string roleName = RoleProvider.GetUserRoleByUsername(UserProvider.GetUsername());
            Guid roleId = RoleProvider.GetRoleIdByName(roleName);

            if (folderPermissionsRepository.GetByFolderId(folderId).Any(x => x.RoleId == roleId))
            {
                filesList = fileRepository.GetFilesByFolderId(folderId).ToList();
            }

            return PartialView("~/Areas/Admin/Views/Shared/Files/ViewFiles.cshtml", filesList);
        }

        #endregion

    }
}