﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Common;
using Common.Models.Admin.Pages;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class PaginaController : ApplicationController
    {

        //
        // GET: /Paginas/

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        public ActionResult _Weergeven()
        {
            var paginaRepository = new PaginaRepository();
            var pageInfoList = paginaRepository.GetList();
            var pageList = pageInfoList.Select(pagina => new PaginaModel
            {
                PaginaId = pagina.PaginaID, 
                Titel = pagina.Titel,
                AuteurNaam = UserProvider.GetFullName(pagina.AuteurId),
                Datum = pagina.Datum,
                Status = pagina.Enabled,
            }).ToList();

            return View(pageList);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        public ActionResult Wijzig(int id)
        {
            var paginaRepository = new PaginaRepository();
            var paginaInfo = paginaRepository.GetById(id);

            var model = new PaginaModel
            {
                Informatie = paginaInfo.Informatie,
                Linknaam = paginaInfo.LinkNaam,
                PaginaId = paginaInfo.PaginaID,
                SeoDescriptie = paginaInfo.SEODescriptie,
                SeoKeywoorden = paginaInfo.SEOKeywoorden,
                SeoTitel = paginaInfo.SEOTitel,
                Titel = paginaInfo.Titel,
                Status = paginaInfo.Enabled
            };

            return View(model);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        public ActionResult Verwijder(int id)
        {
            var paginaRepository = new PaginaRepository();
            var paginaInfo = paginaRepository.GetById(id);

            if (paginaInfo != null)
            {
                paginaRepository.Remove(paginaInfo);
                paginaRepository.Save();

                this.SetError("De pagina is succesvol verwijderd.");

                LogProvider.LogAction(
                    string.Format("De pagina {0} is verwijderd", paginaInfo.Titel));
            }
            else
            {
                this.SetError("De pagina is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        public ActionResult Toevoegen()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        [HttpPost]
        public ActionResult Toevoegen(PaginaModel paginaModel)
        {
            if (ModelState.IsValid)
            {
                var paginaRepository = new PaginaRepository();
                var paginaInfo = new Pagina
                {
                    Informatie = paginaModel.Informatie,
                    LinkNaam = paginaModel.Linknaam,
                    PaginaID = paginaModel.PaginaId,
                    SEODescriptie = paginaModel.SeoDescriptie,
                    SEOKeywoorden = paginaModel.SeoKeywoorden,
                    SEOTitel = paginaModel.SeoTitel,
                    Titel = paginaModel.Titel,
                    AuteurId = UserProvider.GetUserId(),
                    Datum = DateTime.Now
                };

                paginaRepository.Add(paginaInfo);
                paginaRepository.Save();

                LogProvider.LogAction(
                    string.Format("De pagina {0} is toegevoegd", paginaInfo.Titel));

                this.SetSuccess("De pagina is succesvol toegevoegd.");

                return RedirectToAction("Weergeven");
            }
            return View(paginaModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Page)]
        [HttpPost]
        public ActionResult Wijzig(PaginaModel paginaModel)
        {
            if (ModelState.IsValid)
            {
                var paginaRepository = new PaginaRepository();
                Pagina paginaInfo = paginaRepository.GetById(paginaModel.PaginaId);

                paginaInfo.Informatie = paginaModel.Informatie;
                paginaInfo.LinkNaam = paginaModel.Linknaam;
                paginaInfo.PaginaID = paginaModel.PaginaId;
                paginaInfo.SEODescriptie = paginaModel.SeoDescriptie;
                paginaInfo.SEOKeywoorden = paginaModel.SeoKeywoorden;
                paginaInfo.SEOTitel = paginaModel.SeoTitel;
                paginaInfo.Titel = paginaModel.Titel;
                paginaInfo.Enabled = paginaModel.Status;
                paginaRepository.Save();

                LogProvider.LogAction(
                    string.Format("De pagina {0} is gewijzigd", paginaInfo.Titel));

                this.SetSuccess("De pagina is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            return View(paginaModel);
        }

        #region Seo Pages

        public ActionResult SeoPagesWeergeven()
        {
            return View();
        }

        public ActionResult _SeoPagesWeergeven()
        {
            var seoPagesRepository = new SeoPageRepository();

            return View(seoPagesRepository.GetList());
        }

        public ActionResult SeoPageWijzig(int id)
        {
            var seoPageRepository = new SeoPageRepository();

            var seoPageInfo = seoPageRepository.GetById(id);

            if (seoPageInfo != null)
            {
                // Create mapper
                Mapper.CreateMap<SEOPage, SeoPageModel>();

                // Get mapper
                var seoPageModel = Mapper.Map<SeoPageModel>(seoPageInfo);

                return View(seoPageModel);
            }

            this.SetError("De seo page kon niet gevonden worden.");

            return RedirectToAction("SeoPagesWeergeven");
        }

        [HttpPost]
        public ActionResult SeoPageWijzig(SeoPageModel seoPageModel)
        {
            if (ModelState.IsValid)
            {
                var seoPageRepository = new SeoPageRepository();

                var seoPageInfo = seoPageRepository.GetById(seoPageModel.Id);

                if (seoPageInfo != null)
                {
                    seoPageInfo.Titel = seoPageModel.Titel;
                    seoPageInfo.SEODescription = seoPageModel.SeoDescription;
                    seoPageInfo.SEOKeywords = seoPageModel.SeoKeywords;
                    seoPageInfo.Hide = seoPageModel.Hide;

                    seoPageRepository.Save();

                    seoPageRepository.GetList().RefreshFromCache(60);

                    LogProvider.LogAction(string.Format("De seo page: {0}-{1} is gewijzigd.", seoPageInfo.Controller, seoPageInfo.Action));

                    this.SetSuccess("De seo page is succesvol gewijzigd.");
                }
                else
                {
                    this.SetError("De pagina is niet gewijzigd want hij bestaat niet meer.");
                }

                RedirectToAction("SeoPagesWeergeven");

            }
            return View(seoPageModel);
        }
        #endregion
    }
}