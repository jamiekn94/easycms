﻿#region

using System.Web.Mvc;
using System.Web.Security;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class UitloggenController : Controller
    {
        //
        // GET: /Uitloggen/

        public ActionResult Index()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}