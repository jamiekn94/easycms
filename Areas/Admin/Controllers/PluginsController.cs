﻿//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Web.Mvc;
//using AutoMapper;
//using Common;
//using Common.Models.Admin.Plugins;
//using Common.Models.Admin.ViewModel;
//using Common.Providers.Logging;
//using Common.Providers.Messages;
//using Common.Providers.Roles;
//using Common.Repositories;
////using Plugins;
//using Plugins;
//using Widgets.Models;


//namespace Kapsters.Areas.Admin.Controllers
//{
//    public enum PluginStatus
//    {
//        All, Active, InActive
//    }

//  //  [RolePermission(permissionName = RolePermission.PermissionName.Plugins)]
//    public class PluginsController : ApplicationController
//    {
//        //
//        // GET: /Admin/Plugins/

//        public ActionResult Weergeven()
//        {
//            return View("Weergeven", GetPlugins(PluginStatus.All));
//        }

//        [ActionName("Actieve-Plugins-Weergeven")]
//        public ActionResult WeergevenActief()
//        {
//            return View("Weergeven", GetPlugins(PluginStatus.Active));
//        }

//        [ActionName("In-Actieve-Plugins-Weergeven")]
//        public ActionResult WeergevenInActief()
//        {
//            return View("Weergeven", GetPlugins(PluginStatus.InActive));
//        }

//        public ActionResult Toevoegen()
//        {
//            return View();
//        }

//        public ActionResult Activate(int id)
//        {
//            var pluginRepository = new PluginRepository();

//            var pluginInfo = pluginRepository.GetById(id);

//            if (pluginInfo != null)
//            {
//                pluginInfo.Enabled = true;

//                pluginRepository.Save();

//                this.SetSuccess("De plugin is ingeschakeld.");

//                LogProvider.LogAction(string.Format("De plugin {0} is ingeschakeld", pluginInfo.PluginFriendlyName));
//            }

//            return RedirectToPlugins();
//        }

//        public ActionResult DeActivate(int id)
//        {
//            var pluginRepository = new PluginRepository();

//            var pluginInfo = pluginRepository.GetById(id);

//            if (pluginInfo != null)
//            {
//                pluginInfo.Enabled = false;

//                pluginRepository.Save();

//                this.SetSuccess("De plugin is uitgeschakeld.");

//                LogProvider.LogAction(string.Format("De plugin {0} is uitgeschakeld", pluginInfo.PluginFriendlyName));
//            }

//            return RedirectToPlugins();
//        }

//        public ActionResult GetStatus()
//        {
//            return Content("Ok");
//        }

//        public void Restart()
//        {
//            var pluginInstaller = new Install();
//            pluginInstaller.ReStartWebsite();
//        }

//        [HttpPost]
//        public ActionResult Toevoegen(AddPluginModel addPluginModel)
//        {
//            if (ModelState.IsValid)
//            {
//                var pluginInstaller = new Install();
//                var pluginInfo = pluginInstaller.Launch(addPluginModel.PluginPostedFileBase);

//                switch (pluginInfo.TypeInstallation)
//                {
//                    case TypeInstallation.Installation:
//                    {
//                        LogProvider.LogAction(string.Format("De plugin {0} is succesvol geïnstalleerd", pluginInfo.FriendlyPluginName));
//                        this.SetSuccess("De plugin is succesvol geïnstalleerd.");
//                        break;
//                    }
//                    case TypeInstallation.Update:
//                    {
//                        LogProvider.LogAction(string.Format("De plugin {0} is succesvol geupdate", pluginInfo.FriendlyPluginName));
//                        this.SetSuccess("De plugin is succesvol geupdate.");
//                        break;
//                    }
//                    case TypeInstallation.ReInstallation:
//                    {
//                        LogProvider.LogAction(string.Format("De plugin {0} is succesvol geherinstalleerd", pluginInfo.FriendlyPluginName));
//                        this.SetSuccess("De plugin is succesvol geherinstalleerd.");
//                        break;
//                    }
//                }

//                return RedirectToAction("Weergeven", new {restart="true"});
//            }
//            return View(addPluginModel);
//        }

//        #region Private Methods

//        private IEnumerable<ViewPluginsModel> GetPlugins(PluginStatus pluginStatus)
//        {
//            var pluginRepository = new PluginRepository();
//            var pluginPagesRepository = new PluginPagesRepository();

//            var listReturnModel = new Collection<ViewPluginsModel>();
//            IEnumerable<Common.Plugin> listPlugins;

//            switch (pluginStatus)
//            {
//                case PluginStatus.Active:
//                    {
//                        listPlugins = pluginRepository.GetActivePlugins();
//                        break;
//                    }

//                case PluginStatus.InActive:
//                    {
//                        listPlugins = pluginRepository.GetInActivePlugins();
//                        break;
//                    }

//                default:
//                    {
//                        listPlugins = pluginRepository.GetList();
//                        break;
//                    }
//            }

//            foreach (var plugin in listPlugins)
//            {
//                // Create mapper
//                Mapper.CreateMap<Plugin, ViewPluginsModel>();

//                // Get mapper
//                var pluginInfo = Mapper.Map<ViewPluginsModel>(plugin);

//                // Get settings controller
//                pluginInfo.SettingsController = pluginPagesRepository.GetSettingsControllerByPluginId(plugin.PluginId);

//                // Add plugin to list
//                listReturnModel.Add(pluginInfo);
//            }

//            return listReturnModel;
//        }

//        private ActionResult RedirectToPlugins()
//        {
//            if (Request.UrlReferrer.PathAndQuery.ToLower().Contains("actieve-plugins-weergeven"))
//            {
//                return RedirectToAction("Actieve-Plugins-Weergeven");
//            }
//            else if (Request.UrlReferrer.PathAndQuery.ToLower().Contains("in-actieve-plugins-weergeven"))
//            {
//                return RedirectToAction("In-Actieve-Plugins-Weergeven");
//            }
//            else
//            {
//                return RedirectToAction("Weergeven");
//            }
//        }
//        #endregion

//    }
//}
