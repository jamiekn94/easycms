﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Common;
using Common.Models.Admin.Widgets;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;
using Kapsters.Classes.PhotoAlbum;

namespace Kapsters.Areas.Admin.Controllers
{
    [RolePermission(permissionName = RolePermission.PermissionName.Widgets)]
    public class WidgetsController : ApplicationController
    {
        #region Public Functions

        public PartialViewResult GetActiveWidgetsView(int id)
        {
            return PartialView("Shared/ActiveWidgets", GetInstalledWidgets(id));
        }

        /// <summary>
        /// Ajax call that adds the dropped widget to the selected page
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="kindWidget"></param>
        /// <param name="sectionId"></param>
        /// <param name="order"></param>
        public bool AddWidgetToPage(int pageId, int kindWidget, int sectionId, int order)
        {
            var activeWidgetRepository = new ActiveWidgetRepository();
            var widgetRepository = new WidgetRepository();
            var seoPageRepository = new SeoPageRepository();

            activeWidgetRepository.Add(new ActiveWidget
            {
                PageId = pageId,
                SectionId = sectionId,
                OrderWidget = order,
                KindWidget = kindWidget
            });

            activeWidgetRepository.Save();

            AddWidgetKind(kindWidget, activeWidgetRepository.GetLastAddedActiveWidgetId());

            var widgetInfo = widgetRepository.GetById(kindWidget);
            var seoPageInfo = seoPageRepository.GetById(pageId);

            LogProvider.LogAction(string.Format("De widget: {0} is toegevoegd op de pagina: {1} - {2}.", widgetInfo.Name, seoPageInfo.Controller, seoPageInfo.Action));

            return true;
        }

        public ActionResult _Weergeven()
        {
            var bundleViewWidgetsModel = new BundleViewWidgetsModel
            {
                SeoPages = GetSeoPages(),
                Widgets = GetWidgets()
            };

            bundleViewWidgetsModel.InstalledWidgets =
                GetInstalledWidgets(int.Parse(bundleViewWidgetsModel.SeoPages.First().Value));

            return View(bundleViewWidgetsModel);
        }

        public ActionResult Weergeven()
        {
            return View();
        }

        public bool DeleteActiveWidget(int activeWidgetId, int kindWidgetId)
        {
            var activeWidgetRepository = new ActiveWidgetRepository();
            var widgetRepository = new WidgetRepository();
            var seoPageRepository = new SeoPageRepository();

            var activeWidget = activeWidgetRepository.GetById(activeWidgetId);

            if (activeWidget == null){throw new Exception("Widget doesn't exist anymore!");};

            var seoPageInfo = seoPageRepository.GetById(activeWidget.PageId);

            var widgetInfo = widgetRepository.GetById(kindWidgetId);

            activeWidgetRepository.Remove(activeWidget);
            activeWidgetRepository.Save();

            LogProvider.LogAction(string.Format("De widget: {0} op pagina: {1} - {2} is verwijderd.", widgetInfo.Name, seoPageInfo.Controller, seoPageInfo.Action));

            return true;
        }

        public bool UpdateOrderWidgets(string json)
        {
            var activeWidgetRepository = new ActiveWidgetRepository();
            var js = new JavaScriptSerializer();

            // Deserialize json object
            var arrayActiveWidgets = js.Deserialize<IEnumerable<JsonWidgetOrderModel>>(json);

            foreach (var activeWidget in arrayActiveWidgets)
            {
                ActiveWidget dbActiveWidget = activeWidgetRepository.GetById(activeWidget.ActiveWidgetId);

                dbActiveWidget.OrderWidget = activeWidget.WidgetOrder;

                activeWidgetRepository.Save();
            }

            return true;
        }

        #endregion

        #region Private methods

        private void AddWidgetKind(int kindWidgetId, int activeWidgetId)
        {
            switch ((KindWidget)kindWidgetId)
            {
                case KindWidget.Html:
                    {
                        using (var htmlWidgetRepository = new HtmlWidgetRepository())
                        {
                            htmlWidgetRepository.Add(new HtmlWidget
                            {
                                ActiveWidgetId = activeWidgetId
                            });

                            htmlWidgetRepository.Save();
                        }
                        break;
                    }
                case KindWidget.Blog:
                    {
                        using (var blogWidgetRepository = new BlogWidgetsRepository())
                        {
                            blogWidgetRepository.Add(new BlogWidget
                            {
                                ActiveWidgetId = activeWidgetId,
                            });

                            blogWidgetRepository.Save();
                        }
                        break;
                    }
                case KindWidget.News:
                    {
                        using (var newsWidgetRepository = new NewsWidgetRepository())
                        {
                            newsWidgetRepository.Add(new NewsWidget
                            {
                                ActiveWidgetId = activeWidgetId,

                            });

                            newsWidgetRepository.Save();
                        }
                        break;
                    }
                case KindWidget.Photo:
                    {
                        using (var photoWidgetRepository = new PhotoWidgetRepository())
                        {
                            photoWidgetRepository.Add(new PhotoWidget
                            {
                                ActiveWidgetId = activeWidgetId
                            });

                            photoWidgetRepository.Save();
                        }
                        break;
                    }
                case KindWidget.Twitter:
                    {
                        using (var twitterWidgetRepository = new TwitterfeedRWidgetRepository())
                        {
                            twitterWidgetRepository.Add(new TwitterFeedWidget
                            {
                                ActiveWidgetId = activeWidgetId
                            });

                            twitterWidgetRepository.Save();
                        }
                        break;
                    }
                case KindWidget.Social:
                    {
                        using (var socialWidgetRepository = new ShareWidgetRepository())
                        {
                            socialWidgetRepository.Add(new ShareThisWidget
                            {
                                ActiveWidgetId = activeWidgetId
                            });

                            socialWidgetRepository.Save();
                        }
                        break;
                    }
            }
        }

        private IEnumerable<SelectListItem> GetSeoPages()
        {
            var seoPagesRepository = new SeoPageRepository();
            var listReturnPages = new Collection<SelectListItem>();

            foreach (var page in seoPagesRepository.GetList())
            {
                listReturnPages.Add(new SelectListItem
                {
                    Value = page.Id.ToString(CultureInfo.InvariantCulture),
                    Text = page.Controller + " - " + page.Action
                });
            }
            return listReturnPages;
        }

        private IEnumerable<Common.Widget> GetWidgets()
        {
            var widgetsRepository = new Common.Repositories.WidgetRepository();

            return widgetsRepository.GetList();
        }

        private IEnumerable<InstalledWidgetsModel> GetInstalledWidgets(int seoPageId)
        {
            var configurationRepository = new Common.Repositories.ConfiguratieRepository();
            var themeSectionRepository = new Common.Repositories.ThemeSectionRepository();

            var configurationInfo = configurationRepository.GetById(1);

            var returnList = new Collection<InstalledWidgetsModel>();

            foreach (var themeSection in themeSectionRepository.GetSectionsByThemeId(configurationInfo.ThemeId))
            {
                returnList.Add(new InstalledWidgetsModel
                {
                    SectionId = themeSection.SectionId,
                    SectionName = themeSection.FriendlyName,
                    SectionDescription = themeSection.Description,
                    ActiveWidgets = GetActiveWidgets(seoPageId, themeSection.SectionId)
                });
            }
            return returnList;
        }

        private IEnumerable<ActiveWidgetModel> GetActiveWidgets(int seoPageId, int sectionId)
        {
            var activeWidgetsRepository = new Common.Repositories.ActiveWidgetRepository();
            var widgetRepository = new Common.Repositories.WidgetRepository();

            var returnActiveWidgetList = new Collection<ActiveWidgetModel>();

            foreach (var activeWidget in activeWidgetsRepository.GetBySection(sectionId, seoPageId))
            {
                var widget = widgetRepository.GetById(activeWidget.KindWidget);

                returnActiveWidgetList.Add(new ActiveWidgetModel
                {
                    ActiveWidgetId = activeWidget.ActiveWidgetId,
                    Description = widget.Description,
                    FriendlyName = widget.Name,
                    WidgetId = widget.WidgetId,
                    KindWidget = activeWidget.KindWidget
                });
            }
            return returnActiveWidgetList;
        }

        #endregion

        #region Shared Settings Functions

        private void DisplayError()
        {
            this.SetError(
                "De widget is niet gewijzigd, controleer of de widget bestaat en of de velden correct zijn ingevuld.");
        }
        #endregion

        #region Newsarticles settings

        public ActionResult NewsSettings(int activeWidgetId)
        {
            using (var newsWidgetRepository = new NewsWidgetRepository())
            {
                var newsWidget = newsWidgetRepository.GetByActiveWidgetId(activeWidgetId);

                if (newsWidget != null)
                {
                    return View(new EditNewsWidgetModel
                    {
                        NewsWidgetId = newsWidget.NewsWidgetId,
                        Amount = newsWidget.AmountItems,
                        Slide = newsWidget.Slide
                    });
                }

                return null;
            }
        }

        [HttpPost]
        public ActionResult NewsSettings(EditNewsWidgetModel editNewsWidgetModel)
        {
            if (ModelState.IsValid)
            {
                using (var newsWidgetRepository = new NewsWidgetRepository())
                {
                    var newsWidget = newsWidgetRepository.GetById(editNewsWidgetModel.NewsWidgetId);

                    if (newsWidget != null)
                    {
                        newsWidget.AmountItems = editNewsWidgetModel.Amount;
                        newsWidget.Slide = editNewsWidgetModel.Slide;

                        newsWidgetRepository.Save();

                        this.SetSuccess("De widget is succesvol gewijzigd.");
                        return RedirectToAction("Weergeven");
                    }
                }
            }

            this.SetError("De widget is niet gewijzigd, controleer uw velden en of de widget nog bestaat.");

            return RedirectToAction("Weergeven");
        }
        #endregion

        #region Blogarticles settings

        public ActionResult BlogSettings(int activeWidgetId)
        {
            using (var blogWidgetRepository = new BlogWidgetsRepository())
            {
                var blogWidget = blogWidgetRepository.GetByActiveWidgetId(activeWidgetId);

                if (blogWidget != null)
                {
                    return View(new BlogWidgetModel
                    {
                        BlogWidgetId = blogWidget.ActiveWidgetId,
                        Amount = blogWidget.AmountItems,
                        Slide = blogWidget.Slide
                    });
                }

                return null;
            }
        }

        [HttpPost]
        public ActionResult BlogSettings(BlogWidgetModel blogWidgetModel)
        {
            if (ModelState.IsValid)
            {
                using (var blogWidgetRepository = new BlogWidgetsRepository())
                {
                    var blogWidget = blogWidgetRepository.GetById(blogWidgetModel.BlogWidgetId);

                    if (blogWidget != null)
                    {
                        blogWidget.AmountItems = blogWidgetModel.Amount;
                        blogWidget.Slide = blogWidgetModel.Slide;

                        blogWidgetRepository.Save();

                        this.SetSuccess("De widget is succesvol gewijzigd.");
                        return RedirectToAction("Weergeven");
                    }
                }
            }

            this.SetError("De widget is niet gewijzigd, controleer uw velden en of de widget nog bestaat.");

            return RedirectToAction("Weergeven");
        }
        #endregion

        #region Html settings

        public ActionResult HtmlSettings(int activeWidgetId)
        {
            using (var htmlWidgetRepository = new HtmlWidgetRepository())
            {
                var htmlWidgetInfo = htmlWidgetRepository.GetByActiveId(activeWidgetId);

                if (htmlWidgetInfo != null)
                {
                    return View(new EditHtmlWidgetModel
                    {
                        Html = htmlWidgetInfo.Text,
                        Title = htmlWidgetInfo.Title,
                        HtmlWidgetId = htmlWidgetInfo.HtmlWidgetId
                    });
                }
            }

            return View("HtmlSettings");
        }

        [HttpPost]
        public ActionResult HtmlSettings(EditHtmlWidgetModel editHtmlWidgetModel)
        {
            if (ModelState.IsValid)
            {
                using (var htmlWidgetRepository = new HtmlWidgetRepository())
                {
                    var htmlWidgetInfo = htmlWidgetRepository.GetById(editHtmlWidgetModel.HtmlWidgetId);

                    if (htmlWidgetInfo != null)
                    {
                        return View(new EditHtmlWidgetModel
                        {
                            Html = htmlWidgetInfo.Text,
                            Title = htmlWidgetInfo.Title,
                            HtmlWidgetId = htmlWidgetInfo.HtmlWidgetId
                        });
                    }

                    this.SetSuccess("De widget is succesvol gewijzigd.");
                    return RedirectToAction("Weergeven");
                }
            }

            DisplayError();
            return RedirectToAction("Weergeven");
        }
        #endregion

        #region Twitterfeed Settings

        public ActionResult TwitterSettings(int activeWidgetId)
        {
            using (var twitterWidgetRepository = new TwitterfeedRWidgetRepository())
            {
                var twitterWidgetInfo = twitterWidgetRepository.GetByActiveId(activeWidgetId);

                if (twitterWidgetInfo != null)
                {
                    return View(new EditTwitterModule
                    {
                        AmountTweets = twitterWidgetInfo.AmountTweets,
                        ConsumerKey = twitterWidgetInfo.ConsumerKey,
                        ConsumerSecret = twitterWidgetInfo.ConsumerSecret,
                        IncludeReplies = twitterWidgetInfo.IncludeReplies,
                        IncludeRetweets = twitterWidgetInfo.IncludeRetweets,
                        ReturnUrl = twitterWidgetInfo.ReturnUrl,
                        TwitterWidgetId = twitterWidgetInfo.TwitterFeedId,
                        Username = twitterWidgetInfo.Username
                    });
                }
            }

            return null;
        }

        [HttpPost]
        public ActionResult TwitterSettings(EditTwitterModule editTwitterModule)
        {
            if (ModelState.IsValid)
            {
                using (var twitterWidgetRepository = new TwitterfeedRWidgetRepository())
                {
                    var twitterWidgetInfo = twitterWidgetRepository.GetById(editTwitterModule.TwitterWidgetId);

                    if (twitterWidgetInfo != null)
                    {
                        twitterWidgetInfo.AmountTweets = editTwitterModule.AmountTweets;
                        twitterWidgetInfo.ConsumerKey = editTwitterModule.ConsumerKey;
                        twitterWidgetInfo.ConsumerSecret = editTwitterModule.ConsumerSecret;
                        twitterWidgetInfo.IncludeReplies = editTwitterModule.IncludeReplies;
                        twitterWidgetInfo.IncludeRetweets = editTwitterModule.IncludeRetweets;
                        twitterWidgetInfo.ReturnUrl = editTwitterModule.ReturnUrl;
                        twitterWidgetInfo.Username = editTwitterModule.Username;

                        twitterWidgetRepository.Save();

                        this.SetSuccess("De widget is succesvol gewijzigd.");
                        return RedirectToAction("Weergeven");
                    }
                }
            }

            DisplayError();
            return RedirectToAction("Weergeven");
        }

        #endregion

        #region Social Settings

        public ActionResult SocialSettings(int activeWidgetId)
        {
            using (var socialShareRepository = new ShareWidgetRepository())
            {
                var socialShareWidget = socialShareRepository.GetByActiveId(activeWidgetId);

                if (socialShareWidget != null)
                {
                    return View("ShareSettings", new SettingsSocialShareWidgetModel
                    {
                        HasEmail = socialShareWidget.HasEmail,
                        HasFacebook = socialShareWidget.HasFacebook,
                        HasGooglePlus = socialShareWidget.HasGooglePlus,
                        HasLinkedIn = socialShareWidget.HasLinkedIn,
                        HasPinterest = socialShareWidget.HasPinterest,
                        HasTwitter = socialShareWidget.HasTwitter,
                        ShareId = socialShareWidget.ShareId
                    });
                }
            }

            return null;
        }

        [HttpPost]
        public ActionResult SocialSettings(SettingsSocialShareWidgetModel settingsSocialShareWidgetModel)
        {
            if (ModelState.IsValid)
            {
                using (var socialShareRepository = new ShareWidgetRepository())
                {
                    var socialShareWidget = socialShareRepository.GetById(settingsSocialShareWidgetModel.ShareId);

                    if (socialShareWidget != null)
                    {
                        socialShareWidget.HasEmail = settingsSocialShareWidgetModel.HasEmail;
                        socialShareWidget.HasFacebook = settingsSocialShareWidgetModel.HasFacebook;
                        socialShareWidget.HasGooglePlus = settingsSocialShareWidgetModel.HasGooglePlus;
                        socialShareWidget.HasLinkedIn = settingsSocialShareWidgetModel.HasLinkedIn;
                        socialShareWidget.HasPinterest = settingsSocialShareWidgetModel.HasPinterest;
                        socialShareWidget.HasTwitter = settingsSocialShareWidgetModel.HasTwitter;

                        socialShareRepository.Save();

                        this.SetSuccess("De widget is succesvol gewijzigd.");
                        return RedirectToAction("Weergeven");
                    }
                }
            }

            DisplayError();
            return RedirectToAction("Weergeven");
        }

        #endregion

        #region Photo Settings

        public ActionResult PhotoSettings(int activeWidgetId)
        {
            var returnModel = new EditPhotoWidget();

            using (var photoWidgetRepository = new PhotoWidgetRepository())
            {
                using (var photoAlbumRepository = new PhotoAlbumRepository())
                {
                    var photoWidget = photoWidgetRepository.GetByActiveId(activeWidgetId);

                    if (photoWidget == null) { return null;}

                    returnModel.Albums = GetAlbums(photoAlbumRepository.GetList().ToList());
                    returnModel.AlbumId = returnModel.Albums.FirstOrDefault() != null
                        ? int.Parse(returnModel.Albums.First().Value)
                        : 0;
                    returnModel.AmountItems = photoWidget.AmountItems;
                    returnModel.Slide = photoWidget.IsSlide;
                    returnModel.PhotoWidgetId = photoWidget.Id;


                    if (returnModel.Albums.Any())
                    {
                        returnModel.Photos = new PhotoModel
                        {
                            Photos = GetPhotos(returnModel.AlbumId),
                            SelectedPhotos = GetSelectedPhotos(photoWidget.Id)
                        };
                    }
                }
            }

            return View(returnModel);
        }

        [HttpPost]
        public ActionResult PhotoSettings(EditPhotoWidget editPhotoWidget)
        {
            if (ModelState.IsValid)
            {
                using (var photoWidgetRepository = new PhotoWidgetRepository())
                {
                    using (var activeWidgetRepository = new ActiveWidgetRepository())
                    {
                        using (var themeSectionRepository = new ThemeSectionRepository())
                        {
                            var photoWidget = photoWidgetRepository.GetById(editPhotoWidget.PhotoWidgetId);

                            var activeWidget = activeWidgetRepository.GetById(photoWidget.ActiveWidgetId);

                            var themeSection = themeSectionRepository.GetById(activeWidget.SectionId);

                            photoWidget.IsSlide = editPhotoWidget.Slide;
                            photoWidget.AmountItems = editPhotoWidget.AmountItems;

                            photoWidgetRepository.Save();

                            LogProvider.LogAction(string.Format("De foto widget: {0} op {1} is succesvol gewijzigd", editPhotoWidget.PhotoWidgetId, GetDisplayPageName(themeSection)));
                            this.SetSuccess("De widget is succesvol gewijzigd");
                        }
                    }
                }
            }



            return RedirectToAction("Weergeven");
        }

        private string GetDisplayPageName(ThemeSection themeSection)
        {
            string pageName;

            if (themeSection.Controller == "*")
            {
                pageName = "elke pagina";
            }
            else if (themeSection.Action == "*")
            {
                pageName = "elke pagina in: " + themeSection.Controller;
            }
            else
            {
                pageName = string.Format("de pagina {0}-{1}", themeSection.Action, themeSection.Controller);
            }
            return pageName;
        }

        public ActionResult DeleteAndReloadPhotos(int photoId, int albumId, int photoWidgetId)
        {
            using (var albumInPhotoWidgetRepository = new AlbumInPhotoWidgetRepository())
            {
                var photoInAlbumWidget = albumInPhotoWidgetRepository.GetByPhotoAndWidgetId(photoId, photoWidgetId);

                albumInPhotoWidgetRepository.Remove(photoInAlbumWidget);

                albumInPhotoWidgetRepository.Save();
            }

            return View("Shared/Photos", new PhotoModel
            {
                Photos = GetPhotos(albumId),
                SelectedPhotos = GetSelectedPhotos(photoWidgetId)
            });
        }

        public ActionResult AddPhotoAndRefreshPhotos(int photoId, int photoWidgetId)
        {
            int albumId;

            using (var albumInPhotoWidgetRepository = new AlbumInPhotoWidgetRepository())
            {
                using (var photoRepository = new PhotosInAlbumRepository())
                {
                    var photoInfo = photoRepository.GetById(photoId);

                    albumId = photoInfo.FotoAlbumID;

                    albumInPhotoWidgetRepository.Add(new AlbumInPhotoWidget
                    {
                        AlbumId = photoInfo.FotoAlbumID,
                        PhotoId = photoId,
                        PhotoWidgetId = photoWidgetId
                    });

                    albumInPhotoWidgetRepository.Save();
                }
            }

            return View("Shared/Photos", new PhotoModel
            {
                Photos = GetPhotos(albumId),
                SelectedPhotos = GetSelectedPhotos(photoWidgetId)
            });
        }

        public ActionResult GetPhotosByAlbumId(int albumId, int photoWidgetId)
        {
            return View("Shared/Photos", new PhotoModel
            {
                Photos = GetPhotos(albumId),
                SelectedPhotos = GetSelectedPhotos(photoWidgetId)
            });
        }

        private IEnumerable<PhotoEditModel> GetPhotos(int albumId)
        {
            var photoCollection = new Collection<PhotoEditModel>();

            using (var photoRepository = new PhotosInAlbumRepository())
            {
                foreach (var photo in photoRepository.GetPhotosFromAlbum(albumId))
                {
                    photoCollection.Add(new PhotoEditModel
                    {
                        Photo = CreateSelectListItem(photo.Naam, photo.FotoID.ToString(CultureInfo.InvariantCulture)),
                        AlbumId = photo.FotoAlbumID
                    });
                }
            }

            return photoCollection;
        }

        private SelectListItem CreateSelectListItem(string text, string value)
        {
            return new SelectListItem
            {
                Value = value,
                Text = text
            };
        }

        private IEnumerable<SelectListItem> GetAlbums(IEnumerable<Fotoalbum> listPhotoAlbums)
        {
            var returnCollection = new Collection<SelectListItem>();

            foreach (var photoAlbum in listPhotoAlbums)
            {
                returnCollection.Add(CreateSelectListItem(photoAlbum.Titel,
                    photoAlbum.AlbumID.ToString(CultureInfo.InvariantCulture)));
            }

            return returnCollection;
        }

        public IEnumerable<PhotoEditModel> GetSelectedPhotos(int photoWidgetId)
        {
            var photoCollection = new Collection<PhotoEditModel>();

            using (var albumInPhotoWidgetRepository = new AlbumInPhotoWidgetRepository())
            {
                using (var photoRepository = new PhotosInAlbumRepository())
                {
                    var listPhotos = albumInPhotoWidgetRepository.GetByPhotoWidget(photoWidgetId);

                    foreach (var photo in listPhotos)
                    {
                        photoCollection.Add(new PhotoEditModel
                        {
                            Photo = CreateSelectListItem(photoRepository.GetById(photo.PhotoId).Naam, photo.PhotoId.ToString(CultureInfo.InvariantCulture)),
                            AlbumId = photo.AlbumId
                        });
                    }
                }
            }

            return photoCollection;
        }
        #endregion

        //public ActionResult Toevoegen()
        //{
        //    return View();
        //}


        //[HttpPost]
        //public ActionResult Toevoegen(AddWidgetModel addWidgetModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var installWidgetController = new Install();

        //        string widgetFriendlyName = installWidgetController.Launch(addWidgetModel.WidgetPostedFileBase);

        //        this.SetSuccess("De widget is succesvol geïnstalleerd!");

        //        LogProvider.LogAction("De widget " + widgetFriendlyName + " is geïnstalleerd.");

        //        return RedirectToAction("Weergeven");
        //    }
        //    return View(addWidgetModel);
        //}
    }
}
