﻿#region

using System;
using System.IO;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Common;
using Common.Models.Admin.Users;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class GebruikersController : ApplicationController
    {
        //
        // GET: /Gebruikers/

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        public ActionResult _Weergeven()
        {
            return View(UserProvider.GetAllUsers());
        }

         [RolePermission(permissionName = RolePermission.PermissionName.User)]
        public ActionResult Weergeven()
        {
            return View();
        }

         [RolePermission(permissionName = RolePermission.PermissionName.Gastenboek)]
        public ActionResult Verwijder(string username)
        {
            var userRepository = new GebruikersRepository();

            var membershipInfo = UserProvider.GetMembership(username);

            var userInfo = userRepository.GetById(membershipInfo.ProviderUserKey);
            
            // Delete user
            userRepository.Remove(userInfo);

            // Delete membership
            System.Web.Security.Membership.DeleteUser(username, true);

            LogProvider.LogAction(
                string.Format("De gebruiker {0} is verwijderd",
                    userInfo.Voornaam + " " + userInfo.Achternaam));

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        public ActionResult Toevoegen()
        {
            var gebruikersModel = new GebruikersModel();
            var userRepository = new GebruikersRepository();

            gebruikersModel.LandenList = userRepository.GetLanden();
            gebruikersModel.BeveilingsVragenList = userRepository.GetBeveilingsVragen();
            gebruikersModel.GroepenList = userRepository.GetGroepen();

            return View(gebruikersModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        [HttpPost]
        public ActionResult Toevoegen(GebruikersModel gebruikersModel)
        {
            var userRepository = new GebruikersRepository();

            if (ModelState.IsValid)
            {
                UserProvider.AddUser(gebruikersModel.Voornaam, gebruikersModel.Achternaam,
                    gebruikersModel.Email, gebruikersModel.Straat, (int) gebruikersModel.Huisnummer,
                    gebruikersModel.AdresToevoeging, gebruikersModel.Woonplaats, gebruikersModel.Postcode,
                    gebruikersModel.LandId, gebruikersModel.Gebruikersnaam, gebruikersModel.Wachtwoord,
                    userRepository.GetRegistrationQuestion(gebruikersModel.BeveilingsVraagId),
                    gebruikersModel.BeveilingsAntwoord, gebruikersModel.GroepNaam, gebruikersModel.ProfilePhoto, gebruikersModel.SubscriptionNewsletter, true);

                LogProvider.LogAction(
                    string.Format("De gebruiker {0} is toegevoegd",
                        gebruikersModel.Voornaam + " " + gebruikersModel.Achternaam));

                this.SetSuccess("De nieuwe gebruiker is succesvol toegevoegd.");

                return RedirectToAction("Weergeven");
            }


            gebruikersModel.LandenList = userRepository.GetLanden(gebruikersModel.LandId);
            gebruikersModel.BeveilingsVragenList = userRepository.GetBeveilingsVragen(gebruikersModel.BeveilingsVraagId);
            gebruikersModel.GroepenList = userRepository.GetGroepen(gebruikersModel.GroepNaam);

            this.SetError("De gebruiker is niet toegevoegd, controleer uw velden.");

            return View(gebruikersModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        public ActionResult Wijzig(string username)
        {
            // Generate view model
            var bundleModel = new BundleGebruikersPasswordModel
            {
                WijzigGebruikersModel = new WijzigGebruikersModel(),
                PasswordModel = new PasswordModel(),
                ProfilePhotoModel = new ProfilePhotoModel()
            };

            // Get user repository
            var userRepository = new GebruikersRepository();

            // Get specific user
            UserModelInfo userInformation = UserProvider.GetUserInfo(username);

            // Fill model fields
            bundleModel.WijzigGebruikersModel.GebruikersNaam = userInformation.Username;
            bundleModel.WijzigGebruikersModel.Voornaam = userInformation.Firstname;
            bundleModel.WijzigGebruikersModel.Achternaam = userInformation.Lastname;
            bundleModel.WijzigGebruikersModel.AdresToevoeging = userInformation.AdressExtra;
            bundleModel.WijzigGebruikersModel.Huisnummer = userInformation.HouseNumber;
            bundleModel.WijzigGebruikersModel.LandId = userInformation.Land;
            bundleModel.WijzigGebruikersModel.Woonplaats = userInformation.Residence;
            bundleModel.WijzigGebruikersModel.Straat = userInformation.Street;
            bundleModel.WijzigGebruikersModel.Postcode = userInformation.Postcode;
            bundleModel.WijzigGebruikersModel.LandenList = userRepository.GetLanden(userInformation.Land);
            bundleModel.WijzigGebruikersModel.GroepenList = userRepository.GetGroepen(userInformation.RoleName);
            bundleModel.WijzigGebruikersModel.Email = userInformation.Email;
            bundleModel.PasswordModel.Gebruikersnaam = userInformation.Username;
            bundleModel.WijzigGebruikersModel.HasAvatar = userInformation.HasAvatar;

            bundleModel.WijzigGebruikersModel.GroepenList =
                userRepository.GetGroepen(bundleModel.WijzigGebruikersModel.GroepenNaam);

            bundleModel.ProfilePhotoModel.Username = username;

            return View(bundleModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        [HttpPost]
        public ActionResult Wijzig(WijzigGebruikersModel wijzigGebruikersModel)
        {
            var userRepository = new GebruikersRepository();

            if (ModelState.IsValid)
            {
                UserProvider.UpdateUser(wijzigGebruikersModel.Voornaam, wijzigGebruikersModel.Achternaam,
                    wijzigGebruikersModel.Email, wijzigGebruikersModel.Straat, (int) wijzigGebruikersModel.Huisnummer,
                    wijzigGebruikersModel.AdresToevoeging, wijzigGebruikersModel.Woonplaats,
                    wijzigGebruikersModel.Postcode, wijzigGebruikersModel.LandId, wijzigGebruikersModel.ProfilePhoto, wijzigGebruikersModel.GebruikersNaam, wijzigGebruikersModel.SubscriptionNewsletter);

                // Remove user action log
                if (wijzigGebruikersModel.GroepenNaam.Equals("Leden", StringComparison.CurrentCultureIgnoreCase))
                {
                    var userActionLogRepository = new UserActionLogRepository();
                    if (
                        userActionLogRepository.HasActionLog(
                            UserProvider.GetProviderUserKey(wijzigGebruikersModel.GebruikersNaam)))
                    {
                        var userActionLogInfo =
                            userActionLogRepository.GetFirst(
                                UserProvider.GetProviderUserKey(wijzigGebruikersModel.GebruikersNaam));
                        userActionLogRepository.Remove(userActionLogInfo);
                        userActionLogRepository.Save();
                    }
                }
                    // Add user action log
                else
                {
                    var userActionLogRepository = new UserActionLogRepository();
                    var actionLogRepository = new ActionLogRepository();
                    if (
                        !userActionLogRepository.HasActionLog(
                            UserProvider.GetProviderUserKey(wijzigGebruikersModel.GebruikersNaam)))
                    {
                        userActionLogRepository.Add(new UserActionLog
                        {
                            ActionLogId = actionLogRepository.GetHighestActionId(),
                            UserId = UserProvider.GetProviderUserKey(wijzigGebruikersModel.GebruikersNaam)
                        });

                        userActionLogRepository.Save();
                    }
                }

                LogProvider.LogAction(
                    string.Format("De gebruiker {0} is gewijzigd",
                        wijzigGebruikersModel.Voornaam + " " + wijzigGebruikersModel.Achternaam));

                this.SetSuccess("De gebruiker is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            wijzigGebruikersModel.LandenList = userRepository.GetLanden(wijzigGebruikersModel.LandId);
            wijzigGebruikersModel.GroepenList = userRepository.GetGroepen(wijzigGebruikersModel.GroepenNaam);

            var bundleModel = new BundleGebruikersPasswordModel
            {
                WijzigGebruikersModel = wijzigGebruikersModel
            };

            this.SetError("De gebruiker is niet gewijzigd, controleer uw velden.");

            return View(bundleModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        [HttpPost]
        public ActionResult Wachtwoord(PasswordModel passwordModel)
        {
            if (ModelState.IsValid)
            {
                MembershipUser user = UserProvider.GetMembership(passwordModel.Gebruikersnaam);

                UserProvider.UnlockUser(user);
                UserProvider.UpdateMemberShip(user);

                string tmpPassword = UserProvider.ResetPassword(user);

                if (UserProvider.ChangePassword(tmpPassword, passwordModel.Wachtwoord))
                {
                    UserModelInfo userInfo = UserProvider.GetUserInfo(user.UserName);
                    LogProvider.LogAction(
                        string.Format("Het wachtwoord van de gebruiker {0} is gewijzigd",
                            userInfo.FullName));
                    this.SetSuccess("Het wachtwoord van de gebruiker is succesvol gewijzigd.");
                }
                else
                {
                    this.SetError("Het wachtwoord van de gebruiker is niet gewijzigd vanwege een onbekende reden.");
                }

                return RedirectToAction("Weergeven");
            }


            var bundleModel = new BundleGebruikersPasswordModel
            {
                WijzigGebruikersModel = new WijzigGebruikersModel(),
                PasswordModel = passwordModel
            };

            var userRepository = new GebruikersRepository();

            UserModelInfo userInformation = UserProvider.GetUserInfo(passwordModel.Gebruikersnaam);

            bundleModel.WijzigGebruikersModel.Voornaam = userInformation.Firstname;
            bundleModel.WijzigGebruikersModel.Achternaam = userInformation.Lastname;
            bundleModel.WijzigGebruikersModel.AdresToevoeging = userInformation.AdressExtra;
            bundleModel.WijzigGebruikersModel.Email = userInformation.Email;
            bundleModel.WijzigGebruikersModel.Huisnummer = userInformation.HouseNumber;
            bundleModel.WijzigGebruikersModel.LandId = userInformation.Land;
            bundleModel.WijzigGebruikersModel.Woonplaats = userInformation.Residence;
            bundleModel.WijzigGebruikersModel.Straat = userInformation.Street;
            bundleModel.WijzigGebruikersModel.Postcode = userInformation.Postcode;
            bundleModel.WijzigGebruikersModel.LandenList = userRepository.GetLanden(userInformation.Land);
            bundleModel.WijzigGebruikersModel.GroepenList = userRepository.GetGroepen(userInformation.RoleName);

            return View(bundleModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        public ActionResult Verbannen(string username)
        {
            var membership = UserProvider.GetMembership(username);
            membership.Comment = "";
            UserProvider.UpdateMemberShip(membership);
            UserProvider.LockUser(username, "Uw account is verbannen.");

            this.SetSuccess("De gebruiker is succesvol verbannen van de website.");

            LogProvider.LogAction(
                string.Format("De gebruiker {0} is verbannen",
                    UserProvider.GetFullName(UserProvider.GetProviderUserKey(username))));

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        public ActionResult Unbannen(string username)
        {
            this.SetSuccess("De gebruiker is niet meer verbannen van de website.");

            LogProvider.LogAction(
                string.Format("De gebruiker {0} is onverbannen.",
                    username));

            var membership = UserProvider.GetMembership(username);
            membership.Comment = "";
            UserProvider.UpdateMemberShip(membership);
            UserProvider.UnlockUser(username);

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.User)]
        [HttpPost]
        public ActionResult ProfielFoto(ProfilePhotoModel profilePhotoModel)
        {
            var userRepository = new GebruikersRepository();

            var membership = UserProvider.GetMembership(profilePhotoModel.Username);

            var userInfo = userRepository.GetById(membership.ProviderUserKey);

            string fullPath = Server.MapPath("~/ProfielFotos");

            if (profilePhotoModel.Photo != null)
            {
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
                profilePhotoModel.Photo.SaveAs(fullPath + "/" + membership.UserName + ".png");

                userInfo.HasAvatar = true;
            }

            if (userInfo.HasAvatar)
            {
                if (profilePhotoModel.Delete)
                {
                    userInfo.HasAvatar = false;
                    System.IO.File.Delete(fullPath + "/" + membership.UserName + ".png");
                }
            }

            userRepository.Save();

            this.SetSuccess("De profielfoto is gewijzigd.");

            LogProvider.LogAction(
                string.Format("De profielfoto van {0} is gewijzigd",
                   userInfo.Voornaam + " " + userInfo.Achternaam));

            return RedirectToAction("Weergeven");
        }
    }
}