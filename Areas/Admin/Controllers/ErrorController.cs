﻿#region

using System.Web.Mvc;
using Common.Models.Home.Error;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Admin/Error/

        public ActionResult Index(int errorCode)
        {
            var errorModel = new ErrorModel
            {
                ErrorCode = errorCode
            };

            switch (errorCode)
            {
                case 400:
                    {
                        errorModel.Message = "Er heeft een fout plaatsgevonden toen de pagina werd geladen.";
                        break;
                    }
                case 401:
                case 403:
                    {
                        errorModel.Message = "U bent niet gemachtigd om deze pagina te zien.";
                        break;
                    }
                case 404:
                    {
                        errorModel.Message = "De pagina die u zoekt bestaat niet.";
                        break;
                    }
                case 500:
                    {
                        errorModel.Message = "De server heeft een kritieke fout opgelopen.";
                        break;
                    }
                default:
                    {
                        errorModel.Message = "Er heeft zich een fout plaatsgevonden.";
                        break;
                    }
            }
            return View(errorModel);
        }

    }
}
