﻿#region

using System.Web.Mvc;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    internal interface IController
    {
        ActionResult Weergeven();

        ActionResult Wijzig(int id);

        ActionResult Verwijder(int id);
    }
}