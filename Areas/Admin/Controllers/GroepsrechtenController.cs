﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.Data;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;
using WebGrease.Css.Extensions;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class GroepsrechtenController : ApplicationController
    {

        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult _Weergeven()
        {
            return View(GetRolePermissions());
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult _EasyWeergave()
        {
            string[] roles = RoleProvider.GetAllRoles().Where(x => x.Contains("Leden") == false).ToArray();
            var rolePermissionModel = new List<RolePermissionModel>();
            var rolePermissionRepository = new GroepsRechtRepository();

            foreach (string role in roles)
            {
                var model = new RolePermissionModel {RoleName = role};

                foreach (RechtenGroepModel permission in rolePermissionRepository.GetRechtenFromGroepId(role))
                {
                    model.Rights.Add(permission.RechtNaam);
                }

                rolePermissionModel.Add(model);
            }

            return View(rolePermissionModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult EasyWeergave()
        {
            return View();
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult Weergeven(ViewRolePermissionsModel test)
        {
            var rightsRepository = new GroepsRechtRepository();
            IEnumerable<Children> permissionsModel = ProcessRoles(Request.Form);
            var permissionsList = rightsRepository.GetList();

            // Delete all permissions
            rightsRepository.RemoveAll(permissionsList);

            // Save permissions
            rightsRepository.Save();

            // Initialize new permission list
            var newPermissionsList = new Collection<GroepsRecht>();

            foreach (Children permissionItem in permissionsModel)
            {
                newPermissionsList.Add(new GroepsRecht
                {
                    RoleId = RoleProvider.GetRoleIdByName(permissionItem.RoleName),
                    RightID = permissionItem.RightId
                });
            }

            // Add new permissions
            rightsRepository.AddList(newPermissionsList);

            // Save new permissions
            rightsRepository.Save();

            // Add action log
            LogProvider.LogAction(
                string.Format("De groepsrechten zijn gewijzigd"));

            // Success message
            this.SetSuccess("De rechten zijn succesvol gewijzigd.");

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult Toevoegen()
        {
            var rightsRepository = new RightRepository();
            var model = new RightsModel {Rights = new List<Rights>()};

            var listRights = rightsRepository.GetList();

            foreach (Recht right in listRights)
            {
                var rightModel = new Rights
                {
                    RightName = right.RechtNaam,
                    RightId = right.RechtID
                };
                model.Rights.Add(rightModel);
            }

            return View(model);
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.Right)]
        public ActionResult Toevoegen(RightsModel rightsModel)
        {
            if (ModelState.IsValid)
            {
                // Check if the role doesn't exist yet
                if (RoleProvider.GetAllRoles().All(x => x.Contains(rightsModel.RoleName)) == false)
                {
                    RoleProvider.AddRole(rightsModel.RoleName, rightsModel.Rights);
                    LogProvider.LogAction(
                        string.Format("De groep {0} is toegevoegd", rightsModel.RoleName));

                    this.SetSuccess("De nieuwe rol is succesvol toegevoegd.");

                    return RedirectToAction("Weergeven");
                }
                ModelState.AddModelError("RoleName", "Deze rolnaam bestaat al.");
            }

            var rightsRepository = new RightRepository();
            var model = new RightsModel();
            var listRights = rightsRepository.GetList();

            foreach (Recht right in listRights)
            {
                var rightModel = new Rights
                {
                    RightName = right.RechtNaam,
                    RightId = right.RechtID
                };
                model.Rights.Add(rightModel);
            }
            return View(model);
        }

        #region Private Methods Weergeven

        private RightModel GetRight(Recht permission, IEnumerable<Recht> listRights)
        {
            var rightModel = new RightModel
            {
                RightId = permission.RechtID,
                RightName =
                    listRights.Where(x => x.RechtID == permission.RechtID).Select(x => x.RechtNaam).First()
            };
            return rightModel;
        }

        private Children GetChildrenPermission(string role, int rightId, Guid roleId)
        {
            var rolePermissionRepository = new GroepsRechtRepository();

            var item = new Children
            {
                RightId = rightId,
                RoleName = role,
                Selected = rolePermissionRepository.HasPemission(roleId, rightId)
            };

            return item;
        }

        private IEnumerable<ViewRolePermissionsModel> GetRolePermissions()
        {
            // Initialize return model
            var arrayReturnModel = new Collection<ViewRolePermissionsModel>();

            // Initialize rights repository
            var rightRepository = new RightRepository();

            // Get rights
            var listRights = rightRepository.GetList().ToList();

            // Get all roles that don't include users
            string[] roles = RoleProvider.GetAllRoles().Where(x => x.Contains("Leden") == false).ToArray();

            // For each role
            roles.ForEach(role =>
            {
                // Initialize model
                var model = new ViewRolePermissionsModel
                {
                    Permissions = new Permissions { RoleName = role },
                    Children = new List<Children>()
                };

                // Get role id
                Guid roleId = RoleProvider.GetRoleIdByName(role);

                // Get permission
                foreach (Recht permission in listRights)
                {
                    model.Children.Add(GetChildrenPermission(role, permission.RechtID, roleId));

                    model.Permissions.Rights.Add(GetRight(permission, listRights));
                }

                // Add model to list
                arrayReturnModel.Add(model);
            });

            return arrayReturnModel;
        }

        #endregion

        #region Private Methods Weergeven POST

        private IEnumerable<Children> ProcessRoles(NameValueCollection requestForm)
        {
            String[] allKeys = requestForm.AllKeys;

            var listItems = new List<Children>();
            var itemModel = new Children();
            int counter = 0;

            foreach (String localKey in allKeys)
            {
                string itemValue = requestForm.GetValues(localKey).FirstOrDefault();
                switch (counter)
                {
                    case 0:
                        {
                            itemModel = new Children { Selected = Convert.ToBoolean(itemValue) };
                            break;
                        }
                    case 1:
                        {
                            itemModel.RoleName = itemValue;
                            break;
                        }
                    case 2:
                        {
                            if (itemModel.Selected)
                            {
                                itemModel.RightId = Convert.ToInt16(itemValue);
                                listItems.Add(itemModel);
                            }
                            counter = -1;
                            break;
                        }
                }

                counter++;
            }
            return listItems;
        }

        #endregion
    }
}