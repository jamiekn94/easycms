﻿#region

using System.Web.Mvc;
using Common.Providers.Routes;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class ApplicationController : Controller
    {
       // private Stopwatch _stopwatch;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Response.BufferOutput = true;

            #region Check security

            if (!filterContext.RequestContext.HttpContext.Request.IsAjaxRequest() && filterContext.IsChildAction == false)
            {
                #region GetSeo
                var seoRepository = new SeoPageRepository();

                ViewData["SeoTitle"] = seoRepository.GetAdminSiteTitle(RouteProvider.GetController(),
                    RouteProvider.GetAction());
                #endregion

            }

            #endregion

            base.OnActionExecuting(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //if (!filterContext.RequestContext.HttpContext.Request.IsAjaxRequest() &&
            //    filterContext.IsChildAction == false)
            //{
            //    _stopwatch.Stop();

            //    Debug.WriteLine("Server tïjd: " + _stopwatch.Elapsed.Seconds + " - " + _stopwatch.Elapsed.Milliseconds);
            //}
         
        }
    }
}