﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Common.Models.Admin.Themes;
using Common.Models.Admin.Themes.Edit;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Routes;
using Common.Repositories;
using Theme;
using WebGrease.Css.Extensions;

namespace Kapsters.Areas.Admin.Controllers
{
    public class ThemesController : ApplicationController
    {
        //
        // GET: /Admin/Themes/

        #region Installed & Change themes public methods

        [RolePermission(permissionName = RolePermission.PermissionName.Thema)]
        public ActionResult _Weergeven()
        {
            var themeRepository = new ThemeRepository();
            var configurationRepository = new ConfiguratieRepository();

            var configurationInfo = configurationRepository.GetById(1);

            var returnModel = new ViewThemesModel
            {
                Themes = themeRepository.GetList(),
                ActiveThemeId = configurationInfo.ThemeId
            };

            return View(returnModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Thema)]
        public ActionResult EnableTryTheme(string name)
        {
            using (var themeRepository = new ThemeRepository())
            {
                var themeInfo = themeRepository.GetByName(name);

                if (themeInfo != null)
                {
                    Common.Providers.Cookie.Cookie.UpdateCookie(ThemeConfiguration.CookieName, name);
                }
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Thema)]
        public ActionResult DisableTryTheme(string name)
        {
            using (var themeRepository = new ThemeRepository())
            {
                var themeInfo = themeRepository.GetByName(name);

                if (themeInfo != null)
                {
                    Common.Providers.Cookie.Cookie.RemoveCookie(ThemeConfiguration.CookieName);
                }
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Thema)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Thema)]
        public ActionResult Activate(int id)
        {
            var configurationRepository = new ConfiguratieRepository();
            var themeRepository = new ThemeRepository();

            var themeInfo = themeRepository.GetById(id);

            if (themeInfo != null)
            {
                var configurationInfo = configurationRepository.GetById(1);

                configurationInfo.ThemeId = themeInfo.ThemeId;
                configurationRepository.Save();

                configurationRepository.UpdateCachedThemeLocation();

                LogProvider.LogAction(string.Format("Het actieve thema is gewijzigd naar {0}", themeInfo.Name));

                this.SetSuccess("De actieve thema is succesvol gewijzigd.");
            }
            else
            {
                this.SetError("De actieve thema kon niet gewijzigd worden want de geselecteerde thema bestaat niet meer.");
            }
            return RedirectToAction("Weergeven");
        }

        #endregion

        #region Edit content Public Methods

        public ActionResult Wijzig()
        {
            return RedirectToAction("Stylesheets");
        }

        public ActionResult Stylesheets()
        {
            return View("Wijzig", GetBundleViewModel("*.css", string.Format("Themes/{0}/Content/css", GetCurrentTheme()), "Stylesheets"));
        }

        [HttpPost]
        public ActionResult Stylesheets(string selectedPath)
        {
            return View("Wijzig", GetBundleViewModel(selectedPath, "*.css", string.Format("Themes/{0}/Content/css", GetCurrentTheme()), "Stylesheets"));
        }

        public ActionResult Structuur()
        {
            return View("Wijzig", GetBundleViewModel("*.cshtml", string.Format("Themes/{0}/Views", GetCurrentTheme()), "Structuur"));
        }

        [HttpPost]
        public ActionResult Structuur(string selectedPath)
        {
            return View("Wijzig", GetBundleViewModel(selectedPath, "*.cshtml", string.Format("Themes/{0}/Views", GetCurrentTheme()), "Structuur"));
        }

        public ActionResult Javascripts()
        {
            return View("Wijzig", GetBundleViewModel("*.js", string.Format("Themes/{0}/Content/js", GetCurrentTheme()), "Javascripts"));
        }

        [HttpPost]
        public ActionResult Javascripts(string selectedPath)
        {
            return View("Wijzig", GetBundleViewModel(selectedPath, "*.js", string.Format("Themes/{0}/Content/js", GetCurrentTheme()), "Javascripts"));
        }

        public ActionResult Afbeeldingen()
        {
            return View("Wijzig", GetBundleViewModel("*.*", string.Format("Themes/{0}/Content/img", GetCurrentTheme()), "Afbeeldingen"));
        }

        [HttpPost]
        public ActionResult Afbeeldingen(string selectedPath)
        {
            return View("Wijzig", GetBundleViewModel(selectedPath, "*.*", string.Format("Themes/{0}/Content/img", GetCurrentTheme()), "Afbeeldingen"));
        }

        [HttpGet]
        public ActionResult EditFile()
        {
            return RedirectToAction("Wijzig");
        }

        [HttpPost]
        public ActionResult EditFile(EditFileModel EditFileModel)
        {
            if (EditFileModel.IsImage)
            {
                ValidateAndUpdateFileImage(EditFileModel.FilePath, EditFileModel.UploadedImage);
            }
            else
            {
                UpdateFileContent(EditFileModel.FilePath, EditFileModel.FileContent);    
            }

            LogProvider.LogAction(string.Format("Het bestand {0} is gewijzigd", GetFileName(EditFileModel.FilePath)));

            this.SetSuccess("Het bestand is succesvol gewijzigd.");

            return RedirectToAction("Stylesheets");
        }

        #endregion

        #region Edit Content Private Methods

        private string GetPathFolder(string foldername)
        {
            string pathFolder = Path.Combine(HttpRuntime.AppDomainAppPath, foldername);

            return pathFolder;
        }

        private void UpdateFileContent(string filePath, string fileContent)
        {
            ValidateFileExists(filePath);

            using (var streamWriter = new StreamWriter(filePath))
            {
                streamWriter.Write(fileContent);
            }
        }

        private void ValidateAndUpdateFileImage(string filePath, HttpPostedFileBase uploadedImage)
        {
            if (uploadedImage != null)
            {
                if (FileProvider.IsImage(uploadedImage.FileName))
                {
                    ValidateFileExists(filePath);

                    System.IO.File.Delete(filePath);

                    uploadedImage.SaveAs(filePath);
                }
                else
                {
                    this.SetError(string.Format("De afbeelding: {0} bevat een niet toegestane extensie", uploadedImage.FileName));
                }
            }
            else
            {
                this.SetError("U heeft geen afbeelding gekozen");
            }
        }

        private string GetFileContent(string path)
        {
            ValidateFileExists(path);
            string returnContent;

            using (var content = new StreamReader(path))
            {
                returnContent = content.ReadToEnd();
            }

            return returnContent;
        }

        private string GetCurrentTheme()
        {
            using (var configurationRepository = new ConfiguratieRepository())
            {
                return configurationRepository.GetCachedThemePath();
            }
        }

        private void ValidateFileExists(string path)
        {
            if (!System.IO.File.Exists(path))
            {
                throw new Exception(string.Format("Het pad {0} bestaat niet", path));
            }
        }

        private DateTime GetFileLastEditedDate(string path)
        {
            ValidateFileExists(path);

            return System.IO.File.GetLastWriteTime(path);
        }

        private IEnumerable<string> GetFilesInFolder(string pathFolder, string searchPattern)
        {
            return Directory.GetFiles(pathFolder, searchPattern, SearchOption.AllDirectories).ToArray();
        }

        private string GetFileName(string path)
        {
            ValidateFileExists(path);

            return Path.GetFileName(path);
        }

        private FileTabModel[] GetFileTabs(IEnumerable<string> arrayFilePaths)
        {
            var listFileTabModel = new Collection<FileTabModel>();

            foreach (var filepath in arrayFilePaths)
            {
                listFileTabModel.Add(new FileTabModel
                {
                    FilePath = filepath,
                    Name = GetFileName(filepath)
                });
            }

            return listFileTabModel.ToArray();
        }

        private FileTabModel[] GetFileTabs(IEnumerable<string> arrayFilePaths, bool isView)
        {
            var listFileTabModel = new Collection<FileTabModel>();

            foreach (var filepath in arrayFilePaths)
            {
                var directoryInfo = new DirectoryInfo(filepath);

                listFileTabModel.Add(new FileTabModel
                {
                    FilePath = filepath,
                    Name = string.Format("{0} - {1}", directoryInfo.Parent.Name, GetFileName(filepath))
                });
            }

            return listFileTabModel.ToArray();
        }

        private BundleViewModel GetBundleViewModel(string selectedFilePath, string searchPattern, string pathToFiles, string action)
        {
            string stylesheetsPath = GetPathFolder(pathToFiles);

            var arrayFilePaths =
                GetFilesInFolder(stylesheetsPath, searchPattern)
                    .OrderByDescending(file => file == selectedFilePath)
                    .ToArray();

            var bundleModel = new BundleViewModel
            {
                ArrayFileTabs = action.Equals("Structuur") ? GetFileTabs(arrayFilePaths, true) : GetFileTabs(arrayFilePaths),
                BundleEditFileModel = new BundleEditFileModel
                {
                    EditFileModel = new EditFileModel
                    {
                        IsImage = action.Equals("Afbeeldingen", StringComparison.CurrentCultureIgnoreCase)
                    },
                    FileModel = GetFileModel(arrayFilePaths.First(filePath => filePath.Equals(selectedFilePath)))
                },
                ActiveAction = action
            };

            GetEditFileModel(ref bundleModel, arrayFilePaths, action);

            return bundleModel;
        }

        private BundleViewModel GetBundleViewModel(string searchPattern, string pathToFiles, string action)
        {
            string filesPath = GetPathFolder(pathToFiles);

            var arrayFilePaths = GetFilesInFolder(filesPath, searchPattern).ToArray();

            var bundleModel = new BundleViewModel
            {
                ArrayFileTabs = action.Equals("Structuur") ? GetFileTabs(arrayFilePaths, true) : GetFileTabs(arrayFilePaths),
                BundleEditFileModel = new BundleEditFileModel
                {
                    EditFileModel = new EditFileModel
                    {
                        IsImage = action.Equals("Afbeeldingen", StringComparison.CurrentCultureIgnoreCase)
                    }
                },
                ActiveAction = action
            };

            GetEditFileModel(ref bundleModel, arrayFilePaths, action);

            return bundleModel;
        }

        private void GetEditFileModel(ref BundleViewModel bundleModel, string[] arrayFilePaths, string action)
        {
            if (arrayFilePaths.Length > 0)
            {
                bundleModel.BundleEditFileModel.FileModel = new FileModel
                {
                    Content = !action.Equals("Afbeeldingen", StringComparison.CurrentCultureIgnoreCase)? GetFileContent(bundleModel.ArrayFileTabs[0].FilePath) : null,
                    FilePath = bundleModel.ArrayFileTabs[0].FilePath,
                    LastEdited = GetFileLastEditedDate(bundleModel.ArrayFileTabs[0].FilePath),
                    Name = GetFileName(bundleModel.ArrayFileTabs[0].FilePath)
                };

                bundleModel.BundleEditFileModel.EditFileModel.FileContent =
                    bundleModel.BundleEditFileModel.FileModel.Content;
                bundleModel.BundleEditFileModel.EditFileModel.FilePath =
                    bundleModel.BundleEditFileModel.FileModel.FilePath;

                if (action.Equals("Afbeeldingen"))
                {
                    bundleModel.BundleEditFileModel.EditFileModel.DisplayFilePath =
                        GetDisplayFilePath(bundleModel.BundleEditFileModel.EditFileModel.FilePath);
                }
            }
        }

        private string GetDisplayFilePath(string filePath)
        {
            string serverPath = AppDomain.CurrentDomain.BaseDirectory;

            return filePath.Replace(serverPath, "").Replace(@"\\", "/");
        }

        private FileModel GetFileModel(string filePath)
        {
            return new FileModel
            {
                Content = GetFileContent(filePath),
                Name = Path.GetFileName(filePath),
                LastEdited = GetFileLastEditedDate(filePath),
                FilePath = filePath
            };
        }

        #endregion

        //public ActionResult Toevoegen()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Toevoegen(AddThemeModel addThemeModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var themeInstaller = new Theme.Install();
        //        string themeName = themeInstaller.LaunchAndReturnThemeName(addThemeModel.PluginPostedFileBase);

        //        this.SetSuccess("Het thema is succesvol geïnstalleerd.");

        //        LogProvider.LogAction(string.Format("Het thema {0} is geïnstalleerd.", themeName));

        //        return RedirectToAction("Index");
        //    }

        //    return View(addThemeModel);
        //}
    }
}
