﻿#region

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using Common.Models.Admin.Configuration;
using Common.Models.Admin.ViewModel;
using Common.Providers.Caching;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    [RolePermission(permissionName = RolePermission.PermissionName.Configuration)]
    public class ConfiguratieController : ApplicationController
    {
        //
        // GET: /Configuratie/
        public ActionResult Index()
        {
            var configuratieRepository = new ConfiguratieRepository();
            var configuratieInfo = configuratieRepository.GetById(1);

            var modelConfiguratie = new ConfiguratieModel
            {
                Descriptie = configuratieInfo.Descriptie,
                EmailInfoAccount = configuratieInfo.EmailInfoAccount,
                EmailInfoSmtpHost = configuratieInfo.EmailInfoSMTPHost,
                EmailInfoSmtpPort = configuratieInfo.EmailInfoSMTPPort,
                EmailInfoSmtpSecure = configuratieInfo.EmailInfoSMTPSecure,
                EmailInfoWachtwoord = configuratieInfo.EmailInfoWachtwoord,
                EmailNoReplyAccount = configuratieInfo.EmailNoReplyAccount,
                EmailNoReplySmtpHost = configuratieInfo.EmailNoReplySMTPHost,
                EmailNoReplySmtpPort = configuratieInfo.EmailNoReplySMTPPort,
                EmailNoReplySmtpSecure = configuratieInfo.EmailNoReplySMTPSecure,
                EmailNoReplyWachtwoord = configuratieInfo.EmailNoReplyWachtwoord,
                GebruikerVerificatie = configuratieInfo.GebruikersVerificatie,
                HoofdTitel = configuratieInfo.HoofdTitel,
                Keywoorden = configuratieInfo.Keywoorden,
                ReactieBlog = configuratieInfo.ReactieBlog,
                ReactieNieuws = configuratieInfo.ReactieNieuws,
                Titel = configuratieInfo.Titel,
                WelkomEmail = configuratieInfo.WelkomEmail
            };

            return View(modelConfiguratie);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Configuration)]
        [HttpPost]
        public ActionResult Index(ConfiguratieModel configuratieModel)
        {
            if (ModelState.IsValid)
            {
                var configuratieRepository = new ConfiguratieRepository();
                var configuratieInfo = configuratieRepository.GetById(1);

                configuratieInfo.Descriptie = configuratieModel.Descriptie;
                configuratieInfo.EmailInfoAccount = configuratieModel.EmailInfoAccount;
                configuratieInfo.EmailInfoSMTPHost = configuratieModel.EmailInfoSmtpHost;
                configuratieInfo.EmailInfoSMTPPort = configuratieModel.EmailInfoSmtpPort;
                configuratieInfo.EmailInfoSMTPSecure = configuratieModel.EmailInfoSmtpSecure;
                configuratieInfo.EmailInfoWachtwoord = configuratieModel.EmailInfoWachtwoord;
                configuratieInfo.EmailNoReplyAccount = configuratieModel.EmailNoReplyAccount;
                configuratieInfo.EmailNoReplySMTPHost = configuratieModel.EmailNoReplySmtpHost;
                configuratieInfo.EmailNoReplySMTPPort = configuratieModel.EmailNoReplySmtpPort;
                configuratieInfo.EmailNoReplySMTPSecure = configuratieModel.EmailNoReplySmtpSecure;
                configuratieInfo.EmailNoReplyWachtwoord = configuratieModel.EmailNoReplyWachtwoord;
                configuratieInfo.GebruikersVerificatie = configuratieModel.GebruikerVerificatie;
                configuratieInfo.HoofdTitel = configuratieModel.HoofdTitel;
                configuratieInfo.Keywoorden = configuratieModel.Keywoorden;
                configuratieInfo.ReactieBlog = configuratieModel.ReactieBlog;
                configuratieInfo.ReactieNieuws = configuratieModel.ReactieNieuws;
                configuratieInfo.Titel = configuratieModel.Titel;
                configuratieInfo.WelkomEmail = configuratieModel.WelkomEmail;

                configuratieRepository.Save();

                // Delete caching
                DeleteCaching(configuratieModel.DeleteServerCache, configuratieModel.DeleteClientCache);

                LogProvider.LogAction(
                    "De configuratie is gewijzigd");

                this.SetSuccess("De configuratie is succesvol gewijzigd.");
            }

            return View(configuratieModel);
        }

        public ActionResult Statistieken()
        {
            return View(new BundleStatisticsModel
            {
                AmountGuestbookComments = GetAmountGuestbook(),
                AmountNewsletters = GetAmountNewsletter(),
                AmountRoles = GetAmountRoles(),
                BlogStats = GetBlog(),
                NewsStats = GetNews(),
                VisitsStats = GetVisitors(),
                UserStats = GetUsers(),
                FileStats = GetFileStats(),
                ComputerInfo = GetComputerInfo()
            });
        }

        public JsonResult GetVisitorsJson()
        {
            return Json(GetVisitorsGraphics(), JsonRequestBehavior.AllowGet);
        }

        #region Private Methods

        private IEnumerable<UserGraphicStatsModel> GetVisitorsGraphics()
        {
            var collectionGraphicModel = new Collection<UserGraphicStatsModel>();

            using (var statsVisitsRepository = new DashboardStatisticsRepository())
            {
                for (int counter = 0; 6 > counter; counter++)
                {
                    collectionGraphicModel.Add(new UserGraphicStatsModel
                    {
                        Maand = string.Format("{0:MMMM}", DateTime.Now.AddMonths(-counter)),
                        PageViews = statsVisitsRepository.GetList().Where(stats => stats.Date.Month == DateTime.Now.AddMonths(-counter).Month && stats.Date.Year == DateTime.Now.Year).Sum(stats => stats.Pageviews),
                        Visitors = statsVisitsRepository.GetList().Where(stats => stats.Date.Month == DateTime.Now.AddMonths(-counter).Month && stats.Date.Year == DateTime.Now.Year).Sum(stats => stats.Visitors)
                    });
                }
            }
            return collectionGraphicModel.Reverse();
        }

        private long SizeDiskSpaceInMb(DirectoryInfo directoryInfo)
        {
            FileInfo[] fis = directoryInfo.GetFiles();

            long size = fis.Sum(fi => fi.Length);

            DirectoryInfo[] dis = directoryInfo.GetDirectories();

            size += dis.Sum(di => SizeDiskSpaceInMb(di));

            return (size);
        }

        private ComputerInfoModel GetComputerInfo()
        {
            return new ComputerInfoModel
            {
                CpuUsage = GetTotalCpu(),
                InstalledMemoryAmount = GetInstalledRamMemory(),
                ProcessorName = GetProcessorName(),
                UsedMemoryAmount = GetRamMemoryUsed(),
                WindowsVersion = GetWindowsVersion(),
                AppCpuUsage = GetCpu()
            };
        }

        private int GetRamMemoryUsed()
        {
            var performanceCounter = new PerformanceCounter
            {
                CategoryName = "Process",
                CounterName = "Working Set",
                InstanceName = Process.GetCurrentProcess().ProcessName
            };

            return (int)performanceCounter.NextValue() / 1048576;
        }

        private int GetCpu()
        {
            try
            {
                var cpuCounter = new PerformanceCounter
                {
                    CategoryName = "Processor",
                    CounterName = "% Processor Time",
                    InstanceName = Process.GetCurrentProcess().ProcessName
                };

                return (int)cpuCounter.NextValue();
            }
            catch
            {
                return -1;
            }
        }

        private int GetTotalCpu()
        {
            var cpuCounter = new PerformanceCounter
            {
                CategoryName = "Processor",
                CounterName = "% Processor Time",
                InstanceName = "_Total"
            };

            CounterSample test1 = cpuCounter.NextSample();
            Thread.Sleep(1000); // wait some time
            CounterSample test2 = cpuCounter.NextSample();

            var value = CounterSample.Calculate(test1, test2);

            return (int) value;
        }

        private int GetInstalledRamMemory()
        {
            var test = (double)new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory / 1073741824;
            var test2 = Math.Round(test, 0);
            return (int)test2;
        }

        private string GetWindowsVersion()
        {
            return new Microsoft.VisualBasic.Devices.ComputerInfo().OSFullName;
        }

        private string GetProcessorName()
        {
            var searcher = new ManagementObjectSearcher(
    "select * from Win32_Processor");

            string processorName = (from ManagementBaseObject share in searcher.Get() select ((string)share.Properties["Name"].Value)).FirstOrDefault();

            var options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            processorName = regex.Replace(processorName, @" ");

            return processorName;
        }

        private long GetSizeDiskSpaceInMb()
        {
            var directoryInfo = new DirectoryInfo(FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Files));

            FileInfo[] fis = directoryInfo.GetFiles();

            long size = fis.Sum(fi => fi.Length);

            DirectoryInfo[] dis = directoryInfo.GetDirectories();

            size += dis.Sum(di => SizeDiskSpaceInMb(di));

            return (size / 1048576);
        }

        private FileStatsModel GetFileStats()
        {
            return new FileStatsModel
            {
                AmountFiles = Directory.GetFiles(FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Files)).Length,
                DirectorySize = GetSizeDiskSpaceInMb()
            };
        }

        private VisitsStatsModel GetVisitors()
        {
            using (var statsVisitsRepository = new DashboardStatisticsRepository())
            {
                return new VisitsStatsModel
                {
                    HitsAllTime = statsVisitsRepository.GetList().Sum(hits => hits.Pageviews),
                    HitsThisMonth =
                        statsVisitsRepository.GetList()
                            .Where(hits => hits.Date.Month == DateTime.Now.Month && hits.Date.Year == DateTime.Now.Year)
                            .Sum(hits => hits.Pageviews),
                    HitsThisWeek =
                        statsVisitsRepository.GetList()
                            .Where(hits => hits.Date >= GetDateStartWeek() && GetDateEndWeek() >= hits.Date)
                            .Sum(hits => hits.Pageviews),
                    HitsThisYear =
                        statsVisitsRepository.GetList()
                            .Where(hits => hits.Date.Year >= DateTime.Now.Year)
                            .Sum(hits => hits.Pageviews),
                    VisitsAllTime = statsVisitsRepository.GetList().Sum(hits => hits.Visitors),
                    VisitsThisMonth =
                        statsVisitsRepository.GetList()
                            .Where(hits => hits.Date.Month == DateTime.Now.Month && hits.Date.Year == DateTime.Now.Year)
                            .Sum(hits => hits.Visitors),
                    VisitsThisWeek =
                        statsVisitsRepository.GetList()
                            .Where(hits => hits.Date >= GetDateStartWeek() && GetDateEndWeek() >= hits.Date)
                            .Sum(hits => hits.Visitors),
                    VisitsThisYear =
                        statsVisitsRepository.GetList()
                            .Where(hits => hits.Date.Year >= DateTime.Now.Year)
                            .Sum(hits => hits.Visitors)
                };
            }
        }

        private UserStatsModel GetUsers()
        {
            using (var membershipRepository = new MembershipRepository())
            {
                return new UserStatsModel
                {
                    UsersAllTime = membershipRepository.GetList().Count(),
                    UsersLastWeek = membershipRepository.GetList().Count(user => user.CreateDate >= GetDateStartWeek() && GetDateEndWeek() >= user.CreateDate),
                    UsersThisMonth =
                        membershipRepository.GetList()
                            .Count(user => user.CreateDate >= DateTime.Now && user.CreateDate.Year == DateTime.Now.Year),
                    UsersThisYear =
                        membershipRepository.GetList().Count(user => user.CreateDate.Year == DateTime.Now.Year)
                };
            }
        }

        private int GetAmountGuestbook()
        {
            using (var guestbookRepository = new GastenboekRepository())
            {
                return guestbookRepository.GetList().Count();
            }
        }

        private ArticleStatsModel GetNews()
        {
            using (var newsRepository = new Common.Repositories.NieuwsRepository())
            {
                using (var newsCommentsRepository = new Common.Repositories.NewsCommentsRepository())
                {
                    return new ArticleStatsModel
                    {
                        AmountArticles = newsRepository.GetAmountArticles(),
                        AmountComments = newsCommentsRepository.GetList().Count()
                    };
                }
            }
        }

        private ArticleStatsModel GetBlog()
        {
            using (var blogRepository = new Common.Repositories.BlogRepository())
            {
                using (var blogCommentsRepository = new Common.Repositories.BlogReactionRepository())
                {
                    return new ArticleStatsModel
                    {
                        AmountArticles = blogRepository.GetList().Count(),
                        AmountComments = blogCommentsRepository.GetList().Count()
                    };
                }
            }
        }

        private DateTime GetDateStartWeek()
        {
            var currentDate = DateTime.Now;

            var daysAhead = (DayOfWeek.Sunday - (int)currentDate.DayOfWeek);

            currentDate = currentDate.AddDays((int)daysAhead);

            return currentDate;
        }

        private DateTime GetDateEndWeek()
        {
            var currentDate = DateTime.Now;

            var daysAhead = DayOfWeek.Saturday - (int)currentDate.DayOfWeek;

            currentDate = currentDate.AddDays((int)daysAhead);

            return currentDate;
        }

        private int GetAmountNewsletter()
        {
            using (var newsletterRepository = new Common.Repositories.NewsletterRepository())
            {
                return newsletterRepository.GetList().Count();
            }
        }

        private int GetAmountRoles()
        {
            return RoleProvider.GetAllRoles().Count();
        }

        #endregion

        #region Private Methods

        private void DeleteCaching(bool serverCache, bool clientCache)
        {
            if (clientCache)
            {
                ClientCachingProvider.RemoveAllClientCache();
            }

            if (serverCache)
            {
                ServerCachingProvider.RemoveAllServerCache();
            }
        }
        #endregion
    }
}