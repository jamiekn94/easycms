﻿#region

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using Common;
using Common.Models.Admin.Dashboard;
using Common.Models.Admin.Tickets;
using Common.Models.Admin.Users;
using Common.Models.Admin.ViewModel;
using Common.Providers.Caching;
using Common.Providers.Date;
using Common.Providers.Email;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;
using DevTrends.MvcDonutCaching;
using Kapsters.Areas.Admin.Tasks;
using AdminNavigation = Kapsters.Areas.Admin.Functionality.Navigation.AdminNavigation;
using HttpContext = System.Web.HttpContext;
using Membership = System.Web.Security.Membership;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class DashboardController : ApplicationController
    {

        public ActionResult _Index()
        {
            var bundleModel = new BundleDashboardModel();

            AddTickets(ref bundleModel);

            AddBlog(ref bundleModel);

            AddGuestbook(ref bundleModel);

            AddNews(ref bundleModel);

            AddUsers(ref bundleModel);

            SetPermissionsAgenda(ref bundleModel);

            return View(bundleModel);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactDashboardModel contactDashboardModel)
        {
            if (ModelState.IsValid)
            {
                var email = new Email();
                email.SendEmail("info@jamieknoef.nl", "Contactformulier CMS - " + contactDashboardModel.Subject,
                    contactDashboardModel.Message, true);
            }
            return RedirectToAction("Index");
        }

        #region Index Functions

        private void AddTickets(ref BundleDashboardModel bundleModel)
        {
            var rightRepository = new RightRepository();

            var ticketRight = rightRepository.GetById(11);

            // Get tickets
            if (ticketRight.Status && RoleProvider.HasRight(11, RoleProvider.GetRoleId()))
            {
                bundleModel.TicketsReactions = GetTicketsReactions();
                bundleModel.HasTicketPermission = true;
            }
        }

        private void AddBlog(ref BundleDashboardModel bundleModel)
        {
            var rightRepository = new RightRepository();

            // Get blog
            var blogRight = rightRepository.GetById(1);

            if (blogRight.Status && RoleProvider.HasRight(1, RoleProvider.GetRoleId()))
            {
                bundleModel.BlogReactions = GetBlogReactions();
                bundleModel.HasBlogPermission = true;
            }
        }

        private void AddGuestbook(ref BundleDashboardModel bundleModel)
        {
            var rightRepository = new RightRepository();

            // Get guestbook
            var guestbookRight = rightRepository.GetById(13);

            if (guestbookRight.Status && RoleProvider.HasRight(13, RoleProvider.GetRoleId()))
            {
                bundleModel.HasGuestbookPermission = true;
                bundleModel.GuestbookReactions = GetGuestbookReactions();
            }
        }

        private void AddNews(ref BundleDashboardModel bundleModel)
        {
            var rightRepository = new RightRepository();

            // Get news
            var newsRight = rightRepository.GetById(9);

            if (newsRight.Status && RoleProvider.HasRight(9, RoleProvider.GetRoleId()))
            {
                bundleModel.NewsReactions = GetNewsReactions();
                bundleModel.HasNewsPermission = true;

                var newsRepository = new NieuwsRepository();

                // Load news
                List<Nieuwsartikel> listTableNews =
                    newsRepository.GetList().Take(5).OrderByDescending(x => x.ArtikelID).ToList();

                foreach (Nieuwsartikel tableNews in listTableNews)
                {
                    bundleModel.NewsModels.Add(new NieuwsModel
                    {
                        AuteurNaam = UserProvider.GetFullName(tableNews.AuteurId),
                        Informatie = tableNews.Informatie,
                        AantalReacties = tableNews.NieuwsReacties.Count,
                        Titel = tableNews.Titel,
                        ArtikelId = tableNews.ArtikelID,
                        Datum = tableNews.Datum
                    });
                }

            }
        }

        private void AddUsers(ref BundleDashboardModel bundleModel)
        {
            var rightRepository = new RightRepository();

            // Get users
            var usersRight = rightRepository.GetById(5);

            if (usersRight.Status && RoleProvider.HasRight(6, RoleProvider.GetRoleId()))
            {
                bundleModel.UsersAwaiting = GetAmountUsers();

                // Load users
                bundleModel.UsersModels = UserProvider.GetAllUsers(5).OrderByDescending(x => x.CreatedDate).ToList();

                bundleModel.HasUsersPermission = true;
            }
        }

        private void SetPermissionsAgenda(ref BundleDashboardModel bundleModel)
        {
            var rightRepository = new RightRepository();

            // Get users
            var agendaRight = rightRepository.GetById(17);

            if (agendaRight.Status && RoleProvider.HasRight(6, RoleProvider.GetRoleId()))
            {
                bundleModel.HasAgendaPermission = true;
            }
        }

        private int GetBlogReactions()
        {
            var blogReactionsRepository = new BlogReactionRepository();
            return blogReactionsRepository.GetAmountOpenComments();
        }

        private int GetNewsReactions()
        {
            var newsReactionsRepository = new NewsCommentsRepository();
            return newsReactionsRepository.GetAmountOpenComments();
        }

        private int GetGuestbookReactions()
        {
            var guestbookReactionsRepository = new GastenboekRepository();
            return guestbookReactionsRepository.GetAmountOpenComments();
        }

        private int GetTicketsReactions()
        {
            var ticketsRepository = new TicketRepository();
            return ticketsRepository.GetAmountOpenTickets();
        }

        private int GetAmountUsers()
        {
            var membershipRepository = new MembershipRepository();
            return membershipRepository.GetUnverifiedUsers();
        }
        #endregion

        #region Public Ajax Methods

        public JsonResult GetStatistics()
        {
            return Json(CreateStatistics(), JsonRequestBehavior.AllowGet);
        }

        public async Task<string> GetJamieKnoefNews()
        {
            string downloadedContent = string.Empty;

            return await Task<string>.Factory.StartNew(() =>
            {
                try
                {
                    WebRequest request = WebRequest.Create("http://jamieknoef.nl/CMSNews.html");
                    WebResponse response = request.GetResponse();
                    Stream data = response.GetResponseStream();

                    if (data != null)
                    {
                        using (var sr = new StreamReader(data))
                        {
                            downloadedContent = sr.ReadToEnd();
                        }
                    }
                }
                catch
                {
                    return null;
                }
                

                return downloadedContent;
            });
        }

        private IEnumerable<StatisticsDashboardModel> CreateStatistics()
        {
            var dashboardStatisticsRepository = new DashboardStatisticsRepository();
            var returnListStatistics = new Collection<StatisticsDashboardModel>();

            IQueryable<StatsVisit> listStatistics = dashboardStatisticsRepository.GetStatisticsLast7Days();

            foreach (StatsVisit statistic in listStatistics)
            {
                returnListStatistics.Add(new StatisticsDashboardModel
                {
                    Pageviews = statistic.Pageviews,
                    Visitors = statistic.Visitors,
                    Date = string.Format("{0:d MMMM}", statistic.Date)
                });
            }

            return returnListStatistics;
        }

        public void UpdateActionLog()
        {
            var userActionLogRepository = new UserActionLogRepository();
            var actionLogRepository = new ActionLogRepository();

            var userActionLogInfo = userActionLogRepository.GetFirst(UserProvider.GetUserId());
            userActionLogInfo.ActionLogId = actionLogRepository.GetHighestActionId();

            userActionLogRepository.Save();

            //     var cacheManager = new OutputCacheManager();
            //      cacheManager.RemoveItem("Dashboard", "GetActionLogs");
        }

        #endregion

        #region Topbar Actions


        [ChildActionOnly]
        //[DonutOutputCache(Duration = 900, VaryByParam = "none", VaryByCustom = "username",Location = OutputCacheLocation.Server)]
        public ActionResult GetActionLogs()
        {
            var actionLogRepository = new ActionLogRepository();

            var actionLogModel = new ActionLogModel
            {
                AmountNewActionLogs = actionLogRepository.GetAmountNewActionLogs(),
                ActionLogModels = actionLogRepository.GetNewActionLogs()
            };

            return PartialView("GetActionLogs", actionLogModel);
        }

        [ChildActionOnly]
        // [DonutOutputCache(Duration = 900, VaryByParam = "none", VaryByCustom = "username", Location = OutputCacheLocation.Server)]
        public ActionResult GetUsers()
        {
            // Get latest users
            var notApprovedUsersModels = new NotApprovedUsersModel();
            IEnumerable<UserModelInfo> notApprovedUsers =
                UserProvider.GetAllUsers(5)
                    .Where(x => x.IsAccepted == false)
                    .OrderByDescending(x => x.CreatedDate);

            foreach (var user in notApprovedUsers)
            {
                notApprovedUsersModels.Users.Add(new NotApprovedUserModel
                {
                    DateRegistration = user.CreatedDate,
                    Username = user.Username,
                    FullName = user.Firstname + " " + user.Lastname
                });
            }

            notApprovedUsersModels.AmountNotApprovedUsers = notApprovedUsers.Count();

            return PartialView("GetUsers", notApprovedUsersModels);
        }

        [ChildActionOnly]
        //    [DonutOutputCache(Duration = 900, VaryByParam = "none", VaryByCustom = "username", Location = OutputCacheLocation.Server)]
        public ActionResult GetTickets()
        {
            var rightRepository = new RightRepository();
            var ticketRight = rightRepository.GetById(11);

            var ticketModel = new NewTicketModel();

            if (ticketRight.Status)
            {
                // Get open tickets
                var ticketRepository = new TicketRepository();
                IEnumerable<Ticket> ticketList = ticketRepository.Get5OpenTickets();

                var userRepository = new GebruikersRepository();

                foreach (Ticket ticket in ticketList)
                {
                    var userInfo = userRepository.GetFirstAndLastName(ticket.TicketGebruikersId);

                    ticketModel.TicketModels.Add(new NewTicketArticleModel
                    {
                        Onderwerp = ticket.Onderwerp,
                        Voornaam = userInfo.Firstname,
                        Achternaam = userInfo.Lastname,
                        TimeAgo = DateProvider.GetNiceDate(ticket.DatumOpen),
                        TicketID = ticket.TicketID
                    });
                }

                ticketModel.AmountNewTickets = ticketRepository.GetAmountOpenTickets();

            }

            ticketModel.Status = ticketRight.Status;

            return PartialView("GetTickets", ticketModel);
        }

        [ChildActionOnly]
        public ActionResult GetRights()
        {
            var adminNavigationFunctionality = new AdminNavigation();

            return PartialView("Navigation", adminNavigationFunctionality.GetNavigation());
        }
        #endregion

    }
}