﻿#region

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Newsletter;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class NieuwsbriefController : ApplicationController
    {

        //
        // GET: /Nieuwsbrief/

        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult Verwijder(int id)
        {
            var newsletterRepository = new NewsletterRepository();
            var newsletterAttachmentsRepistory = new NewsletterAttachmentsRepository();

            // Delete attachments
            var attachmentsList = newsletterAttachmentsRepistory.GetAttachments(id);
            newsletterAttachmentsRepistory.RemoveAll(attachmentsList);
            newsletterAttachmentsRepistory.Save();

            // Delete newsletter
            var newsletterInfo = newsletterRepository.GetById(id);

            if (newsletterInfo != null)
            {
                newsletterRepository.Remove(newsletterInfo);
                newsletterRepository.Save();

                // Delete the folder
                Directory.Delete(FileProvider.GetImagePath(Common.Providers.Files.FilePaths.Newsletter) + newsletterInfo.NewsletterId, true);

                // Log
                LogProvider.LogAction(
                    string.Format("De nieuwsbrief #{0} is verwijderd", newsletterInfo.NewsletterId));
            }
            else
            {
                this.SetError("De nieuwsbrief is niet verwijderd want hij bestaat niet.");
            }

            this.SetSuccess("De nieuwsbrief is succesvol verwijderd.");

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult Wijzig(int id)
        {
            var newsletterRepository = new NewsletterRepository();

            var newsletterInfo = newsletterRepository.GetById(id);

            var editNewsletterModel = new EditNewsLetterModel
            {
                Subject = newsletterInfo.Subject,
                Body = newsletterInfo.Body,
                NewsletterId = newsletterInfo.NewsletterId,
                OldAttachments = GetAttachments(id),
                DateLaunch = newsletterInfo.ScheduledTime
            };

            return View(editNewsletterModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public void VerwijderBestand(int attachmentId)
        {
            var newsletterAttachmentsRepistory = new NewsletterAttachmentsRepository();
            var newsletterAttachment = newsletterAttachmentsRepistory.GetById(attachmentId);

            newsletterAttachmentsRepistory.Remove(newsletterAttachment);
            newsletterAttachmentsRepistory.Save();

            FileProvider.DeleteFile(FilePaths.Newsletter, newsletterAttachment.newsLetterId.ToString(CultureInfo.InvariantCulture), newsletterAttachment.fileName, true);

            LogProvider.LogAction(
                string.Format("Het nieuwsbrief bestand {0} is verwijderd", newsletterAttachment.fileName));
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult Wijzig(EditNewsLetterModel newsletterModel)
        {
            if (ModelState.IsValid)
            {
                var newsletterRepository = new NewsletterRepository();
                var newsletterInfo = newsletterRepository.GetById(newsletterModel.NewsletterId);

                newsletterInfo.Subject = newsletterModel.Subject;
                newsletterInfo.Body = newsletterModel.Body;

                newsletterRepository.Save();

                // Manage the schedule
                EditSchedule(newsletterModel, newsletterInfo);

                // Add files
                AddFiles(newsletterModel.UploadAttachments, newsletterInfo.NewsletterId);

                // Remove old files
                RemoveOldFiles(newsletterModel.OldAttachments);

                LogProvider.LogAction(
                    string.Format("De nieuwsbrief #{0} is gewijzigd", newsletterModel.NewsletterId));

                this.SetSuccess("De nieuwsbrief is succesvol gewijzigd!");

                return RedirectToAction("Weergeven");
            }

            // Load old attachments
            newsletterModel.OldAttachments = GetAttachments(newsletterModel.NewsletterId);

            return View(newsletterModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult _Weergeven()
        {
            var newsletterRepository = new NewsletterRepository();
            var newsletterAttachmentsRepistory = new NewsletterAttachmentsRepository();
            var newslettersList = newsletterRepository.GetList();
            var viewnewslettersList = new Collection<viewNewsletterModel>();

            foreach (Newsletter newsletter in newslettersList)
            {
                viewnewslettersList.Add(new viewNewsletterModel
                {
                    Subject = newsletter.Subject,
                    NewsletterId = newsletter.NewsletterId,
                    AmountAttachments = newsletterAttachmentsRepistory.GetAmountAttachments(newsletter.NewsletterId),
                    Status = newsletter.Status
                });
            }
            return View(viewnewslettersList);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult Toevoegen()
        {
            return View();
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.Nieuwsbrief)]
        public ActionResult Toevoegen(NewsletterModel newsletterModel)
        {
            if (ModelState.IsValid)
            {
                var newsletterRepository = new NewsletterRepository();

                var newsletterInfo = new Newsletter
                {
                    Body = newsletterModel.Body,
                    Status = false,
                    ScheduledTime = newsletterModel.DateLaunch,
                    Subject = newsletterModel.Subject
                };

                newsletterRepository.Add(newsletterInfo);

                newsletterRepository.Save();

                if (newsletterModel.Schedule)
                {
                    // Add files
                    AddFiles(newsletterModel.Attachments, newsletterInfo.NewsletterId);

                    // Schedule the newsletter
                    var scheduledNewsletter = new NewsletterProvider();
                    scheduledNewsletter.ScheduleNewsarticle(newsletterInfo.NewsletterId);

                    LogProvider.LogAction(
                        string.Format("De nieuwsbrief #{0} is toegevoegd", newsletterInfo.NewsletterId));

                    this.SetSuccess("De nieuwsbrief is succesvol toegevoegd!");
                }
                else
                {
                    var newsletter = new NewsletterProvider();
                    newsletter.Send(newsletterInfo.NewsletterId, newsletterInfo.Subject, newsletterInfo.Subject, newsletterModel.Attachments);

                    newsletterInfo.Status = false;

                    LogProvider.LogAction(string.Format("De nieuwsbrief #{0} is verzonden", newsletterInfo.NewsletterId));

                    this.SetSuccess("De nieuwsbrief wordt momenteel verzonden, u hoeft deze pagina niet open te houden.");
                }


                newsletterRepository.Save();

                return RedirectToAction("Weergeven");
            }
            return View(newsletterModel);
        }

        #region Private Methods

        private Collection<Attachment> GetAttachments(int newsletterId)
        {
            var attachments = new Collection<Attachment>();
            var newsletterAttachmentsRepistory = new NewsletterAttachmentsRepository();
            var attachmentsList =
                newsletterAttachmentsRepistory.GetAttachments(newsletterId);

            foreach (var attachment in attachmentsList)
            {
                attachments.Add(new Attachment
                {
                    AttachmentId = attachment.AttachmentId,
                    FileName = attachment.fileName
                });
            }

            return attachments;
        }

        private void EditSchedule(EditNewsLetterModel newsletterModel, Newsletter newsletterInfo)
        {
            // Schule or delete the newsletter
            if (newsletterModel.Schedule)
            {
                // Schedule the newsletter
                var scheduledNewsletter = new NewsletterProvider();
                scheduledNewsletter.ScheduleNewsarticle(newsletterInfo.NewsletterId);
            }
            else
            {
                // Delete old newsletter schedule
                NewsletterSchedules.RemoveScheduler(newsletterInfo.NewsletterId);
            }
        }

        private void RemoveOldFiles(IEnumerable<Attachment> arrayAttachments)
        {
            var newsletterAttachmentsRepistory = new NewsletterAttachmentsRepository();

            // Delete old files
            foreach (Attachment attachment in arrayAttachments.Where(x => x.Checked))
            {
                NewsletterAttachment attachmentInfo =
                    newsletterAttachmentsRepistory.GetById(attachment.AttachmentId);

                newsletterAttachmentsRepistory.Remove(attachmentInfo);

                newsletterAttachmentsRepistory.Save();
            }
        }

        private void AddFiles(IEnumerable<HttpPostedFileBase> arrayPostedFileBases, int newsletterId)
        {
            foreach (var postedFile in arrayPostedFileBases)
            {
                // Check if we uploaded a file
                if (postedFile != null)
                {
                    // Initialize attachments newsletter repository
                    var newsletterAttachmentsRepository = new NewsletterAttachmentsRepository();

                        // Add folder
                        FileProvider.AddFolderDirectory(FilePaths.Banners,
                            newsletterId.ToString(CultureInfo.InvariantCulture), false);

                        // Holds the filename
                        string fileName = postedFile.FileName;

                        // Check if the uploaded file is an image
                        if (FileProvider.IsImage(postedFile.FileName))
                        {
                            fileName = FileProvider.AddAndReturnImageName(postedFile, FilePaths.Newsletter,
                                newsletterId.ToString(CultureInfo.InvariantCulture));
                        }
                        else
                        {
                            FileProvider.AddFile(postedFile, FilePaths.Newsletter, newsletterId.ToString(CultureInfo.InvariantCulture));
                        }

                        // Add attachment
                        newsletterAttachmentsRepository.Add(new NewsletterAttachment
                        {
                            newsLetterId = newsletterId,
                            fileName = fileName,
                        });

                        // Save changes to db
                        newsletterAttachmentsRepository.Save();
                    }
            }
        }

        private void EditImage(int bannerSliderId, HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                // Initialize banner slider repository
                var bannerSliderRepository = new BannerSliderRepository();

                // Get banner info
                var bannerInfo = bannerSliderRepository.GetById(bannerSliderId);

                // Check if the news article exists
                if (bannerInfo != null)
                {
                    // Delete previous image
                   FileProvider.DeleteFile(FilePaths.Banners, bannerSliderId.ToString(CultureInfo.InvariantCulture),
                        bannerInfo.Image, false);

                    // Create directory
                    FileProvider.AddFolderDirectory(FilePaths.Banners, bannerSliderId.ToString(CultureInfo.InvariantCulture),
                        true);

                    // Save image
                    string fileName = FileProvider.AddAndReturnImageName(postedFile, FilePaths.Banners,
                        bannerSliderId.ToString(CultureInfo.InvariantCulture));

                    // Update banner
                    bannerInfo.Image = fileName;

                    // Save changes
                    bannerSliderRepository.Save();
                }
            }
        }
        #endregion
    }
}