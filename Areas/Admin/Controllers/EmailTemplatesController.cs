﻿#region

using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class EmailTemplatesController : ApplicationController
    {
        //
        // GET: /EmailTemplates/

        [RolePermission(permissionName = RolePermission.PermissionName.EmailTemplate)]
        public ActionResult _Weergeven()
        {
            return View(new EmailTemplateRepository().GetList());
        }

        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.EmailTemplate)]
        public ActionResult Wijzig(int id)
        {
            var emailTemplateModel = new EmailTemplateModel();
            EmailTemplate emailTemplateInfo = new EmailTemplateRepository().GetById(id);

            emailTemplateModel.Bericht = emailTemplateInfo.Bericht;
            emailTemplateModel.Onderwerp = emailTemplateInfo.Onderwerp;
            emailTemplateModel.TemplateId = emailTemplateInfo.ID;

            return View(emailTemplateModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.EmailTemplate)]
        [HttpPost]
        public ActionResult Wijzig(EmailTemplateModel emailTemplateModel)
        {
            if (ModelState.IsValid)
            {
                var emailTemplateRepository = new EmailTemplateRepository();
                EmailTemplate emailTemplateInfo = emailTemplateRepository.GetById(emailTemplateModel.TemplateId);

                emailTemplateInfo.Bericht = emailTemplateModel.Bericht;
                emailTemplateInfo.Onderwerp = emailTemplateModel.Onderwerp;

                emailTemplateRepository.Save();

                // Success message
                this.SetSuccess("De email template is succesvol gewijzigd.");

                // Log
                LogProvider.LogAction(
                    string.Format("De email template #{0} is gewijzigd", emailTemplateModel.TemplateId));

                // Redirect
                return RedirectToAction("Weergeven");
            }

            return View(emailTemplateModel);
        }
    }
}