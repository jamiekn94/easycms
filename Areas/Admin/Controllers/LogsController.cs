﻿#region

using System.Collections.ObjectModel;
using System.Web.Mvc;
using Common.Models.Admin.Logs;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class LogsController : ApplicationController
    {

        //
        // GET: /Logs/

        [RolePermission(permissionName = RolePermission.PermissionName.Log)]
        public ActionResult Foutmeldingen()
        {
            //var errorLogRepository = new ErrorLogRepository();
            //return View(errorLogRepository.GetList());
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Log)]
        public ActionResult ShowError(int id)
        {
            var errorLogRepository = new ErrorLogRepository();
            var errorLogInfo = errorLogRepository.GetById(id);
            return View(errorLogInfo);
        }

        public ActionResult Activiteiten()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Log)]
        public ActionResult _Activiteiten()
        {
            var actionLogRepository = new ActionLogRepository();
            var listActivities = actionLogRepository.GetList();

            var userRepository = new GebruikersRepository();

            var listModel = new Collection<ActivitiesModel>();

            foreach (var activity in listActivities)
            {
                var userInfo = userRepository.GetById(activity.UserId);

                listModel.Add(new ActivitiesModel
                {
                    Action = activity.Action,
                    Controller = activity.Controller,
                    Date = activity.Date,
                    Fullname = userInfo.Voornaam + " " + userInfo.Achternaam,
                    Information = activity.Information
                });
            }
            return View(listModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Log)]
        public ActionResult VerwijderErrors()
        {
            var errorLogRepository = new ErrorLogRepository();
            errorLogRepository.RemoveAll();
            errorLogRepository.Save();

            LogProvider.LogAction(
                string.Format("Het error logboek is geleegd"));

            this.SetSuccess("Het error logboek is geleegd.");

            return RedirectToAction("Error");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Log)]
        public ActionResult VerwijderActiviteiten()
        {
            var actionLogRepository = new ActionLogRepository();
            actionLogRepository.RemoveAll();
            actionLogRepository.Save();

            LogProvider.LogAction(
                string.Format("Het activiteiten logboek is geleegd"));

            this.SetSuccess("De activiteiten logboek is geleegd.");

            return RedirectToAction("Activiteiten");
        }
    }
}