﻿#region

using System;
using System.Web.Mvc;
using AutoMapper;
using Common;
using Common.Models.Admin.Navigation;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Navigation;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class LinksController : ApplicationController
    {
        private readonly GetNavigation _getNavigation;

        public LinksController()
        {
            _getNavigation = new GetNavigation();
        }

        //
        // GET: /Blog/

        [RolePermission(permissionName = RolePermission.PermissionName.Links)]
        public ActionResult _Weergeven()
        {
            var linkRepository = new LinkRepository();
            return View(linkRepository.GetList());
        }

        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Links)]
        public ActionResult Wijzig(int id)
        {
            var linkRepository = new LinkRepository();
            Link linkInfo = linkRepository.GetById(id);
            var model = new LinkModel
            {
                Id = linkInfo.LinkID,
                LinkUrl = linkInfo.LinkUrl,
                Naam = linkInfo.Naam,
                Omschrijving = linkInfo.Omschrijving,
                Status = linkInfo.Status,
                BlogArticles = _getNavigation.GetBlogArticles(Convert.ToInt16(linkInfo.BlogArticleId)),
                BuildInPages = _getNavigation.GetBuildInPages(Convert.ToInt16(linkInfo.SeoPageId)),
                DynamicPages = _getNavigation.GetDynamicPages(Convert.ToInt16(linkInfo.DynamicPageId)),
                NewsArticles = _getNavigation.GetNewsArticles(Convert.ToInt16(linkInfo.NewsArticleId)),
                PhotoAlbums = _getNavigation.GetNewsArticles(Convert.ToInt16(linkInfo.PhotoAlbumId)),
                ExternalLink = linkInfo.ExternalUrl,
                NavigationTypes = _getNavigation.GetTypes(),
                NavigationType = (NavigationType)linkInfo.Type
            };
            return View(model);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Links)]
        public ActionResult Verwijder(int id)
        {
            var linkRepository = new LinkRepository();
            Link linkInfo = linkRepository.GetById(id);

            if (linkInfo != null)
            {
                linkRepository.Remove(linkInfo);
                linkRepository.Save();

                LogProvider.LogAction(
                    string.Format("De link {0} is verwijderd", linkInfo.Naam));

                this.SetSuccess("De link is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De link is niet verwijderd want hij bestaat niet");
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Links)]
        [HttpPost]
        public ActionResult Wijzig(LinkModel linkModel)
        {
            if (ModelState.IsValid)
            {
                var linkRepository = new LinkRepository();
                Link linkInfo = linkRepository.GetById(linkModel.Id);

                linkInfo.LinkUrl = linkModel.LinkUrl;
                linkInfo.Naam = linkModel.Naam;
                linkInfo.Omschrijving = linkModel.Omschrijving;
                linkInfo.Status = linkModel.Status;

                linkInfo.Type = (byte)linkModel.NavigationType;
                linkInfo.SeoPageId = 0;
                linkInfo.NewsArticleId = 0;
                linkInfo.BlogArticleId = 0;
                linkInfo.DynamicPageId = 0;
                linkInfo.PhotoAlbumId = 0;

                switch (linkModel.NavigationType)
                {
                    case NavigationType.BuildIn:
                        {
                            if (linkModel.BuildInPageId == 0)
                            {
                                ModelState.AddModelError("BuildInPageId", "U heeft geen pagina gekozen");
                            }
                            linkModel.BuildInPageId = linkModel.BuildInPageId;
                            break;
                        }
                    case NavigationType.Dynamic:
                        {
                            if (linkModel.DynamicPageId == 0)
                            {
                                ModelState.AddModelError("DynamicPageId", "U heeft geen pagina gekozen.");
                            }
                            linkInfo.DynamicPageId = linkModel.DynamicPageId;
                            break;
                        }
                    case NavigationType.News:
                        {
                            if (linkModel.NewsArticleId == 0)
                            {
                                ModelState.AddModelError("NewsArticleId", "U heeft nieuwsartikel gekozen.");
                            }
                            linkInfo.NewsArticleId = linkModel.NewsArticleId;
                            break;
                        }
                    case NavigationType.Blog:
                        {
                            if (linkModel.BlogArticleId == 0)
                            {
                                ModelState.AddModelError("BlogArticleId", "U heeft blogartikel gekozen.");
                            }
                            linkInfo.BlogArticleId = linkModel.BlogArticleId;
                            break;
                        }
                    case NavigationType.Photo:
                        {
                            if (linkModel.BlogArticleId == 0)
                            {
                                ModelState.AddModelError("PhotoAlbumId", "U heeft fotoalbum gekozen.");
                            }
                            linkInfo.PhotoAlbumId = linkModel.PhotoAlbumId;
                            break;
                        }
                    case NavigationType.External:
                        {
                            Uri externalUrl;
                            if (!Uri.TryCreate(linkModel.ExternalLink, UriKind.Absolute, out externalUrl))
                            {
                                ModelState.AddModelError("ExternalLink", "Uw externe link is geen geldige link.");
                            }
                            linkInfo.ExternalUrl = linkModel.ExternalLink;
                            break;
                        }
                }

                linkRepository.Save();

                LogProvider.LogAction(
                    string.Format("De link {0} is gewijzigd", linkModel.Naam));

                this.SetSuccess("De link is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            // Model validation failed
            linkModel.BlogArticles = _getNavigation.GetBlogArticles(linkModel.BlogArticleId);
            linkModel.BuildInPages = _getNavigation.GetBuildInPages(linkModel.BuildInPageId);
            linkModel.DynamicPages = _getNavigation.GetDynamicPages(linkModel.DynamicPageId);
            linkModel.NewsArticles = _getNavigation.GetNewsArticles(linkModel.NewsArticleId);
            linkModel.PhotoAlbums = _getNavigation.GetPhotoAlbums(linkModel.PhotoAlbumId);
            linkModel.NavigationTypes = _getNavigation.GetTypes();

            return View(linkModel);

        }

        [RolePermission(permissionName = RolePermission.PermissionName.Links)]
        public ActionResult Toevoegen()
        {
            var linkModel = new LinkModel
            {
                NavigationTypes = _getNavigation.GetTypes(),
                BuildInPages = _getNavigation.GetBuildInPages(),
                DynamicPages = _getNavigation.GetDynamicPages(),
                NewsArticles = _getNavigation.GetNewsArticles(),
                BlogArticles = _getNavigation.GetBlogArticles(),
                PhotoAlbums = _getNavigation.GetPhotoAlbums(),
                Status = true
            };
            return View(linkModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Links)]
        [HttpPost]
        public ActionResult Toevoegen(LinkModel linkModel)
        {
            if (ModelState.IsValid)
            {
                var linkRepository = new LinkRepository();

                // Create mapper
                Mapper.CreateMap<LinkModel, Link>();

                // Get mapper
                var paginaInfo = Mapper.Map<Link>(linkModel);

                switch (linkModel.NavigationType)
                {
                    case NavigationType.BuildIn:
                        {
                            if (linkModel.BuildInPageId == 0)
                            {
                                ModelState.AddModelError("BuildInPageId", "U heeft geen pagina gekozen");
                            }
                            paginaInfo.SeoPageId = linkModel.BuildInPageId;
                            break;
                        }
                    case NavigationType.Dynamic:
                        {
                            if (linkModel.DynamicPageId == 0)
                            {
                                ModelState.AddModelError("DynamicPageId", "U heeft geen pagina gekozen.");
                            }
                            paginaInfo.DynamicPageId = linkModel.DynamicPageId;
                            break;
                        }
                    case NavigationType.News:
                        {
                            if (linkModel.NewsArticleId == 0)
                            {
                                ModelState.AddModelError("NewsArticleId", "U heeft nieuwsartikel gekozen.");
                            }
                            paginaInfo.NewsArticleId = linkModel.NewsArticleId;
                            break;
                        }
                    case NavigationType.Blog:
                        {
                            if (linkModel.BlogArticleId == 0)
                            {
                                ModelState.AddModelError("BlogArticleId", "U heeft blogartikel gekozen.");
                            }
                            paginaInfo.BlogArticleId = linkModel.BlogArticleId;
                            break;
                        }
                    case NavigationType.Photo:
                        {
                            if (linkModel.PhotoAlbumId == 0)
                            {
                                ModelState.AddModelError("PhotoAlbumId", "U heeft fotoalbum gekozen.");
                            }
                            paginaInfo.PhotoAlbumId = linkModel.PhotoAlbumId;
                            break;
                        }
                    case NavigationType.External:
                        {
                            Uri externalUrl;
                            if (!Uri.TryCreate(linkModel.ExternalLink, UriKind.Absolute, out externalUrl))
                            {
                                ModelState.AddModelError("ExternalLink", "Uw externe link is geen geldige link.");
                            }
                            paginaInfo.ExternalUrl = linkModel.ExternalLink;
                            break;
                        }
                }

                linkRepository.Add(paginaInfo);
                linkRepository.Save();

                LogProvider.LogAction(
                    string.Format("De link {0} is toegevoegd", linkModel.Naam));

                this.SetSuccess("De link is succesvol toegevoegd.");

                return RedirectToAction("Weergeven");
            }

            // Model validation failed
            linkModel.BlogArticles = _getNavigation.GetBlogArticles(linkModel.BlogArticleId);
            linkModel.BuildInPages = _getNavigation.GetBuildInPages(linkModel.BuildInPageId);
            linkModel.DynamicPages = _getNavigation.GetDynamicPages(linkModel.DynamicPageId);
            linkModel.NewsArticles = _getNavigation.GetNewsArticles(linkModel.NewsArticleId);
            linkModel.PhotoAlbums = _getNavigation.GetPhotoAlbums(linkModel.PhotoAlbumId);
            linkModel.NavigationTypes = _getNavigation.GetTypes();
            return View(linkModel);
        }
    }
}