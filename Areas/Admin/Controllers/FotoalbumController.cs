﻿#region

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class FotoalbumController : ApplicationController
    {
        private PhotosInAlbumRepository PhotosInAlbumRepository { get; set; }
        private PhotoAlbumRepository PhotoAlbumRepository { get; set; }

        public FotoalbumController()
        {
            // Initialize photos in album repository
             PhotosInAlbumRepository = new PhotosInAlbumRepository();

            // Initialize photo album repository
             PhotoAlbumRepository = new PhotoAlbumRepository();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        public ActionResult _Weergeven()
        {
            var model = new Collection<FotoAlbumModel>();

            var photoAlbumList = PhotoAlbumRepository.GetList();

            foreach (var photoAlbum in photoAlbumList)
            {
                model.Add(new FotoAlbumModel
                {
                    AantalFotos = PhotosInAlbumRepository.GetAmountPhotos(photoAlbum.AlbumID),
                    AlbumId = photoAlbum.AlbumID,
                    Titel = photoAlbum.Titel,
                    Status = photoAlbum.Status
                });
            }

            return View(model);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        public ActionResult Wijzig(int id)
        {
            // Get photoalbum
            Fotoalbum photoAlbum = PhotoAlbumRepository.GetById(id);

            // Get all photos from album
            IQueryable<PhotosInAlbum> photosInAlbumList = PhotosInAlbumRepository.GetPhotosFromAlbum(id);

            // Initialize foto model
            var model = new EditFotoAlbum
            {
                AlbumId = photoAlbum.AlbumID,
                Descriptie = photoAlbum.Descriptie,
                Keywoorden = photoAlbum.Keywoorden,
                Linknaam = photoAlbum.Linknaam,
                Status = photoAlbum.Status,
                Titel = photoAlbum.Titel
            };

            // Add items into the model
            foreach (PhotosInAlbum photo in photosInAlbumList)
            {
                model.OudeFotos.Add(new EditFotosModel
                {
                    FotoId = photo.FotoID,
                    Name = photo.Naam
                });
            }
            return View(model);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        public ActionResult Verwijder(int id)
        {
            // Get photo album
            Fotoalbum fotoAlbum = PhotoAlbumRepository.GetById(id);

            if (fotoAlbum != null)
            {
                // Get photos
                IQueryable<PhotosInAlbum> photosList = PhotosInAlbumRepository.GetPhotosFromAlbum(fotoAlbum.AlbumID);

                // Remove photos from db
                PhotosInAlbumRepository.RemoveAll(photosList);

                // Save
                PhotosInAlbumRepository.Save();

                // Delete photos on disk
                FileProvider.DeleteDirectory(FilePaths.PhotoAlbums, fotoAlbum.AlbumID.ToString(), true);

                // Delete album
                PhotoAlbumRepository.Remove(fotoAlbum);

                // Save
                PhotoAlbumRepository.Save();

                // Message
                this.SetSuccess("De fotoalbum is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De fotoalbum is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        [HttpPost]
        public ActionResult Wijzig(EditFotoAlbum editFotoAlbum)
        {
            if (ModelState.IsValid)
            {
                // Get photoalbum
                Fotoalbum photoAlbum = PhotoAlbumRepository.GetById(editFotoAlbum.AlbumId);

                // Update values
                photoAlbum.Descriptie = editFotoAlbum.Descriptie;
                photoAlbum.Keywoorden = editFotoAlbum.Keywoorden;
                photoAlbum.Linknaam = editFotoAlbum.Linknaam;
                photoAlbum.Status = editFotoAlbum.Status;
                photoAlbum.Titel = editFotoAlbum.Titel;

                // Save album
                PhotoAlbumRepository.Save();

                // Do we need to upload new photos?
                AddPhoto(editFotoAlbum.NieuweFotos, photoAlbum.AlbumID);

                // Do we need to delete old fotos?
                DeletePhotos(editFotoAlbum.OudeFotos, editFotoAlbum.AlbumId);

                this.SetSuccess("U heeft het fotoalbum succesvol gewijzigd.");

                LogProvider.LogAction(string.Format("Het album: #{0}", editFotoAlbum.AlbumId));

                return RedirectToAction("Weergeven");
            }

            return View(editFotoAlbum);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        public ActionResult Toevoegen()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.FotoAlbum)]
        [HttpPost]
        public ActionResult Toevoegen(FotoAlbumModel fotoAlbum)
        {
            if (ModelState.IsValid)
            {
                // Add photo album
                var photoAlbum = new Fotoalbum
                {
                    Descriptie = fotoAlbum.Descriptie,
                    Keywoorden = fotoAlbum.Keywoorden,
                    Linknaam = fotoAlbum.Linknaam,
                    Status = fotoAlbum.Status,
                    Titel = fotoAlbum.Titel
                };

                PhotoAlbumRepository.Add(photoAlbum);

                // Save photoalbum in db
                PhotoAlbumRepository.Save();

                // Add photo album fotos
                AddPhoto(fotoAlbum.Fotos, photoAlbum.AlbumID);

                this.SetSuccess("De fotoalbum is succesvol toegevoegd!");

                return RedirectToAction("Weergeven");
            }

            return View(fotoAlbum);
        }

        #region Private Methods

        private void AddPhoto(IEnumerable<HttpPostedFileBase> uploadedPhotos, int photoAlbumId)
        {
            foreach (var uploadedFile in uploadedPhotos)
            {
                // Check if we uploaded a file
                if (uploadedFile != null)
                {
                    // Add folder
                    FileProvider.AddFolderDirectory(FilePaths.PhotoAlbums,
                        photoAlbumId.ToString(CultureInfo.InvariantCulture), true);

                    // Add image to directory
                    string fileName = FileProvider.AddAndReturnImageName(uploadedFile, FilePaths.PhotoAlbums,
                        photoAlbumId.ToString(CultureInfo.InvariantCulture));

                    // Add image in db
                    PhotosInAlbumRepository.Add(new PhotosInAlbum
                    {
                        FotoAlbumID = photoAlbumId,
                        Naam = fileName
                    });

                    // Save changes to db
                    PhotosInAlbumRepository.Save();
                }
            }
        }

        private void DeletePhotos(IEnumerable<EditFotosModel> editFotosModels, int photoAlbumId)
        {
            if (editFotosModels != null)
            {
                foreach (var photo in editFotosModels.Where(x => x.Checked))
                {
                    // Get photo info
                    var photoInfo = PhotosInAlbumRepository.GetById(photo.FotoId);

                    // Check if the blog article exists
                    if (photoInfo != null)
                    {
                        // Delete previous image
                        FileProvider.DeleteFile(FilePaths.PhotoAlbums, photoAlbumId.ToString(CultureInfo.InvariantCulture),
                             photoInfo.Naam, true);

                        PhotosInAlbumRepository.Remove(photoInfo);
                        // Save changes
                        PhotosInAlbumRepository.Save();
                    }
                }
            }
        }

        #endregion
    }
}