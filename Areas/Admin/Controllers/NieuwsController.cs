﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class NieuwsController : ApplicationController
    {
        private NieuwsRepository NewsRepository { get; set; }
        private NewsCommentsRepository NewsCommentsRepository { get; set; }

        public NieuwsController()
        {
            NewsRepository = new NieuwsRepository();
            NewsCommentsRepository = new NewsCommentsRepository();
        }

        //[OutputCache(SqlDependency = "Pubs:NieuwsArtikel", Duration = 600)]
        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult _Weergeven()
        {
            var blogsList = NewsRepository.GetList();

            var modelNewsList = blogsList.Select(artikel => new NieuwsModel
            {
                ArtikelId = artikel.ArtikelID,
                AantalReacties = artikel.NieuwsReacties.Count,
                AuteurNaam = UserProvider.GetFullName(artikel.AuteurId),
                Datum = artikel.Datum,
                Informatie = artikel.Informatie,
                ReactiesToegestaan = artikel.ReactiesToegestaan,
                Titel = artikel.Titel,
                Status = artikel.Status,
                Keywoorden = artikel.Keywords
            }).ToList();

            return View(modelNewsList);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult Wijzig(int id)
        {
            Nieuwsartikel nieuwsInfo = NewsRepository.GetById(id);

            var nieuwsModel = new NieuwsModel
            {
                Titel = nieuwsInfo.Titel,
                Informatie = nieuwsInfo.Informatie,
                ArtikelId = nieuwsInfo.ArtikelID,
                Status = nieuwsInfo.Status,
                ReactiesToegestaan = nieuwsInfo.ReactiesToegestaan,
                Keywoorden = nieuwsInfo.Keywords,
                Intro = nieuwsInfo.Intro
            };

            return View(nieuwsModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult Verwijder(int id)
        {
            Nieuwsartikel nieuwsInfo = NewsRepository.GetById(id);

            if (nieuwsInfo != null)
            {
                NewsRepository.Remove(nieuwsInfo);
                NewsRepository.Save();
                LogProvider.LogAction(
                    string.Format("De nieuwsartikel #{0} is verwijderd", nieuwsInfo.ArtikelID));
                this.SetSuccess("Het nieuwsartikel is succesvol verwijderd.");
            }
            else
            {
                this.SetError("Het nieuwsartikel is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        [HttpPost]
        public ActionResult Wijzig(NieuwsModel nieuwsModel)
        {
            if (ModelState.IsValid)
            {
                Nieuwsartikel nieuwsInfo = NewsRepository.GetById(nieuwsModel.ArtikelId);

                nieuwsInfo.Titel = nieuwsModel.Titel;
                nieuwsInfo.Informatie = nieuwsModel.Informatie;
                nieuwsInfo.Status = nieuwsModel.Status;
                nieuwsInfo.ReactiesToegestaan = nieuwsModel.ReactiesToegestaan;
                nieuwsInfo.Keywords = nieuwsModel.Keywoorden;
                nieuwsInfo.Intro = nieuwsModel.Intro;

                NewsRepository.Save();

                EditImage(nieuwsInfo.ArtikelID, nieuwsModel.UploadImage);

                LogProvider.LogAction(
                    string.Format("Het nieuwsartikel #{0} is gewijzigd", nieuwsInfo.ArtikelID));

                this.SetSuccess("Het nieuwsartikel is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            this.SetError("Het nieuwsartikel is niet gewijzigd, controleer uw velden.");

            return View(nieuwsModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        [HttpPost]
        public ActionResult Toevoegen(NieuwsModel nieuwsModel)
        {
            if (ModelState.IsValid)
            {
                var newsTable = new Nieuwsartikel
                {
                    AuteurId = UserProvider.GetUserId(),
                    Datum = DateTime.Now,
                    Informatie = nieuwsModel.Informatie,
                    Titel = nieuwsModel.Titel,
                    ReactiesToegestaan = nieuwsModel.ReactiesToegestaan,
                    Status = nieuwsModel.Status,
                    Keywords = nieuwsModel.Keywoorden,
                    Intro = nieuwsModel.Intro,
                    Image = FileProvider.GetNewImageName(nieuwsModel.UploadImage)
                };

                NewsRepository.Add(newsTable);
                NewsRepository.Save();

                AddImage(nieuwsModel.UploadImage, newsTable.ArtikelID);

                // Get news info
                var newsInfo = NewsRepository.GetLastAdded();

                LogProvider.LogAction(
                    string.Format("Het nieuwsartikel #{0} is toegevoegd", newsInfo.ArtikelID));

                this.SetSuccess("Het artikel is succesvol toegevoegd.");

                return RedirectToAction("Weergeven");
            }
            return View(nieuwsModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult Toevoegen()
        {
            return View(new NieuwsModel
            {
                Status = true,
                ReactiesToegestaan = true
            });
        }

        [HttpGet]
        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        [ActionName("Reactie-Wijzigen")]
        public ActionResult WijzigReactie(int id)
        {
            NieuwsReactie newsReactionInfo = NewsCommentsRepository.GetById(id);

            var model = new EditNewsReactionModel
            {
                Message = newsReactionInfo.Bericht,
                ReactionId = newsReactionInfo.ReactieID,
                Status = newsReactionInfo.Status
            };

            return View(model);
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        [ActionName("Reactie-Wijzigen")]
        public ActionResult WijzigReactie(EditNewsReactionModel editNewsReactionModel)
        {
            if (ModelState.IsValid)
            {
                NieuwsReactie newsReactionInfo = NewsCommentsRepository.GetById(editNewsReactionModel.ReactionId);

                newsReactionInfo.Bericht = editNewsReactionModel.Message;
                newsReactionInfo.Status = editNewsReactionModel.Status;

                NewsCommentsRepository.Save();

                LogProvider.LogAction(
                    string.Format("De nieuwsreactie #{0} is gewijzigd", newsReactionInfo.ReactieID));

                this.SetSuccess("De nieuwsreactie is succesvol gewijzigd");

                return RedirectToAction("Reacties-Weergeven", new { id = editNewsReactionModel.ReactionId });
            }
            return View(editNewsReactionModel);
        }


        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult VerwijderReactie(int id)
        {
            NieuwsReactie reactionInfo = NewsCommentsRepository.GetById(id);

            if (reactionInfo != null)
            {
                NewsCommentsRepository.Remove(reactionInfo);
                NewsCommentsRepository.Save();

                LogProvider.LogAction(
                    string.Format("De nieuwsreactie #{0} is verwijderd", reactionInfo.ReactieID));

                this.SetSuccess("De nieuws reactie is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De nieuws reactie is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Reacties-Weergeven");
        }

        [ActionName("_Reacties-Weergeven")]
        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult _ReactiesWeergeven(int id)
        {
            var listReactionsTable = NewsCommentsRepository.GetListByArticleId(id);

            var listModels = listReactionsTable.Select(row => new NewsReactionsModel
            {
                Name = row.ReactieGebruikersId == null ? row.Name : UserProvider.GetFullName((Guid)row.ReactieGebruikersId),
                ReactionId = row.ReactieID,
                Status = row.Status,
                Date = row.Datum
            }).ToList();

            return View(listModels);
        }

        [ActionName("Reacties-Weergeven")]
        [RolePermission(permissionName = RolePermission.PermissionName.News)]
        public ActionResult ReactiesWeergeven(int id)
        {
            return View(id);
        }

        #region Private Methods

        private void AddImage(HttpPostedFileBase postedFile, int newsArticleId)
        {
            // Check if we uploaded a file
            if (postedFile != null)
            {
                // Add folder
                FileProvider.AddFolderDirectory(FilePaths.Blogs, newsArticleId.ToString(CultureInfo.InvariantCulture), false);

                // Add image
                FileProvider.AddAndReturnImageName(postedFile, FilePaths.Blogs,
                    newsArticleId.ToString(CultureInfo.InvariantCulture));
            }
        }

        private void EditImage(int newsId, HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                // Get news info
                var newsInfo = NewsRepository.GetById(newsId);

                // Check if the news article exists
                if (newsInfo != null)
                {
                    // Delete previous image
                    FileProvider.DeleteFile(FilePaths.News, newsId.ToString(CultureInfo.InvariantCulture),
                         newsInfo.Image, false);

                    // Create directory
                    FileProvider.AddFolderDirectory(FilePaths.News, newsId.ToString(CultureInfo.InvariantCulture),
                        true);

                    // Save image
                    string fileName = FileProvider.AddAndReturnImageName(postedFile, FilePaths.News,
                        newsId.ToString(CultureInfo.InvariantCulture));

                    // Update news article
                    newsInfo.Image = fileName;

                    // Save changes
                    NewsRepository.Save();
                }
            }
        }

        #endregion
    }
}