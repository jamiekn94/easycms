﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.ViewModel;
using Common.Providers.Files;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Roles;
using Common.Providers.Users;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
     [RolePermission(permissionName = RolePermission.PermissionName.Blog)]
    public class BlogController : ApplicationController
    {
        //
        // GET: /Blog/

        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Blog)]
        public ActionResult _Weergeven()
        {
            var blogRepository = new BlogRepository();
            var db = new DataClassesDataContext();
            var blogsList = blogRepository.GetList();

            var modelBlogList = new List<BlogModel>();

            using (var blogCommentRepository = new BlogReactionRepository())
            {
                modelBlogList.AddRange(blogsList.Select(blog => new BlogModel
                {
                    BlogId = blog.BlogID,
                    AantalReacties = blogCommentRepository.GetAmountComments(blog.BlogID),
                    AuteurNaam = UserProvider.GetFullName(blog.AuteurID),
                    Datum = blog.Datum, Informatie = blog.Informatie,
                    ReactiesToegestaan = blog.ReactiesToegestaan,
                    Titel = blog.Titel, 
                    Keywords = blog.Keywords,
                    Status = blog.Status
                }));
            }

            return View(modelBlogList);
        }

        public ActionResult Wijzig(int id)
        {
            var blogRepository = new BlogRepository();

            Blog blogInfo = blogRepository.GetById(id);

            if (blogInfo != null)
            {
                var blogModel = new BlogModel
                {
                    Titel = blogInfo.Titel,
                    Informatie = blogInfo.Informatie,
                    BlogId = blogInfo.BlogID,
                    Keywords = blogInfo.Keywords,
                    ReactiesToegestaan = blogInfo.ReactiesToegestaan,
                    Status = blogInfo.Status,
                    Intro = blogInfo.Intro
                };

                return View(blogModel);
            }

            this.SetError("Dit blog artikel bestaat niet.");

            return RedirectToAction("Weergeven");
        }

        public ActionResult Verwijder(int id)
        {
            var blogRepository = new BlogRepository();
            Blog blogInfo = blogRepository.GetById(id);

            if (blogInfo != null)
            {
                blogRepository.Remove(blogInfo);
                blogRepository.Save();
                this.SetSuccess("De blog is succesvol verwijderd.");
                LogProvider.LogAction(
                    string.Format("Blog artikel #{0} is verwijderd", blogInfo.BlogID));
            }
            else
            {
                this.SetError("De blog is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        [HttpPost]
        public ActionResult Wijzig(BlogModel blogModel)
        {
            if (ModelState.IsValid)
            {
                var blogRepository = new BlogRepository();

                Blog blogInfo = blogRepository.GetById(blogModel.BlogId);

                blogInfo.Titel = blogModel.Titel;
                blogInfo.Informatie = blogModel.Informatie;
                blogInfo.Keywords = blogModel.Keywords;
                blogInfo.ReactiesToegestaan = blogModel.ReactiesToegestaan;
                blogInfo.Status = blogModel.Status;
                blogInfo.Intro = blogModel.Intro;

                blogRepository.Save();

                EditBlogImage(blogInfo.BlogID, blogModel.Image);

                LogProvider.LogAction(
                    string.Format("Blog artikel #{0} is gewijzigd", blogInfo.BlogID));

                this.SetSuccess("De blog is succesvol gewijzigd.");

                return RedirectToAction("Weergeven");
            }

            return View(blogModel);
        }

        public ActionResult Toevoegen()
        {
            return View(new BlogModel
            {
                Status = true,
                ReactiesToegestaan = true
            });
        }

        [HttpPost]
        public ActionResult Toevoegen(BlogModel blogModel)
        {
            if (ModelState.IsValid)
            {
                var blogRepository = new BlogRepository();

                var blogInfo = new Blog
                {
                    AuteurID = UserProvider.GetUserId(),
                    Datum = DateTime.Now,
                    Informatie = blogModel.Informatie,
                    ReactiesToegestaan = blogModel.ReactiesToegestaan,
                    Titel = blogModel.Titel,
                    Keywords = blogModel.Keywords,
                    Status = blogModel.Status,
                    Intro = blogModel.Intro,
                    Image = blogModel.Image.FileName
                };

                blogRepository.Add(blogInfo);
                blogRepository.Save();

               AddBlogImage(blogModel.Image);

                LogProvider.LogAction(
                    string.Format("Blog artikel #{0} is toegevoegd", blogInfo.BlogID));

                this.SetSuccess("Het artikel is succesvol toegevoegd.");

                return RedirectToAction("Weergeven");
            }
            return View(blogModel);
        }

         [ActionName("Reactie-Wijzigen")]
         [HttpPost]
        public ActionResult WijzigReactie(BlogReactionModel blogReactionModel)
        {
            if (ModelState.IsValid)
            {
                var blogReactionRepository = new BlogReactionRepository();
                var blogReactionInfo = blogReactionRepository.GetById(blogReactionModel.BlogReactionId);

                blogReactionInfo.Bericht = blogReactionModel.Message;
                blogReactionInfo.Status = blogReactionModel.Status;

                blogReactionRepository.Save();

                LogProvider.LogAction(
                    string.Format("De blog reactie #{0} is gewijzigd", blogReactionInfo.ReactieID));

                this.SetSuccess("De blog reactie is succesvol gewijzigd");

                return RedirectToAction("Reacties-Weergeven", new { id = blogReactionModel.BlogReactionId });
            }
            return View(blogReactionModel);
        }

        public ActionResult VerwijderReactie(int id)
        {
            var blogReactionRepository = new BlogReactionRepository();
            var reactionInfo = blogReactionRepository.GetById(id);

            if (reactionInfo != null)
            {
                blogReactionRepository.Remove(reactionInfo);
                blogReactionRepository.Save();

                LogProvider.LogAction(
                    string.Format("De blog reactie #{0} is verwijderd", reactionInfo.ReactieID));

                this.SetSuccess("De blog reactie is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De blog reactie is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Reacties-Weergeven");
        }

         [ActionName("Reacties-weergeven")]
        public ActionResult ReactiesWeergeven(int id)
        {
            return View(id);
        }

        public ActionResult _ReactiesWeergeven(int id)
        {
            var blogReactionRepository = new BlogReactionRepository();

            var listReactionsTable = blogReactionRepository.GetListByArticleId(id);

            var listModels = (from row in listReactionsTable
                let name = row.ReactieGebruikersID == null ? row.Name : UserProvider.GetFullName((Guid) row.ReactieGebruikersID)
                select new BlogReactionModel
                {
                    Name = name, BlogReactionId = row.ReactieID, Status = row.Status, Date = row.Datum
                }).ToList();

            return View(listModels);
        }

        [ActionName("Reactie-Wijzigen")]
        [HttpGet]
        public ActionResult WijzigReactie(int id)
        {
            var blogReactionRepository = new BlogReactionRepository();
            var model = new BlogReactionModel();

            var newsReactionInfo = blogReactionRepository.GetById(id);

            model.Message = newsReactionInfo.Bericht;
            model.BlogReactionId = newsReactionInfo.ReactieID;
            model.Status = newsReactionInfo.Status;
            model.BlogReactionId = id;

            return View(model);
        }

        #region Private Methods

        private void AddBlogImage(HttpPostedFileBase postedFile)
        {
            // Check if we uploaded a file
            if (postedFile != null)
            {
                 // Initialize blog repository
                var blogRepository = new BlogRepository();

                // Get blog info
                var blogInfo = blogRepository.GetLastAded();

                // Check if the blog article exists
                if (blogInfo != null)
                {
                    // Add folder
                    FileProvider.AddFolderDirectory(FilePaths.Blogs, blogInfo.BlogID.ToString(CultureInfo.InvariantCulture), false);

                    // Add image
                    string fileName = FileProvider.AddAndReturnImageName(postedFile, FilePaths.Blogs,
                        blogInfo.BlogID.ToString(CultureInfo.InvariantCulture));

                    // Update blog article
                    blogInfo.Image = fileName;

                    // Save changes to db
                    blogRepository.Save();
                }
            }
        }

        private void EditBlogImage(int blogId, HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                // Initialize blog repository
                var blogRepository = new BlogRepository();

                // Get blog info
                var blogInfo = blogRepository.GetById(blogId);

                // Check if the blog article exists
                if (blogInfo != null)
                {
                    // Delete previous image
                   FileProvider.DeleteFile(FilePaths.Blogs, blogId.ToString(CultureInfo.InvariantCulture),
                        blogInfo.Image, false);

                    // Create directory
                    FileProvider.AddFolderDirectory(FilePaths.Blogs, blogId.ToString(CultureInfo.InvariantCulture),
                        true);

                    // Save image
                    string fileName = FileProvider.AddAndReturnImageName(postedFile, FilePaths.Blogs,
                        blogId.ToString(CultureInfo.InvariantCulture));

                    // Update blog article
                    blogInfo.Image = fileName;

                    // Save changes
                    blogRepository.Save();
                }
            }
        }

        #endregion
    }
}