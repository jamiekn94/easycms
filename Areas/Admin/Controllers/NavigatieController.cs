﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Models.Admin.Navigation;
using Common.Models.Admin.ViewModel;
using Common.Providers.Logging;
using Common.Providers.Messages;
using Common.Providers.Navigation;
using Common.Providers.Roles;
using Common.Repositories;

#endregion

namespace Kapsters.Areas.Admin.Controllers
{
    public class NavigatieController : ApplicationController
    {
        private readonly GetNavigation _getNavigation;

        public NavigatieController()
        {
            _getNavigation = new GetNavigation();
        }

        //
        // GET: /Navigatie/

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        public ActionResult _Weergeven()
        {
            var navigatieRepository = new NavigatieRepository();
            var subNavigatieRepository = new SubNavigatieRepository();
            var hoofdNavigatieList = navigatieRepository.GetList();
            var navigatieModelList = hoofdNavigatieList.Select(hoofdMenu => new NavigatieModel
            {
                Naam = hoofdMenu.Naam,
                Status = hoofdMenu.Status,
                NavigatieId = hoofdMenu.ID,
                AmountSubCategories = subNavigatieRepository.GetAmountSubCategories(hoofdMenu.ID)
            }).ToList();

            return View(navigatieModelList);
        }

        public ActionResult Weergeven()
        {
            return View();
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [ActionName("_Sub-menus-weergeven")]
        public ActionResult _SubMenusWeergeven(int id)
        {
            var subNavigationRepository = new SubNavigatieRepository();
            var listSubNavigation = subNavigationRepository.GetSubCategorieen(id);

            var listNavigationModel = new Collection<SubNavigatieModel>();

            foreach (var subNavigation in listSubNavigation)
            {
                listNavigationModel.Add(new SubNavigatieModel
                {
                   Naam = subNavigation.Naam,
                   Status = subNavigation.Status
                });
            }

            return View("Shared/_Sub-Menus-Weergeven", listNavigationModel);
        }

        [ActionName("Sub-menus-weergeven")]
        public ActionResult SubMenusWeergeven(int id)
        {
            return View(id);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        public ActionResult Wijzig(int id)
        {
            var navigatieRepository = new NavigatieRepository();
            var navigatieInfo = navigatieRepository.GetById(id);
            var model = new NavigatieModel
            {
                Naam = navigatieInfo.Naam,
                NavigatieId = id,
                Status = navigatieInfo.Status,
                Visibility = navigatieInfo.Visibility,
                VisibilityOptions = GetVisibilityOptions((VisibilityOptions)navigatieInfo.Visibility),
                BlogArticles = _getNavigation.GetBlogArticles(Convert.ToInt16(navigatieInfo.BlogArticleId)),
                BuildInPages = _getNavigation.GetBuildInPages(Convert.ToInt16(navigatieInfo.SeoPageId)),
                DynamicPages = _getNavigation.GetDynamicPages(Convert.ToInt16(navigatieInfo.DynamicPageId)),
                NewsArticles = _getNavigation.GetNewsArticles(Convert.ToInt16(navigatieInfo.NewsArticleId)),
                PhotoAlbums = _getNavigation.GetPhotoAlbums(Convert.ToInt16(navigatieInfo.PhotoAlbumId)),
                ExternalLink = navigatieInfo.ExternalUrl,
                NavigationTypes = _getNavigation.GetTypes(),
                NavigationType = (NavigationType) navigatieInfo.Type
            };

            return View(model);
        }

        [HttpPost]
        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        public ActionResult Wijzig(NavigatieModel navigatieModel)
        {
            if (ModelState.IsValid)
            {
                var navigatieRepository = new NavigatieRepository();
                var navigatieInfo = navigatieRepository.GetById(navigatieModel.NavigatieId);
                navigatieInfo.Naam = navigatieModel.Naam;
                navigatieInfo.Status = navigatieModel.Status;
                navigatieInfo.Visibility = navigatieModel.Visibility;
                navigatieInfo.Type = (byte) navigatieModel.NavigationType;
                navigatieInfo.SeoPageId = 0;
                navigatieInfo.NewsArticleId = 0;
                navigatieInfo.BlogArticleId = 0;
                navigatieInfo.DynamicPageId = 0;
                navigatieInfo.PhotoAlbumId = 0;

                switch (navigatieModel.NavigationType)
                {
                    case NavigationType.BuildIn:
                        {
                            if (navigatieModel.BuildInPageId == 0)
                            {
                                ModelState.AddModelError("BuildInPageId", "U heeft geen pagina gekozen");
                            }
                            navigatieInfo.SeoPageId = navigatieModel.BuildInPageId;
                            break;
                        }
                    case NavigationType.Dynamic:
                        {
                            if (navigatieModel.DynamicPageId == 0)
                            {
                                ModelState.AddModelError("DynamicPageId", "U heeft geen pagina gekozen.");
                            }
                            navigatieInfo.DynamicPageId = navigatieModel.DynamicPageId;
                            break;
                        }
                    case NavigationType.News:
                        {
                            if (navigatieModel.NewsArticleId == 0)
                            {
                                ModelState.AddModelError("NewsArticleId", "U heeft nieuwsartikel gekozen.");
                            }
                            navigatieInfo.NewsArticleId = navigatieModel.NewsArticleId;
                            break;
                        }
                    case NavigationType.Blog:
                    {
                        if (navigatieModel.BlogArticleId == 0)
                        {
                            ModelState.AddModelError("BlogArticleId", "U heeft blogartikel gekozen.");
                        }
                        navigatieInfo.BlogArticleId = navigatieModel.BlogArticleId;
                        break;
                    }

                    case NavigationType.Photo:
                        {
                            if (navigatieModel.BlogArticleId == 0)
                            {
                                ModelState.AddModelError("PhotoAlbumId", "U heeft fotoalbum gekozen.");
                            }
                            navigatieInfo.PhotoAlbumId = navigatieModel.PhotoAlbumId;
                            break;
                        }
                    case NavigationType.External:
                        {
                            Uri externalUrl;
                            if (!Uri.TryCreate(navigatieModel.ExternalLink, UriKind.Absolute, out externalUrl))
                            {
                                ModelState.AddModelError("ExternalLink", "Uw externe link is geen geldige link.");
                            }
                            navigatieInfo.ExternalUrl = navigatieModel.ExternalLink;
                            break;
                        }
                }

                if (ModelState.IsValid)
                {
                    navigatieRepository.Save();

                    LogProvider.LogAction(
                        string.Format("De navigatie knop {0} is gewijzigd", navigatieModel.Naam));

                    this.SetSuccess("De navigatie menu is succesvol gewijzigd.");

                    return RedirectToAction("Weergeven");
                }
            }

            navigatieModel.BlogArticles = _getNavigation.GetBlogArticles(navigatieModel.BlogArticleId);
            navigatieModel.BuildInPages = _getNavigation.GetBuildInPages(navigatieModel.BuildInPageId);
            navigatieModel.DynamicPages = _getNavigation.GetDynamicPages(navigatieModel.DynamicPageId);
            navigatieModel.NewsArticles = _getNavigation.GetNewsArticles(navigatieModel.NewsArticleId);
            navigatieModel.PhotoAlbums = _getNavigation.GetPhotoAlbums(navigatieModel.PhotoAlbumId);
            navigatieModel.VisibilityOptions = GetVisibilityOptions((VisibilityOptions)navigatieModel.Visibility);
            navigatieModel.NavigationTypes = _getNavigation.GetTypes();

            return View(navigatieModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        public ActionResult Verwijder(int id)
        {
            var navigatieRepository = new NavigatieRepository();
            var navigatieInfo = navigatieRepository.GetById(id);

            if (navigatieInfo != null)
            {
                navigatieRepository.Remove(navigatieInfo);

                LogProvider.LogAction(
                    string.Format("De navigatie knop {0} is verwijderd", navigatieInfo.Naam));

                this.SetSuccess("De navigatie menu is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De navigatie menu is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [HttpPost]
        public ActionResult WijzigSubMenu(SubNavigatieModel subNavigatieModel)
        {
            if (ModelState.IsValid)
            {
                var navigatieRepository = new NavigatieRepository();
                var subNavigatieRepository = new SubNavigatieRepository();

                var navigatieInfo = subNavigatieRepository.GetById(subNavigatieModel.SubCategorieId);
                navigatieInfo.Naam = subNavigatieModel.Naam;
                navigatieInfo.Status = subNavigatieModel.Status;
                navigatieInfo.Type = (byte) subNavigatieModel.NavigationType;
                navigatieInfo.SeoPageId = 0;
                navigatieInfo.NewsArticleId = 0;
                navigatieInfo.BlogArticleId = 0;
                navigatieInfo.DynamicPageId = 0;
                navigatieInfo.PhotoAlbumId = 0;

                switch (subNavigatieModel.NavigationType)
                {
                    case NavigationType.BuildIn:
                    {
                        if (subNavigatieModel.BuildInPageId == 0)
                        {
                            ModelState.AddModelError("BuildInPageId", "U heeft geen pagina gekozen");
                        }
                        navigatieInfo.SeoPageId = subNavigatieModel.BuildInPageId;
                        break;
                    }
                    case NavigationType.Dynamic:
                    {
                        if (subNavigatieModel.DynamicPageId == 0)
                        {
                            ModelState.AddModelError("DynamicPageId", "U heeft geen pagina gekozen.");
                        }
                        navigatieInfo.DynamicPageId = subNavigatieModel.DynamicPageId;
                        break;
                    }
                    case NavigationType.News:
                    {
                        if (subNavigatieModel.NewsArticleId == 0)
                        {
                            ModelState.AddModelError("NewsArticleId", "U heeft nieuwsartikel gekozen.");
                        }
                        navigatieInfo.NewsArticleId = subNavigatieModel.NewsArticleId;
                        break;
                    }
                    case NavigationType.Blog:
                    {
                        if (subNavigatieModel.BlogArticleId == 0)
                        {
                            ModelState.AddModelError("BlogArticleId", "U heeft blogartikel gekozen.");
                        }
                        navigatieInfo.BlogArticleId = subNavigatieModel.BlogArticleId;
                        break;
                    }
                    case NavigationType.Photo:
                    {
                        if (subNavigatieModel.BlogArticleId == 0)
                        {
                            ModelState.AddModelError("PhotoAlbumId", "U heeft fotoalbum gekozen.");
                        }
                        navigatieInfo.PhotoAlbumId = subNavigatieModel.PhotoAlbumId;
                        break;
                    }
                    case NavigationType.External:
                    {
                        Uri externalUrl;
                        if (!Uri.TryCreate(subNavigatieModel.ExternalLink, UriKind.Absolute, out externalUrl))
                        {
                            ModelState.AddModelError("ExternalLink", "Uw externe link is geen geldige link.");
                        }
                        navigatieInfo.ExternalUrl = subNavigatieModel.ExternalLink;
                        break;
                    }
                }

                if (ModelState.IsValid)
                {
                    navigatieRepository.Save();

                    LogProvider.LogAction(
                        string.Format("De submenu knop {0} is gewijzigd", navigatieInfo.Naam));

                    this.SetSuccess("De sub navigatie menu is succesvol gewijzigd.");

                    return RedirectToAction("Weergeven");
                }

            }

            // Model validation failed
            subNavigatieModel.BlogArticles = _getNavigation.GetBlogArticles(subNavigatieModel.BlogArticleId);
            subNavigatieModel.BuildInPages = _getNavigation.GetBuildInPages(subNavigatieModel.BuildInPageId);
            subNavigatieModel.DynamicPages = _getNavigation.GetDynamicPages(subNavigatieModel.DynamicPageId);
            subNavigatieModel.NewsArticles = _getNavigation.GetNewsArticles(subNavigatieModel.NewsArticleId);
             subNavigatieModel.PhotoAlbums = _getNavigation.GetPhotoAlbums(Convert.ToInt16(subNavigatieModel.PhotoAlbumId));
            subNavigatieModel.NavigationTypes = _getNavigation.GetTypes();
            return View(subNavigatieModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        public ActionResult Toevoegen()
        {
            var navigationModel = new NavigatieModel
            {
                VisibilityOptions = GetVisibilityOptions(),
                NavigationTypes = _getNavigation.GetTypes(),
                BuildInPages = _getNavigation.GetBuildInPages(),
                DynamicPages = _getNavigation.GetDynamicPages(),
                NewsArticles = _getNavigation.GetNewsArticles(),
                BlogArticles = _getNavigation.GetBlogArticles(),
                PhotoAlbums = _getNavigation.GetPhotoAlbums(),
                Status = true
            };
            return View(navigationModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [HttpPost]
        public ActionResult Toevoegen(NavigatieModel navigatieModel)
        {
            if (ModelState.IsValid)
            {
                var navigatieRepository = new NavigatieRepository();
                var navigatieInfo = new Navigatie
                {
                    Naam = navigatieModel.Naam,
                    Status = navigatieModel.Status,
                    Visibility = navigatieModel.Visibility,
                    Type = (byte) navigatieModel.NavigationType
                };

                switch (navigatieModel.NavigationType)
                {
                    case NavigationType.BuildIn:
                        {
                            if (navigatieModel.BuildInPageId == 0)
                            {
                                ModelState.AddModelError("BuildInPageId", "U heeft geen pagina gekozen");
                            }
                            navigatieInfo.SeoPageId = navigatieModel.BuildInPageId;
                            break;
                        }
                    case NavigationType.Dynamic:
                        {
                            if (navigatieModel.DynamicPageId == 0)
                            {
                                ModelState.AddModelError("DynamicPageId", "U heeft geen pagina gekozen.");
                            }
                            navigatieInfo.DynamicPageId = navigatieModel.DynamicPageId;
                            break;
                        }
                    case NavigationType.News:
                        {
                            if (navigatieModel.NewsArticleId == 0)
                            {
                                ModelState.AddModelError("NewsArticleId", "U heeft nieuwsartikel gekozen.");
                            }
                            navigatieInfo.NewsArticleId = navigatieModel.NewsArticleId;
                            break;
                        }
                    case NavigationType.Blog:
                        {
                            if (navigatieModel.BlogArticleId == 0)
                            {
                                ModelState.AddModelError("BlogArticleId", "U heeft blogartikel gekozen.");
                            }
                            navigatieInfo.BlogArticleId = navigatieModel.BlogArticleId;
                            break;
                        }
                    case NavigationType.Photo:
                        {
                            if (navigatieModel.BlogArticleId == 0)
                            {
                                ModelState.AddModelError("PhotoAlbumId", "U heeft fotoalbum gekozen.");
                            }
                            navigatieInfo.PhotoAlbumId = navigatieModel.PhotoAlbumId;
                            break;
                        }
                    case NavigationType.External:
                        {
                            Uri externalUrl;
                            if (!Uri.TryCreate(navigatieModel.ExternalLink, UriKind.Absolute, out externalUrl))
                            {
                                ModelState.AddModelError("ExternalLink", "Uw externe link is geen geldige link.");
                            }
                            navigatieInfo.ExternalUrl = navigatieModel.ExternalLink;
                            break;
                        }
                }
                    navigatieRepository.Add(navigatieInfo);

                    navigatieRepository.Save();

                    LogProvider.LogAction(
                        string.Format("De submenu navigatie knop {0} is toegevoegd", navigatieModel.Naam));

                    this.SetSuccess("De navigatie menu is succesvol toegevoegd.");

                    return RedirectToAction("Weergeven");
                }

            // Model validation failed
            navigatieModel.BlogArticles = _getNavigation.GetBlogArticles(navigatieModel.BlogArticleId);
            navigatieModel.BuildInPages = _getNavigation.GetBuildInPages(navigatieModel.BuildInPageId);
            navigatieModel.DynamicPages = _getNavigation.GetDynamicPages(navigatieModel.DynamicPageId);
            navigatieModel.NewsArticles = _getNavigation.GetNewsArticles(navigatieModel.NewsArticleId);
            navigatieModel.PhotoAlbums = _getNavigation.GetPhotoAlbums();
            navigatieModel.VisibilityOptions = GetVisibilityOptions((VisibilityOptions)navigatieModel.Visibility);
            navigatieModel.NavigationTypes = _getNavigation.GetTypes();

            return View(navigatieModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [ActionName("Sub-menu-wijzigen")]
        public ActionResult SubMenuWijzigen(int id)
        {
            var subNavigatieRepository = new SubNavigatieRepository();
            var navigatieRepository = new NavigatieRepository();
            var navigatieInfo = subNavigatieRepository.GetById(id);

            var model = new SubNavigatieModel
            {
                Naam = navigatieInfo.Naam,
                HoofdCategorieen = navigatieRepository.GetHoofdCategorieen(navigatieInfo.HoofdNavigatieId),
                Status = navigatieInfo.Status,
                BlogArticles = _getNavigation.GetBlogArticles(Convert.ToInt16(navigatieInfo.BlogArticleId)),
                BuildInPages = _getNavigation.GetBuildInPages(Convert.ToInt16(navigatieInfo.SeoPageId)),
                DynamicPages = _getNavigation.GetDynamicPages(Convert.ToInt16(navigatieInfo.DynamicPageId)),
                NewsArticles = _getNavigation.GetNewsArticles(Convert.ToInt16(navigatieInfo.NewsArticleId)),
                PhotoAlbums = _getNavigation.GetPhotoAlbums(Convert.ToInt16(navigatieInfo.PhotoAlbumId)),
                ExternalLink = navigatieInfo.ExternalUrl,
                NavigationTypes = _getNavigation.GetTypes(),
                NavigationType = (NavigationType)navigatieInfo.Type,
                SubCategorieId = navigatieInfo.SubNavigatieId
            };

            return View(model);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [ActionName("Sub-menu-toevoegen")]
        public ActionResult SubMenuToevoegen()
        {
            var navigatieRepository = new NavigatieRepository();
            var model = new SubNavigatieModel
            {
                HoofdCategorieen = navigatieRepository.GetHoofdCategorieen(),
                NavigationTypes = _getNavigation.GetTypes(),
                BuildInPages = _getNavigation.GetBuildInPages(),
                DynamicPages = _getNavigation.GetDynamicPages(),
                NewsArticles = _getNavigation.GetNewsArticles(),
                BlogArticles = _getNavigation.GetBlogArticles(),
                PhotoAlbums =  _getNavigation.GetPhotoAlbums(),
                Status = true
            };

            return View(model);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [HttpPost]
        public ActionResult SubMenuToevoegen(SubNavigatieModel subNavigatieModel)
        {
            if (ModelState.IsValid)
            {
                var subNavigatieRepository = new SubNavigatieRepository();

                var navigatieInfo = new SubNavigatie
                {
                    Naam = subNavigatieModel.Naam,
                    Status = subNavigatieModel.Status,
                    HoofdNavigatieId = subNavigatieModel.HoofdCategorieId,
                    Type = (byte) subNavigatieModel.NavigationType
                };

                switch (subNavigatieModel.NavigationType)
                {
                    case NavigationType.BuildIn:
                    {
                        if (subNavigatieModel.BuildInPageId == 0)
                        {
                            ModelState.AddModelError("BuildInPageId", "U heeft geen pagina gekozen");
                        }
                        navigatieInfo.SeoPageId = subNavigatieModel.BuildInPageId;
                        break;
                    }
                    case NavigationType.Dynamic:
                    {
                        if (subNavigatieModel.DynamicPageId == 0)
                        {
                            ModelState.AddModelError("DynamicPageId", "U heeft geen pagina gekozen.");
                        }
                        navigatieInfo.DynamicPageId = subNavigatieModel.DynamicPageId;
                        break;
                    }
                    case NavigationType.News:
                    {
                        if (subNavigatieModel.NewsArticleId == 0)
                        {
                            ModelState.AddModelError("NewsArticleId", "U heeft nieuwsartikel gekozen.");
                        }
                        navigatieInfo.NewsArticleId = subNavigatieModel.NewsArticleId;
                        break;
                    }
                    case NavigationType.Blog:
                    {
                        if (subNavigatieModel.BlogArticleId == 0)
                        {
                            ModelState.AddModelError("BlogArticleId", "U heeft blogartikel gekozen.");
                        }
                        navigatieInfo.BlogArticleId = subNavigatieModel.BlogArticleId;
                        break;
                    }
                    case NavigationType.Photo:
                    {
                        if (subNavigatieModel.BlogArticleId == 0)
                        {
                            ModelState.AddModelError("PhotoAlbumId", "U heeft fotoalbum gekozen.");
                        }
                        navigatieInfo.PhotoAlbumId = subNavigatieModel.PhotoAlbumId;
                        break;
                    }
                    case NavigationType.External:
                    {
                        Uri externalUrl;
                        if (!Uri.TryCreate(subNavigatieModel.ExternalLink, UriKind.Absolute, out externalUrl))
                        {
                            ModelState.AddModelError("ExternalLink", "Uw externe link is geen geldige link.");
                        }
                        navigatieInfo.ExternalUrl = subNavigatieModel.ExternalLink;
                        break;
                    }
                }

                if (ModelState.IsValid)
                {
                    subNavigatieRepository.Add(navigatieInfo);

                    subNavigatieRepository.Save();

                    LogProvider.LogAction(
                        string.Format("De submenu navigatie knop {0} is toegevoegd", navigatieInfo.Naam));

                    this.SetSuccess("De sub navigatie menu is succesvol toegevoegd.");
                }

                return RedirectToAction("Weergeven");
            }

            var navigatieRepository = new NavigatieRepository();
            subNavigatieModel.HoofdCategorieen = navigatieRepository.GetHoofdCategorieen();
            subNavigatieModel.BlogArticles = _getNavigation.GetBlogArticles(subNavigatieModel.BlogArticleId);
            subNavigatieModel.BuildInPages = _getNavigation.GetBuildInPages(subNavigatieModel.BuildInPageId);
            subNavigatieModel.DynamicPages = _getNavigation.GetDynamicPages(subNavigatieModel.DynamicPageId);
            subNavigatieModel.NewsArticles = _getNavigation.GetNewsArticles(subNavigatieModel.NewsArticleId);
            subNavigatieModel.PhotoAlbums = _getNavigation.GetPhotoAlbums(subNavigatieModel.PhotoAlbumId);
            subNavigatieModel.NavigationTypes = _getNavigation.GetTypes();

            return View(subNavigatieModel);
        }

        [RolePermission(permissionName = RolePermission.PermissionName.Navigation)]
        [ActionName("Sub-menu-verwijderen")]
        public ActionResult SubMenuVerwijderen(int id)
        {
            var subNavigatieRepository = new SubNavigatieRepository();
            var subNavigatieInfo = subNavigatieRepository.GetById(id);

            if (subNavigatieInfo != null)
            {
                subNavigatieRepository.Remove(subNavigatieInfo);

                LogProvider.LogAction(
                    string.Format("De sub menu {0} is verwijderd", subNavigatieInfo.Naam));

                this.SetSuccess("De sub navigatie menu is succesvol verwijderd.");
            }
            else
            {
                this.SetError("De sub navigatie menu is niet verwijderd want hij bestaat niet.");
            }

            return RedirectToAction("Weergeven");
        }

        #region Private Functions

        private IEnumerable<SelectListItem> GetVisibilityOptions(VisibilityOptions visibilityOption = VisibilityOptions.NotSpecified)
        {
            return new Collection<SelectListItem>
            {
                new SelectListItem {Text = "Iedereen", Value = ((int) VisibilityOptions.Everyone).ToString(CultureInfo.InvariantCulture), Selected = visibilityOption == VisibilityOptions.Everyone},
                new SelectListItem {Text = "Ingelogde gebruikers", Value = "1", Selected = visibilityOption == VisibilityOptions.LoggedIn},
                new SelectListItem {Text = "Niet ingelogde gebruikers", Value = "2", Selected = visibilityOption == VisibilityOptions.Everyone},
            }.OrderBy(x => x.Selected);
        }
        #endregion
    }
}