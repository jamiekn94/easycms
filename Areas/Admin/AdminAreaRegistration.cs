﻿#region

using System.Web.Mvc;
using System.Web.Optimization;
using Kapsters.Areas.Admin.Bundle;
using Kapsters.Areas.Admin.Functionality.Error;

#endregion

namespace Kapsters.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Admin"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin",
                "Admin",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
                );
            context.MapRoute(
                "Admin_Default",
                "Admin/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
                );

            RegisterBundles();
            //  RegisterSQLDepedency();
        }

        //private void RegisterSQLDepedency()
        //{
        //    var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    SqlDependency.Start(connectionString);
        //}

        private void RegisterBundles()
        {
            CMSBundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}