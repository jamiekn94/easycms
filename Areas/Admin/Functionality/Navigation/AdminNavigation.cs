﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Common;
using Common.Models.Admin.Navigation;
using Common.Providers.Roles;
using Common.Providers.Routes;
using Common.Providers.Users;
using Common.Repositories;
using WebGrease.Css.Extensions;

namespace Kapsters.Areas.Admin.Functionality.Navigation
{
    public class AdminNavigation
    {
        public IEnumerable<AdminNavigationModel> GetNavigation()
        {
            var returnList = new Collection<AdminNavigationModel>();

            var adminNavigationRepository = new AdminNavigationRepository();
            var groupRightRepository = new GroepsRechtRepository();
            var rightRepository = new RightRepository();

            var listGroupRights = groupRightRepository.GetRightsFromGroupId(RoleProvider.GetRoleId()).ToList();

            var listAvailableRight = rightRepository.GetList().FromCache(60).ToList();
            
            var listDbNavigation = adminNavigationRepository.GetList().FromCache(60).ToList();

            foreach (var parentNavigationModel in
                from parentNavigation in listDbNavigation.Where(x => x.ParentId == null)
                where listAvailableRight.Any(x=> x.RechtID == parentNavigation.RightId && x.Status)
                where listGroupRights.Any(x => x.RechtId == parentNavigation.RightId)
                select new AdminNavigationModel
            {
                Action = parentNavigation.Action,
                Controller = parentNavigation.Controller,
                Name = parentNavigation.Name,
                ArraySubNavigationModels = GetSubNavigation(listDbNavigation, parentNavigation.NavigationId),
                Icon = parentNavigation.Icon,
                CurrentPage = RouteProvider.GetController()
                        .Equals(parentNavigation.Controller, StringComparison.CurrentCultureIgnoreCase)
            })
            {
                returnList.Add(parentNavigationModel);
            }
           
            return returnList;
        }

        private IEnumerable<AdminNavigationModel> GetSubNavigation(IEnumerable<Common.AdminNavigation> arrayDbNavigation, int parentId)
        {
            var returnList = new Collection<AdminNavigationModel>();

            var dbNavigation = arrayDbNavigation.Where(x => x.ParentId == parentId);

            dbNavigation.ForEach(x => returnList.Add(new AdminNavigationModel
            {
                Action = x.Action,
                Controller = x.Controller,
                Name = x.Name,
                Icon = x.Icon
            }));

            return returnList;
        }
    }
}