﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kapsters.Areas.Admin.Functionality.Error
{
    public class ErrorHandlerAdmin : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            filterContext.Result = new RedirectResult("/Admin/Dashboard?Error=Er heeft zich een onbekende fout plaatsgevonden, de error is opgeslagen. Excuses voor het ongemak.", false);
        }
    }
}