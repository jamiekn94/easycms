﻿#region

using System.Net;
using System.Net.Mail;
using Quartz;

#endregion

namespace Kapsters.Areas.Admin.Tasks.Email
{
    public class Job : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var mail = new MailMessage();
            mail.To.Add("Jamie.knoef@gmail.com");
            mail.From = new MailAddress("Jamie.knoef@gmail.com");
            mail.Subject = "Scheduled Task";
            mail.Body = "Like a boss.";
            var smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new NetworkCredential("Jamie.knoef@gmail.com", "knoef1994");
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}