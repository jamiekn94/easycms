﻿#region

using System;
using System.Collections.Specialized;
using Quartz;
using Quartz.Impl;

#endregion

namespace Kapsters.Areas.Admin.Tasks
{
    public class Task : IScheduledJob
    {
        public void Run(string identityName, string schedule, string className)
        {
            // Get an instance of the Quartz.Net scheduler
            var schd = GetScheduler(identityName);

            // Start the scheduler if its in standby
            if (!schd.IsStarted)
                schd.Start();

            var hai = Type.GetType(className);
            // Object o = (Activator.CreateInstance(hai));  

            // Define the Job to be scheduled
            var job = JobBuilder.Create(hai)
                .WithIdentity(identityName, "IT")
                .RequestRecovery()
                .Build();

            // Associate a trigger with the Job
            var trigger = (ICronTrigger) TriggerBuilder.Create()
                .WithIdentity(identityName, "IT")
                .WithCronSchedule(schedule)
                .StartAt(DateTime.UtcNow)
                .WithPriority(1)
                .Build();

            // Validate that the job doesn't already exists
            if (schd.CheckExists(new JobKey(identityName, "IT")))
            {
                schd.DeleteJob(new JobKey(identityName, "IT"));
            }

            schd.ScheduleJob(job, trigger);
        }

        private static IScheduler GetScheduler(string identityName)
        {
            try
            {
                var properties = new NameValueCollection();
                properties["quartz.scheduler.instanceName"] = identityName;

                // Get a reference to the scheduler
                var sf = new StdSchedulerFactory(properties);

                return sf.GetScheduler();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}