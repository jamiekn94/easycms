﻿namespace Kapsters.Areas.Admin.Tasks
{
    public interface IScheduledJob
    {
        void Run(string identityName, string schedule, string className);
    }
}