﻿$(document).ready(function () {

    //#region Bootstrap switch
    var bootstrapSwitch = $('.make-switch');
    if (bootstrapSwitch.length > 0) {
        $.fn.bootstrapSwitch.defaults.onText = "Aan";
        $.fn.bootstrapSwitch.defaults.offText = "Uit";
        $.fn.bootstrapSwitch.defaults.onColor = 'success';
        $.fn.bootstrapSwitch.defaults.offColor = 'warning';
        bootstrapSwitch.bootstrapSwitch();
    }
    //#endregion

    //#region Datatable
    function initializeDataTable() {
        var dataTable = $("#datatable");

        if (dataTable.length > 0) {
            $('#datatable').footable();
        }

        //$(document).on("AjaxLoadingComplete", $('#datatable').footable());
    }
    //#endregion

    //#region Confirm deletion
    $(document).on('click', 'a.btn-danger', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $("#dialog-delete-confirm").dialog({
            resizable: false,
            height: 180,
            modal: true,
            draggable: false,
            dialogClass: 'deleteDialog',
            buttons: {
                "Verwijderen": function () {
                    $(this).dialog("close");
                    window.location.href = link;
                },
                "Annuleren": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
    //#endregion

  
    //#region Update actionlog ajax
    var hasClickedActionLog = false;

    $(document).on('click', '#actionLog button', function () {
        hasClickedActionLog = true;
    });

    $(document).on('click', '#actionLog button', function() {
        if ($(this).parent().hasClass('open')) {
            updateActionLog();
        }
    });

    function updateActionLog() {
        $('#actionLog button').html('<span class="glyphicon glyphicon-comment"></span> <b>0</b>');

        $.ajax({
            url: '/Dashboard/UpdateActionLog',
            method: 'POST'
        }).done(function () {

            hasClickedActionLog = false;

            //$('#actionLog #actionItems .dropdown-items').html('');
            //$('#actionLog #actionItems .dropdown-items').append('<li>U heeft geen acties meer om te bekijken.</li>');
        }).fail(function() {
            alert('Het is niet gelukt om uw action logs te verwijderen, probeer het later nog eens.');
        });
    }

    $(document).on('click', function () {
        if (hasClickedActionLog) {
            updateActionLog();
            hasClickedActionLog = false;
        }
    });

    //$(document).on('#actionLog button', function () {
    //    if (!$(this).parent().hasClass('open')) {
    //        $(this).html('<span class="glyphicon glyphicon-comment"></span> <b>0</b>');
    //        $.ajax({
    //            url: '/Dashboard/UpdateActionLog',
    //            method: 'POST'
    //        });
    //    }
    //});
    //#endregion

    //#region Sidemenu
    // If window is under 1200 we remove the sidemenu collapsed class
    // At <1200px CSS media queries will take over and JS will only interfere

    // Get html of the nav tabs
    var tabs = null;

    getPanelTabs();

    function getPanelTabs() {
        tabs = $('.panel > .panel-heading > .nav.panel-tabs').html();
    }

    $(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth < 1200) {

            if ($('body').hasClass('sidebar-collapsed')) {
                $('body').removeClass('sidebar-collapsed');
            }

            setSmallHeading();

        } else {
            if ($('.panel-heading .nav.panel-tabs').length > 0) {
                if ($('.panel-heading .nav.panel-tabs').html().length == 0) {
                    $('.panel-heading .nav.panel-tabs').prepend(tabs);
                    $('#small-heading').remove();
                }
            }

        }
    });

    // Collapse Side Menu on Click
    $(".sidebar-toggle").click(toggleSideMenu);

    // Adds a single class to body which we use to
    // collapse entire side menu via preset CSS
    function toggleSideMenu() {
        if ($('body').hasClass('sidebar-collapsed')) {
            $('body').removeClass('sidebar-collapsed');
        } else {
            $('body').addClass('sidebar-collapsed');
        }
    }

    if ($(window).width() < 1200) {
        setSmallHeading();
    }

    function setSmallHeading() {

        if ($('.panel-heading .nav.panel-tabs').length > 0) {
            if ($('.panel-heading .nav.panel-tabs').html().length > 0) {
                // Delete tabs
                $('.panel-heading .nav.panel-tabs').html("");

                var divContent = "";
                var divEndResult = "";

                $(tabs).each(function () {
                    var element = $(this).find('a');
                    var title = element.text();
                    var link = element.attr('href');

                    if (title.length > 0) {

                        divContent += '<a href="' + link + '" class="btn btn-default btn-gradient btn-block">' + title + '</a>';
                    }

                });

                divEndResult = '<div id="small-heading">' + divContent + '</div>';

                $('.panel > .panel-body').prepend(divEndResult);
            }
        }
    }

    //SideMenu animated accordion toggle
    $('#sidebar-menu > .nav > li > a').click(function (e) {
        if ($(this).parent().find("ul").length > 0) {
            e.preventDefault();
            if (!$(this).parent().find("ul").hasClass("collapsed")) {
                $(this).parent().find("ul").slideDown(350);
                $(this).parent().find("ul").addClass('collapsed');
            } else {
                $(this).parent().find("ul").slideUp(350);
                $(this).parent().find("ul").removeClass('collapsed');
            }
        }
    });

    //#endregion

    //#region Start tooltip
    function initToolTip() {
        var toolTip = $('.tool-tip');
        if (toolTip.length > 0) {
            toolTip.tooltip();
        } 
       
    }

    initToolTip();
    //#endregion

    //#region Select
    window.initializeSelect = function() {
        var select = $('.search');
        if (select.length > 0) {
            $('.search').select2();
        }
    }
    //#endregion

    //#region Ajax Loader
    window.showLoadingScreen = function () {
        console.log('showLoadingScreen!');
        $('#loadingScreen').show();
    }

    window.hideLoadingScreen = function () {
        console.log('Hide loading screen!');
        $('#loadingScreen').hide();
    }
    //#endregion

    //#region Load ajax page
    var viewContentDiv = $('#viewContent');

    if (viewContentDiv.length > 0) {
        showLoadingScreen();
        var url = viewContentDiv.data('url');
        $.get(url, function(data) {
            viewContentDiv.replaceWith(data);
        }).fail(function() {
            alert('Het is niet gelukt om de gegevens op te halen.');
        }).done(function() {
            initializeSelect();
            initToolTip();
            initializeDataTable();
            getPanelTabs();
            // Trigger events that have subscribed to the ajax completed event
            $.event.trigger({
                type: "AjaxLoadingComplete",
            });

           // Upload button replacer
            //if (typeof window.initializeFileReplacer !== 'undefined' && $.isFunction(initializeFileReplacer)) {
            //    window.initializeFileReplacer();
            //}

    }).always(function() {
        hideLoadingScreen();
        });
    }
    //#endregion

    //#region Client side validation
    $.validator.methods.date = function (value, element) {
        Globalize.culture("nl-NL");
        // you can alternatively pass the culture to parseDate instead of
        // setting the culture above, like so:
        // parseDate(value, null, "en-AU")
        return this.optional(element) || Globalize.parseDate(value, "dd-MM-yyyy H:mm") !== null || Globalize.parseDate(value, "dd-MM-yyyy") !== null;
    };
    //#endregion

});

