﻿$(document).ready(function () {
    window.initializeFileReplacer = function() {
        if ($('input[type="file"]').length > 0) {
            var replaceButton = "<input type='button' value='Bestand(en) selecteren' class='btn btn-default btn-gradient'/>";
            var fileList = "<ul id='fileList'><li>Geen bestanden geselecteerd.</li></ul>";
            $('input[type="file"]').after(replaceButton);
            $('input[type="file"]').parent().find('input[type="button"]').after(fileList);
            $('input[type="file"]').css({ 'Visibility': 'hidden', 'height': '0px' });
        }

        var buttonElement = $('input[type="file"]').parent().find('input[type="button"]');
        $(buttonElement).click(function () {
            $(this).parent().find('input[type="file"]').click();
        });

        $('input[type="file"]').change(function () {
            var files = $(this).get(0).files;
            var ul = $(this).parent().find('#fileList');
            ul.html(null);
            if (files.length > 0) {
                for (var i = 0; i < files.length; i++) {
                    ul.append("<li>" + files[i].name + "</li>");
                }
            } else {
                ul.append("<li>Geen bestanden geselecteerd.</li>");
            }
        });
    }
    initializeFileReplacer();
});