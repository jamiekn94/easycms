﻿(function(e) {
    function o(t, o, u) {
        var a = t.success;
        var f = e.extend({}, t.data || {}, { "start-min": n(o, "u"), "start-max": n(u, "u"), singleevents: true, "max-results": 9999 });
        var l = t.currentTimezone;
        if (l) {
            f.ctz = l = l.replace(" ", "_");
        }
        return e.extend({}, t, {
            url: t.url.replace(/\/basic$/, "/full") + "?alt=json-in-script&callback=?",
            dataType: "jsonp",
            data: f,
            startParam: false,
            endParam: false,
            success: function(t) {
                var n = [];
                if (t.feed.entry) {
                    e.each(t.feed.entry, function(t, s) {
                        var o = s["gd$when"][0]["startTime"];
                        var u = r(o, true);
                        var a = r(s["gd$when"][0]["endTime"], true);
                        var f = o.indexOf("T") == -1;
                        var c;
                        e.each(s.link, function(e, t) {
                            if (t.type == "text/html") {
                                c = t.href;
                                if (l) {
                                    c += (c.indexOf("?") == -1 ? "?" : "&") + "ctz=" + l;
                                }
                            }
                        });
                        if (f) {
                            i(a, -1);
                        }
                        n.push({ id: s["gCal$uid"]["value"], title: s["title"]["$t"], url: c, start: u, end: a, allDay: f, location: s["gd$where"][0]["valueString"], description: s["content"]["$t"] });
                    });
                }
                var o = [n].concat(Array.prototype.slice.call(arguments, 1));
                var u = s(a, this, o);
                if (e.isArray(u)) {
                    return u;
                }
                return n;
            }
        });
    }

    var t = e.fullCalendar;
    var n = t.formatDate;
    var r = t.parseISO8601;
    var i = t.addDays;
    var s = t.applyAll;
    t.sourceNormalizers.push(function(e) {
        if (e.dataType == "gcal" || e.dataType === undefined && (e.url || "").match(/^(http|https):\/\/www.google.com\/calendar\/feeds\//)) {
            e.dataType = "gcal";
            if (e.editable === undefined) {
                e.editable = false;
            }
        }
    });
    t.sourceFetchers.push(function(e, t, n) {
        if (e.dataType == "gcal") {
            return o(e, t, n);
        }
    });
    t.gcalFeed = function(t, n) { return e.extend({}, n, { url: t, dataType: "gcal" }); };
})(jQuery)