﻿function HideEverything() {
    $('#buildInPages').hide();
    $('#DynamicPages').hide();
    $('#NewsArticles').hide();
    $('#BlogArticles').hide();
    $('#PhotoAlbums').hide();
    $('#ExternalLink').hide();
}

$(document).ready(function () {
    $('.col-md-2 input[type="radio"]').change(function () {
        var valueTypeId = $(this).val();

        switch (valueTypeId) {
            case "0":
                {
                    HideEverything();
                    $('#buildInPages').show();
                    break;
                }
            case "1":
                {
                    HideEverything();
                    $('#DynamicPages').show();
                    break;
                }
            case "2":
                {
                    HideEverything();
                    $('#NewsArticles').show();
                    break;
                }
            case "3":
                {
                    HideEverything();
                    $('#BlogArticles').show();
                    break;
                }
            case "4":
                {
                    HideEverything();
                    $('#ExternalLink').show();
                    break;
                }
            case "5":
                {
                    HideEverything();
                    $('#PhotoAlbums').show();
                    break;
                }
        }
    });
});