﻿$(document).ready(function() {

    // Event - Save changes
    $('#editTheme #btnSaveChanges').click(function () {
        
        // Submit form
        $('#editTheme #editFileForm').submit();
    });


    // Event - Click on file
    $('#editTheme #filesTab li').click(function (e) {

        var selectedPath = $(this).find('input[type=hidden]').eq(1).val();

        $(this).find('input[type=hidden]').eq(0).val(selectedPath);

        $(this).find('form').submit();

        e.preventDefault();
    });

    // Event - Save file
    $('#editTheme #btnSave').click(function () {

        // Submit form
        $('#editTheme #editFileForm').submit();
    });
});