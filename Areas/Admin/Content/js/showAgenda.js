﻿// Define Calendar options and events
$(document).ready(function () {  

    window.initAgenda = function (){
        $('#calendar').fullCalendar({
            lang: 'nl',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            monthNames: ["Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"],
            monthNamesShort: ['Jan', 'Feb', 'Mrt', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sept', 'Okt', 'Nov', 'Dec'],
            dayNames: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag'],
            dayNamesShort: ['Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za', 'Zo'],
            buttonText: {
                today: 'Vandaag',
                month: 'Maand',
                week: 'Week',
                day: 'Dag'
            },
            editable: false,
            events:
            {
                url: '/Agenda/GetAgenda',
                color: '#0070ab',
                textColor: 'white' // an option!
            },
            eventClick: function (event) {
                if (event.url) {
                    window.open(event.url);
                }
            }
        });
    }
});

