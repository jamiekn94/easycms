﻿//$.validator.unobtrusive.adapters.addSingleVal("numbers", "required");
//$.validator.unobtrusive.adapters.addSingleVal("adress", "required");
//$.validator.unobtrusive.adapters.addSingleVal("alphanumeric", "pattern");
//$.validator.unobtrusive.adapters.addSingleVal("housenumber", "required");
$.validator.unobtrusive.adapters.addSingleVal("customregex", "pattern");
$.validator.unobtrusive.adapters.addSingleVal("fileextension", "allowedextensions");
$.validator.unobtrusive.adapters.addSingleVal("maxfile", "sizebytes");
//$.validator.unobtrusive.adapters.addSingleVal("link", "required");
//$.validator.unobtrusive.adapters.addSingleVal("personsName", "required");
//$.validator.unobtrusive.adapters.addSingleVal("place", "required");
//$.validator.unobtrusive.adapters.addSingleVal("postcode", "required");
//$.validator.unobtrusive.adapters.addSingleVal("street", "required");
//$.validator.unobtrusive.adapters.addSingleVal("telphone", "required");
//$.validator.unobtrusive.adapters.addSingleVal("username", "required");


$.validator.addMethod("customregex", function(value, element, pattern) {
    return value.match(pattern);
});

if ($(document).has('input[type="file"]').length > 0) {
    window.setInterval(function() {
        var fileElement = $(document).find("input[type='file']");
        if (fileElement.val() != "")
            fileElement.valid();
    }, 1000);
}

$.validator.addMethod("fileextension", function(value, element, allowedextensions) {
    var arrayAllowedExtensions = allowedextensions.split(',');

    var fileExtension = "." + value.split('.').pop();
    var bool = false;

    $.each(arrayAllowedExtensions, function(index) {

        if (arrayAllowedExtensions[index] == fileExtension) {
            bool = true;
        }
    });

    return bool;
});

$.validator.addMethod("maxfile", function(value, element, sizebytes) {

    if (sizebytes >= element.files[0].size) {
        return true;
    } else {
        return false;
    }
});