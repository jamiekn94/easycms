﻿$(document).ready(function() {
    $('#fileCheckboxContainer .item').click(function() {
        if ($(this).parent().find('input[type="checkbox"]').prop("checked")) {
            $(this).parent().find('input[type="checkbox"]').attr("checked", false);
        } else {
            $(this).parent().find('input[type="checkbox"]').attr("checked", true);
        }
        $(this).parent().find('span').fadeToggle(500);
    });
});