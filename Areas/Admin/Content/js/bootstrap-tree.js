$(document).ready(function() {
    $(".tree > ul").attr("role", "tree").find("ul").attr("role", "group");
    $(".tree").find("li:has(ul)").addClass("parent_li").attr("role", "treeitem").find(" > span").on("click", function(e) {
        var t = $(this).parent("li.parent_li").find(" > ul > li");
        if (t.css("display") == "list-item") {
            t.hide("fast");
            $(this).attr("title", "Expand this branch").find(" > i").addClass("glyphicons-circle_plus").removeClass("glyphicons-circle_minus");
        } else {
            t.show("fast");
            $(this).attr("title", "Collapse this branch").find(" > i").addClass("glyphicons-circle_minus").removeClass("glyphicons-circle_plus");
        }
        e.stopPropagation();
    });
    var e = $(".tree .parent_li .parent_li ul li");
    e.hide();
})