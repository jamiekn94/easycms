//  Author: Louis Holladay
//  Website: AdminDesigns.com
//  Last Updated: 01/01/14 
// 
//  This file is reserved for changes made by the user 
//  as it's often a good idea to seperate your work from 
//  the theme. It makes modifications, and future theme
//  updates much easier 
// 

//  Place custom styles below this line 
///////////////////////////////////////

$(document).ready(function() {

    // Navigation quick search
    $('#sidebar-search input[type="text"]').keyup(function() {
        var textboxValue = $(this).val();

        $('#sidebar-menu li a .sidebar-title').each(function(index) {

            if ($(this).text().toLowerCase().search(textboxValue.toLowerCase()) == -1) {
                $(this).parent().parent().hide('fast');
            } else {
                $(this).parent().parent().show('fast');
            }

        });
    });

    $('table tr').click(function() {
        if ($(this).next().hasClass('isSubMenu')) {
            $(this).next().find('.subMenu').toggle('slow');
        }
    });

});