﻿$.extend(true, $.fn.dataTable.defaults, { sDom: "<'row'<'col-sm-12 datatables-toolbar'<'pull-right'f><'pull-left'i>r<'clearfix'>>>t<'row'<'col-sm-12 datatables-footer'<'pull-left'l><'pull-right'p><'clearfix'>>>", sPaginationType: "bs_normal", oLanguage: { sLengthMenu: "Aantal items weergeven _MENU_", sSearch: "", sInfo: "Resultaat _START_ tot _END_  weergeven - Totaal: _TOTAL_ items", sInfoEmpty: "Er zijn geen resultaten", sZeroRecords: "Geen resultaten gevonden" } });
$.extend($.fn.dataTableExt.oStdClasses, { sWrapper: "dataTables_wrapper form-inline" });
$.fn.dataTableExt.oApi.fnPagingInfo = function(e) { return { iStart: e._iDisplayStart, iEnd: e.fnDisplayEnd(), iLength: e._iDisplayLength, iTotal: e.fnRecordsTotal(), iFilteredTotal: e.fnRecordsDisplay(), iPage: e._iDisplayLength === -1 ? 0 : Math.ceil(e._iDisplayStart / e._iDisplayLength), iTotalPages: e._iDisplayLength === -1 ? 0 : Math.ceil(e.fnRecordsDisplay() / e._iDisplayLength) }; };
$.extend($.fn.dataTableExt.oPagination, {
    bs_normal: {
        fnInit: function(e, t, n) {
            var r = e.oLanguage.oPaginate;
            var i = function(t) {
                t.preventDefault();
                if (e.oApi._fnPageChange(e, t.data.action)) {
                    n(e);
                }
            };
            $(t).append('<ul class="pagination">' + '<li class="prev disabled"><a href="#"><i class="fa fa-caret-left"></i> &nbsp;' + r.sPrevious + "</a></li>" + '<li class="next disabled"><a href="#">' + r.sNext + '&nbsp;<i class="fa fa-caret-right"></i> </a></li>' + "</ul>");
            var s = $("a", t);
            $(s[0]).bind("click.DT", { action: "previous" }, i);
            $(s[1]).bind("click.DT", { action: "next" }, i);
        },
        fnUpdate: function(e, t) {
            var n = 5;
            var r = e.oInstance.fnPagingInfo();
            var i = e.aanFeatures.p;
            var s, o, u, a, f, l, c = Math.floor(n / 2);
            if (r.iTotalPages < n) {
                f = 1;
                l = r.iTotalPages;
            } else if (r.iPage <= c) {
                f = 1;
                l = n;
            } else if (r.iPage >= r.iTotalPages - c) {
                f = r.iTotalPages - n + 1;
                l = r.iTotalPages;
            } else {
                f = r.iPage - c + 1;
                l = f + n - 1;
            }
            for (s = 0, o = i.length; s < o; s++) {
                $("li:gt(0)", i[s]).filter(":not(:last)").remove();
                for (u = f; u <= l; u++) {
                    a = u == r.iPage + 1 ? 'class="active"' : "";
                    $("<li " + a + '><a href="#">' + u + "</a></li>").insertBefore($("li:last", i[s])[0]).bind("click", function(n) {
                        n.preventDefault();
                        e._iDisplayStart = (parseInt($("a", this).text(), 10) - 1) * r.iLength;
                        t(e);
                    });
                }
                if (r.iPage === 0) {
                    $("li:first", i[s]).addClass("disabled");
                } else {
                    $("li:first", i[s]).removeClass("disabled");
                }
                if (r.iPage === r.iTotalPages - 1 || r.iTotalPages === 0) {
                    $("li:last", i[s]).addClass("disabled");
                } else {
                    $("li:last", i[s]).removeClass("disabled");
                }
            }
        }
    },
    bs_two_button: {
        fnInit: function(e, t, n) {
            var r = e.oLanguage.oPaginate;
            var i = e.oClasses;
            var s = function(t) {
                if (e.oApi._fnPageChange(e, t.data.action)) {
                    n(e);
                }
            };
            var o = '<ul class="pagination">' + '<li class="prev"><a class="' + e.oClasses.sPagePrevDisabled + '" tabindex="' + e.iTabIndex + '" role="button"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;' + r.sPrevious + "</a></li>" + '<li class="next"><a class="' + e.oClasses.sPageNextDisabled + '" tabindex="' + e.iTabIndex + '" role="button">' + r.sNext + '&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></li>' + "</ul>";
            $(t).append(o);
            var u = $("a", t);
            var a = u[0], f = u[1];
            e.oApi._fnBindAction(a, { action: "previous" }, s);
            e.oApi._fnBindAction(f, { action: "next" }, s);
            if (!e.aanFeatures.p) {
                t.id = e.sTableId + "_paginate";
                a.id = e.sTableId + "_previous";
                f.id = e.sTableId + "_next";
                a.setAttribute("aria-controls", e.sTableId);
                f.setAttribute("aria-controls", e.sTableId);
            }
        },
        fnUpdate: function(e, t) {
            if (!e.aanFeatures.p) {
                return;
            }
            var n = e.oInstance.fnPagingInfo();
            var r = e.oClasses;
            var i = e.aanFeatures.p;
            var s;
            for (var o = 0, u = i.length; o < u; o++) {
                if (n.iPage === 0) {
                    $("li:first", i[o]).addClass("disabled");
                } else {
                    $("li:first", i[o]).removeClass("disabled");
                }
                if (n.iPage === n.iTotalPages - 1 || n.iTotalPages === 0) {
                    $("li:last", i[o]).addClass("disabled");
                } else {
                    $("li:last", i[o]).removeClass("disabled");
                }
            }
        }
    },
    bs_four_button: {
        fnInit: function(e, t, n) {
            var r = e.oLanguage.oPaginate;
            var i = e.oClasses;
            var s = function(t) {
                if (e.oApi._fnPageChange(e, t.data.action)) {
                    n(e);
                }
            };
            $(t).append('<ul class="pagination">' + '<li class="disabled"><a  tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPageFirst + '"><span class="glyphicon glyphicon-backward"></span>&nbsp;' + r.sFirst + "</a></li>" + '<li class="disabled"><a  tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPagePrevious + '"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;' + r.sPrevious + "</a></li>" + '<li><a tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPageNext + '">' + r.sNext + '&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></li>' + '<li><a tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPageLast + '">' + r.sLast + '&nbsp;<span class="glyphicon glyphicon-forward"></span></a></li>' + "</ul>");
            var o = $("a", t);
            var u = o[0], a = o[1], f = o[2], l = o[3];
            e.oApi._fnBindAction(u, { action: "first" }, s);
            e.oApi._fnBindAction(a, { action: "previous" }, s);
            e.oApi._fnBindAction(f, { action: "next" }, s);
            e.oApi._fnBindAction(l, { action: "last" }, s);
            if (!e.aanFeatures.p) {
                t.id = e.sTableId + "_paginate";
                u.id = e.sTableId + "_first";
                a.id = e.sTableId + "_previous";
                f.id = e.sTableId + "_next";
                l.id = e.sTableId + "_last";
            }
        },
        fnUpdate: function(e, t) {
            if (!e.aanFeatures.p) {
                return;
            }
            var n = e.oInstance.fnPagingInfo();
            var r = e.oClasses;
            var i = e.aanFeatures.p;
            var s;
            for (var o = 0, u = i.length; o < u; o++) {
                if (n.iPage === 0) {
                    $("li:eq(0)", i[o]).addClass("disabled");
                    $("li:eq(1)", i[o]).addClass("disabled");
                } else {
                    $("li:eq(0)", i[o]).removeClass("disabled");
                    $("li:eq(1)", i[o]).removeClass("disabled");
                }
                if (n.iPage === n.iTotalPages - 1 || n.iTotalPages === 0) {
                    $("li:eq(2)", i[o]).addClass("disabled");
                    $("li:eq(3)", i[o]).addClass("disabled");
                } else {
                    $("li:eq(2)", i[o]).removeClass("disabled");
                    $("li:eq(3)", i[o]).removeClass("disabled");
                }
            }
        }
    },
    bs_full: {
        fnInit: function(e, t, n) {
            var r = e.oLanguage.oPaginate;
            var i = e.oClasses;
            var s = function(t) {
                if (e.oApi._fnPageChange(e, t.data.action)) {
                    n(e);
                }
            };
            $(t).append('<ul class="pagination">' + '<li class="disabled"><a  tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPageFirst + '">' + r.sFirst + "</a></li>" + '<li class="disabled"><a  tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPagePrevious + '">' + r.sPrevious + "</a></li>" + '<li><a tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPageNext + '">' + r.sNext + "</a></li>" + '<li><a tabindex="' + e.iTabIndex + '" class="' + i.sPageButton + " " + i.sPageLast + '">' + r.sLast + "</a></li>" + "</ul>");
            var o = $("a", t);
            var u = o[0], a = o[1], f = o[2], l = o[3];
            e.oApi._fnBindAction(u, { action: "first" }, s);
            e.oApi._fnBindAction(a, { action: "previous" }, s);
            e.oApi._fnBindAction(f, { action: "next" }, s);
            e.oApi._fnBindAction(l, { action: "last" }, s);
            if (!e.aanFeatures.p) {
                t.id = e.sTableId + "_paginate";
                u.id = e.sTableId + "_first";
                a.id = e.sTableId + "_previous";
                f.id = e.sTableId + "_next";
                l.id = e.sTableId + "_last";
            }
        },
        fnUpdate: function(e, t) {
            if (!e.aanFeatures.p) {
                return;
            }
            var n = e.oInstance.fnPagingInfo();
            var r = $.fn.dataTableExt.oPagination.iFullNumbersShowPages;
            var i = Math.floor(r / 2);
            var s = Math.ceil(e.fnRecordsDisplay() / e._iDisplayLength);
            var o = Math.ceil(e._iDisplayStart / e._iDisplayLength) + 1;
            var u = "";
            var a, f, l, c;
            var h = e.oClasses;
            var p, d, v, m;
            var g = e.aanFeatures.p;
            var y = function(n) {
                e.oApi._fnBindAction(this, { page: n + a - 1 }, function(n) {
                    e.oApi._fnPageChange(e, n.data.page);
                    t(e);
                    n.preventDefault();
                });
            };
            if (e._iDisplayLength === -1) {
                a = 1;
                f = 1;
                o = 1;
            } else if (s < r) {
                a = 1;
                f = s;
            } else if (o <= i) {
                a = 1;
                f = r;
            } else if (o >= s - i) {
                a = s - r + 1;
                f = s;
            } else {
                a = o - Math.ceil(r / 2) + 1;
                f = a + r - 1;
            }
            for (l = a; l <= f; l++) {
                u += o !== l ? '<li><a tabindex="' + e.iTabIndex + '">' + e.fnFormatNumber(l) + "</a></li>" : '<li class="active"><a tabindex="' + e.iTabIndex + '">' + e.fnFormatNumber(l) + "</a></li>";
            }
            for (l = 0, c = g.length; l < c; l++) {
                m = g[l];
                if (!m.hasChildNodes()) {
                    continue;
                }
                $("li:gt(1)", g[l]).filter(":not(li:eq(-2))").filter(":not(li:eq(-1))").remove();
                if (n.iPage === 0) {
                    $("li:eq(0)", g[l]).addClass("disabled");
                    $("li:eq(1)", g[l]).addClass("disabled");
                } else {
                    $("li:eq(0)", g[l]).removeClass("disabled");
                    $("li:eq(1)", g[l]).removeClass("disabled");
                }
                if (n.iPage === n.iTotalPages - 1 || n.iTotalPages === 0) {
                    $("li:eq(-1)", g[l]).addClass("disabled");
                    $("li:eq(-2)", g[l]).addClass("disabled");
                } else {
                    $("li:eq(-1)", g[l]).removeClass("disabled");
                    $("li:eq(-2)", g[l]).removeClass("disabled");
                }
                $(u).insertBefore("li:eq(-2)", g[l]).bind("click", function(r) {
                    r.preventDefault();
                    e._iDisplayStart = (parseInt($("a", this).text(), 10) - 1) * n.iLength;
                    t(e);
                });
            }
        }
    }
});
if ($.fn.DataTable.TableTools) {
    $.extend(true, $.fn.DataTable.TableTools.classes, { container: "DTTT btn-group", buttons: { normal: "btn", disabled: "disabled" }, collection: { container: "DTTT_dropdown dropdown-menu", buttons: { normal: "", disabled: "disabled" } }, print: { info: "DTTT_print_info modal" }, select: { row: "active" } });
    $.extend(true, $.fn.DataTable.TableTools.DEFAULTS.oTags, { collection: { container: "ul", button: "li", liner: "a" } });
}