﻿$(document).ready(function () {

    // Subscribe to the ajax loaded event - Initialize upload file replacer script
    $(document).on("AjaxLoadingComplete", initializeFileReplacer);
    $(document).on("AjaxLoadingComplete", initializeFileFunctionality);

    //#region On page load

    // Show the first and second level of the folder structure
    var hideElements = $('.tree .parent_li .parent_li ul li ul li');
    hideElements.hide();

    $('#AddFolder').modal({
        show: false
    });

    $('#DeleteFolder').modal({
        show: false
    });

    $('.search').select2({
        width: '100%'
    });

    //#endregion

    //#region Events
    function initializeFileFunctionality() {

        // Add folder - public map or role permissions - hide and show
        $('body').on('change', '#AddFolder #publicMap', function() {
            var checkboxValue = $('#AddFolder #publicMap');

            if (checkboxValue.prop('checked')) {
                $('#AddFolder #choseRoles').hide('fast');
            } else {
                $('#AddFolder #choseRoles').show('fast');
            }
        });

        // Update selected file id and toggle selectedItem class for the file
        $('body').on('click', '#files .col-md-3', function() {

            var fileId = $(this).find('#contentFileId').val();
            $('#fileId').val(fileId);

            if ($(this).hasClass('selectedItem')) {
                $(this).removeClass('selectedItem');
                EmptyFileId();

            } else {
                $('#files .col-md-3').removeClass('selectedItem');
                $(this).addClass('selectedItem');
            }
        });

        // Delete file
        $('#DeleteFile').on('show.bs.modal', function(e) {
            if (CheckFileId() == true) {

                showLoadingScreen();

                // Get file id
                var fileId = GetFileId();
                $('#DeleteFileModel_FileId').val(fileId);

                $.ajax({
                    type: "Get",
                    url: "/Files/GetFile",
                    data: {
                        fileId: fileId
                    }
                }).success(function(data) {
                    var result = $.parseJSON(data);
                    $('#deleteFileName').text(result['FileName']);
                    $('#deleteFolderName').text(result['FolderName']);
                }).error(function() {
                    alert('Ajax call is mislukt, kon het bestand niet ophalen.');
                }).always(function() {
                    hideLoadingScreen();
                });
            } else {
                return false;
            }
        });

        // Add folder
        $('#AddFolder').on('show.bs.modal', function (e) {

            if (CheckFolderId() == true) {
                // Get file id
                var folderId = $('#folderId').val();
                $('#AddFolder #FolderActionModel_ValueFolderId').val(folderId);
            } else {
                return false;
            }
        });

        // Edit folder
        $('#EditFolder').on('show.bs.modal', function(e) {

            if (CheckFolderId() == true) {

                // Get folder id
                var folderId = GetFolderId();

                // Can't delete the primary map
                if (folderId != 3) {

                    showLoadingScreen();

                    $('#EditFolder #FolderActionModel_ValueFolderId').val(GetFolderId());

                    // Get foldername
                    $.ajax({
                        type: "Get",
                        url: "/Files/GetFolderName",
                        data: {
                            folderId: folderId
                        }
                    }).success(function(data) {
                        $('#EditFolder #FolderActionModel_FolderName').val(data);
                    }).error(function() {
                        alert('Ajax call is mislukt, kon de foldernaam niet ophalen.');
                    }).always(function() {
                        hideLoadingScreen();
                    });

                    // Get permissions
                    $.ajax({
                        url: "/Files/GetFolderPermissions",
                        data: {
                            folderId: folderId
                        }
                    }).success(function(data) {
                        var json = $.parseJSON(data);
                        var selectedValues = [];
                        $.each(json, function(bb) {
                            selectedValues.push(json[bb].Id);
                        });
                        $(".search").select2('val', selectedValues);
                    }).error(function() {
                        alert('Ajax call is mislukt, kon de rollen niet ophalen');
                    }).always(function() {
                        hideLoadingScreen();
                    });

                } else {
                    alert('U kunt de hoofdmap niet wijzigen.');
                    return false;
                }
            } else {
                return false;
            }

        });


        // Delete folder
        $('#DeleteFolder').on('show.bs.modal', function(e) {
            alert('delete folder');
            if (CheckFolderId() == true) {
                // Get folder id
                var folderId = GetFolderId();

                // Can't delete the primary map
                if (folderId != 3) {
                    showLoadingScreen();
                    $('#DeleteFolderModel_FolderId').val(GetFolderId());

                    // Get foldername
                    $.ajax({
                        type: "Get",
                        url: "/Files/GetFolderName",
                        data: {
                            folderId: folderId
                        }
                    }).success(function(data) {
                        $('#deleteFolderName').text(data);
                    }).error(function() {
                        alert('Ajax call is mislukt, kon de foldernaam niet ophalen.');
                    }).always(function() {
                        hideLoadingScreen();
                    });
                } else {
                    alert('U kunt de hoofdmap niet verwijderen.');
                    return false;
                }
            } else {
                return false;
            }

        });

        // Add file
        $('#AddFile').on('show.bs.modal', function (e) {

            if (CheckFolderId() == true) {
                $('#FileModel_ValueFolderId').val(GetFolderId());
            } else {
                return false;
            }
        });

        // Edit file
        $('#EditFile').on('show.bs.modal', function(e) {
            if (CheckFileId() == true) {

                showLoadingScreen();

                // Show the textbox and label again
                $('#EditFileModel_Content').show();
                $('#EditFileModel_Content').parent().find('label').show();

                // Get file id
                var fileId = GetFileId();
                $('#EditFileModel_FileId').val(fileId);

                $.ajax({
                    type: "Get",
                    url: "/Files/GetFile",
                    data: {
                        fileId: fileId
                    }
                }).success(function(data) {
                    var result = $.parseJSON(data);
                    $('#EditFileModel_FileName').val(result['FileName']);
                    if (result['Content'] != null) {
                        $('#EditFileModel_Content').text(result['Content']);
                    } else {
                        $('#EditFileModel_Content').hide();
                        $('#EditFileModel_Content').parent().find('label').hide();
                    }
                }).error(function() {
                    alert('Ajax call is mislukt, kon het bestand niet ophalen.');
                }).always(function() {
                    hideLoadingScreen();
                });
            } else {
                return false;
            }
        });

        // Launch checkbox value change event
        $('body').on('click', $(':checkbox').parent(), function(evt) {
            if (evt.target.type !== 'checkbox') {
                var $checkbox = $(":checkbox", this);
                $checkbox.attr('checked', !$checkbox.attr('checked'));
                $checkbox.change();
            }
        });


        $('body').find('.tree li:has(ul)').addClass('parent_li').attr('role', 'treeitem');

        // Load files from the selected folder
        $('body').on('click', '.tree li:has(ul) > span', function(e) {

            var children = $(this).parent('li.parent_li').find(' > ul > li');

            // Isn't this the filemanager label itself?
            if (!$(this).hasClass('head')) {
                // Remove previous class
                $('#folders .tree span').removeClass('selectedItem');
                $(this).addClass('selectedItem');

                // Get folder id
                var folderId = $(this).parent().find('#treeFolderId').val();

                // Load files -- only if it's not the head of the folder list
                if (!$(this).hasClass('head') && folderId != GetFolderId()) {
                    LoadFiles(folderId);
                }

                // Update folder id
                $('#folderId').val(folderId);
                if (children.css('display') == "list-item") {
                    children.hide('fast', function() {
                    });
                    $(this).attr('title', 'Expand this branch').find(' > i').addClass('glyphicons-circle_plus').removeClass('glyphicons-circle_minus');
                } else {
                    children.show('fast', function() {
                    });
                    $(this).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicons-circle_minus').removeClass('glyphicons-circle_plus');
                }
                e.stopPropagation();
            }
        });

        //#endregion

        //#region Functions

        // Set the active file id to none
        function EmptyFileId() {
            $('#fileId').val("");
        }

        // Load files from a specific folder
        function LoadFiles(folderId) {
            showLoadingScreen();
            $.ajax({
                type: "Get",
                url: "/Files/LoadFiles",
                data: {
                    folderId: folderId
                }
            }).success(function(data) {
                $('#files').html(data);
            }).error(function() {
                alert('Ajax call is mislukt, kon het bestand niet ophalen.');
            }).always(function() {
                hideLoadingScreen();
            });
        }

        // Get the active folder id
        function GetFolderId() {
            return $('#folderId').val();
        }

        // Check if a folder is selected
        function CheckFolderId() {
            if ($('#folderId').val() == "") {
                alert('U heeft geen folder gekozen.');
                return false;
            } else {
                return true;
            }
        }

        // Check if there is a file selected
        function CheckFileId() {
            if (GetFileId() == "") {
                alert('U heeft geen bestand gekozen.');
                return false;
            } else {
                return true;
            }
        }

        // Get file id
        function GetFileId() {
            return $('#fileId').val();
        }

        // Update files height to match the folders height
        function updateHeight() {
            if ($('#folders').outerHeight(true) > $('#filesContainer').outerHeight(true) - 20) {
                $('#filesContainer').height($('#folders').outerHeight(true) + 20);
            } else {
                $('#filesContainer').height('auto');
            }
        }

        //#endregion

        //#region Context Menu - Right mouse click - File events
        // Right mouse click on files
        $.contextMenu({
            // define which elements trigger this menu
            selector: "#files .col-md-3",
            trigger: 'right',
            // define the elements of the menu
            items: {
                download: {
                    name: "Download",
                    icon: 'download',
                    callback: function() {

                        var fileId = GetFileId();
                        var url = "http://" + $('#url').val() + "/Admin/Files/Download?id=" + fileId;

                        window.open(url, 'Download bestand');
                    }
                },
                edit: {
                    name: "Wijzigen",
                    icon: 'edit',
                    callback: function() {
                        $('#EditFile').modal({
                            show: true
                        });
                    }
                },
                delete: {
                    name: "Verwijderen",
                    icon: 'delete',
                    callback: function() {
                        $('#DeleteFile').modal({
                            show: true
                        });
                    }
                }
            }
        });
        //#endregion
    }
});
