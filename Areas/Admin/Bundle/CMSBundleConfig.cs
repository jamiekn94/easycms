﻿#region

using System.Collections.Generic;
using System.IO;
using System.Web.Optimization;

#endregion

namespace Kapsters.Areas.Admin.Bundle
{
    internal class NonOrderingBundleOrderer : IBundleOrderer
    {
        public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            return files;
        }
    }

    public class CMSBundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            var styleBundle = new StyleBundle("~/Areas/Admin/Content/stylesheets").Include(
                //"~/Areas/Admin/Content/css/animate.css",
                //"~/Areas/Admin/Content/css/pages.css",
                
                "~/Areas/Admin/Content/css/font-awesome-min.css",
                "~/Areas/Admin/Content/css/glyphicons-min.css",
                "~/Areas/Admin/Content/css/fonts.css",
                "~/Areas/Admin/Content/css/bootstrap-min.css",
                "~/Areas/Admin/Content/css/theme.css",
                "~/Areas/Admin/Content/css/jquery-ui.css",
                "~/Areas/Admin/Content/css/pages.css",
                "~/Areas/Admin/Content/css/custom.css",
                "~/Areas/Admin/Content/css/responsive.css"
                );

            var styleDatabundle = new StyleBundle("~/Areas/Admin/Content/css/datatables")
                .Include("~/Areas/Admin/Content/css/fooTable/footable-core-min.css");

            var tinyMce = new ScriptBundle("~/Areas/Admin/Content/tinymce")
                .Include("~/Areas/Admin/Content/js/tinymce/tinymce-min.js")
                .Include("~/Areas/Admin/Content/js/tinymce/langs/nl.js");

            var selectTwo = new ScriptBundle("~/Areas/Admin/Content/select2")
               .Include("~/Areas/Admin/Content/js/select2-min.js")
               .Include("~/Areas/Admin/Content/js/select2_locale_nl.js");

            var dataTables = new ScriptBundle("~/Areas/Admin/Content/js/datatables")
                .Include("~/Areas/Admin/Content/js/fooTable/footable.js")
                .Include("~/Areas/Admin/Content/js/fooTable/footable-filter.js")
                .Include("~/Areas/Admin/Content/js/fooTable/footable-grid.js")
                .Include("~/Areas/Admin/Content/js/fooTable/footable-sort.js");


            var jsBundle = new ScriptBundle("~/Areas/Admin/Content/javascripts").Include(
                "~/Areas/Admin/Content/js/jquery-min.js",
                "~/Areas/Admin/Content/js/bootstrap-min.js",
                "~/Areas/Admin/Content/js/shared.js",
                "~/Areas/Admin/Content/js/custom.js",
                "~/Areas/Admin/Content/js/customizer.js",
                "~/Areas/Admin/Content/js/jquery-ui-min.js",
                "~/Areas/Admin/Content/js/uniform-min.js",
                "~/Areas/Admin/Content/js/jquery-unobtrusive-ajax-min.js",
                "~/Areas/Admin/Content/js/jquery-validate.js",
                "~/Areas/Admin/Content/js/jquery-validate-unobtrusive-min.js",
                "~/Areas/Admin/Content/js/ajaxForm.js",
                "~/Areas/Admin/Content/js/customValidationAdapters.js",
                "~/Areas/Admin/Content/js/globalize.js",
                "~/Areas/Admin/Content/js/globalize-culture-nl-NL.js"
                //"~/Content/js/responsive/html5Shiv.js",
                //"~/Content/js/respond.js"
                );

            jsBundle.Orderer = new NonOrderingBundleOrderer();
            tinyMce.Orderer = new NonOrderingBundleOrderer();
            selectTwo.Orderer = new NonOrderingBundleOrderer();
            dataTables.Orderer = new NonOrderingBundleOrderer();
            styleBundle.Orderer = new NonOrderingBundleOrderer();
            styleDatabundle.Orderer = new NonOrderingBundleOrderer();

            bundles.Add(styleBundle);
            bundles.Add(jsBundle);
            bundles.Add(tinyMce);
            bundles.Add(selectTwo);
            bundles.Add(dataTables);
            bundles.Add(styleDatabundle);

            BundleTable.EnableOptimizations = false;
        }
    }
}