﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Common;
using Common.Models.Home.SEO;
using Common.Providers.Cookie;
using Common.Providers.Users;
using Common.Repositories;

namespace Kapsters.Infrastructure.ActionFilters
{
    public class GlobalActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            #region Check Post

            if (System.Web.HttpContext.Current.Request.HttpMethod == "POST")
            {
                // Impossible - If there is an post, there should be an url refferer
                if (System.Web.HttpContext.Current.Request.UrlReferrer == null)
                {
                    // End response
                    System.Web.HttpContext.Current.Response.End();

                    // Redirect back to home
                    System.Web.HttpContext.Current.Response.Redirect("~/Common/Controllers/Home");
                }
            }
            #endregion

            if ((!filterContext.HttpContext.Request.IsAjaxRequest() && filterContext.Controller.ControllerContext.IsChildAction == false))
            {
                #region Seo

                var seoPageRepository = new SeoPageRepository();

                var seoPageInfo = seoPageRepository.GetSeo((string)filterContext.RouteData.Values["controller"],
                    (string)filterContext.RouteData.Values["action"], false);

                if (seoPageInfo != null)
                {
                    var seoModel = new SeoModel
                    {
                        Description = seoPageInfo.SEODescription,
                        Title = seoPageInfo.Titel,
                        Keywords = seoPageInfo.SEOKeywords,
                        Hide = seoPageInfo.Hide
                    };

                    filterContext.Controller.ViewData["SEO"] = seoModel;
                }

                #endregion

                #region Visits & Hits

                var statsVisitsRepository = new DashboardStatisticsRepository();

                if (System.Web.HttpContext.Current.Request.UserAgent != null)
                {
                    string hostAdress = System.Web.HttpContext.Current.Request.UserAgent.ToLower();

                        // Isn't this pingdom?
                        if (!hostAdress.Contains("pingdom"))
                        {
                            #region Hits

                            // Useragent
                            string strRequestUrl = System.Web.HttpContext.Current.Request.Url.ToString();
                            // If the beginrequest doesn't get called by a file
                            string strReferrerUrl = System.Web.HttpContext.Current.Request.UrlReferrer == null
                                ? ""
                                : System.Web.HttpContext.Current.Request.UrlReferrer.ToString();

                            // Is it another page?
                            if (strReferrerUrl != strRequestUrl)
                            {
                                // Check if a new column has been made for the statistics
                                if (!statsVisitsRepository.HasTodayColumn())
                                {
                                    // Insert value
                                    statsVisitsRepository.Add(new StatsVisit
                                    {
                                        Date = DateTime.Now,
                                        Pageviews = 1,
                                        Visitors = 1
                                    });
                                }
                                else
                                {
                                    // Update value
                                    statsVisitsRepository.UpdatePageViews();
                                }
                            }

                            #endregion

                            #region Visits

                            // Unique visit?
                            if (Cookie.GetCookie("Visitor") != DateTime.Now.ToShortDateString())
                            {
                                statsVisitsRepository.UpdateVisitors();
                                Cookie.SetCookie("Visitor", DateTime.Now.ToShortDateString(),
                                    DateTime.Now.AddYears(1));
                            }

                            #endregion

                            // Save
                            statsVisitsRepository.Save();
                        }
                }

                #endregion

                #region Check Bann

                // If the user is authenticated
                if (filterContext.Controller.ControllerContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    // If the user is logged out
                    if (UserProvider.IsUserLocked(filterContext.Controller.ControllerContext.HttpContext.User.Identity.Name))
                    {
                        // End current response
                        filterContext.Controller.ControllerContext.HttpContext.Response.End();

                        // Redirect to log out
                        filterContext.Controller.ControllerContext.HttpContext.Response.Redirect("/Uitloggen");

                        // Get user
                        MembershipUser userInfo = System.Web.Security.Membership.GetUser(filterContext.Controller.ControllerContext.HttpContext.User.Identity.Name);

                        // Set message
                        if (userInfo != null)
                            Common.Providers.Messages.Message.SetError(filterContext.Controller, userInfo.Comment);
                    }
                }
                #endregion

                base.OnActionExecuting(filterContext);
            }
        }
    }
}