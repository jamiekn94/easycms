﻿#region

using System.Web.Mvc;
using Common.Models.Home.SEO;

#endregion

namespace Kapsters.Classes.Seo
{
    public static class Seo
    {
        public static void SetSeo(this Controller controller, string title, string description, string keywords)
        {
            controller.ViewData["SEO"] = new SeoModel
            {
                Title = title,
                Description = description,
                Keywords = keywords
            };
        }
    }
}