﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Common;
using Common.Models.Home.Fotoalbum;
using Common.Repositories;
using PagedList;

namespace Kapsters.Classes.PhotoAlbum
{
    public class PhotoAlbumService
    {
        public StaticPagedList<PhotoAlbumModel> GetPhotoAlbums(int page)
        {
            var photoAlbumRepository = new PhotoAlbumRepository();
            var tempListPhotoAlbums = new Collection<PhotoAlbumModel>();

            foreach (var photoAlbum in photoAlbumRepository.GetPage(page, GetAmountItemsPerPage()))
            {
                tempListPhotoAlbums.Add(CreatePhotoAlbum(photoAlbum, false));
            }

            var pagesAndAmountItems = photoAlbumRepository.GetAmount(GetAmountItemsPerPage());

            return new StaticPagedList<PhotoAlbumModel>(tempListPhotoAlbums, page, GetAmountItemsPerPage(), pagesAndAmountItems[1]);
        }

        public StaticPagedList<PhotoAlbumModel> GetPhotoAlbums(int page, int amountAlbums)
        {
            var photoAlbumRepository = new PhotoAlbumRepository();
            var tempListPhotoAlbums = new Collection<PhotoAlbumModel>();

            foreach (var photoAlbum in photoAlbumRepository.GetPage(page, amountAlbums))
            {
                tempListPhotoAlbums.Add(CreatePhotoAlbum(photoAlbum, false));
            }

            var pagesAndAmountItems = photoAlbumRepository.GetAmount(amountAlbums);

            return new StaticPagedList<PhotoAlbumModel>(tempListPhotoAlbums, page, amountAlbums, pagesAndAmountItems[1]);
        }

        public StaticPagedList<PhotoAlbumModel> GetPhotoAlbumsBySearch(string searchTerm, int page)
        {
            var photoAlbumRepository = new PhotoAlbumRepository();
            var tempListPhotoAlbums = new Collection<PhotoAlbumModel>();

            var pagesAndAmountItems = photoAlbumRepository.GetAmount(GetAmountItemsPerPage(), searchTerm);

            foreach (var photoAlbum in photoAlbumRepository.GetPagesBySearch(searchTerm, page, GetAmountItemsPerPage()))
            {
                tempListPhotoAlbums.Add(CreatePhotoAlbum(photoAlbum, false));
            }
            return new StaticPagedList<PhotoAlbumModel>(tempListPhotoAlbums, page, GetAmountItemsPerPage(), pagesAndAmountItems[1]);
        }

        private int GetAmountItemsPerPage()
        {
            const int itemsPage = 5;

            return itemsPage;
        }

        private PhotoAlbumModel CreatePhotoAlbum(Fotoalbum photoAlbum, bool selectAllPhotos)
        {
            if (photoAlbum != null)
            {
                var newPhotoAlbum = new PhotoAlbumModel
                {
                    AlbumId = photoAlbum.AlbumID,
                    Title = photoAlbum.Titel,
                    Url = CleanUrl.GetCleanUrl(photoAlbum.Titel),
                    Photos = selectAllPhotos ? GetPhotos(photoAlbum.AlbumID) : GetFirstPhoto(photoAlbum.AlbumID),
                    Description = photoAlbum.Descriptie,
                    Keywords = photoAlbum.Keywoorden
                };

                return newPhotoAlbum;

            }
            return null;
        }


        public PhotoAlbumModel GetAlbum(int photoAlbumId, bool selectAllPhotos)
        {
            var photoAlbumRepository = new PhotoAlbumRepository();

            var photoAlbumInfo = photoAlbumRepository.GetById(photoAlbumId);

            if (photoAlbumInfo != null)
            {
                var newPhotoAlbum = new PhotoAlbumModel
                {
                    AlbumId = photoAlbumInfo.AlbumID,
                    Title = photoAlbumInfo.Titel,
                    Url = CleanUrl.GetCleanUrl(photoAlbumInfo.Titel),
                    Photos = selectAllPhotos ? GetPhotos(photoAlbumId) : GetFirstPhoto(photoAlbumId),
                    Description = photoAlbumInfo.Descriptie,
                    Keywords = photoAlbumInfo.Keywoorden
                };

                return newPhotoAlbum;

            }
            return null;
        }

        private IEnumerable<PhotoModel> GetPhotos(int photoAlbumId)
        {
            var photoRepository = new PhotosInAlbumRepository();

            var photosDbList = photoRepository.GetPhotosFromAlbum(photoAlbumId);

            var returnList = new Collection<PhotoModel>();

            foreach (var photo in photosDbList)
            {
                returnList.Add(new PhotoModel
                {
                    Name = photo.Naam,
                    PhotoId = photo.FotoID
                });
            }

            return returnList;
        }

        private IEnumerable<PhotoModel> GetFirstPhoto(int photoAlbumId)
        {
            var photoRepository = new PhotosInAlbumRepository();

            var photoInfo = photoRepository.GetFirstPhotoFromAlbum(photoAlbumId);

            var returnList = new Collection<PhotoModel>();

            if (photoInfo != null)
            {
                returnList.Add(new PhotoModel
                {
                    Name = photoInfo.Naam,
                    PhotoId = photoInfo.FotoID
                });
            }

            return returnList;
        }
    }
}