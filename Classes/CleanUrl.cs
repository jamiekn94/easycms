﻿#region

using System;
using System.Text.RegularExpressions;

#endregion

namespace Kapsters.Classes
{
    public class CleanUrl
    {
        public static string GetCleanUrl(string input)
        {
            var r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)",
                RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

            var strRemovedSpecialCharacters = r.Replace(input, String.Empty);
            return strRemovedSpecialCharacters.Replace(" ", "-");
        }
    }
}