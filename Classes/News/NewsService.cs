﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Common;
using Common.Models.Home.News;
using Common.Providers.Caching;
using Common.Providers.Users;
using Common.Repositories;
using PagedList;

namespace Kapsters.Classes.News
{
    public class NewsService
    {
        public IEnumerable<NewsReactionModel> GetNewsComments(int newsArticleId)
        {
            var newsCommentsRepository = new NewsCommentsRepository();

            var listDatabaseComments = newsCommentsRepository.GetListByArticleId(newsArticleId);

            var listCommentsModel = new Collection<NewsReactionModel>();

            foreach (var newsReaction in listDatabaseComments)
            {
                string name;
                string username = string.Empty;

                if (newsReaction.ReactieGebruikersId == null)
                {
                    name = newsReaction.Name;
                }
                else
                {
                    var reactionIserInformation =
                        UserProvider.GetUserInfo(newsReaction.ReactieGebruikersId);
                    name =
                        reactionIserInformation.FullName;
                    username = reactionIserInformation.Username;
                }

                listCommentsModel.Add(new NewsReactionModel
                {
                    Date = newsReaction.Datum,
                    Message = newsReaction.Bericht,
                    Name = name,
                    Username = username,
                    CommentId = newsReaction.ReactieID
                });
            }

            return listCommentsModel;
        }

        private int GetAmountItemsToShow()
        {
            // Amount of news articles to show on every page
            const int amountItems = 5;

            return amountItems;
        }

        public StaticPagedList<NewsModel> GetNewsArticlesBySearchTerm(int page, string searchTerm)
        {
            // Initialize repositories
            var newsRepository = new NieuwsRepository();

            // Get news articles from this database page
            var listNews = newsRepository.GetArticlesBySearchTerm(searchTerm, page, GetAmountItemsToShow());

            // Temp list to hold the news articles
            var listNewsModel = new Collection<NewsModel>();

            // ADd models to the list
            foreach (var news in listNews)
            {
                listNewsModel.Add(GetBasicArticleInfo(news.ArtikelID));
            }

            // Get max amount pages of news articles there are and how much items
            var pagesAndItemsAmount = newsRepository.GetAmountSearchTerm(GetAmountItemsToShow(), searchTerm);

            // Set news articles to list
            return new StaticPagedList<NewsModel>(listNewsModel, page, GetAmountItemsToShow(),
                pagesAndItemsAmount[1]);
        }

        public StaticPagedList<NewsModel> GetNewsArticles(int? page)
        {
            // Initialize repositories
            var newsRepository = new NieuwsRepository();

            // Get news articles from this database page
            var listNews = newsRepository.GetPage(page ?? 1, GetAmountItemsToShow());

            // Temp list to hold the news articles
            var listNewsModel = new Collection<NewsModel>();

            // ADd models to the list
            foreach (var news in listNews)
            {
                listNewsModel.Add(CreateNewsModel(news));
            }

            // Get max amount pages of news articles there are and how much items
            var pagesAndItemsAmount = newsRepository.GetAmount(GetAmountItemsToShow());

            // Set news articles to list
            return new StaticPagedList<NewsModel>(listNewsModel, page ?? 1, GetAmountItemsToShow(),
                pagesAndItemsAmount[1]);

        }

        public StaticPagedList<NewsModel> GetNewsArticles(int? page, int amountArticles)
        {
            // Initialize repositories
            var newsRepository = new NieuwsRepository();

            // Get news articles from this database page
            var listNews = newsRepository.GetPage(page ?? 1, amountArticles);

            // Temp list to hold the news articles
            var listNewsModel = new Collection<NewsModel>();

            // ADd models to the list
            foreach (var news in listNews)
            {
                listNewsModel.Add(CreateNewsModel(news));
            }

            // Get max amount pages of news articles there are and how much items
            var pagesAndItemsAmount = newsRepository.GetAmount(amountArticles);

            // Set news articles to list
            return new StaticPagedList<NewsModel>(listNewsModel, page ?? 1, amountArticles,
                pagesAndItemsAmount[1]);

        }

        private NewsModel CreateNewsModel(Nieuwsartikel newsDbArticle)
        {

            if (newsDbArticle != null)
            {
                // Initialize news reactions repository
                var newsReactionsRepository = new NewsCommentsRepository();

                // Get userinfo from this author
                var userInfo = UserProvider.GetUserInfo(newsDbArticle.AuteurId);

                return new NewsModel
                {
                    AuthorName = userInfo.FullName,
                    NewsId = newsDbArticle.ArtikelID,
                    Date = newsDbArticle.Datum,
                    AmountComments = newsReactionsRepository.GetAmountComments(newsDbArticle.ArtikelID),
                    CommentsEnabled = newsDbArticle.ReactiesToegestaan,
                    Information = newsDbArticle.Informatie,
                    Title = newsDbArticle.Titel,
                    Username = userInfo.Username,
                    Keywords = newsDbArticle.Keywords,
                    Url = Classes.CleanUrl.GetCleanUrl(newsDbArticle.Titel),
                    Image = newsDbArticle.Image,
                    Intro = newsDbArticle.Intro
                };
            }
            return null;
        }

        public NewsModel GetBasicArticleInfo(int newsArticleId)
        {
            // Initialize repositories
            var newsRepository = new NieuwsRepository();

            // Get news info
            var newsInfo = newsRepository.GetById(newsArticleId);

            if (newsInfo != null)
            {
                return CreateNewsModel(newsInfo);
            }

            return null;
        }

        public IEnumerable<string> GetKeywords()
        {
            Collection<string> returnList;

            if (ServerCachingProvider.IsSet("NewsKeywords"))
            {
                returnList = ServerCachingProvider.Get("NewsKeywords") as Collection<string>;
            }
            else
            {
                var newsRepository = new NieuwsRepository();

                var arrayDbKeywords = newsRepository.GetKeywords();

                returnList = new Collection<string>();

                foreach (var dbKeyword in arrayDbKeywords)
                {
                    var arrayKeywords = dbKeyword.Split(',');

                    foreach (var keyword in arrayKeywords)
                    {
                        string cleanKeyword = keyword.Trim();

                        if (returnList.All(x => x != cleanKeyword))
                        {
                            returnList.Add(cleanKeyword);
                        }
                    }
                }

                ServerCachingProvider.Set("NewsKeywords", returnList, 15);
            }
            return returnList;
        }

        public StaticPagedList<NewsModel> GetArticlesByKeyword(int page, string keyword)
        {
            // Initialize repositories
            var newsRepository = new NieuwsRepository();

            // Get news articles from this database page
            var listNews = newsRepository.GetArticlesByKeyword(keyword, page, GetAmountItemsToShow());

            // Temp list to hold the news articles
            var listNewsModel = new Collection<NewsModel>();

            // ADd models to the list
            foreach (var news in listNews)
            {
                listNewsModel.Add(GetBasicArticleInfo(news.ArtikelID));
            }

            // Get max amount pages of news articles there are and how much items
            var pagesAndItemsAmount = newsRepository.GetAmountKeywords(GetAmountItemsToShow(), keyword);

            // Set news articles to list
            return new StaticPagedList<NewsModel>(listNewsModel, page, GetAmountItemsToShow(),
                pagesAndItemsAmount[1]);
        }
    }
}