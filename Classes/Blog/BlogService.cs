﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Common.Models.Home.Blog;
using Common.Providers.Users;
using Common.Repositories;
using PagedList;

namespace Kapsters.Classes.Blog
{
    public class BlogService
    {
        private int GetAmountItemsToShow()
        {
            const int amountItemsPage = 5;

            return amountItemsPage;
        }

        public StaticPagedList<BlogModel> GetBlogArticles(int? page)
        {
            var blogRepository = new BlogRepository();

            var listBlog = blogRepository.GetPage(page ?? 1, GetAmountItemsToShow());

            var listBlogViewModel = new Collection<BlogModel>();

            foreach (var blog in listBlog)
            {
                listBlogViewModel.Add(CreateBlogModel(blog));
            }

            var pagesAndItemsAmount = blogRepository.GetAmount(GetAmountItemsToShow());

            return new StaticPagedList<BlogModel>(listBlogViewModel, page ?? 1, GetAmountItemsToShow(), pagesAndItemsAmount[1]);
        }

        public StaticPagedList<BlogModel> GetBlogArticles(int? page, int amountArticles)
        {
            var blogRepository = new BlogRepository();

            var listBlog = blogRepository.GetPage(page ?? 1, amountArticles);

            var listBlogViewModel = new Collection<BlogModel>();

            foreach (var blog in listBlog)
            {
                listBlogViewModel.Add(CreateBlogModel(blog));
            }

            var pagesAndItemsAmount = blogRepository.GetAmount(amountArticles);

            return new StaticPagedList<BlogModel>(listBlogViewModel, page ?? 1, amountArticles, pagesAndItemsAmount[1]);
        }

        public StaticPagedList<BlogModel> GetNewsArticlesBySearchTerm(int page, string searchTerm)
        {
            // Initialize repositories
            var blogRepository = new BlogRepository();

            // Get news articles from this database page
            var listBlog = blogRepository.GetArticlesBySearchTerm(searchTerm, page, GetAmountItemsToShow());

            // Temp list to hold the news articles
            var listBlogArticles = new Collection<BlogModel>();

            // ADd models to the list
            foreach (var blogArticle in listBlog)
            {
                listBlogArticles.Add(CreateBlogModel(blogArticle));
            }

            // Get max amount pages of blog articles there are and how much items
            var pagesAndItemsAmount = blogRepository.GetAmount(GetAmountItemsToShow(), searchTerm);

            // Set news articles to list
            return new StaticPagedList<BlogModel>(listBlogArticles, page, GetAmountItemsToShow(),
                pagesAndItemsAmount[1]);
        }

        private BlogModel CreateBlogModel(Common.Blog blogArticle)
        {
            var blogReactionsRepository = new BlogReactionRepository();

            if (blogArticle != null)
            {
                var userInfo = UserProvider.GetUserInfo(blogArticle.AuteurID);

                return new BlogModel
                {
                    AuthorName = userInfo.FullName,
                    BlogId = blogArticle.BlogID,
                    Date = blogArticle.Datum,
                    AmountComments = blogReactionsRepository.GetAmountComments(blogArticle.BlogID),
                    CommentsEnabled = blogArticle.ReactiesToegestaan,
                    Information = blogArticle.Informatie,
                    Title = blogArticle.Titel,
                    Username = userInfo.Username,
                    Keywords = blogArticle.Keywords,
                    Url = Classes.CleanUrl.GetCleanUrl(blogArticle.Titel),
                    Image = blogArticle.Image,
                    Intro = blogArticle.Intro
                };
            }

            return null;
        }

        public BlogModel GetBlogInfo(int blogId)
        {
            var blogRepository = new BlogRepository();
            var blogReactionsRepository = new BlogReactionRepository();

            var blogInfo = blogRepository.GetById(blogId);

            if (blogInfo != null)
            {
                var userInfo = UserProvider.GetUserInfo(blogInfo.AuteurID);

                return new BlogModel
                {
                    AuthorName = userInfo.FullName,
                    BlogId = blogInfo.BlogID,
                    Date = blogInfo.Datum,
                    AmountComments = blogReactionsRepository.GetAmountComments(blogInfo.BlogID),
                    CommentsEnabled = blogInfo.ReactiesToegestaan,
                    Information = blogInfo.Informatie,
                    Title = blogInfo.Titel,
                    Username = userInfo.Username,
                    Keywords = blogInfo.Keywords,
                    Url = Classes.CleanUrl.GetCleanUrl(blogInfo.Titel),
                    Image = blogInfo.Image,
                    Intro = blogInfo.Intro
                };
            }

            return null;
        }

        public IEnumerable<BlogReactionModel> GetBlogComments(int blogId)
        {
            var blogReactionsRepository = new BlogReactionRepository();

            var returnList = new Collection<BlogReactionModel>();

            foreach (var blogReaction in blogReactionsRepository.GetListByArticleId(blogId))
            {
                string name;
                string username = string.Empty;

                if (blogReaction.ReactieGebruikersID == null)
                {
                    name = blogReaction.Name;
                }
                else
                {
                    var reactionUserInformation =
                        UserProvider.GetUserInfo(blogReaction.ReactieGebruikersID);
                    name =
                        reactionUserInformation.FullName;
                    username = reactionUserInformation.Username;
                }

                returnList.Add(new BlogReactionModel
                {
                    Date = blogReaction.Datum,
                    Message = blogReaction.Bericht,
                    Name = name,
                    Username = username,
                    CommentId = blogReaction.ReactieID
                });
            }

            return returnList;
        }

        public StaticPagedList<BlogModel> GetArticlesByKeyword(int page, string keyword)
        {
            // Initialize repositories
            var blogRepository = new BlogRepository();

            // Get news articles from this database page
            var listBlog = blogRepository.GetArticlesByKeyword(keyword, page, GetAmountItemsToShow());

            // Temp list to hold the news articles
            var listBlogModel = new Collection<BlogModel>();

            // ADd models to the list
            foreach (var blog in listBlog)
            {
                listBlogModel.Add(GetBlogInfo(blog.BlogID));
            }

            // Get max amount pages of news articles there are and how much items
            var pagesAndItemsAmount = blogRepository.GetAmountKeywords(GetAmountItemsToShow(), keyword);

            // Set news articles to list
            return new StaticPagedList<BlogModel>(listBlogModel, page, GetAmountItemsToShow(),
                pagesAndItemsAmount[1]);
        }
    }
}