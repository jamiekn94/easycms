﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIShell.OSGi.MvcCore.Custom
{
    class CustomOptions
    {
        public bool Cache { get; set; }
        public int AmountMinutes { get; set; }
    }
}
