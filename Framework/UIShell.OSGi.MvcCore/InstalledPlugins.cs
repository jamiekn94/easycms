﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Common.Repositories;

namespace UIShell.OSGi.MvcCore
{
    class InstalledPlugins
    {
        // Kind -- 0 = page, 1 = partial
        public static string GetPluginName(ControllerContext controllerContext)
        {
            return GetPluginName((string) controllerContext.RouteData.Values["controller"],
                (string) controllerContext.RouteData.Values["action"]);
        }

        public static string GetPluginName(RequestContext requestContext)
        {
            return GetPluginName((string)requestContext.RouteData.Values["controller"],
                (string)requestContext.RouteData.Values["action"]);
        }

        public static string GetPluginName(string controller, string action)
        {
            var pluginPagesRepository = new PluginPagesRepository();

            var pluginPage =
                pluginPagesRepository.GetList().FromCache(15).FirstOrDefault(x => x.Action.ToLower() == action.ToLower() && x.Controller.ToLower() == controller.ToLower());

            if (pluginPage != null)
            {
                if (pluginPage.PluginId != null)
                {
                    var pluginRepository = new PluginRepository();
                    return pluginRepository.GetSymbolicNameById((int)pluginPage.PluginId);
                }
            }

            return string.Empty;
        }
    }
}
