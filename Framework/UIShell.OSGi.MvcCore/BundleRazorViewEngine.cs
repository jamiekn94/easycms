using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UIShell.OSGi.MvcCore
{
    public class BundleRazorViewEngine : RazorViewEngine, IBundleViewEngine
    {
        public BundleRazorViewEngine(IBundle bundle)
        {
            Bundle = bundle;
            var bundleRelativePath = Utility.MapPathReverse(bundle.Location);

            AreaViewLocationFormats = Utility.RedirectToBundlePath(AreaViewLocationFormats, bundleRelativePath).ToArray();
            AreaMasterLocationFormats = Utility.RedirectToBundlePath(AreaMasterLocationFormats, bundleRelativePath).ToArray();
            AreaPartialViewLocationFormats = Utility.RedirectToBundlePath(AreaPartialViewLocationFormats, bundleRelativePath).ToArray();
            ViewLocationFormats = Utility.RedirectToBundlePath(ViewLocationFormats, bundleRelativePath).ToArray();
            MasterLocationFormats = Utility.RedirectToBundlePath(MasterLocationFormats, bundleRelativePath).ToArray();
            PartialViewLocationFormats = Utility.RedirectToBundlePath(PartialViewLocationFormats, bundleRelativePath).ToArray();

        }

        private void AddPluginToRequestContext()
        {
            HttpContext.Current.Request.Headers.Add("isPlugin", "1");
        }

        private void SetViewLocations(string pluginName)
        {
            ViewLocationFormats = new[]
            {
                "~/Plugins/Installed/" + pluginName + "/Views/{1}/{0}.cshtml",
                "~/Plugins/Installed/" + pluginName + "/Shared/{0}.cshtml"
            };


            MasterLocationFormats = new[]
            {
                "~/Plugins/Installed/" + pluginName + "/{1}/{0}.cshtml",
                "~/Plugins/Installed/" + pluginName + "/Shared/{0}.cshtml"
            };


            PartialViewLocationFormats = new[]
            {
                "~/Plugins/Installed/" + pluginName + "/Views/{1}/{0}.cshtml",
                "~/Plugins/Installed/" + pluginName + "/Shared/{0}.cshtml"
            };

            FileExtensions = new[] {"cshtml"};
        }

        public IBundle Bundle { get; private set; }

        public string SymbolicName
        {
            get { return Bundle.SymbolicName; }
        }

        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName,
                                                  string masterName, bool useCache)
        {
            object symbolicName = InstalledPlugins.GetPluginName(controllerContext);

            if (symbolicName != null && Bundle.SymbolicName.Equals(symbolicName))
            {
                SetViewLocations(symbolicName.ToString());
                AddPluginToRequestContext();
                return base.FindView(controllerContext, viewName, masterName, useCache);
            }
            return new ViewEngineResult(new string[0]);
        }

        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName,
                                                         bool useCache)
        {
            object symbolicName = InstalledPlugins.GetPluginName(controllerContext);
            if (symbolicName != null && Bundle.SymbolicName.Equals(symbolicName))
            {
                SetViewLocations(symbolicName.ToString());
                AddPluginToRequestContext();
                return base.FindPartialView(controllerContext, partialViewName, useCache);
            }
            return new ViewEngineResult(new string[0]);
        }
    }
}