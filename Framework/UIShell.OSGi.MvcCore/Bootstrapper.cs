﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Common.Repositories;
using UIShell.OSGi.MvcCore;
using UIShell.OSGi.MvcCore.Custom;
using UIShell.OSGi.Utility;
using System.Threading;
using System.Reflection;
using UIShell.OSGi.Configuration.BundleManifest;
using System.Web.Compilation;
using System.IO;
using UIShell.OSGi.Core.Service;
using UIShell.OSGi.Loader;
using System.Web.Mvc;
using System.Web.Routing;


namespace UIShell.OSGi.MvcCore
{
    public class Bootstrapper
    {

        private static Dictionary<BundleData, IList<Assembly>> _registeredBunldeCache = new Dictionary<BundleData, IList<Assembly>>();

        public BundleRuntime BundleRuntime { get; private set; }

        protected virtual BundleRuntime CreateBundleRuntime()
        {
            return new BundleRuntime();
        }

        public void StartBundleRuntime()
        {
            FileLogUtility.Debug("WebSite is starting.");
            AddPreDefinedRefAssemblies();
            // Set SQLCE compact before BundleRuntime starting.
            AppDomain.CurrentDomain.SetData("SQLServerCompactEditionUnderWebHosting", true);

            BundleRuntime = CreateBundleRuntime();
            FileLogUtility.Debug("Framework is starting.");

            BundleRuntime.Start();

            //force to start all plugins
            LoadBundleResources();

            FileLogUtility.Debug("Framework is started.");

            ControllerBuilder.Current.SetControllerFactory(new BundleRuntimeControllerFactory());

            RegisterGlobalFilters(GlobalFilters.Filters);
        }

        private void LoadBundleResources()
        {
            foreach (var bundle in BundleRuntime.Framework.Bundles)
            {
                var bundleData = 
                    BundleRuntime.Instance.GetFirstOrDefaultService<IBundleInstallerService>()
                                 .GetBundleDataByName(bundle.SymbolicName);

                if (bundleData == null)
                {
                    continue;
                }

                // Check if this is a old plugin
                if (!IsOldPlugin(bundleData.Path))
                {
                    // Check if the dll exists - otherwise throw an exception
                    ValidateDll(bundleData.Name, bundleData.Path, bundleData.Runtime.Assemblies[0].Path);

                    //register bundle assemblies to BuildManager.
                    var assemblies = this.AddReferencedAssemblies(bundleData.SymbolicName);
                    if (assemblies != null && assemblies.Count > 0)
                    {
                        _registeredBunldeCache[bundleData] = assemblies;
                    }
                }
            }
        }

        private void ValidateDll(string name, string path, string dllLocation)
        {
            if (!File.Exists(path + "/" + dllLocation))
            {
                throw new Exception(string.Format("DLL niet gevonden voor {0}. Gezocht naar het pad: {1}{2}", name, path, dllLocation));
            }
        }

        public virtual void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        protected virtual void AddPreDefinedRefAssemblies()
        {
            AddReferencedAssembly(typeof(BundleRuntime).Assembly);

            AddReferencedAssembly(GetType().Assembly);
        }

        protected virtual void Application_End(object sender, EventArgs e)
        {
            BundleRuntime.Stop();
        }

        private bool IsOldPlugin(string fullPath)
        {
            string pluginDirectoryName = GetPluginLocationName(fullPath);

            bool returnValue = DetermineOldPlugin(pluginDirectoryName);

            if (DetermineOldPlugin(pluginDirectoryName))
            {
                try
                {
                    Directory.Delete(fullPath, true);
                }
                catch(Exception ex)
                {
                   throw new Exception("Could not delete the old plugin directory", ex);
                }
            }
            return returnValue;
        }

        private string GetPluginLocationName(string fullPath)
        {
            // Get directory name
            string directoryName = new DirectoryInfo(fullPath).Name;

            // Return directory name
            return directoryName;
        }

        private bool DetermineOldPlugin(string directoryName)
        {
            var pluginRepository = new PluginRepository();

            var pluginInfo = pluginRepository.GetByPluginLocation(directoryName);

            if (pluginInfo == null)
            {
                return true;
            }

            return false;
        }

        #region IBundleRuntimeMvcHost Members

        public virtual IList<Assembly> AddReferencedAssemblies(string bundleSymbolicName)
        {
            //Check if this bundle still exist or not.
            var bundleData = BundleRuntime.Instance.GetFirstOrDefaultService<IBundleInstallerService>().GetBundleDataByName(bundleSymbolicName);
            if (bundleData == null)
            {
                return new List<Assembly>();
            }

            //already registered its assembiles
            IList<Assembly> registeredItems;
            if (_registeredBunldeCache.TryGetValue(bundleData, out registeredItems))
            {
                return registeredItems;
            }

            var serviceContainer = BundleRuntime.Framework.ServiceContainer;
            var service = serviceContainer.GetFirstOrDefaultService<IRuntimeService>();
            var assemlbies = service.LoadBundleAssembly(bundleSymbolicName);
            assemlbies.ForEach(AddReferencedAssembly);
            //cache the assemblies
            _registeredBunldeCache[bundleData] = assemlbies;

            return assemlbies;
        }

        public void AddReferencedAssembly(Assembly assembly)
        {
            //todo:use reflection to add assembly to Build Manager if the app_start is finished.
            BuildManager.AddReferencedAssembly(assembly);

         //   _appDomain.Load(assembly.GetName());
        }


        #endregion
    }
}
