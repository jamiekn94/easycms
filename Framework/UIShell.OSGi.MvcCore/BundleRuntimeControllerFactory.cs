﻿using System;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Routing;
using UIShell.OSGi.Loader;
using System.Web.SessionState;
using UIShell.OSGi.Utility;

namespace UIShell.OSGi.MvcCore
{
    public class BundleRuntimeControllerFactory : DefaultControllerFactory
    {
        public override IController CreateController(System.Web.Routing.RequestContext requestContext, string controllerName)
        {
            //http://localhost:1616/Blog/Index?plugin=BlogPlugin

            IController controller = null;

            
            try
            {
                // Try to create a controller
                controller = base.CreateController(requestContext, controllerName);

                var resolver = BundleRuntime.Instance.GetFirstOrDefaultService<IInjectionService>();

                if (resolver != null)
                {
                    // Inject application controller
                    return resolver.InjectProperties(controller);//.Resolve(type) as IController;
                }
            }
            catch (Exception)
            {
                // Controller doesn't exist - 404 error   
                requestContext.HttpContext.Response.Redirect("/Error/400", true);
            }

            return controller;
        }

        protected override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
            var symbolicName = InstalledPlugins.GetPluginName(requestContext);
            if (symbolicName != null)
            {
                var controllerType = ControllerTypeCache.GetControllerType(symbolicName, controllerName);
                if (controllerType != null)
                {
                    return controllerType;
                }

                var controllerTypeName = controllerName + "Controller";
                var runtimeService = BundleRuntime.Instance.GetFirstOrDefaultService<IRuntimeService>();
                var assemblies = runtimeService.LoadBundleAssembly((string)symbolicName);

                foreach (var assembly in assemblies)
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        if (type.Name.Contains(controllerTypeName) && typeof(IController).IsAssignableFrom(type))
                        {
                            controllerType = type;
                            ControllerTypeCache.AddControllerType(symbolicName.ToString(), controllerName, controllerType);
                            return controllerType;
                        }
                    }
                }
            }

            try
            {
                //if exists duplicated controller type, below will throw an exception.
               return  base.GetControllerType(requestContext, controllerName);
            }
            catch (Exception)
            {
                //intentionally suppress the duplication error.
            }

            requestContext.RouteData.DataTokens["Namespaces"] = DefaultControllerConfig.DefaultNamespaces;
            var result = base.GetControllerType(requestContext, controllerName);
            if (result != null)
            {
                return result;
            }
            return null;
        }
    }
}