﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Security.AccessControl;
//using System.Security.Principal;
//using System.Web;
//using System.Xml.Linq;
//using Common;
//using ICSharpCode.SharpZipLib.Zip;
//using Widgets.Models;
//using File = System.IO.File;

//namespace Widgets
//{
//    public class Install
//    {
//        protected string ManifestPath { get; set; }
//        protected string ManifestContent { get; set; }
//        protected XDocument ManifestDocument { get; set; }

//        protected XNamespace GetNamespace
//        {
//            get { return XNamespace.Get("urn:uiosp-bundle-manifest-2.0"); }
//        }

//        protected virtual string InstallationsLocation
//        {
//            get { return HttpContext.Current.Server.MapPath(@"Widgets\Installations").Replace(@"\Admin\Widgets", ""); }
//        }

//        protected virtual string InstalledLocation
//        {
//            get { return HttpContext.Current.Server.MapPath(@"Widgets\Installed").Replace(@"\Admin\Widgets", ""); }
//        }

//        public void ReStartWebsite()
//        {
//            HttpRuntime.UnloadAppDomain();
//        }

//        protected void CreateInstallationDirectory()
//        {
//            try
//            {
//                if (!Directory.Exists(InstalledLocation))
//                {
//                    Directory.CreateDirectory(InstalledLocation);
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Couldn't create the installation directory for the widget", ex);
//            }
//        }

//        protected void SetPermissions()
//        {
//            DirectorySecurity sec = Directory.GetAccessControl(InstalledLocation);
//            // Using this instead of the "Everyone" string means we work on non-English systems.
//            var everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
//            sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.FullControl | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
//            Directory.SetAccessControl(InstalledLocation, sec);
//        }

//        protected void RunSqlScript(string locationScript)
//        {
//            string scriptContent;

//            try
//            {
//                var scriptFileInfo = new FileInfo(locationScript);

//                using (var scriptReader = scriptFileInfo.OpenText())
//                {
//                    scriptContent = scriptReader.ReadToEnd();
//                }
//            }
//            catch (FileNotFoundException ex)
//            {
//                throw new Exception("Could not find sql install script: " + locationScript, ex);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Unknown sql install script error: " + locationScript, ex);
//            }
//            try
//            {
//                var datasource = new DataClassesDataContext();

//                datasource.ExecuteCommand(scriptContent);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Invalid sql installation script: " + locationScript, ex);
//            }

//        }

//        protected bool CanRunScripts(string versionScript, string versionWidget, string oldVersionWidget)
//        {
//            try
//            {
//                var arrayVersionScript = versionScript.Split('.').Select(int.Parse).ToList();
//                var arrayVersionWidget = versionWidget.Split('.').Select(int.Parse).ToList();
//                var arrayOldVersionWidget = oldVersionWidget.Split('.').Select(int.Parse).ToList();

//                for (int counter = 0; arrayVersionWidget.Count > counter; counter++)
//                {
//                    // Only run the script if the new version is higher than the old version
//                    if ((arrayVersionWidget[counter] > arrayOldVersionWidget[counter]) &&
//                        (arrayVersionWidget[counter] > arrayVersionScript[counter]))
//                    {
//                        return true;
//                    }

//                    // Equal version, we may run this script too
//                    if ((arrayVersionWidget[counter] == arrayOldVersionWidget[counter] &&
//                         arrayVersionWidget.Count - counter == 1)
//                        &&
//                        (arrayVersionWidget[counter] == arrayVersionScript[counter] &&
//                         arrayVersionWidget.Count - counter == 1))
//                    {
//                        return true;
//                    }
//                }
//            }
//            catch (IndexOutOfRangeException ex)
//            {
//                throw new Exception("Incorrect version number during determining if we may run this script", ex);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("An unknown exception occured during determining if we may run this script", ex);
//            }
//            return false;
//        }

//        protected void LaunchAllSqlScripts(IEnumerable<ScriptModel> sqlScripts, string pathPluginDirectory, string currentVersionWidget, string oldVersionWidget)
//        {
//            if (sqlScripts != null)
//            {
//                foreach (var script in sqlScripts)
//                {
//                    if (CanRunScripts(script.Version, currentVersionWidget, oldVersionWidget))
//                    {
//                        string scriptPath = InstalledLocation + "/" + pathPluginDirectory + "/" + script.Path;
//                        RunSqlScript(scriptPath);
//                    }
//                }
//            }
//        }

//        protected void LoadManifestDocument()
//        {
//            try
//            {
//                ManifestDocument = XDocument.Load(ManifestPath);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Couldn't load the manifest document", ex);
//            }
//        }

//        protected void DeleteOldDirectory(string pathOldDirectory)
//        {
//            if (!Directory.Exists(pathOldDirectory)) { throw new Exception("Couldn't find the old folder to delete"); }

//            Directory.Delete(pathOldDirectory, true);
//        }

//        /// <summary>
//        /// Add or update the widget - Return the record of this widget
//        /// </summary>
//        /// <returns>Old widget version</returns>
//        protected string UpdateWidgetAndReturnOldVersion(WidgetInfoModel manifestWidgetInfo)
//        {
//            try
//            {
//                string oldWidgetVersion = "00.00.00";

//                var widgetRepository = new Common.Repositories.WidgetRepository();

//                // Get previous widget
//                var previousWidgetInfo = widgetRepository.GetByName(manifestWidgetInfo.WidgetCodeName);

//                // Check if there is a previous widget
//                if (previousWidgetInfo != null)
//                {
//                    // Check if the path of the plugin has been changed
//                    if (!previousWidgetInfo.PathWidget.Equals(manifestWidgetInfo.PathWidget,
//                        StringComparison.CurrentCultureIgnoreCase))
//                    {
//                        string oldWidgetPath = InstalledLocation + "/" + previousWidgetInfo.PathWidget;

//                        DeleteOldDirectory(oldWidgetPath);
//                    }

//                    // Set old widget version
//                    oldWidgetVersion = previousWidgetInfo.Version;

//                    // Update current widget
//                    previousWidgetInfo.ActionLocation = manifestWidgetInfo.ActionLocation;
//                    previousWidgetInfo.AssemblyLocation = manifestWidgetInfo.AssemblyLocation;
//                    previousWidgetInfo.ControllerLocation = manifestWidgetInfo.ControllerLocation;
//                    previousWidgetInfo.Description = manifestWidgetInfo.WidgetDescription;
//                    previousWidgetInfo.Version = manifestWidgetInfo.Version;
//                    previousWidgetInfo.WidgetFriendlyName = manifestWidgetInfo.WidgetFriendlyName;
//                    previousWidgetInfo.PathWidget = manifestWidgetInfo.PathWidget;
//                }
//                else
//                {
//                    // Add the new widget
//                    var widgetItem = new Common.Widget
//                    {
//                        WidgetCodeName = manifestWidgetInfo.WidgetCodeName,
//                        WidgetFriendlyName = manifestWidgetInfo.WidgetFriendlyName,
//                        AssemblyLocation = manifestWidgetInfo.AssemblyLocation,
//                        ControllerLocation = manifestWidgetInfo.ControllerLocation,
//                        ActionLocation = manifestWidgetInfo.ActionLocation,
//                        Version = manifestWidgetInfo.Version,
//                        Description = manifestWidgetInfo.WidgetDescription,
//                        PathWidget = manifestWidgetInfo.PathWidget
//                    };

//                    widgetRepository.Add(widgetItem);
//                }

//                widgetRepository.Save();

//                return oldWidgetVersion;
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Couldn't load or add the properties from the widget into the database", ex);
//            }
//        }

//        protected IEnumerable<ScriptModel> GetSqlScripts()
//        {
//            return
//             (from db in ManifestDocument.Descendants(GetNamespace + "Script") select new ScriptModel { Path = db.Attribute("Path").Value, Version = db.Attribute("Version").Value }).ToList();
//        }

//        private WidgetInfoModel GetManifest(string pathWidget)
//        {
//            try
//            {
//                XNamespace ns = XNamespace.Get("urn:uiosp-bundle-manifest-2.0");

//                var widgetInfoModel = new WidgetInfoModel
//                {
//                    ActionLocation = ManifestDocument.Descendants(ns + "Action").Attributes("Path").First().Value,
//                    ControllerLocation = ManifestDocument.Descendants(ns + "Controller").Attributes("Path").First().Value,
//                    AssemblyLocation = ManifestDocument.Descendants(ns + "Assembly").Attributes("Path").First().Value,
//                    Version = ManifestDocument.Root.Attribute("Version").Value,
//                    WidgetCodeName = ManifestDocument.Root.Attribute("SymbolicName").Value,
//                    WidgetDescription = ManifestDocument.Root.Attribute("Description").Value,
//                    WidgetFriendlyName = ManifestDocument.Root.Attribute("Name").Value,
//                    PathWidget = pathWidget,
//                    SqlScripts = GetSqlScripts()
//                };

//                ManifestDocument = null;

//                return widgetInfoModel;
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Couldn't load the properties from the widget manifest.", ex);
//            }
//        }

//        protected string UnzipAndReturnDirectoryLocation(string locationZipFile, string widgetFullPath)
//        {
//            var fastZip = new FastZip();

//            try
//            {
//                fastZip.ExtractZip(locationZipFile, widgetFullPath, "");
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Couldn't unzip the files from the widget", ex);
//            }
//            return widgetFullPath;
//        }

//        protected string GetTimestamp()
//        {
//            return DateTime.Now.ToString("HHmmssddMMyyyy");
//        }

//        protected WidgetPathModel CreateAndReturnDirectoryLocation(string zipName)
//        {
//            try
//            {
//                // Get file extension
//                string fileExtension = Path.GetExtension(zipName);

//                // Get plugin name for the directory
//                string directoryWidgetName = zipName.Replace(fileExtension, "") + "-" + GetTimestamp();

//                // Set full path to plugin
//                string fullPathWidget = InstalledLocation + "/" + directoryWidgetName;

//                // Check if the directory doesn't exist yet
//                if (Directory.Exists(fullPathWidget))
//                {
//                    // Todo: - Mark the plugin to not load this assembly
//                    // Todo: - Schedule to delete this path when the app starts again

//                    //  Directory.Delete(fullPathWidget, true);
//                }

//                // Create directory
//                Directory.CreateDirectory(fullPathWidget);

//                // Return widgetpath model
//                return new WidgetPathModel
//                {
//                    FullPathPlugin = fullPathWidget,
//                    WidgetLocation = directoryWidgetName
//                };
//            }
//            catch (DirectoryNotFoundException ex)
//            {
//                throw new Exception("Couldn't find the directory for creating a widget", ex);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("An error occured when creating a path for the widget", ex);
//            }
//        }

//        protected void SetManifest(string pluginDirectoryPath)
//        {
//            ManifestPath = pluginDirectoryPath + "/Manifest.xml";

//            if (!File.Exists(ManifestPath)) { throw new FileNotFoundException("Manifest niet gevonden - " + ManifestPath); }

//            ManifestContent = File.OpenText(ManifestPath).ReadToEnd();
//        }

//        protected TypeInstallation GetTypeInstallation(string oldVersion, string newVersion)
//        {
//            // Equal version - Reinstallation
//            if (oldVersion.Equals(newVersion, StringComparison.CurrentCultureIgnoreCase))
//            {
//                return TypeInstallation.ReInstallation;
//            }
//            // If it's not an equal version then is it a update?
//            else if (CanRunScripts(newVersion, newVersion, oldVersion))
//            {
//                return TypeInstallation.Update;
//            }
//            // None of the above so it's a brand new installation
//            else
//            {
//                return TypeInstallation.Installation;
//            }
//        }

//        protected void SaveFile(HttpPostedFileBase zipfile)
//        {
//            zipfile.SaveAs(InstallationsLocation + "/" + zipfile.FileName);
//        }

//        public virtual string Launch(HttpPostedFileBase zipFile)
//        {
//            // Create installation directory for the widget
//            CreateInstallationDirectory();

//            // Save installation package
//            SaveFile(zipFile);

//            // Create directory for the widget and get the paths to the widget
//            var widgetPathsInfo = CreateAndReturnDirectoryLocation(zipFile.FileName);

//            // Unzip and get the full path to the widget
//            string pluginDirectoryPath = UnzipAndReturnDirectoryLocation(InstallationsLocation + "/" + zipFile.FileName, widgetPathsInfo.FullPathPlugin);

//            // Set manifest
//            SetManifest(pluginDirectoryPath);

//            // Load xml database
//            LoadManifestDocument();

//            // Get manifest info
//            var manifestInfo = GetManifest(widgetPathsInfo.WidgetLocation);

//            // Add or update widget database record
//            string oldWidgetVersion = UpdateWidgetAndReturnOldVersion(manifestInfo);

//            // Run all sql scripts - for each version
//            LaunchAllSqlScripts(manifestInfo.SqlScripts, widgetPathsInfo.WidgetLocation, manifestInfo.Version, oldWidgetVersion);

//            // Return widget name
//            return manifestInfo.WidgetFriendlyName;
//        }
//    }
//}
