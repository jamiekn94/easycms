﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using Common.Providers.Routes;
using Common.Repositories;

namespace Widgets
{
    public static class Widget
    {
        private enum KindWidget
        {
            Html = 1,
            Blog = 2,
            Nieuws = 3,
            FotoAlbum = 4,
            TwitterFeed = 5
        }

        public static void RenderWidgetSection(this HtmlHelper htmlHelper, string sectionName)
        {
            // Initialize repositores  --
            var configuratieRepository = new ConfiguratieRepository();
            var themeSectionRepository = new ThemeSectionRepository();
            var activeWidgetsRepository = new ActiveWidgetRepository();
            var seoPageRepository = new SeoPageRepository();

            // Get active theme id
            int activeThemeId = configuratieRepository.GetById(1).ThemeId;

            // Get theme section id
            int themeSectionId = themeSectionRepository.GetSectionId(activeThemeId, sectionName);

            // Get controller
            var controller = RouteProvider.GetController();

            // Get action
            var action = RouteProvider.GetAction();

            // Get page
            var seoPage = seoPageRepository.GetSeo(controller, action, false);

            // If this page exists
            if (seoPage != null)
            {
                // Get active widgets for this page
                var listActiveWidgets = activeWidgetsRepository.GetBySection(themeSectionId, seoPage.Id);

                // For each widget render this action
                foreach (var activeWidget in listActiveWidgets)
                {
                    // Render
                    RenderWidget(activeWidget.KindWidget, activeWidget.ActiveWidgetId, htmlHelper);
                }
            }
        }

        private static void RenderWidget(int kindWidget, int activeWidgetId, HtmlHelper htmlHelper)
        {
            var enumKind = (KindWidget)kindWidget;

            switch (enumKind)
            {
                case KindWidget.Html:
                    {
                        RenderHtml(activeWidgetId, htmlHelper);
                        break;
                    }
                case KindWidget.Blog:
                    {
                        RenderBlogArticles(activeWidgetId, htmlHelper);
                        break;
                    }
                case KindWidget.FotoAlbum:
                    {
                        RenderPhotoAlbums(activeWidgetId, htmlHelper);
                        break;
                    }
                case KindWidget.Nieuws:
                    {
                        RenderNewsArticles(activeWidgetId, htmlHelper);
                        break;
                    }
                case KindWidget.TwitterFeed:
                    {
                        RenderTwitterFeed(activeWidgetId, htmlHelper);
                        break;
                    }
            }
        }

        private static void RenderHtml(int activeWidgetId, HtmlHelper htmlHelper)
        {
            htmlHelper.RenderAction("Html", "Widgets", new { activeWidgetId });
        }

        private static void RenderPhotoAlbums(int activeWidgetId, HtmlHelper htmlHelper)
        {
            htmlHelper.RenderAction("FotoAlbum", "Widgets", new { activeWidgetId });
        }

        private static void RenderNewsArticles(int activeWidgetId, HtmlHelper htmlHelper)
        {
            htmlHelper.RenderAction("Nieuws", "Widgets", new { activeWidgetId });
        }

        private static void RenderBlogArticles(int activeWidgetId, HtmlHelper htmlHelper)
        {
            htmlHelper.RenderAction("Blog", "Widgets", new { activeWidgetId });
        }

        private static void RenderTwitterFeed(int activeWidgetId, HtmlHelper htmlHelper)
        {
            htmlHelper.RenderAction("TwitterFeed", "Widgets", new { activeWidgetId });
        }
    }
}
