﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Widgets.Models
{
    public class WidgetPathModel
    {
        /// <summary>
        /// Holds the directory name of the added widget
        /// </summary>
        public string WidgetLocation { get; set; }

        /// <summary>
        /// Full path to the plugin
        /// </summary>
        public string FullPathPlugin { get; set; }
    }
}
