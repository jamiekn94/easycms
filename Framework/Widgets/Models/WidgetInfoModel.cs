﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Widgets.Models
{
    public class WidgetInfoModel
    {
        /// <summary>
        /// Is used by checking for previous installed widgets and for looking up the the correct viewengine
        /// </summary>
        public string WidgetCodeName { get; set; }

        /// <summary>
        /// Display name to the user
        /// </summary>
        public string WidgetFriendlyName { get; set; }

        public string WidgetDescription { get; set; }

        public string Version { get; set; }

        /// <summary>
        /// Location of the bin file
        /// </summary>
        public string AssemblyLocation { get; set; }

        /// <summary>
        /// Controller that the widget needs to call
        /// </summary>
        public string ControllerLocation { get; set; }

        /// <summary>
        /// Action that the widget needs to call
        /// </summary>
        public string ActionLocation { get; set; }

        /// <summary>
        /// Holds the folder name of where the widget is stored
        /// </summary>
        public string PathWidget { get; set; }

        /// <summary>
        /// Holds the available sql scripts
        /// </summary>
        public IEnumerable<ScriptModel> SqlScripts { get; set; }
    }
}
