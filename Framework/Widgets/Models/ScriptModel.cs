﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Widgets.Models
{
   public class ScriptModel
    {
        public string Path { get; set; }
        public string Version { get; set; }
    }
}
