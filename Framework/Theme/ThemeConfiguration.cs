﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Common.Providers.Caching;
using Common.Repositories;
using Microsoft.Ajax.Utilities;
using Theme.Models;

namespace Theme
{
   public class ThemeConfiguration
    {
       public const string CookieName = "Theme";
       public const string HttpContextName = "Theme";

       public static bool HasThemeCookie()
       {
           return !string.IsNullOrEmpty(GetThemeCookie());
       }

       public static string GetThemeCookie()
       {
           if (HttpContext.Current.Handler != null)
           {
               return Common.Providers.Cookie.Cookie.GetCookie(CookieName);
           }
           return string.Empty;
       }

       public static bool ValidateThemeCookie()
       {
           using (var themeRepository = new ThemeRepository())
           {
               var themeInfo = themeRepository.GetByName(GetThemeCookie());

               return themeInfo != null;
           }
       }

       public static string GetHttpContextTheme()
       {
           return (string)ClientCachingProvider.GetClientCache(HttpContextName);
       }

       public static void SetHttpContextTheme(string themeName)
       {
           ClientCachingProvider.AddClientCache(HttpContextName, themeName);

       }

       public static void RemoveHttpContextTheme()
       {
           ClientCachingProvider.RemoveClientCache(HttpContextName);
       }

       public static bool DoesHttpContextThemeExists()
       {
           return ClientCachingProvider.ClientCacheExists(HttpContextName);
       }

       public static bool IsNewTheme()
       {
           if (!DoesHttpContextThemeExists() || !GetThemeCookie().Equals(GetHttpContextTheme()))
           {
               return true;
           }
           else if (DoesHttpContextThemeExists() && !GetHttpContextTheme().Equals(GetThemeCookie()))
           {
               return false;
           }

           return false;
       }
    }
}
