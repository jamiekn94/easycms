﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Common.Providers.Routes;

namespace Theme
{
    public class ThemeSetter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!RouteProvider.GetArea().Equals("admin", StringComparison.CurrentCultureIgnoreCase) && !filterContext.IsChildAction && !filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                if (ThemeConfiguration.HasThemeCookie())
                {
                    bool isValidTheme = ThemeConfiguration.ValidateThemeCookie();

                    if (isValidTheme && ThemeConfiguration.IsNewTheme())
                    {
                        ThemeConfiguration.SetHttpContextTheme(ThemeConfiguration.GetThemeCookie());
                        ViewEngines.Engines.Clear();
                        ViewEngines.Engines.Add(new ThemeViewEngine(ThemeConfiguration.GetThemeCookie()));
                    }

                    // The theme in this cookie doesn't exist, add the default engine
                    else if (!isValidTheme)
                    {
                        ViewEngines.Engines.Clear();
                        ViewEngines.Engines.Add(new ThemeViewEngine());
                    }
                }
            }
        }
    }
}
