﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Common.Repositories;
using Microsoft.Ajax.Utilities;

namespace Theme
{
    public class ThemeViewEngine : RazorViewEngine
    {
        public ThemeViewEngine()
        {
            var themeLocation = GetDatabaseThemeLocation();

            ViewLocationFormats = GetViewLocationFormats(themeLocation);

            MasterLocationFormats = GetMasterLocationFormats(themeLocation);

            PartialViewLocationFormats = GetPartialViewLocationFormats(themeLocation);

            AreaViewLocationFormats = GetAreaViewLocationFormats();

            AreaPartialViewLocationFormats = GetAreaPartialViewLocationFormats();

            FileExtensions = new[] { "cshtml" };
        }

        public ThemeViewEngine(string themeName)
        {
            ViewLocationFormats = GetViewLocationFormats(themeName);

            MasterLocationFormats = GetMasterLocationFormats(themeName);

            PartialViewLocationFormats = GetPartialViewLocationFormats(themeName);

            AreaViewLocationFormats = GetAreaViewLocationFormats();

            AreaPartialViewLocationFormats = GetAreaPartialViewLocationFormats();

            FileExtensions = new[] { "cshtml" };
        }

        private string GetDatabaseThemeLocation()
        {
            var configuratieRepository = new ConfiguratieRepository();
            string themeLocation = configuratieRepository.GetCachedThemePath();
            return themeLocation;
        }

        private string[] GetMasterLocationFormats(string themeLocation)
        {
            var arrayLocationFormats = new List<string>
            {   "~/Themes/" + themeLocation + "/Views/{1}/{0}.cshtml",
                "~/Themes/" + themeLocation + "/Views/Shared/{0}.cshtml"
            };

            return arrayLocationFormats.ToArray();
        }

        private string[] GetViewLocationFormats(string themeLocation)
        {
            var arrayLocationFormats = new List<string>
            {
                "~/Themes/" + themeLocation + "/Views/{1}/{0}.cshtml",
                "~/Themes/" + themeLocation + "/Views/Shared/{0}.cshtml",
                "~/Themes/" + themeLocation + "/Views/{1}/Shared/{0}.cshtml"
            };

            return arrayLocationFormats.ToArray();
        }

        private string[] GetPartialViewLocationFormats(string themeLocation)
        {
            var arrayLocationFormats = new List<string>
            {
                 "~/Themes/" + themeLocation + "/Views/{1}/{0}.cshtml",
                "~/Themes/" + themeLocation + "/Views/Shared/{0}.cshtml",
                "~/Themes/" + themeLocation + "/Views/{1}/Shared/{0}.cshtml"
            };


            return arrayLocationFormats.ToArray();
        }

        private string[] GetAreaViewLocationFormats()
        {
            var arrayLocationFormats = new List<string>
            {
                   "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml",
                "~/Areas/{2}/Views/{1}/Shared/{0}.cshtml"
            };

            return arrayLocationFormats.ToArray();
        }

        private string[] GetAreaPartialViewLocationFormats()
        {
            var arrayLocationFormats = new List<string>
            {  "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml",
                "~/Areas/{2}/Views/{1}/Shared/{0}.cshtml"
            };

            return arrayLocationFormats.ToArray();
        }

    }
}
