﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using Common;
using Common.Repositories;
using Theme.Models;

namespace Theme
{
    //public class Install
    //{
    //    /// <summary>
    //    /// Get the path to the installations folder
    //    /// </summary>
    //    private string ThemeStoreInstallationFilesPath
    //    {
    //        get { return HttpContext.Current.Server.MapPath("~/Themes/Installations"); }
    //    }

    //    /// <summary>
    //    /// Get the path to the themes folder
    //    /// </summary>
    //    private string ThemeInstallationPath
    //    {
    //        get { return HttpContext.Current.Server.MapPath("~/Themes"); }
    //    }

    //    /// <summary>
    //    /// Save the .zip file into the installation directory
    //    /// </summary>
    //    /// <param name="uploadedFile"></param>
    //    /// <returns></returns>
    //    private string SaveAndReturnFilePath(HttpPostedFileBase uploadedFile)
    //    {
    //        string filePath = ThemeStoreInstallationFilesPath + "/" + uploadedFile.FileName;

    //        if (!Directory.Exists(ThemeStoreInstallationFilesPath)) { throw new DirectoryNotFoundException("The installation directory for storing theme files doesn't exist."); }

    //        uploadedFile.SaveAs(filePath);

    //        return filePath;
    //    }

    //    /// <summary>
    //    /// Store the files from the zipped file into the installation directory
    //    /// </summary>
    //    /// <param name="fileUploadPath"></param>
    //    /// <returns></returns>
    //    private InstallPathModel UnzipAndReturnInstallPath(string fileUploadPath)
    //    {
    //        string fileName = Path.GetFileName(fileUploadPath);

    //        string fileExtension = Path.GetExtension(fileName);

    //        string directoryName = fileName.Replace(fileExtension, "");

    //        string filePath = ThemeInstallationPath + "\\" + directoryName;

    //        if (Directory.Exists(filePath))
    //        {
    //            Directory.Delete(filePath, true);
    //        }

    //        Directory.CreateDirectory(filePath);

    //        var fastZip = new FastZip();

    //        try
    //        {
    //            fastZip.ExtractZip(fileUploadPath, filePath, "");
    //        }
    //        catch(Exception ex)
    //        {
    //            throw new Exception("Couldn't unzip the file in the theme installation.", ex);
    //        }

    //        return new InstallPathModel
    //        {
    //            InstalledPath = filePath,
    //            LocationName = directoryName
    //        };
    //    }

    //    /// <summary>
    //    /// Get the properties from the manifest file
    //    /// </summary>
    //    /// <param name="installedPath"></param>
    //    /// <returns></returns>
    //    private ThemeModel GetXml(InstallPathModel installedPath)
    //    {
    //        string pathManifest = installedPath.InstalledPath + "/" + "Manifest.xml";

    //        if (!File.Exists(pathManifest)) { throw new FileNotFoundException("Couldn't find the theme manifest file."); }

    //        XDocument xmlDocument = XDocument.Load(pathManifest);

    //        try
    //        {
    //            string themeName = xmlDocument.Descendants().Elements("Name").First().Value;
    //            string themeDescription = xmlDocument.Descendants().Elements("Description").First().Value;
    //            string themeImage = xmlDocument.Descendants().Elements("Image").First().Value;
    //            string author = xmlDocument.Descendants().Elements("Author").First().Value;
    //            string authorWebsite = xmlDocument.Descendants().Elements("AuthorWebsite").First().Value;
    //            string themeWebsite = xmlDocument.Descendants().Elements("ThemeWebsite").First().Value;
    //            string version = xmlDocument.Descendants().Elements("Version").First().Value;

    //            return new ThemeModel()
    //            {
    //                ThemeDescription = themeDescription,
    //                ThemeImage = themeImage,
    //                ThemeName = themeName,
    //                Author = author,
    //                AuthorWebsite = authorWebsite,
    //                ThemeWebsite = themeWebsite,
    //                Version = version,
    //                ThemeSections = GetThemeSections(xmlDocument),
    //                Location = installedPath.LocationName
    //            };

    //        }
    //        catch
    //        {
    //            throw new Exception("Couldn't load a property from the manifest file.");
    //        }
    //        finally
    //        {
    //            // Let the gc collect it
    //            xmlDocument = null;
    //        }
    //    }

    //    /// <summary>
    //    /// Get the theme sections from the manifest file
    //    /// </summary>
    //    /// <param name="manifest"></param>
    //    /// <returns></returns>
    //    private IEnumerable<ThemeSectionModel> GetThemeSections(XDocument manifest)
    //    {
    //        var returnListThemeSections = new Collection<ThemeSectionModel>();

    //        try
    //        {
    //            foreach (var themeSection in manifest.Descendants("ThemeSection"))
    //            {
    //                returnListThemeSections.Add(new ThemeSectionModel
    //                {
    //                    SectionFriendlyName = themeSection.Element("FriendlyName").Value,
    //                    SectionCodeName = themeSection.Element("CodeName").Value,
    //                    SectionDescription = themeSection.Element("Description").Value,
    //                    Controller = themeSection.Element("Controller").Value,
    //                    Action = themeSection.Element("Action").Value
    //                });
    //            }
    //        }
    //        catch(Exception ex)
    //        {
    //            throw new Exception("An error occured during loading the theme sections.", ex);
    //        }
    //        return returnListThemeSections;
    //    }

    //    private void DeletePreviousLocation(string location)
    //    {
    //        string pathToThemeFolder = ThemeInstallationPath + "/" + location;
            
    //        if(!Directory.Exists(pathToThemeFolder)){throw new DirectoryNotFoundException(string.Format("Couldn't delete the previous theme folder from: {0}", pathToThemeFolder));}

    //        Directory.Delete(pathToThemeFolder, true);
    //    }

    //    /// <summary>
    //    /// Update or add the new theme into the database.
    //    /// If the location file has been changed then the previous theme folder will be removed because we added a new folder for this theme
    //    /// </summary>
    //    /// <param name="themeInfo"></param>
    //    /// <returns>The id of the theme record</returns>
    //    private int UpdateThemeAndReturnId(ThemeModel themeInfo)
    //    {
    //        var themeRepository = new ThemeRepository();

    //        int themeId;

    //        try
    //        {
    //            // Get previous theme
    //            var previousTheme = themeRepository.GetByName(themeInfo.ThemeName);

    //            // Update previous theme
    //            if (previousTheme != null)
    //            {
    //                previousTheme.Author = themeInfo.Author;
    //                previousTheme.AuthorWebsite = themeInfo.AuthorWebsite;
    //                previousTheme.Description = themeInfo.ThemeDescription;
    //                previousTheme.Image = themeInfo.ThemeImage;

    //                // Delete previous theme location - We are using a new folder for this theme now
    //                if (!previousTheme.Location.Equals(themeInfo.Location, StringComparison.CurrentCultureIgnoreCase))
    //                {
    //                    DeletePreviousLocation(previousTheme.Location);   
    //                }

    //                previousTheme.Location = themeInfo.Location;
    //                previousTheme.ThemeWebsite = themeInfo.ThemeWebsite;
    //                previousTheme.Version = themeInfo.Version;

    //                themeRepository.Save();

    //                themeId = previousTheme.ThemeId;
    //            }
    //            else
    //            {
    //                // Add new theme
    //                themeRepository.Add(new Common.Theme
    //                {
    //                    Author = themeInfo.Author,
    //                    AuthorWebsite = themeInfo.AuthorWebsite,
    //                    Image = themeInfo.ThemeImage,
    //                    Description = themeInfo.ThemeDescription,
    //                    Name = themeInfo.ThemeName,
    //                    ThemeWebsite = themeInfo.ThemeWebsite,
    //                    Version = themeInfo.Version,
    //                    Location = themeInfo.Location
    //                });

    //                themeRepository.Save();
    //                themeId = themeRepository.GetLastAddedId();
    //            }
    //          themeRepository.Save();
    //        }
    //        catch(Exception ex)
    //        {
    //            throw new Exception("Couldn't insert the theme into the database.", ex);
    //        }

    //        return themeId;
    //    }

    //    /// <summary>
    //    /// Update or insert the theme sections in the database
    //    /// </summary>
    //    /// <param name="themeSections"></param>
    //    /// <param name="themeId"></param>
    //    private void UpdateThemeSections(IEnumerable<ThemeSectionModel> themeSections, int themeId)
    //    {
    //        try
    //        {
    //            var themeSectionRepository = new ThemeSectionRepository();

    //            foreach (var themeSection in themeSections)
    //            {
    //                // Get previous theme
    //                var previousTheme = themeSectionRepository.GetByName(themeSection.SectionCodeName);

    //                // Check if we need to update this theme section description
    //                if (previousTheme != null && previousTheme.Description.Equals(themeSection.SectionDescription, StringComparison.CurrentCultureIgnoreCase) == false)
    //                {
    //                    // Update properties
    //                    previousTheme.Controller = themeSection.Controller;
    //                    previousTheme.Action = themeSection.Action;
    //                    previousTheme.FriendlyName = themeSection.SectionFriendlyName;
    //                    previousTheme.Description = themeSection.SectionDescription;

    //                    // Save
    //                    themeSectionRepository.Save();
    //                }
    //                // Check if we need to add this theme
    //                else if (previousTheme == null)
    //                {
    //                    // Add new theme section
    //                    themeSectionRepository.Add(new ThemeSection
    //                    {
    //                        Action = themeSection.Action,
    //                        Controller = themeSection.Controller,
    //                        Description = themeSection.SectionDescription,
    //                        FriendlyName = themeSection.SectionFriendlyName,
    //                        CodeName = themeSection.SectionCodeName,
    //                        ThemeId = themeId
    //                    });

    //                    // Save
    //                    themeSectionRepository.Save();
    //                }
    //            }
    //        }
    //        catch(Exception ex)
    //        {
    //            throw new Exception("Could not remove the previous installed theme from the database", ex);
    //        }
    //    }

    //    /// <summary>
    //    /// Start the installation of the theme
    //    /// </summary>
    //    /// <param name="uploadedFile"></param>
    //    /// <returns></returns>
    //    public string LaunchAndReturnThemeName(HttpPostedFileBase uploadedFile)
    //    {
    //        // Save uploaded file
    //        string themeInstallationPath = SaveAndReturnFilePath(uploadedFile);

    //        // Unzip
    //        var themeInstalledPath = UnzipAndReturnInstallPath(themeInstallationPath);

    //        // Get Xml
    //        var xmlThemeInfo = GetXml(themeInstalledPath);

    //        // Insert theme
    //        int themeId = UpdateThemeAndReturnId(xmlThemeInfo);

    //        // Remove previous theme
    //        UpdateThemeSections(xmlThemeInfo.ThemeSections, themeId);

    //        return xmlThemeInfo.ThemeName;
    //    }
    //}
}
