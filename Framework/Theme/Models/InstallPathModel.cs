﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theme.Models
{
    public class InstallPathModel
    {
        public string LocationName { get; set; }
        public string InstalledPath { get; set; }
    }
}
