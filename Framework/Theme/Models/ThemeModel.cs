﻿using System.Collections.Generic;

namespace Theme.Models
{
    public class ThemeModel
    {
        public string ThemeName { get; set; }
        public string ThemeDescription { get; set; }
        public string ThemeImage { get; set; }
        public string Author { get; set; }
        public string AuthorWebsite { get; set; }
        public string ThemeWebsite { get; set; }
        public string Version { get; set; }
        public string Location { get; set; }

        public IEnumerable<ThemeSectionModel> ThemeSections { get; set; }
    }
}
