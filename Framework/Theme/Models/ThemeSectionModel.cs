﻿namespace Theme.Models
{
    public class ThemeSectionModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string SectionFriendlyName { get; set; }
        public string SectionCodeName { get; set; }
        public string SectionDescription { get; set; }
    }
}
