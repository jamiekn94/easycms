﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.Models
{
    public class PageModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public bool HidePage { get; set; }
    }
}
