﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widgets.Models;

namespace Plugins.Models
{
    public class ReturnModel
    {
        public string FriendlyPluginName { get; set; }
        public TypeInstallation TypeInstallation { get; set; }
    }
}
