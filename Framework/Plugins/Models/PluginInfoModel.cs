﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widgets.Models;

namespace Plugins.Models
{
   public class PluginInfoModel
    {
        public string CodeName { get; set; }
        public string FriendlyName { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public bool HasSettings { get; set; }
        public string PluginLocation { get; set; }
        public string SettingsController { get; set; }
        public string AssemblyLocation { get; set; }
        public string AuthorName { get; set; }
        public string AuthorWebsite { get; set; }
        public string PluginWebsite { get; set; }

       public IEnumerable<ScriptModel> SqlScripts { get; set; }
       public IEnumerable<PageModel> Pages { get; set; }
    }
}
