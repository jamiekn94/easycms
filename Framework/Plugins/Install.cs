﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;
using AutoMapper;
using Common;
using Common.Repositories;
using Plugins.Models;

namespace Plugins
{
    public class Install : Widgets.Install
    {
        /// <summary>
        /// Path of where the .zip files are stored
        /// </summary>
        protected override string InstallationsLocation
        {
            get { return HttpContext.Current.Server.MapPath(@"Plugins\Installations").Replace(@"\Admin\Plugins", ""); }
        }

        /// <summary>
        /// Path of where the unzipped plugins are stored are stored
        /// </summary>
        protected override string InstalledLocation
        {
            get { return HttpContext.Current.Server.MapPath(@"Plugins\Installed").Replace(@"\Admin\Plugins", ""); }
        }

        /// <summary>
        /// Namespace of the manifest
        /// </summary>
        protected XNamespace GetNameSpace
        {
            get { return XNamespace.Get("urn:uiosp-bundle-manifest-2.0"); }
        }

        /// <summary>
        /// Reads the manifest properties and return a class with the info of the plugin
        /// </summary>
        /// <param name="pluginLocation">The directory name of where the plugin is stored</param>
        /// <returns></returns>
        private PluginInfoModel GetPluginInfo(string pluginLocation)
        {
            try
            {
                string pluginCodeName = ManifestDocument.Root.Attribute("SymbolicName").Value;
                string pluginFriendlyName = ManifestDocument.Root.Attribute("Name").Value;
                string version = ManifestDocument.Root.Attribute("Version").Value;
                var assemblyLocation = ManifestDocument.Descendants(GetNameSpace + "Assembly").Attributes("Path").First().Value;
                var authorName = ManifestDocument.Descendants(GetNameSpace + "Author").Attributes("Name").First().Value;
                var pluginWebsite = ManifestDocument.Descendants(GetNameSpace + "Plugin").Attributes("Website").First().Value;
                var pluginDescription = ManifestDocument.Descendants(GetNameSpace + "Plugin").Attributes("Description").First().Value;
                var authorWebsite = ManifestDocument.Descendants(GetNameSpace + "Author").Attributes("Website").First().Value;
                var settingsEnabled = Convert.ToBoolean(ManifestDocument.Descendants(GetNameSpace + "Settings").Attributes("Enabled").First().Value);
                var settingsController = ManifestDocument.Descendants(GetNameSpace + "Settings").Attributes("Controller").First().Value;

                return new PluginInfoModel
                {
                    AssemblyLocation = assemblyLocation,
                    AuthorName = authorName,
                    AuthorWebsite = authorWebsite,
                    CodeName = pluginCodeName,
                    Description = pluginDescription,
                    FriendlyName = pluginFriendlyName,
                    HasSettings = settingsEnabled,
                    PluginWebsite = pluginWebsite,
                    SettingsController = settingsController,
                    Version = version,
                    PluginLocation = pluginLocation,
                    SqlScripts = GetSqlScripts(),
                    Pages = GetPages()
                };
            }
            catch (Exception ex)
            {
                throw new Exception("Could not load one or more properties from the plugin manifest", ex);
            }
        }

        /// <summary>
        /// Update or add the plugin into the database
        /// </summary>
        /// <param name="pluginInfo">Manifest info</param>
        /// <returns></returns>
        private Plugin UpdatePluginReturnDbRecord(PluginInfoModel pluginInfo)
        {
            var pluginRepository = new PluginRepository();

            var previousPluginInfo = pluginRepository.GetByName(pluginInfo.CodeName);

            // Update this plugin
            if (previousPluginInfo != null)
            {
                previousPluginInfo.AssemblyLocation = pluginInfo.AssemblyLocation;
                previousPluginInfo.Author = pluginInfo.AuthorName;
                previousPluginInfo.AuthorWebsite = pluginInfo.AuthorWebsite;
                previousPluginInfo.Description = pluginInfo.Description;
                previousPluginInfo.PluginFriendlyName = pluginInfo.FriendlyName;
                previousPluginInfo.PluginWebsite = pluginInfo.PluginWebsite;
                previousPluginInfo.Version = pluginInfo.Version;
                previousPluginInfo.Settings = pluginInfo.HasSettings;
                previousPluginInfo.LocationPlugin = pluginInfo.PluginLocation;

                pluginRepository.Save();

                return previousPluginInfo;
            }
            else
            {
                // Add plugin
                var pluginItem = new Plugin()
                {
                    PluginCodeName = pluginInfo.CodeName,
                    PluginFriendlyName = pluginInfo.FriendlyName,
                    AssemblyLocation = pluginInfo.AssemblyLocation,
                    Version = pluginInfo.Version,
                    Author = pluginInfo.AuthorName,
                    AuthorWebsite = pluginInfo.AuthorName,
                    PluginWebsite = pluginInfo.PluginWebsite,
                    Settings = pluginInfo.HasSettings,
                    Description = pluginInfo.Description,
                    LocationPlugin = pluginInfo.PluginLocation
                };

                pluginRepository.Add(pluginItem);

                pluginRepository.Save();

                return pluginRepository.GetLastAddedPlugin();
            }
        }

        /// <summary>
        /// Gets the pages from the manifest
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PageModel> GetPages()
        {
            try
            {
                return (from page in ManifestDocument.Descendants(GetNameSpace + "Page")
                        select new PageModel
                        {
                            Action = page.Attribute("Action").Value,
                            Controller = page.Attribute("Controller").Value,
                            Description = page.Attribute("Description").Value,
                            Keywords = page.Attribute("Keywords").Value,
                            Title = page.Attribute("Title").Value,
                            HidePage = Convert.ToBoolean(page.Attribute("SeoHidePage").Value)
                        });
            }
            catch (Exception ex)
            {
                throw new Exception("Could not load the plugin pages from the manifest", ex);
            }
        }

        /// <summary>
        /// Get the previous plugin from the database
        /// </summary>
        /// <param name="codeName">The unique name of a plugin</param>
        /// <returns>Plugin database record, if there aren't any then it will return a not existing record with default values</returns>
        private Plugin GetPreviousPlugin(string codeName)
        {
            var pluginRepository = new PluginRepository();

            var previousPluginInfo = pluginRepository.GetByName(codeName);

            if (previousPluginInfo != null)
            {
                return previousPluginInfo;
            }
            else
            {
                return new Plugin
                {
                    AssemblyLocation = string.Empty,
                    Author = string.Empty,
                    AuthorWebsite = string.Empty,
                    Description = string.Empty,
                    Enabled = false,
                    LocationPlugin = string.Empty,
                    Version = "00.00.00",
                    PluginWebsite = string.Empty,
                    PluginCodeName = string.Empty,
                    PluginFriendlyName = string.Empty,
                    PluginId = -1,
                    Settings = false
                };
            }
        }


        /// <summary>
        /// Delete the previous plugin directory if a new directory has been created for the plugin
        /// </summary>
        /// <param name="oldLocation">Full old path to the plugin directory</param>
        /// <param name="newLocation">Full new path to the plugin directory</param>
        private void DeletePreviousPluginDirectory(string oldLocation, string newLocation)
        {
            if (!string.IsNullOrEmpty(oldLocation))
            {
                if (!newLocation.Equals(oldLocation, StringComparison.CurrentCultureIgnoreCase))
                {
                    string fullPathOldPluginLocation = InstalledLocation + "/" + oldLocation;
                    DeleteOldDirectory(fullPathOldPluginLocation);
                }
            }
        }

        /// <summary>
        /// Remove all the previous plugin pages and the seo pages related
        /// </summary>
        /// <param name="pluginId">Unique pluginid from the database plugins table</param>
        private void RemovePluginPages(int pluginId)
        {
            var pluginPagesRepository = new PluginPagesRepository();

            if (pluginId != -1)
            {
                var arrayPages = pluginPagesRepository.GetPagesByPluginId(pluginId);

                foreach (var pluginPage in arrayPages)
                {
                    pluginPagesRepository.Remove(pluginPage);
                }

                pluginPagesRepository.Save();
            }

            pluginPagesRepository.Dispose();
        }

        private void HandlePages(int pluginId, IEnumerable<PageModel> arrayPages)
        {
            // Remove previous seo pages
            RemovePluginPages(pluginId);

            // Add new seo pages
           var arraySeoPageModels = AddSeoAndReturnPages(arrayPages);

            // Add plugin pages pages
            AddPluginPages(pluginId, arraySeoPageModels);
        }

        private void AddPluginPages(int pluginId, IEnumerable<SeoPageModel> arraySeoPageModels)
        {
            var pluginPageRepository = new PluginPagesRepository();

            foreach (var pluginPage in arraySeoPageModels)
            {
                pluginPageRepository.Add(new PluginPage
                {
                    Action = pluginPage.Action,
                    Controller = pluginPage.Controller,
                    PluginId = pluginId,
                    SeoPageId = pluginPage.SeoPageId,
                });

                pluginPageRepository.Save();
            }
            pluginPageRepository.Dispose();
        }

        /// <summary>
        /// Add the new plugin pages and return an array of pagemodels including the seoPageId
        /// </summary>
        /// <param name="arrayPages"></param>
        private IEnumerable<SeoPageModel> AddSeoAndReturnPages(IEnumerable<PageModel> arrayPages)
        {
            var seoPagesRepository = new SeoPageRepository();

            var arraySeoPages = new Collection<SeoPageModel>();

            foreach (var seoPage in arrayPages)
            {
                seoPagesRepository.Add(new SEOPage
                {
                    Action = seoPage.Action,
                    Controller = seoPage.Controller,
                    Titel = seoPage.Title,
                    SEODescription = seoPage.Description,
                    SEOKeywords = seoPage.Keywords,
                    Hide = seoPage.HidePage
                });

                seoPagesRepository.Save();

                // Create mapper
                Mapper.CreateMap<PageModel, SeoPageModel>();
               var seoPageModel = Mapper.Map<SeoPageModel>(seoPage);
                seoPageModel.SeoPageId = seoPagesRepository.GetLastAddedPageId();

                arraySeoPages.Add(seoPageModel);
            }

            seoPagesRepository.Dispose();

            return arraySeoPages;
        }

        /// <summary>
        /// Start installing/updating/re-installing the plugin
        /// </summary>
        /// <param name="zipFile"></param>
        /// <returns></returns>
        public new ReturnModel Launch(HttpPostedFileBase zipFile)
        {
            // Create installation directory if needed
            CreateInstallationDirectory();

            // Save installation package
            SaveFile(zipFile);

            // Create directory for the widget and get the paths to the widget
            var pluginPathInfo = CreateAndReturnDirectoryLocation(zipFile.FileName);

            // Unzip
            string pluginDirectoryPath = UnzipAndReturnDirectoryLocation(InstallationsLocation + "/" + zipFile.FileName, pluginPathInfo.FullPathPlugin);

            // Set manifest
            SetManifest(pluginDirectoryPath);

            // Read xml
            LoadManifestDocument();

            // Get manifest info
            var manifestInfo = GetPluginInfo(pluginPathInfo.WidgetLocation);

            // Get previous plugin
            var previousPluginInfo = GetPreviousPlugin(manifestInfo.CodeName);

            // Delete previous plugin directory
            DeletePreviousPluginDirectory(previousPluginInfo.LocationPlugin, manifestInfo.PluginLocation);

            // Run all sql scripts - for each version
            LaunchAllSqlScripts(manifestInfo.SqlScripts, pluginPathInfo.WidgetLocation, manifestInfo.Version, previousPluginInfo.Version);

            // Add plugin into database
            var pluginDbInfo = UpdatePluginReturnDbRecord(manifestInfo);

            // Add seo pages & plugin pages
            HandlePages(pluginDbInfo.PluginId, manifestInfo.Pages);

            // Determine type installation
            var typeInstallation = GetTypeInstallation(previousPluginInfo.Version, manifestInfo.Version);

            // Return the friendly name and type of the installation
            return new ReturnModel
            {
                FriendlyPluginName = pluginDbInfo.PluginFriendlyName,
                TypeInstallation = typeInstallation
            };
        }
    }
}
