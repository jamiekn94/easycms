﻿#region

using System;
using System.Security.AccessControl;
using System.Web.Optimization;

#endregion

namespace Kapsters
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            string themeName;

            using (var configurationRepository = new Common.Repositories.ConfiguratieRepository())
            {
                themeName = configurationRepository.GetCachedThemePath();
            }

            var stylesBundle = new StyleBundle("~/Content/stylesheets").Include(
                string.Format("~/Themes/{0}/Content/css/bootstrap-fire.css", themeName),
                 string.Format("~/Themes/{0}/Content/css/Site.css", themeName),
                 string.Format("~/Themes/{0}/Content/css/font-awesome-min.css", themeName)
                );

            var scriptsBundle = new ScriptBundle("~/Content/javascripts").Include(
                string.Format("~/Themes/{0}/Content/js/bootstrap-min.js", themeName)
                );

            bundles.Add(stylesBundle);
            bundles.Add(scriptsBundle);


            BundleTable.EnableOptimizations = false;
        }
    }
}