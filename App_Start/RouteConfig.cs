﻿#region

using System.Web.Mvc;
using System.Web.Routing;

#endregion

namespace Kapsters
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Route for pages
            routes.MapRoute("Pages", "Pagina/{id}/{title}",
                new {controller = "Pagina", action = "Index", title = UrlParameter.Optional},
                new[] {"Kapsters.Controllers"}
                );

            // Route for blog articles
            routes.MapRoute("BlogArticles", "Blog/Artikel/{id}/{title}",
                new {controller = "Blog", action = "Artikel", title = UrlParameter.Optional},
                new { id = @"\d+" },
                new[] {"Kapsters.Controllers"}
                );

            // Route for news articles
            routes.MapRoute("NewsArticles", "Nieuws/Artikel/{id}/{title}",
                new {controller = "Nieuws", action = "Artikel",  title = UrlParameter.Optional},
                new {id = @"\d+"},
                new[] {"Kapsters.Controllers"}
                );

            // Route for photo albums
            routes.MapRoute("Photoalbum", "Fotoalbum/Album/{id}/{title}",
                new {controller = "Fotoalbum", action = "Album", title = UrlParameter.Optional},
                new { id = @"\d+" },
                new[] {"Kapsters.Controllers"}
                );

            // Route for blog search
            routes.MapRoute("SearchBlog", "Blog/Zoek",
                new { controller = "Blog", action = "Zoek", page = UrlParameter.Optional },
                new[] { "Kapsters.Controllers" }
                );

            // Route for Blog paging
            routes.MapRoute("PagingBlog", "Blog/{page}",
                new {controller = "Blog", action = "index", page = UrlParameter.Optional},
                new { page = @"\d+" },
                new[] {"Kapsters.Controllers"}
                );

            // Route for News search
            routes.MapRoute("SearchNews", "Nieuws/Zoek",
                new { controller = "Nieuws", action = "Zoek", page = UrlParameter.Optional },
                new[] { "Kapsters.Controllers" }
                );

            // Route for News keyword
            routes.MapRoute("KeywordNews", "Nieuws/ZoekKeywoord/{page}/{keyword}",
                new { controller = "Nieuws", action="ZoekKeywoord",  page = UrlParameter.Optional, keyword = UrlParameter.Optional },
                new[] { "Kapsters.Controllers" }
                );

            // Route for News paging
            routes.MapRoute("PagingNews", "Nieuws/{page}",
                new {controller = "Nieuws", action = "index", page = UrlParameter.Optional},
                new { page = @"\d+" },
                new[] {"Kapsters.Controllers"}
                );

            // Route for admin errors
            routes.MapRoute("AdminError", "Admin/Error/{errorCode}",
                new {errorCode = 000, controller = "Error", action = "index"},
                new[] {"Kapsters.Areas.Admin.Controllers"}
                );

            // Route for errors
            routes.MapRoute("Error", "Error/{errorCode}",
                new {errorCode = 000, controller = "Error", action = "index"}, new[] {"Kapsters.Controllers"}
                );

            // Default home route
            routes.MapRoute("Default", "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}, new[] {"Kapsters.Controllers"}
                );
        }
    }
}