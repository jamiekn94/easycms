﻿#region

using System.Web.Mvc;
using Theme;

#endregion

namespace Kapsters
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ThemeSetter());
            filters.Add(new Infrastructure.ActionFilters.GlobalActionFilter());
        }
    }
}